<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="Sapics_execution"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="842"
		 pageHeight="595"
		 columnWidth="782"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="MARORDRE" isForPrompting="true" class="java.lang.Integer"/>
	<parameter name="LOTORDRE" isForPrompting="true" class="java.lang.Integer"/>
	<queryString><![CDATA[select m.exe_ordre||'-'||m.mar_index code_marche, mar_libelle, l.lot_index, lot_libelle, a.att_ordre,
decode(f.adr_prenom,null,f.adr_nom,f.adr_nom||' '||f.adr_prenom) fourniss, exde.cde_ordre, exde.aee_eng_ht,
exde.exe_ordre exercice_exec, to_date(to_char(exde.date_eng,'DD/MM/YYYY'),'DD/MM/YYYY') date_eng, 
decode(sign(length(exde.cde_lib)-150),1,substr(replace(exde.cde_lib,chr(10),' '),1,150)||'...',replace(exde.cde_lib,chr(10),' ')) cde_lib,
decode(sign(length(exdf.libelle)-150),1,substr(replace(exdf.libelle,chr(10),' '),1,150)||'...',replace(exdf.libelle,chr(10),' ')) num_fact, 
to_date(to_char(exdf.date_exec,'DD/MM/YYYY'),'DD/MM/YYYY') date_fact, exdf.date_liqu, exdf.aee_liq_ht, 
exdf.aee_execution, exde.eng_numero, org_ub
from jefy_marches.marche m, jefy_marches.lot l, jefy_marches.attribution a, grhum.v_fournis_grhum f,
jefy_marches.v_jefy_execution_detail_eng exde, jefy_marches.v_jefy_execution_detail_fact exdf
where m.mar_ordre = l.mar_ordre
and l.lot_ordre = a.lot_ordre
and m.mar_suppr = 'N' and m.mar_valide = 'O'
and l.lot_suppr = 'N' and l.lot_valide = 'O'
and a.att_suppr = 'N' and a.att_valide = 'O'
and a.att_ordre = exde.att_ordre
and exde.cde_ordre = exdf.cde_ordre (+)
and a.fou_ordre = f.fou_ordre
and m.mar_ordre = $P{MARORDRE}
and l.lot_ordre = $P{LOTORDRE}
order by m.exe_ordre, m.mar_index, l.lot_index, a.att_ordre, exde.exe_ordre, f.adr_nom, exde.eng_numero, date_fact, date_liqu]]></queryString>

	<field name="CODE_MARCHE" class="java.lang.String"/>
	<field name="LOT_INDEX" class="java.lang.String"/>
	<field name="ATT_ORDRE" class="java.math.BigDecimal"/>
	<field name="FOURNISS" class="java.lang.String"/>
	<field name="CDE_ORDRE" class="java.lang.Integer"/>
	<field name="EXERCICE_EXEC" class="java.math.BigDecimal"/>
	<field name="CDE_LIB" class="java.lang.String"/>
	<field name="DATE_ENG" class="java.util.Date"/>
	<field name="AEE_ENG_HT" class="java.math.BigDecimal"/>
	<field name="NUM_FACT" class="java.lang.String"/>
	<field name="DATE_FACT" class="java.util.Date"/>
	<field name="DATE_LIQU" class="java.sql.Timestamp"/>
	<field name="AEE_LIQ_HT" class="java.math.BigDecimal"/>
	<field name="AEE_EXECUTION" class="java.math.BigDecimal"/>
	<field name="ENG_NUMERO" class="java.math.BigDecimal"/>
	<field name="ORG_UB" class="java.lang.String"/>
	<field name="MAR_LIBELLE" class="java.lang.String"/>
	<field name="LOT_LIBELLE" class="java.lang.String"/>

	<variable name="TOTAL_LIQUID_CDE" class="java.math.BigDecimal" resetType="Group" resetGroup="CDE_ORDRE" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_LIQ_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_EXEC_CDE" class="java.math.BigDecimal" resetType="Group" resetGroup="CDE_ORDRE" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_EXECUTION}]]></variableExpression>
	</variable>
	<variable name="TOTAL_LIQUID_FR" class="java.math.BigDecimal" resetType="Group" resetGroup="FOURNIS" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_LIQ_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_EXEC_FR" class="java.math.BigDecimal" resetType="Group" resetGroup="FOURNIS" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_EXECUTION}]]></variableExpression>
	</variable>
	<variable name="TOTAL_LIQUID_EX" class="java.math.BigDecimal" resetType="Group" resetGroup="EXER_EXEC" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_LIQ_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_EXEC_EX" class="java.math.BigDecimal" resetType="Group" resetGroup="EXER_EXEC" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_EXECUTION}]]></variableExpression>
	</variable>
	<variable name="TOTAL_LIQUID_LOT" class="java.math.BigDecimal" resetType="Group" resetGroup="LOT" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_LIQ_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_EXEC_LOT" class="java.math.BigDecimal" resetType="Group" resetGroup="LOT" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_EXECUTION}]]></variableExpression>
	</variable>
	<variable name="TOTAL_LIQUID_MR" class="java.math.BigDecimal" resetType="Group" resetGroup="CODE_MARCHE" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_LIQ_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_EXEC_MR" class="java.math.BigDecimal" resetType="Group" resetGroup="CODE_MARCHE" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_EXECUTION}]]></variableExpression>
	</variable>
	<variable name="TOTAL_ENG_FR" class="java.math.BigDecimal" resetType="Group" resetGroup="FOURNIS" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_ENG_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_ENG_EX" class="java.math.BigDecimal" resetType="Group" resetGroup="EXER_EXEC" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_ENG_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_ENG_LOT" class="java.math.BigDecimal" resetType="Group" resetGroup="LOT" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_ENG_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_ENG_MR" class="java.math.BigDecimal" resetType="Group" resetGroup="CODE_MARCHE" calculation="Sum">
		<variableExpression><![CDATA[$F{AEE_ENG_HT}]]></variableExpression>
	</variable>

		<group  name="CODE_MARCHE" isStartNewPage="true" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{CODE_MARCHE}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="782"
						height="18"
						key="textField-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{MAR_LIBELLE}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="LOT" isStartNewPage="true" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{LOT_INDEX}]]></groupExpression>
			<groupHeader>
			<band height="29"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="3"
						width="782"
						height="23"
						backcolor="#FFFFCC"
						key="textField-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="5" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["LOT N°"+$F{LOT_INDEX}+" : "+$F{LOT_LIBELLE}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="22"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="13"
						y="5"
						width="460"
						height="16"
						forecolor="#333333"
						backcolor="#FFFFCC"
						key="textField-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Total pour le lot N°"+$F{LOT_INDEX}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="580"
						y="4"
						width="100"
						height="18"
						backcolor="#FFFFCC"
						key="textField-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_LIQUID_LOT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="681"
						y="4"
						width="100"
						height="18"
						backcolor="#FFFFCC"
						key="textField-19"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_EXEC_LOT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="478"
						y="4"
						width="100"
						height="18"
						backcolor="#FFFFCC"
						key="textField-25"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_ENG_LOT}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="EXER_EXEC" isStartNewPage="true" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{EXERCICE_EXEC}]]></groupExpression>
			<groupHeader>
			<band height="25"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="782"
						height="23"
						forecolor="#333333"
						backcolor="#CCFFCC"
						key="textField-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="5" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Number"><![CDATA[$F{EXERCICE_EXEC}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="22"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="13"
						y="4"
						width="460"
						height="16"
						forecolor="#333333"
						backcolor="#CCFFCC"
						key="textField-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Total pour l'exercice "+$F{EXERCICE_EXEC}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="580"
						y="3"
						width="100"
						height="18"
						backcolor="#CCFFCC"
						key="textField-15"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_LIQUID_FR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="681"
						y="3"
						width="100"
						height="18"
						backcolor="#CCFFCC"
						key="textField-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_EXEC_FR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="478"
						y="4"
						width="100"
						height="18"
						backcolor="#CCFFCC"
						key="textField-24"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_ENG_FR}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="FOURNIS" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{FOURNISS}]]></groupExpression>
			<groupHeader>
			<band height="33"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="479"
						y="17"
						width="100"
						height="16"
						key="staticText-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<text><![CDATA[Reste engagé]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="580"
						y="17"
						width="100"
						height="16"
						key="staticText-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<text><![CDATA[Liquidé]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="681"
						y="17"
						width="100"
						height="16"
						key="staticText-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<text><![CDATA[Exécuté]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="13"
						y="2"
						width="769"
						height="15"
						forecolor="#333333"
						backcolor="#FFCC99"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="5" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{FOURNISS}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="13"
						y="1"
						width="460"
						height="16"
						forecolor="#333333"
						backcolor="#FFCC99"
						key="textField-11"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Total pour le fournisseur "+$F{FOURNISS}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="580"
						y="0"
						width="100"
						height="18"
						backcolor="#FFCC99"
						key="textField-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_LIQUID_FR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="681"
						y="0"
						width="100"
						height="18"
						backcolor="#FFCC99"
						key="textField-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_EXEC_FR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="FOURNIS"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="478"
						y="1"
						width="100"
						height="18"
						backcolor="#FFCC99"
						key="textField-23"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_ENG_FR}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="CDE_ORDRE" >
			<groupExpression><![CDATA[$F{CDE_ORDRE}]]></groupExpression>
			<groupHeader>
			<band height="54"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="14"
						y="0"
						width="402"
						height="32"
						key="textField-5"
						stretchType="RelativeToTallestObject"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Eng N°"+$F{ENG_NUMERO}+" : "+$F{CDE_LIB}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="422"
						y="17"
						width="56"
						height="18"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{DATE_ENG}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="480"
						y="17"
						width="100"
						height="18"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{AEE_ENG_HT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="CDE_ORDRE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="581"
						y="17"
						width="100"
						height="18"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{NUM_FACT}!= null ? $V{TOTAL_LIQUID_CDE} : new BigDecimal("0.00")]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="422"
						y="0"
						width="57"
						height="16"
						key="staticText-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font/>
					</textElement>
				<text><![CDATA[Date eng.
]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="CDE_ORDRE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="682"
						y="17"
						width="100"
						height="18"
						key="textField-7"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="2" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{NUM_FACT}!= null ? $V{TOTAL_EXEC_CDE} : new BigDecimal("0.00")]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="55"
						y="34"
						width="69"
						height="17"
						key="staticText-5">
							<printWhenExpression><![CDATA[new Boolean($F{NUM_FACT}!= null)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Liquidation(s)]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="150"
						y="34"
						width="312"
						height="17"
						key="staticText-6">
							<printWhenExpression><![CDATA[new Boolean($F{NUM_FACT}!= null)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[N° facture]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="465"
						y="34"
						width="56"
						height="16"
						key="staticText-7">
							<printWhenExpression><![CDATA[new Boolean($F{NUM_FACT}!= null)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font/>
					</textElement>
				<text><![CDATA[Date fact
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="522"
						y="34"
						width="56"
						height="16"
						key="staticText-8">
							<printWhenExpression><![CDATA[new Boolean($F{NUM_FACT}!= null)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font/>
					</textElement>
				<text><![CDATA[Date liquid
]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="54"
						y="50"
						width="727"
						height="0"
						forecolor="#CCCCCC"
						key="line-1">
							<printWhenExpression><![CDATA[new Boolean($F{NUM_FACT}!= null)]]></printWhenExpression>
						</reportElement>
					<graphicElement stretchType="NoStretch" pen="1Point"/>
				</line>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="2"  isSplitAllowed="true" >
				<line direction="BottomUp">
					<reportElement
						x="0"
						y="1"
						width="781"
						height="0"
						forecolor="#CCFFCC"
						key="line"/>
					<graphicElement stretchType="NoStretch" pen="2Point"/>
				</line>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="34"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="4"
						width="782"
						height="30"
						key="textField-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="14" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["EXECUTION DU MARCHE "+$F{CODE_MARCHE}+" PAR LOT"]]></textFieldExpression>
				</textField>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="17"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new Boolean($F{NUM_FACT}!= null)]]></printWhenExpression>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="150"
						y="2"
						width="312"
						height="15"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{NUM_FACT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="465"
						y="2"
						width="56"
						height="15"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{DATE_FACT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="523"
						y="2"
						width="56"
						height="15"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.sql.Timestamp"><![CDATA[$F{DATE_LIQU}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="581"
						y="2"
						width="100"
						height="15"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{AEE_LIQ_HT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="682"
						y="2"
						width="100"
						height="15"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{AEE_EXECUTION}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="589"
						y="6"
						width="170"
						height="11"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="8" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + "/"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="761"
						y="6"
						width="21"
						height="11"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left">
						<font size="8" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[""+$V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="3"
						width="782"
						height="0"
						forecolor="#CCCCCC"
						key="line"/>
					<graphicElement stretchType="NoStretch" pen="2Point"/>
				</line>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="6"
						width="54"
						height="11"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
