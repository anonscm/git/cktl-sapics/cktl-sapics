/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sapics.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.TimeZone;

import javax.swing.JFrame;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.InterfaceApplicationCocktail;
import org.cocktail.application.client.eof.EOFonction;
import org.cocktail.application.client.eof.EOUtilisateurFonction;
import org.cocktail.sapics.client.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 *
 */
public class ApplicationClient extends ApplicationCocktail implements InterfaceApplicationCocktail {
	public static final String NOM_APPLICATION = "SAPICS - Gestion des Marchés";
	private static final String DEFAULT_TIME_ZONE_KEY = "DEFAULT_TIME_ZONE";

	private static final String SERVER_KEY = "SERVER";
	private static final String CLIENT_KEY = "CLIENT";

	// FONCTIONS
	public static final String ID_FONC_ADMIN = "ADMIN";
	public static final String ID_FONC_BASCULE = "BASCUL";
	public static final String ID_FONC_ATTRIBUTIONS = "GESATT";
	public static final String ID_FONC_LBUDS = "GESLBUD";
	public static final String ID_FONC_MARCHES = "GESMAR";
	public static final String ID_FONC_NOMENCLATURES = "GESNOM";
	public static final String ID_FONC_CONS_ATTR = "CONSATT";
	public static final String ID_FONC_CONS_EXEC = "CONSEXEC";
	public static final String ID_FONC_CONS_LBUD = "CONSLBUD";
	public static final String ID_FONC_CONS_MARCHES = "CONSMAR";
	public static final String ID_FONC_CONS_NOMENCLATURES = "CONSNOM";

	private MyByteArrayOutputStream redirectedOutStream, redirectedErrStream;
	private String temporaryDir;

	/**
	 * Constructeur
	 */
	public ApplicationClient() {
		setWithLogs(true);
		setTYAPSTRID("MARCHES");
		setNAME_APP("SAPICS");
	}

	/**
	 * 
	 */
	public void initMonApplication() {
		super.initMonApplication();

		System.out.println("ApplicationClient.initMonApplication() FONCTIONS : "
				+ getMesUtilisateurFonction().valueForKey(EOUtilisateurFonction.FONCTION_KEY + "." + EOFonction.FON_ID_INTERNE_KEY));
		ServerProxy.clientSideRequestSetLoginParametres(getAppEditingContext(), getUserInfos().login(), getIpAdress());
		initTimeZones(ServerProxy.clientSideRequestGetParam(getAppEditingContext(), DEFAULT_TIME_ZONE_KEY));
		Superviseur.sharedInstance(getAppEditingContext()).init();
		System.out.println("ApplicationClient.initMonApplication() " + getMesUtilisateurFonction());
		try {
			temporaryDir = System.getProperty("java.io.tmpdir");
			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}
		} catch (Exception e) {
			System.out.println("Impossible de recuperer le repertoire temporaire !");
		}
		// redirection des logs clients.
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));

		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
		System.setProperty("apple.laf.useScreenMenuBar", "true");
	}

	public void finishInitialization() {
		try {
			super.finishInitialization();

			try {
				compareJarVersionsClientAndServer();
			} catch (Exception e) {
				e.printStackTrace();
				this.fenetreDeDialogueInformation(e.getMessage());
				quit();
			}

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void initTimeZones(String timezone) {
		NSTimeZone theTimezone = NSTimeZone.timeZoneWithName("GMT", false); // Valeur par defaut
		if (timezone != null && !timezone.trim().equals("")) {
			NSTimeZone ntz = NSTimeZone.timeZoneWithName(timezone, false);
			if (ntz == null) {
				System.out.println("Le Timezone defini n'est pas valide (" + timezone + ")");
			} else {
				theTimezone = ntz;
			}
		}

		TimeZone.setDefault(theTimezone);
		NSTimeZone.setDefaultTimeZone(theTimezone);
		System.out.println("     --> NSTimeZone par defaut utilise dans l'application " + NSTimeZone.defaultTimeZone());
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		System.out.println("     --> Les NSTimestampFormatter analyseront les dates avec le NSTimeZone " + ntf.defaultParseTimeZone());
		System.out.println("     --> Les NSTimestampFormatter afficheront les dates avec le NSTimeZone " + ntf.defaultFormatTimeZone());
	}

	private String getIpAdress() {
		try {
			final String s = java.net.InetAddress.getLocalHost().getHostAddress();
			final String s2 = java.net.InetAddress.getLocalHost().getHostName();
			return s + " / " + s2;
		} catch (UnknownHostException e) {
			return "Machine inconnue";
		}
	}

	/**
	 * @return Recuperation de la liste des codes ministeres utilises
	 */
	public NSArray getListeMinisteres() {
		NSArray myListe = new NSArray();
		NSArray ministeres = ServerProxy.clientSideRequestSqlQuery(getAppEditingContext(),
				"select distinct c_ministere CODE_MINISTERE from jefy_paf.kx_05 order by c_ministere");
		myListe = (NSArray) ministeres.valueForKey("CODE_MINISTERE");
		return myListe;
	}

	/**
	 * 
	 * L'Utilisateur possede t il les droits pour la fonction passee en parametre ?
	 * 
	 * @param idFonction
	 *            code de la fonction a consulter
	 * @return <code>true</code> si l'utilisateur a les droits
	 */
	public boolean hasFonction(String idFonction) {
		return ((NSArray) getMesUtilisateurFonction().valueForKey(EOUtilisateurFonction.FONCTION_KEY + "." + EOFonction.FON_ID_INTERNE_KEY))
				.containsObject(idFonction);
	}

	public void creationDesPrivileges() {
		super.creationDesPrivileges();
		/*
		 * Examples for(int i = 0; i < getMesUtilisateurFonction().count(); i++) {
		 * if(((EOUtilisateurFonction)getMesUtilisateurFonction
		 * ().objectAtIndex(i)).fonction().fonIdInterne().equals("CONSULT")) addPrivileges(new Privileges("CONSULT"));
		 * if(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(
		 * "CREATION")) addPrivileges(new Privileges("CREATION"));
		 * if(((EOUtilisateurFonction)getMesUtilisateurFonction()
		 * .objectAtIndex(i)).fonction().fonIdInterne().equals("MODIF")) addPrivileges(new Privileges("MODIF"));
		 * if(((EOUtilisateurFonction
		 * )getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals("SUPP")) addPrivileges(new
		 * Privileges("SUPP")); }
		 */
	}

	public String returnTempStringName() {
		java.util.Calendar currentTime = java.util.Calendar.getInstance();
		String lastModif = "-" + currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "." + currentTime.get(java.util.Calendar.MONTH) + "."
				+ currentTime.get(java.util.Calendar.YEAR) + "-" + currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h"
				+ currentTime.get(java.util.Calendar.MINUTE) + "m" + currentTime.get(java.util.Calendar.SECOND);
		return lastModif;
	}

	public String outLogs() {
		return redirectedOutStream.toString();
	}

	public String errLogs() {
		return redirectedErrStream.toString();
	}

	public void cleanLogs(String type) {
		if (type.equals(CLIENT_KEY)) {
			redirectedOutStream.reset();
			redirectedErrStream.reset();
		}

		if (type.equals(SERVER_KEY)) {
			ServerProxy.clientSideRequestCleanLogs(getAppEditingContext());
		}
	}

	public void sendLog(String typeLog) {
		String adminMail = ServerProxy.clientSideRequestGetParam(getAppEditingContext(), "ADMIN_MAIL");
		String emetteur = adminMail;

		try {
			if (getUserInfos().email() != null) {
				emetteur = getUserInfos().email();
			}
			String sujet = "SAPICS LOGS - ";
			sujet = sujet + DateCtrl.dateToString(new NSTimestamp(), "%d/%m/%Y %H:%M");

			String message = "LOGS CLIENT ET SERVEUR.";
			message = message + "\nINDIVIDU CONNECTE : " + getUserInfos().nom() + " " + getUserInfos().prenom();

			message = message + "\n\n************* LOGS CLIENT *****************";
			message = message + "\nOUTPUT log :\n\n" + redirectedOutStream.toString() + "\n\nERROR log :\n\n" + redirectedErrStream.toString();

			message = message + "\n\n************* LOGS SERVER *****************";
			message = message + "\n*****************************************";
			String outLog = ServerProxy.clientSideRequestOutLog(getAppEditingContext());
			String errLog = ServerProxy.clientSideRequestErrLog(getAppEditingContext());
			message = message + "\nOUTPUT log SERVER :\n\n" + outLog + "\n\nERROR log SERVER :\n\n" + errLog;

			StringBuffer mail = new StringBuffer();
			mail.append(message);

			ServerProxy.clientSideRequestSendMail(getAppEditingContext(), emetteur, adminMail, null, sujet, mail.toString());
			EODialogs.runInformationDialog("ENVOI MAIL", "Le mail a bien été envoyé.");
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
	}

	/**
	 * 
	 * @return
	 */
	public JFrame mainFrame() {
		return Superviseur.sharedInstance(getAppEditingContext());
	}

	/**
	 * 
	 * @param bool
	 */
	public void setGlassPane(boolean bool) {
		Superviseur.sharedInstance(getAppEditingContext()).setGlassPane(bool);
	}

	public void openURL(String URL) {
		try {
			Runtime runTime = Runtime.getRuntime();
			runTime.exec(new String[] {"rundll32", "url.dll,FileProtocolHandler", URL});
		} catch (Exception exception) {
			EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "
					+ URL + "\nMESSAGE : " + exception.getMessage());
			exception.printStackTrace();
		}
	}

	/**
	 * Creation d'un report en fonction d'un array de valeurs et d'un nom
	 */
	public void exportExcel(String chaine, String reportName) {
		String fileName;
		File aFile;
		FileOutputStream anOutputStream = null;

		if (chaine != null) {
			fileName = temporaryDir.concat(reportName.concat(".csv"));
			aFile = new File(fileName);
			try {
				anOutputStream = new FileOutputStream(aFile);
				anOutputStream.write(chaine.getBytes());
				anOutputStream.close();
			} catch (Exception exception) {
				System.out.println(this.getClass().getName() + ".imprimer() - Exception : " + exception.getMessage());
				return;
			}
			getToolsCocktailSystem().openFile(fileName);
		}
	}

	public String getTemporaryDir() {
		return temporaryDir;
	}

	/**
	 * Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	 */
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;

		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}

		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}
}
