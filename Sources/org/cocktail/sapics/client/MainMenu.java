/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.sapics.client.admin.BasculeExerciceCtrl;
import org.cocktail.sapics.client.admin.CataloguesCtrl;
import org.cocktail.sapics.client.admin.CodesNomenclaturesCtrl;
import org.cocktail.sapics.client.admin.ParametrageCtrl;
import org.cocktail.sapics.client.admin.TrancheMapaCtrl;
import org.cocktail.sapics.client.editions.ReportsJasperCtrl;
import org.cocktail.sapics.client.marches.MarchesCtrl;
import org.cocktail.sapics.client.utilities.CocktailConstantes;
import org.cocktail.sapics.client.utilities.ConnectedUsersCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class MainMenu extends JMenuBar {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5987291130665882662L;

	public static MainMenu sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient) ApplicationClient.sharedApplication();

	private JMenu menuApplication, menuAdministration, menuBudget, menuEditions, menuOutils;
	private JMenuItem itemUtilisateursConnectes, itemCodesNomenclatures, itemTranches, itemCatalogues, itemBascule;
	private JMenuItem itemEditionMarcheExecution, itemEditionMarchesExercice, itemEditionListeCn;
	private JMenuItem itemParametrage;

	protected EOEditingContext ec;

	/** Constructeur */
	public MainMenu(EOEditingContext globalEc) {

		ec = globalEc;

		menuApplication = new JMenu("Application");
		menuAdministration = new JMenu("Administration");
		menuBudget = new JMenu("Budget");
		menuEditions = new JMenu("Editions");
		menuOutils = new JMenu("Outils");

		// Application
		itemUtilisateursConnectes = new JMenuItem(new ActionConnectedUsers());
		menuApplication.add(itemUtilisateursConnectes);
		menuApplication.add(new JMenuItem(new ActionQuitter("Quitter")));
		// Administration
		itemParametrage = new JMenuItem(new ActionParametrage("Paramétrage Application"));
		menuAdministration.add(itemParametrage);

		if (NSApp.hasFonction(ApplicationClient.ID_FONC_BASCULE)) {
			itemBascule = new JMenuItem(new ActionBascule("Changement d'exercice"));
			menuAdministration.add(itemBascule);
		}

		itemCodesNomenclatures = new JMenuItem(new ActionCodesNomenclatures("Codes Nomenclatures"));
		menuAdministration.add(itemCodesNomenclatures);

		itemTranches = new JMenuItem(new ActionTranches("Gestion des seuils (Tranches)"));
		menuAdministration.add(itemTranches);

		itemCatalogues = new JMenuItem(new ActionCatalogues("Catalogues"));
		// itemCatalogues.setEnabled(false);
		menuAdministration.add(itemCatalogues);

		menuApplication.setIcon(CocktailConstantes.ICON_APP_LOGO);
		menuAdministration.setIcon(CocktailConstantes.ICON_OUTILS_16);
		menuBudget.setIcon(CocktailConstantes.ICON_EURO_16);
		menuEditions.setIcon(CocktailConstantes.ICON_PRINTER_16);

		itemEditionMarcheExecution = new JMenuItem(new ActionEditionMarcheExecution("Marchés en cours d'exécution au "
				+ DateCtrl.dateToString(new NSTimestamp())));
		itemEditionMarchesExercice = new JMenuItem(new ActionEditionMarchesExercice("Marchés de l'exercice en cours "));
		itemEditionListeCn = new JMenuItem(new ActionEditionCodesNomenclatures("Codes Nomenclatures"));

		menuEditions.add(itemEditionMarcheExecution);
		menuEditions.add(itemEditionMarchesExercice);
		menuEditions.add(itemEditionListeCn);

		// Ajout des menus
		add(menuApplication);
		add(menuAdministration);
		add(menuEditions);
		// add(menuOutils);
		add(new MenuAide(NSApp, "?"));
	}

	/**
	 * @param editingContext
	 * @return
	 */
	public static MainMenu sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new MainMenu(editingContext);
		}
		return sharedInstance;
	}

	/** Quitte l'application */
	private class ActionQuitter extends AbstractAction {
		private static final long serialVersionUID = 1649038147011393740L;

		public ActionQuitter(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_EXIT_16);
		}

		public void actionPerformed(ActionEvent event) {
			NSApp.quit();
		}
	}

	private class ActionParametrage extends AbstractAction {
		private static final long serialVersionUID = -1994042411973523254L;

		public ActionParametrage(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_OUTILS_22);
		}

		public void actionPerformed(ActionEvent event) {
			ParametrageCtrl.sharedInstance(ec).open();
		}
	}

	private class ActionBascule extends AbstractAction {
		private static final long serialVersionUID = -1994042411973523254L;

		public ActionBascule(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_OUTILS_22);
		}

		public void actionPerformed(ActionEvent event) {
			BasculeExerciceCtrl.sharedInstance(ec).basculer();
		}
	}

	private class ActionCodesNomenclatures extends AbstractAction {
		private static final long serialVersionUID = -1207821857934815862L;

		public ActionCodesNomenclatures(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_PARAMS_16);
		}

		public void actionPerformed(ActionEvent event) {
			CodesNomenclaturesCtrl.sharedInstance(ec).open();
		}
	}

	private class ActionTranches extends AbstractAction {
		private static final long serialVersionUID = -1207821857934815862L;

		public ActionTranches(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_PARAMS_16);
		}

		public void actionPerformed(ActionEvent event) {
			TrancheMapaCtrl.sharedInstance(ec).open();
		}
	}

	private class ActionCatalogues extends AbstractAction {
		private static final long serialVersionUID = -1207821857934815862L;

		public ActionCatalogues(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_PARAMS_16);
		}

		public void actionPerformed(ActionEvent event) {
			CataloguesCtrl.sharedInstance(ec).open();
		}
	}

	private class ActionEditionCodesNomenclatures extends AbstractAction {
		private static final long serialVersionUID = -1207821857934815862L;

		public ActionEditionCodesNomenclatures(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_PRINTER_16);
		}

		public void actionPerformed(ActionEvent event) {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.takeValueForKey(NSApp.getApplicationParametre("REP_BASE_JASPER_PATH"), "REP_BASE_PATH");
			parametres.setObjectForKey(MarchesCtrl.sharedInstance(ec).getExercice().exeExercice(), "annee");
			ReportsJasperCtrl.sharedInstance(ec).printListeCodesNomenclatures(parametres);
		}
	}

	private class ActionEditionMarcheExecution extends AbstractAction {
		private static final long serialVersionUID = -1207821857934815862L;

		public ActionEditionMarcheExecution(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_PRINTER_16);
		}

		public void actionPerformed(ActionEvent event) {
			NSMutableDictionary parametres = new NSMutableDictionary();
			ReportsJasperCtrl.sharedInstance(ec).printListeMarchesEnCours(parametres);
		}
	}

	private class ActionEditionMarchesExercice extends AbstractAction {
		private static final long serialVersionUID = -1207821857934815862L;

		public ActionEditionMarchesExercice(String name) {
			putValue(Action.NAME, name);
			putValue(AbstractAction.SMALL_ICON, CocktailConstantes.ICON_PRINTER_16);
		}

		public void actionPerformed(ActionEvent event) {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(MarchesCtrl.sharedInstance(ec).getExercice().exeExercice(), "annee");
			System.out.println("MainMenu.ActionEditionMarchesExercice.actionPerformed() " + parametres);
			ReportsJasperCtrl.sharedInstance(ec).printListeMarches(parametres);
		}
	}

	private final class ActionConnectedUsers extends AbstractAction {
		private static final long serialVersionUID = 5125101540436981811L;

		public ActionConnectedUsers() {
			super("Utilisateurs connectés");
		}

		public void actionPerformed(final ActionEvent e) {
			ConnectedUsersCtrl.sharedInstance(ec).open();
		}
	}
}
