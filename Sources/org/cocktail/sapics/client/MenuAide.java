/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.cocktail.sapics.client.utilities.XLogs;

public class MenuAide extends JMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7072681739468669328L;

	private ApplicationClient NSApp;

	// private JMenuItem itemVersion, itemDocumentation, itemForums, itemSiteWeb;
	private JMenuItem itemLogs;

	/**
	 * Constructeur
	 * 
	 * @param app
	 *            L'application cliente
	 * @param titre
	 *            Le titre de la fenêtre
	 */
	public MenuAide(ApplicationClient app, String titre) {
		super(titre);
		NSApp = app;
		initObject();
	}

	/** */
	public void initObject() {
		// itemForums = new JMenuItem(new ActionForums("Forums"));
		// itemDocumentation = new JMenuItem(new ActionDocumentation("Documentation "));
		// itemSiteWeb = new JMenuItem(new ActionSiteWeb("Site Web"));
		itemLogs = new JMenuItem(new ActionLogs("Logs Client / Serveur"));

		// add(itemForums);
		// add(itemSiteWeb);
		// add(itemDocumentation);
		add(itemLogs);

	}

	/** */
	public class ActionForums extends AbstractAction {
		private static final long serialVersionUID = -8395388490884630148L;

		public ActionForums(String name) {
			putValue(Action.NAME, name);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent event) {
			NSApp.openURL("http://www.cocktail.org/cgi-bin/WebObjects/Forum.woa/1/wo/dH2C3MQBZCNsR9O6gVC0ww/5.3.5.0.1.3.5.15.3");
		}
	}

	public class ActionSiteWeb extends AbstractAction {
		private static final long serialVersionUID = 5421401898993620863L;

		public ActionSiteWeb(String name) {
			putValue(Action.NAME, name);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent event) {
			NSApp.openURL("http://www.univ-lr.fr/apps/support/papaye/");
		}
	}

	/** */
	public class ActionDocumentation extends AbstractAction {
		private static final long serialVersionUID = 1019147224997899786L;

		public ActionDocumentation(String name) {
			putValue(Action.NAME, name);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent event) {
			NSApp.openURL("http://www.cocktail.org/cgi-bin/WebObjects/Webgenie.woa/wa/page?type=184;20089;1393;1494;32108");
		}
	}

	/** */
	public class ActionLogs extends AbstractAction {
		private static final long serialVersionUID = 3024284057494664345L;

		public ActionLogs(String name) {
			putValue(Action.NAME, name);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent event) {
			new XLogs("MESSAGES CLIENT ET SERVEUR");
		}
	}

}
