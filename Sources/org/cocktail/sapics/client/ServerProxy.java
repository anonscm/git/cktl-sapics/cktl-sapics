/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author cpinsard
 */
public final class ServerProxy {
	private static final String SESSION_KEY = "session";

	private static final String REQUEST_BD_CONNEXION_NAME = "clientSideRequestBdConnexionName";
	private static final String REQUEST_CLEAN_LOGS = "clientSideRequestCleanLogs";
	private static final String REQUEST_SEND_MAIL = "clientSideRequestSendMail";
	private static final String REQUEST_ERR_LOG = "clientSideRequestErrLog";
	private static final String REQUEST_OUT_LOG = "clientSideRequestOutLog";
	private static final String REQUEST_GET_CONNECTED_USERS = "clientSideRequestGetConnectedUsers";
	private static final String REQUEST_BASCULE_EXERCICE = "clientSideRequestBasculeExercice";
	private static final String REQUEST_GET_PARAM = "clientSideRequestGetParam";
	private static final String REQUEST_SAVE_PREFERENCE = "clientSideRequestSavePreference";
	private static final String REQUEST_REMOVE_FROM_UTILISATEURS_CONNECTES = "removeFromUtilisateursConnectes";
	private static final String REQUEST_ADD_TO_UTILISATEURS_CONNECTES = "clientSideRequestAddToUtilisateursConnectes";
	private static final String REQUEST_SET_LOGIN_PARAMETRES = "clientSideRequestSetLoginParametres";
	private static final String REQUEST_PRIMARY_KEY_FOR_OBJECT = "clientSideRequestPrimaryKeyForObject";
	private static final String REQUEST_APP_VERSION = "clientSideRequestAppVersion";
	private static final String REQUEST_SQL_QUERY = "clientSideRequestSqlQuery";

	private ServerProxy() {
	}

	/**
	 * Permet d'invoquer une méthode sur la classe Session du serveur
	 * 
	 */
	private static final class RemoteMethod<T> {
		private EOEditingContext edc;
		private String method;
		private Class[] argTypes;
		private Object[] args;

		private RemoteMethod(EOEditingContext ec, String methodName, Class[] argumentTypes, Object[] arguments) {
			edc = ec;
			method = methodName;
			argTypes = argumentTypes;
			args = arguments;
		}

		private T invoke() {
			return (T) ((EODistributedObjectStore) edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(edc, SESSION_KEY, method, argTypes, args, false);
		}

		public static <E> E invokeStatic(EOEditingContext ec, String methodName, Class[] argumentTypes, Object[] arguments) {
			return new RemoteMethod<E>(ec, methodName, argumentTypes, arguments).invoke();
		}
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param sql
	 *            La requête
	 * @return Le résultat de la requête
	 */
	public static NSArray clientSideRequestSqlQuery(EOEditingContext ec, final String sql) {
		Class[] clazz = new Class[] {String.class};
		Object[] args = new Object[] {sql};
		return RemoteMethod.invokeStatic(ec, REQUEST_SQL_QUERY, clazz, args);

		// return new RemoteMethod<NSArray>(ec, REQUEST_SQL_QUERY, clazz, args).invoke();
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @return La version de l'application
	 */
	public static String clientSideRequestAppVersion(EOEditingContext ec) {
		// return new RemoteMethod<String>(ec, REQUEST_APP_VERSION, null, null).invoke();
		return RemoteMethod.invokeStatic(ec, REQUEST_APP_VERSION, null, null);
	}

	/**
	 * Recuperation d'une cle primaire
	 * 
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param eo
	 *            Objet pour lequel on veut recuperer la cle primaire
	 * 
	 * @return Retourne un dictionnaire contenant les cles primaires et leur valeur
	 */
	public static NSDictionary clientSideRequestPrimaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) {
		Class[] clazz = new Class[] {EOEnterpriseObject.class};
		Object[] args = new Object[] {eo};
		// return new RemoteMethod<NSDictionary>(ec, REQUEST_PRIMARY_KEY_FOR_OBJECT, clazz, args).invoke();
		return RemoteMethod.invokeStatic(ec, REQUEST_PRIMARY_KEY_FOR_OBJECT, clazz, args);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param login
	 *            Le login
	 * @param ip
	 *            L'adresse ip
	 */
	public static void clientSideRequestSetLoginParametres(EOEditingContext ec, String login, String ip) {
		Class[] clazz = new Class[] {String.class, String.class};
		Object[] args = new Object[] {login, ip};
		// new RemoteMethod<Boolean>(ec, REQUEST_SET_LOGIN_PARAMETRES, clazz, args).invoke();
		RemoteMethod.invokeStatic(ec, REQUEST_SET_LOGIN_PARAMETRES, clazz, args);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param parametres
	 *            L'utilisateur
	 */
	public static void clientSideRequestAddToUtilisateursConnectes(EOEditingContext ec, NSDictionary parametres) {
		Class[] clazz = new Class[] {NSDictionary.class};
		Object[] args = new Object[] {parametres};
		RemoteMethod.invokeStatic(ec, REQUEST_ADD_TO_UTILISATEURS_CONNECTES, clazz, args);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param parametres
	 *            L'utilisateur
	 */
	public static void clientSideRequestRemoveFromUtilisateursConnectes(EOEditingContext ec, NSDictionary parametres) {
		Class[] clazz = new Class[] {NSDictionary.class};
		Object[] args = new Object[] {parametres};
		RemoteMethod.invokeStatic(ec, REQUEST_REMOVE_FROM_UTILISATEURS_CONNECTES, clazz, args);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param util
	 *            Un {@link EOEnterpriseObject}
	 * @param prefKey
	 *            Clé
	 * @param newValue
	 *            Valeur
	 * @param defaultValue
	 *            Valeur par défaut
	 * @param description
	 *            Description
	 * @return Un {@link EOEnterpriseObject}
	 * @throws Exception
	 *             Si erreur d'enregistrement
	 */
	public static EOEnterpriseObject clientSideRequestSavePreference(final EOEditingContext ec, final EOEnterpriseObject util, final String prefKey,
			final String newValue, final String defaultValue, final String description) throws Exception {
		Class[] clazz = new Class[] {EOEnterpriseObject.class, String.class, String.class, String.class, String.class};
		Object[] args = new Object[] {util, prefKey, newValue, defaultValue, description};
		return RemoteMethod.invokeStatic(ec, REQUEST_SAVE_PREFERENCE, clazz, args);
	}

	/**
	 * Recupere le parametre paramKey, en cherchant dans l'ordre: la table parametresTableName(), le fichier
	 * configFileName(), la table configTableName()
	 * 
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param paramKey
	 *            Le paramètre
	 * @return La valeur du parametre trouve (String) ou null si rien trouve
	 */
	public static String clientSideRequestGetParam(EOEditingContext ec, String paramKey) {
		Class[] clazz = new Class[] {String.class};
		Object[] args = new Object[] {paramKey};
		return RemoteMethod.invokeStatic(ec, REQUEST_GET_PARAM, clazz, args);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param parametres
	 *            Les paramètres
	 * @throws Exception
	 *             Si erreur lors de la bascule
	 */
	public static void clientSideRequestBasculeExercice(EOEditingContext ec, NSDictionary parametres) throws Exception {
		Class[] clazz = new Class[] {NSDictionary.class};
		Object[] args = new Object[] {parametres};
		RemoteMethod.invokeStatic(ec, REQUEST_BASCULE_EXERCICE, clazz, args);

	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @return Un tableau de NSDictionary stockant les infos sur les utilisateurs connectes.
	 */
	public static NSArray clientSideRequestGetConnectedUsers(EOEditingContext ec) {
		return RemoteMethod.invokeStatic(ec, REQUEST_GET_CONNECTED_USERS, null, null);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @return Les logs
	 */
	public static String clientSideRequestOutLog(EOEditingContext ec) {
		return RemoteMethod.invokeStatic(ec, REQUEST_OUT_LOG, null, null);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @return Les logs d'erreur
	 */
	public static String clientSideRequestErrLog(EOEditingContext ec) {
		return RemoteMethod.invokeStatic(ec, REQUEST_ERR_LOG, null, null);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @param mailFrom
	 *            Expéditeur
	 * @param mailTo
	 *            Destinataire
	 * @param mailCc
	 *            Copie
	 * @param mailSubject
	 *            Sujet
	 * @param mailBody
	 *            Corps
	 * @throws Exception
	 *             Si erreur lors de l'envoi
	 */
	public static void clientSideRequestSendMail(EOEditingContext ec, String mailFrom, String mailTo, String mailCc, String mailSubject, String mailBody)
			throws Exception {
		Class[] clazz = new Class[] {String.class, String.class, String.class, String.class, String.class};
		Object[] args = new Object[] {mailFrom, mailTo, mailCc, mailSubject, mailBody};
		RemoteMethod.invokeStatic(ec, REQUEST_SEND_MAIL, clazz, args);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 */
	public static void clientSideRequestCleanLogs(EOEditingContext ec) {
		RemoteMethod.invokeStatic(ec, REQUEST_CLEAN_LOGS, null, null);
	}

	/**
	 * @param ec
	 *            L'{@link EOEditingContext}
	 * @return Le nom de la connexion
	 */
	public static String clientSideRequestBdConnexionName(EOEditingContext ec) {
		return RemoteMethod.invokeStatic(ec, REQUEST_BD_CONNEXION_NAME, null, null);
	}
}
