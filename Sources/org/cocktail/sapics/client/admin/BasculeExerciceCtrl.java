/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client.admin;

import org.cocktail.sapics.client.ServerProxy;
import org.cocktail.sapics.client.eof.model.EOCodeExer;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Effectue la bascule d'exercice
 */
public class BasculeExerciceCtrl {
	private static BasculeExerciceCtrl sharedInstance;
	private EOEditingContext ec;

	/**
	 * @param editingContext
	 *            Un {@link EOEditingContext}
	 */
	public BasculeExerciceCtrl(EOEditingContext editingContext) {
		super();
		ec = editingContext;
	}

	/**
	 * @param editingContext
	 *            Un {@link EOEditingContext}
	 * @return L'instance partagée
	 */
	public static BasculeExerciceCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new BasculeExerciceCtrl(editingContext);
		}
		return sharedInstance;
	}

	/**
	 * Fait la bascule d'exercice
	 */
	public void basculer() {
		EOExercice lastExercice = EOCodeExer.findLastExercice(ec);
		EOExercice nouvelExercice = EOExercice.find(ec, new Integer(lastExercice.exeExercice().intValue() + 1));

		if (nouvelExercice == null) {
			EODialogs.runInformationDialog("ERREUR", "L''exercice " + (lastExercice.exeExercice().intValue() + 1) + " n'est pas encore créé !");
			return;
		}

		if (!EODialogs.runConfirmOperationDialog("Attention", "Confirmez vous la bascule pour l'exercice " + nouvelExercice.exeExercice() + " ?", "OUI", "NON")) {
			return;
		}

		try {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(nouvelExercice.exeExercice(), EOExercice.EXE_EXERCICE_KEY);
			ServerProxy.clientSideRequestBasculeExercice(ec, parametres);
			EODialogs.runInformationDialog("OK", "La bascule d'exercice pour " + nouvelExercice.exeExercice() + " a bien été effectuée !");
		} catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
		}
	}
}
