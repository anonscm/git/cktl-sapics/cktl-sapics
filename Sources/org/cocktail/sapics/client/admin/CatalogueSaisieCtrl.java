package org.cocktail.sapics.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.sapics.client.eof.model.EOCatalogue;
import org.cocktail.sapics.client.eof.model.EOFournis;
import org.cocktail.sapics.client.gui.CatalogueSaisieView;
import org.cocktail.sapics.client.lots.FournisseurSelectCtrl;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * 
 */
public class CatalogueSaisieCtrl {
	private static CatalogueSaisieCtrl sharedInstance;
	private EOEditingContext ec;
	private CatalogueSaisieView myView;
	private EOCatalogue currentCatalogue;
	private EOFournis currentFournis;

	/**
	 * @param globalEc
	 *            Un {@link EOEditingContext}
	 */
	public CatalogueSaisieCtrl(EOEditingContext globalEc) {
		super();
		ec = globalEc;
		myView = new CatalogueSaisieView(new JFrame(), true);
		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnGetDebut().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDebut();
			}
		});

		myView.getBtnGetFin().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFin();
			}
		});

		myView.getBtnGetFournisseur().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFournisseur();
			}
		});

		myView.getTfDebut().addFocusListener(new ListenerTextFieldDateDebut());
		myView.getTfDebut().addActionListener(new ActionListenerDateDebut());

		myView.getTfFin().addFocusListener(new ListenerTextFieldDateFin());
		myView.getTfFin().addActionListener(new ActionListenerDateFin());

	}

	/**
	 * @param editingContext
	 *            Un {@link EOEditingContext}
	 * @return L'instance partagée
	 */
	public static CatalogueSaisieCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new CatalogueSaisieCtrl(editingContext);
		}
		return sharedInstance;
	}

	/**
	 * @param catalogue
	 *            Un {@link EOCatalogue}
	 */
	public void modifier(EOCatalogue catalogue) {
		currentCatalogue = catalogue;
		actualiser();
		myView.setVisible(true);
	}

	private void clearTextField() {
		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getAreaCommentaire().setText("");
		myView.getAreaLibelle().setText("");
	}

	private void actualiser() {
		clearTextField();

		myView.getTfDebut().setText(DateCtrl.dateToString(currentCatalogue.catDateDebut()));
		myView.getTfFin().setText(DateCtrl.dateToString(currentCatalogue.catDateFin()));

		currentFournis = currentCatalogue.fournis();

		if (currentFournis != null) {
			myView.getTfFournisseur().setText(currentFournis.nomComplet());
		}
		if (currentCatalogue.catLibelle() != null) {
			myView.getAreaLibelle().setText(currentCatalogue.catLibelle());
		}
		if (currentCatalogue.catCommentaire() != null) {
			myView.getAreaCommentaire().setText(currentCatalogue.catCommentaire());
		}
	}

	private void getFournisseur() {
		EOFournis fournisseur = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();
		if (fournisseur != null) {
			currentFournis = fournisseur;
			myView.getTfFournisseur().setText(currentFournis.nomComplet());
		}
	}

	private void getDebut() {
		CocktailUtilities.setMyTextField(myView.getTfDebut());
		CocktailUtilities.showDatePickerPanel(myView);
	}

	private void getFin() {
		CocktailUtilities.setMyTextField(myView.getTfFin());
		CocktailUtilities.showDatePickerPanel(myView);
	}

	/**
	 *
	 */
	private void valider() {
		try {
			if (myView.getAreaLibelle().getText().length() > 0) {
				currentCatalogue.setCatLibelle(myView.getAreaLibelle().getText());
			}

			if (myView.getAreaCommentaire().getText().length() > 0) {
				currentCatalogue.setCatCommentaire(myView.getAreaCommentaire().getText());
			} else {
				currentCatalogue.setCatCommentaire(" ");
			}

			if (myView.getTfDebut().getText().length() > 0) {
				currentCatalogue.setCatDateDebut(DateCtrl.stringToDate(myView.getTfDebut().getText()));
			}

			if (myView.getTfFin().getText().length() > 0) {
				currentCatalogue.setCatDateFin(DateCtrl.stringToDate(myView.getTfFin().getText()));
			}

			if (currentFournis != null) {
				currentCatalogue.setFournisRelationship(currentFournis);
			}

			ec.saveChanges();
			myView.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Annule les opérations en cours
	 */
	public void annuler() {
		ec.revert();
		myView.dispose();
	}

	private class ActionListenerDateDebut implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ("".equals(myView.getTfDebut().getText())) {
				return;
			}

			String myDate = DateCtrl.dateCompletion(myView.getTfDebut().getText());
			if ("".equals(myDate)) {
				myView.getTfDebut().selectAll();
				EODialogs.runInformationDialog("Date non valide", "La date d'effet n'est pas valide !");
			} else {
				myView.getTfDebut().setText(myDate);
			}
		}
	}

	private class ListenerTextFieldDateDebut implements FocusListener {
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			if ("".equals(myView.getTfDebut().getText())) {
				return;
			}

			String myDate = DateCtrl.dateCompletion(myView.getTfDebut().getText());
			if ("".equals(myDate)) {
				EODialogs.runInformationDialog("Date Invalide", "La date d'effet n'est pas valide !");
				myView.getTfDebut().selectAll();
			} else {
				myView.getTfDebut().setText(myDate);
			}
		}
	}

	private class ActionListenerDateFin implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ("".equals(myView.getTfFin().getText())) {
				return;
			}

			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
			if ("".equals(myDate)) {
				myView.getTfFin().selectAll();
				EODialogs.runInformationDialog("Date non valide", "La date de fin n'est pas valide !");
			} else {
				myView.getTfFin().setText(myDate);
			}
		}
	}

	private class ListenerTextFieldDateFin implements FocusListener {
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			if ("".equals(myView.getTfFin().getText())) {
				return;
			}

			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
			if ("".equals(myDate)) {
				EODialogs.runInformationDialog("Date Invalide", "La date de fin n'est pas valide !");
				myView.getTfFin().selectAll();
			} else {
				myView.getTfFin().setText(myDate);
			}
		}
	}
}