package org.cocktail.sapics.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.attributions.ArticleSaisieCtrl;
import org.cocktail.sapics.client.attributions.OptionSaisieCtrl;
import org.cocktail.sapics.client.eof.model.EOArticle;
import org.cocktail.sapics.client.eof.model.EOArticleMarche;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOAttributionCatalogue;
import org.cocktail.sapics.client.eof.model.EOCatalogue;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticle;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticleOpt;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.eof.model.EOFournis;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOTypeEtat;
import org.cocktail.sapics.client.gui.CataloguesView;
import org.cocktail.sapics.client.marches.MarchesCtrl;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.DateCtrl;
import org.cocktail.sapics.client.utilities.StringCtrl;
import org.cocktail.sapics.client.eof.model.EOMarche;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 *
 */
public class CataloguesCtrl {
	private static CataloguesCtrl sharedInstance;
	private org.cocktail.sapics.client.ApplicationClient NSApp = (ApplicationClient) ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	private EODisplayGroup eodCatalogues = new EODisplayGroup(), eodArticles = new EODisplayGroup(), eodOptions = new EODisplayGroup();
	private CataloguesView myView;

	private ListenerCatalogues listenerCatalogue = new ListenerCatalogues();
	private ListenerArticles listenerArticle = new ListenerArticles();
	private ListenerValidite listenerValidite = new ListenerValidite();

	private EOAttributionCatalogue currentAttributionCatalogue;
	private EOCatalogue currentCatalogue;
	private EOCatalogueArticle currentCatalogueArticle;
	private EOCatalogueArticleOpt currentOption;
	private EOExercice currentExercice;

	/**
	 * @param globalEc
	 *            Un {@link EOEditingContext}
	 */
	public CataloguesCtrl(EOEditingContext globalEc) {
		super();
		ec = globalEc;
		myView = new CataloguesView(new JFrame(), true, eodCatalogues, eodArticles, eodOptions);
		myView.getBtnAddCatalogue().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterCatalogue();
			}
		});

		myView.getBtnUpdateCatalogue1().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierCatalogue();
			}
		});

		myView.getBtnDelCatalogue().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerCatalogue();
			}
		});

		myView.getBtnAddArticle().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterArticle();
			}
		});

		myView.getBtnUpdateArticle().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierArticle();
			}
		});

		myView.getBtnDelArticle().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerArticle();
			}
		});

		myView.getBtnAddOption().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterOption();
			}
		});

		myView.getBtnUpdateOption().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierOption();
			}
		});

		myView.getBtnDelOption().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerOption();
			}
		});

		myView.getTfFiltreCatalogue().getDocument().addDocumentListener(new DocumentListenerCatalogue());
		myView.getTfFiltreFournisseur().getDocument().addDocumentListener(new DocumentListenerCatalogue());
		myView.getTfFiltreMarche().getDocument().addDocumentListener(new DocumentListenerCatalogue());
		myView.getTfFiltreLot().getDocument().addDocumentListener(new DocumentListenerCatalogue());

		myView.setListeExercices(EOExercice.find(ec));
		currentExercice = EOExercice.exerciceCourant(ec);
		myView.getExercices().setSelectedItem(currentExercice);
		myView.getExercices().setSelectedIndex(0);
		myView.getExercices().addActionListener(new PopupExerciceListener());

		myView.getMyEOTableCatalogues().addListener(listenerCatalogue);
		myView.getMyEOTableArticles().addListener(listenerArticle);
		myView.getMyEOTableOptions().addListener(new ListenerOptions());

		myView.getCheckAnnule().addItemListener(listenerValidite);
		myView.getCheckValide().addItemListener(listenerValidite);

		myView.getCheckValide().setSelected(true);

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CataloguesCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new CataloguesCtrl(editingContext);
		}
		return sharedInstance;
	}

	private class PopupExerciceListener implements ActionListener {
		public PopupExerciceListener() {
			super();
		}

		public void actionPerformed(ActionEvent anAction) {
			currentExercice = null;
			if (myView.getExercices().getSelectedIndex() > 0) {
				currentExercice = (EOExercice) myView.getExercices().getSelectedItem();
			}
			filter();
		}
	}

	/**
	 * Ouvre la fenêtre
	 */
	public void open() {
		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		currentExercice = MarchesCtrl.sharedInstance(ec).getExercice();
		myView.getExercices().setSelectedItem(currentExercice);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		NSApp.setGlassPane(false);
	}

	private void clearTextField() {
	}

	private void actualiser() {
		clearTextField();
		eodCatalogues.setObjectArray(EOAttributionCatalogue.findCataloguesValides(ec));
		filter();
	}

	private void ajouterCatalogue() {
		EOCatalogue newCatalogue = EOCatalogue.creer(ec);
		CatalogueSaisieCtrl.sharedInstance(ec).modifier(newCatalogue);
		actualiser();
	}

	private void supprimerCatalogue() {
		if (!EODialogs.runConfirmOperationDialog("Attention", "Désirez vous réellement invalider ce catalogue ?", "OUI", "NON")) {
			return;
		}

		try {
			currentCatalogue.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));
			ec.saveChanges();
			eodCatalogues.deleteSelection();
			myView.getMyEOTableCatalogues().updateData();
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}

	private void ajouterArticle() {
		EOArticle newArticle = EOArticle.creer(ec);
		EOArticleMarche newArticleMarche = EOArticleMarche.creer(ec, newArticle);
		EOCatalogueArticle newCatalogueArticle = EOCatalogueArticle.creer(ec, currentCatalogue);
		newCatalogueArticle.setArticleRelationship(newArticle);
		ArticleSaisieCtrl.sharedInstance(ec).modifier(null, newCatalogueArticle, newArticleMarche);
		listenerCatalogue.onSelectionChanged();
	}

	private void modifierCatalogue() {
		CatalogueSaisieCtrl.sharedInstance(ec).modifier(currentCatalogue);
		myView.getMyEOTableCatalogues().updateUI();
	}

	private void modifierArticle() {
		EOArticleMarche articleMarche = EOArticleMarche.findForArticle(ec, currentCatalogueArticle.article());
		if (articleMarche == null) {
			articleMarche = EOArticleMarche.creer(ec, currentCatalogueArticle.article());
		}

		ArticleSaisieCtrl.sharedInstance(ec).modifier(null, currentCatalogueArticle, articleMarche);
	}

	private void supprimerArticle() {

		if (!EODialogs.runConfirmOperationDialog("Attention", "Désirez vous réellement invalider cet article ?", "OUI", "NON")) {
			return;
		}

		try {
			for (int i = 0; i < eodOptions.displayedObjects().count(); i++) {
				EOCatalogueArticleOpt option = (EOCatalogueArticleOpt) eodOptions.displayedObjects().objectAtIndex(i);
				option.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));
			}
			currentCatalogueArticle.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));
			ec.saveChanges();
			listenerCatalogue.onSelectionChanged();
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}

	private void ajouterOption() {
		EOArticle newArticle = EOArticle.creer(ec);
		EOArticleMarche newArticleMarche = EOArticleMarche.creer(ec, newArticle);
		EOCatalogueArticleOpt newCatalogueArticleOpt = EOCatalogueArticleOpt.creer(ec, currentCatalogue);
		newCatalogueArticleOpt.setArticleRelationship(newArticle);
		newArticle.setPereRelationship(currentCatalogueArticle.article());
		OptionSaisieCtrl.sharedInstance(ec).modifier(null, newCatalogueArticleOpt, newArticleMarche);
		listenerArticle.onSelectionChanged();
	}

	private void modifierOption() {
		EOArticleMarche articleMarche = EOArticleMarche.findForArticle(ec, currentOption.article());
		if (articleMarche == null) {
			articleMarche = EOArticleMarche.creer(ec, currentOption.article());
		}
		OptionSaisieCtrl.sharedInstance(ec).modifier(null, currentOption, articleMarche);
		listenerArticle.onSelectionChanged();
	}

	private void supprimerOption() {
		if (!EODialogs.runConfirmOperationDialog("Attention", "Désirez vous réellement supprimer cet option ?", "OUI", "NON")) {
			return;
		}

		try {
			currentOption.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));
			ec.saveChanges();
			listenerArticle.onSelectionChanged();
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}

	private class ListenerCatalogues implements ZEOTable.ZEOTableListener {

		public void onDbClick() {
			modifierCatalogue();
		}

		public void onSelectionChanged() {
			CRICursor.setWaitCursor(myView);
			currentAttributionCatalogue = (EOAttributionCatalogue) eodCatalogues.selectedObject();
			eodArticles.setObjectArray(new NSArray());
			eodOptions.setObjectArray(new NSArray());
			if (currentAttributionCatalogue != null) {
				currentCatalogue = currentAttributionCatalogue.catalogue();
				eodArticles.setObjectArray(EOCatalogueArticle.findForCatalogue(ec, currentCatalogue));
			}

			myView.getMyEOTableArticles().updateData();
			myView.getMyEOTableOptions().updateData();
			updateUI();
			CRICursor.setDefaultCursor(myView);
		}
	}

	private class ListenerArticles implements ZEOTable.ZEOTableListener {

		public void onDbClick() {
			modifierArticle();
		}

		public void onSelectionChanged() {
			currentCatalogueArticle = (EOCatalogueArticle) eodArticles.selectedObject();
			eodOptions.setObjectArray(new NSArray());
			if (currentCatalogueArticle != null) {
				eodOptions.setObjectArray(EOCatalogueArticleOpt.findForArticle(ec, currentCatalogueArticle.article()));
			}

			myView.getMyEOTableOptions().updateData();
			updateUI();
		}
	}

	private class ListenerOptions implements ZEOTable.ZEOTableListener {

		public void onDbClick() {
			modifierOption();
		}

		public void onSelectionChanged() {
			currentOption = (EOCatalogueArticleOpt) eodOptions.selectedObject();
			updateUI();
		}
	}

	/**
	 * Classe d'ecoute pour le changement d'affichage de la liste des contrats.
	 */
	private class ListenerValidite implements ItemListener {
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				filter();
			}
		}
	}

	private void updateUI() {
		myView.getBtnAddCatalogue().setEnabled(false);
		myView.getBtnAddCatalogue().setVisible(false);
		myView.getBtnUpdateCatalogue1().setEnabled(currentCatalogue != null);
		myView.getBtnDelCatalogue().setEnabled(currentCatalogue != null);

		myView.getBtnAddArticle().setEnabled(currentCatalogue != null);
		myView.getBtnUpdateArticle().setEnabled(currentCatalogueArticle != null);
		myView.getBtnDelArticle().setEnabled(currentCatalogueArticle != null);

		myView.getBtnAddOption().setEnabled(currentCatalogue != null && currentCatalogueArticle != null);
		myView.getBtnUpdateOption().setEnabled(currentOption != null);
		myView.getBtnDelOption().setEnabled(currentOption != null);
	}

	/**
	 * @return Le qualifier pour le filtre
	 */
	public EOQualifier getFilterQualifier() {
		System.out.println("CataloguesCtrl.getFilterQualifier() " + currentExercice);
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObjectsFromArray(getFiltreExercice());
		mesQualifiers.addObjectsFromArray(getFiltreTypeEtat());
		mesQualifiers.addObjectsFromArray(getFiltreCatalogue());
		mesQualifiers.addObjectsFromArray(getFiltreFournisseur());
		mesQualifiers.addObjectsFromArray(getFiltreLot());
		mesQualifiers.addObjectsFromArray(getFiltreMarche());
		return new EOAndQualifier(mesQualifiers);
	}

	private NSArray<EOQualifier> getFiltreExercice() {
		String toCatalogue = EOAttributionCatalogue.CATALOGUE_KEY + ".";
		if (currentExercice != null) {
			NSTimestamp dateExerDebut = DateCtrl.stringToDate("01/01/" + currentExercice.exeExercice());
			NSTimestamp dateExerFin = DateCtrl.stringToDate("31/12/" + currentExercice.exeExercice());

			EOQualifier qualExer1 = new EOKeyValueQualifier(toCatalogue + EOCatalogue.CAT_DATE_FIN_KEY, EOQualifier.QualifierOperatorEqual, null);
			EOQualifier qualExer2 = new EOKeyValueQualifier(toCatalogue + EOCatalogue.CAT_DATE_FIN_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo,
					dateExerDebut);
			EOQualifier qualExer3 = new EOKeyValueQualifier(toCatalogue + EOCatalogue.CAT_DATE_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo,
					dateExerFin);

			EOQualifier qualOr = new EOOrQualifier(
                                    new NSArray<EOQualifier>(new EOQualifier[]{qualExer1, qualExer2})
                                );
			return new NSArray<EOQualifier>(new EOQualifier[]{qualOr, qualExer3});
		}
		return NSArray.emptyArray();
	}

	private NSArray<EOQualifier> getFiltreTypeEtat() {
		String toCatalogue = EOAttributionCatalogue.CATALOGUE_KEY + ".";
		EOTypeEtat typeEtat;
		if (myView.getCheckAnnule().isSelected()) {
			typeEtat = EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE);
		} else {
			typeEtat = EOTypeEtat.find(ec, EOTypeEtat.ETAT_VALIDE);
		}
		EOQualifier qualTypeEtat = new EOKeyValueQualifier(toCatalogue + EOCatalogue.TYPE_ETAT_KEY, EOQualifier.QualifierOperatorEqual, typeEtat);
		return new NSArray<EOQualifier>(qualTypeEtat);
	}

	private NSArray<EOQualifier> getFiltreCatalogue() {
		String toCatalogue = EOAttributionCatalogue.CATALOGUE_KEY + ".";
		String filtreCatalogue = myView.getTfFiltreCatalogue().getText();
		if (!StringCtrl.chaineVide(filtreCatalogue)) {
			EOQualifier qualFiltreCatalogue = new EOKeyValueQualifier(toCatalogue + EOCatalogue.CAT_LIBELLE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtreCatalogue + "*");
			return new NSArray<EOQualifier>(qualFiltreCatalogue);
		}
		return NSArray.emptyArray();
	}

	private NSArray<EOQualifier> getFiltreFournisseur() {
		String toCatalogue = EOAttributionCatalogue.CATALOGUE_KEY + ".";
		String filtreFournisseur = myView.getTfFiltreFournisseur().getText();
		if (!StringCtrl.chaineVide(filtreFournisseur)) {
			EOQualifier qualFiltreFournisseur = new EOKeyValueQualifier(toCatalogue + EOCatalogue.FOURNIS_KEY + "." + EOFournis.NOM_COMPLET_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtreFournisseur + "*");
			return new NSArray<EOQualifier>(qualFiltreFournisseur);
		}
		return NSArray.emptyArray();
	}

	private NSArray<EOQualifier> getFiltreMarche() {
		String toLot = EOAttributionCatalogue.ATTRIBUTION_KEY + "." + EOAttribution.LOT_KEY + ".";
		String filtreMarche = myView.getTfFiltreMarche().getText();
		if (!StringCtrl.chaineVide(filtreMarche)) {
			EOQualifier qualFiltreMarche = new EOKeyValueQualifier(toLot + EOLot.MARCHE_KEY + "." + EOMarche.MAR_LIBELLE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtreMarche + "*");
			return new NSArray<EOQualifier>(qualFiltreMarche);
		}
		return NSArray.emptyArray();
	}

	private NSArray<EOQualifier> getFiltreLot() {
		String toLot = EOAttributionCatalogue.ATTRIBUTION_KEY + "." + EOAttribution.LOT_KEY + ".";
		String filtreLot = myView.getTfFiltreLot().getText();
		if (!StringCtrl.chaineVide(filtreLot)) {
			EOQualifier qualFiltreLot = new EOKeyValueQualifier(toLot + EOLot.LOT_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
					+ filtreLot + "*");
			return new NSArray<EOQualifier>(qualFiltreLot);
		}
		return NSArray.emptyArray();
	}

	/**
	 * Filtre les résultats
	 */
	public void filter() {
		eodCatalogues.setQualifier(getFilterQualifier());
		eodCatalogues.updateDisplayedObjects();
		myView.getMyEOTableCatalogues().updateData();
	}

	private class DocumentListenerCatalogue implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();
		}

		public void removeUpdate(DocumentEvent e) {
			filter();
		}
	}

}
