package org.cocktail.sapics.client.admin;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.sapics.client.eof.model.EOCodeExer;
import org.cocktail.sapics.client.eof.model.EOCodeMarche;
import org.cocktail.sapics.client.eof.model.EOTypeCn;
import org.cocktail.sapics.client.eof.model.EOTypeCodeMarche;
import org.cocktail.sapics.client.gui.CodeNomenclatureSaisieView;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 *
 */
public class CodeNomenclatureSaisieCtrl {
	private static CodeNomenclatureSaisieCtrl sharedInstance;
	private EOEditingContext ec;
	private CodeNomenclatureSaisieView myView;
	private EOCodeExer currentCodeExer;
	private EOCodeMarche currentCodeMarche;
	private boolean modeModification;

	/**
	 * @param globalEc
	 *            Un {@link EOEditingContext}
	 */
	public CodeNomenclatureSaisieCtrl(EOEditingContext globalEc) {
		super();
		myView = new CodeNomenclatureSaisieView(new JFrame(), true);
		ec = globalEc;
		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.setTypesCodes(EOTypeCn.find(ec));
		myView.getCheckRecherche().setSelected(true);
	}

	/**
	 * 
	 * @param editingContext
	 *            Un {@link EOEditingContext}
	 * @return L'instance partagée
	 */
	public static CodeNomenclatureSaisieCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new CodeNomenclatureSaisieCtrl(editingContext);
		}
		return sharedInstance;
	}

	/**
	 * @param codeExer
	 *            L'exercice
	 */
	public void modifier(EOCodeExer codeExer) {
		currentCodeExer = codeExer;
		modeModification = false;
		System.out.println("CodeNomenclatureSaisieCtrl.modifier() " + currentCodeExer);

		if (codeExer.codemarche() != null) {
			currentCodeMarche = currentCodeExer.codemarche();
		}

		if (currentCodeMarche != null && currentCodeMarche.cmCode() != null) {
			myView.getTfTitre().setText("Modification d'un code nomenclature");
			modeModification = true;
		} else {
			myView.getTfTitre().setText("Saisie d'un nouveau code nomenclature");
		}
		clearTextField();

		if (currentCodeMarche == null || currentCodeMarche.pere() == null) {
			CocktailUtilities.initTextField(myView.getTfDomaine(), false, true);			
		} else if (currentCodeMarche.pere().cmNiveau() != null && currentCodeMarche.pere().cmNiveau() == 1) {
			CocktailUtilities.initTextField(myView.getTfFamille(), false, true);
		} 		
		actualiser();
		myView.setVisible(true);
	}

	private void clearTextField() {
		myView.getAreaDetail().setText("");
		myView.getTfMontant().setText("");
		myView.getTfLibelle().setText("");

		CocktailUtilities.initTextField(myView.getTfDomaine(), true, false);
		CocktailUtilities.initTextField(myView.getTfFamille(), true, false);
	}

	private void actualiser() {
		actualiserCodeExer();
		actualiserCodeMarche();
		actualiserModifiable();
	}

	private void actualiserModifiable() {
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, currentCodeMarche.isModifiable());
		if (!currentCodeMarche.isModifiable()) {
			CocktailUtilities.initTextField(myView.getTfDomaine(), false, false);
			CocktailUtilities.initTextField(myView.getTfFamille(), false, false);
		}
		myView.getTypeCode().setEnabled(currentCodeMarche.isModifiable());		
	}

	private void actualiserCodeExer() {
		if (currentCodeExer == null) {
			return;
		}
		myView.getCheck3Cmp().setSelected(currentCodeExer.ce3Cmp().intValue() == 1);
		myView.getCheckAutres().setSelected(currentCodeExer.ceAutres().intValue() == 1);
		myView.getCheckMonopole().setSelected(currentCodeExer.ceMonopole().intValue() == 1);
		myView.getCheckRecherche().setSelected(currentCodeExer.ceRech().equals("O"));

		myView.getTfMontant().setText(currentCodeExer.ceControle().toString());

		if (currentCodeExer.typeCN() != null) {
			myView.getTypeCode().setSelectedItem(currentCodeExer.typeCN());
		}
	}

	private void actualiserCodeMarche() {
		if (currentCodeMarche == null) {
			return;
		}

		if (currentCodeMarche.cmLib() != null) {
			myView.getTfLibelle().setText(currentCodeMarche.cmLib());
		}

		if (currentCodeMarche.cmDetail() != null) {
			myView.getAreaDetail().setText(currentCodeMarche.cmDetail());
		}

		// Le cmcode existe, on est en modification
		NSArray detail = null;
		if (currentCodeMarche.cmCode() != null) {
			detail = NSArray.componentsSeparatedByString(currentCodeMarche.cmCode(), ".");
		} else if (currentCodeMarche.pere() != null) {
			detail = NSArray.componentsSeparatedByString(currentCodeMarche.pere().cmCode(), ".");
		}

		if (detail != null) {
			switch (detail.count()) {
			case 1:
				myView.getTfDomaine().setText((String) detail.objectAtIndex(0));
				break;
			case 2:
				myView.getTfDomaine().setText((String) detail.objectAtIndex(0));
				myView.getTfFamille().setText((String) detail.objectAtIndex(1));
				break;
			case 3:
				myView.getTfDomaine().setText((String) detail.objectAtIndex(0));
				myView.getTfFamille().setText((String) detail.objectAtIndex(1));
				break;
			default:
				break;
			}
		}
	}

	/**
	 *
	 */
	private void valider() {
		String code = null;
		try {
			checkCodeMarche();
			// Construction du code et verification de son existence
			code = constructionCode();
		} catch (ValidationException v) {
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(v));
			return;
		}

		try {
			if (myView.getTfLibelle().getText().length() == 0) {
				EODialogs.runErrorDialog("ERREUR", "Vous devez entrer un libellé pour ce code nomenclature !");
				return;
			}

			if (myView.getTfLibelle().getText().length() > 0) {
				currentCodeMarche.setCmLib(myView.getTfLibelle().getText());
			}

			if (myView.getAreaDetail().getText().length() > 0) {
				currentCodeMarche.setCmDetail(myView.getAreaDetail().getText());
			}
			currentCodeMarche.setCmCode(code);
			if (currentCodeMarche.toTypeCodeMarche() == null) {
				currentCodeMarche.setToTypeCodeMarcheRelationship(EOTypeCodeMarche.getTypeLocal(ec));
			}

			validerCodeExer();

			System.out.println("CodeNomenclatureSaisieCtrl.valider() UPDATE CODE " + currentCodeMarche.cmCode() + " , NIVEAU  : "
					+ currentCodeMarche.cmNiveau());

			ec.saveChanges();

			if (modeModification) {
				EODialogs.runInformationDialog("OK", "Le code nomenclature " + currentCodeMarche.cmCode() + " a bien été modifié.");
			} else {
				EODialogs.runInformationDialog("OK", "Le code nomenclature " + currentCodeMarche.cmCode() + "  a bien été ajouté.");
			}
			myView.dispose();
		} catch (ValidationException v) {
			ec.revert();
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(v));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void validerCodeExer() {
		currentCodeExer.setTypeCNRelationship((EOTypeCn) myView.getTypeCode().getSelectedItem());
		currentCodeExer.setCe3Cmp((myView.getCheck3Cmp().isSelected()) ? new Integer(1) : new Integer(0));
		currentCodeExer.setCeAutres((myView.getCheckAutres().isSelected()) ? new Integer(1) : new Integer(0));
		currentCodeExer.setCeMonopole((myView.getCheckMonopole().isSelected()) ? new Integer(1) : new Integer(0));
		currentCodeExer.setCeRech((myView.getCheckRecherche().isSelected()) ? "O" : "N");
		if (currentCodeExer.ceActif() == null) {
			currentCodeExer.setCeActif("O");
		}
		myView.getTfMontant().setText((NSArray.componentsSeparatedByString(myView.getTfMontant().getText(), ",")).componentsJoinedByString("."));

		BigDecimal ceControle = new BigDecimal(myView.getTfMontant().getText());
		ceControle = ceControle.setScale(2, BigDecimal.ROUND_HALF_UP);

		currentCodeExer.setCeControle(ceControle);
	}

	private String constructionCode() {
		String code = myView.getTfDomaine().getText();

		if (myView.getTfFamille().getText().length() > 0) {
			code = code.concat("." + myView.getTfFamille().getText());
		}

		if (!modeModification) {
			EOCodeExer codeExer = EOCodeExer.codeExerForCodeMarche(ec, currentCodeExer.exercice(), code);
			if (codeExer != null && codeExer.ceSuppr().equals("O")) {
				if (EODialogs.runConfirmOperationDialog("Attention", "Ce code existe déjà mais est supprimé, voulez vous le réactiver ?", "OUI", "NON")) {
					try {
						ec.revert();
						codeExer.setCeSuppr("N");
						ec.saveChanges();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				annuler();
			}
		}

		return code;
	}

	private void checkCodeMarche() {
		if (myView.getTfDomaine().getText().length() == 0) {
			throw new ValidationException("Vous devez entrer un domaine pour ce code nomenclature !");
		}

		if (currentCodeExer.codemarche().pere() != null) {
			if (currentCodeExer.codemarche().pere().cmNiveau().intValue() == 1) {
				if (myView.getTfFamille().getText().length() == 0) {
					throw new ValidationException("Vous devez entrer une famille pour ce code nomenclature !");
				}
				currentCodeMarche.setCmNiveau(new Integer(2));
			} 
		} else {
			currentCodeMarche.setCmNiveau(new Integer(1));
		}
	}

	/**
	 *
	 */
	public void annuler() {
		ec.revert();
		myView.dispose();
	}
}
