package org.cocktail.sapics.client.admin;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer;
import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOCodeExer;
import org.cocktail.sapics.client.eof.model.EOCodeMarche;
import org.cocktail.sapics.client.eof.model.EOCodeMarcheFour;
import org.cocktail.sapics.client.eof.model.EOCodeMarchePlanco;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.eof.model.EOFournis;
import org.cocktail.sapics.client.eof.model.EOMapaCe;
import org.cocktail.sapics.client.eof.model.EOMapaTrancheExer;
import org.cocktail.sapics.client.eof.model.EOTypeCn;
import org.cocktail.sapics.client.gui.CodesNomenclaturesView;
import org.cocktail.sapics.client.lots.FournisseurSelectCtrl;
import org.cocktail.sapics.client.plancomptable.PlanComptableSelectCtrl;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.sapics.client.swing.ZEOTableCellRenderer;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.CocktailConstantes;
import org.cocktail.sapics.client.utilities.CocktailUtilities;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 *
 */
public class CodesNomenclaturesCtrl {
	private static CodesNomenclaturesCtrl sharedInstance;
	private org.cocktail.sapics.client.ApplicationClient NSApp = (ApplicationClient) ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	private EODisplayGroup eodNiveau1 = new EODisplayGroup(), eodNiveau2 = new EODisplayGroup();
	private EODisplayGroup eodFournis = new EODisplayGroup(), eodSeuils = new EODisplayGroup();
	private EODisplayGroup eodDerivationComptable = new EODisplayGroup();

	private CodesNomenclaturesView myView;

	private NiveauxRenderer rendererNiveaux = new NiveauxRenderer();

	private ListenerNiveau1 listenerNiveau1 = new ListenerNiveau1();
	private ListenerNiveau2 listenerNiveau2 = new ListenerNiveau2();
	private ListenerFournis listenerFournis = new ListenerFournis();
	private ListenerSeuil listenerSeuil = new ListenerSeuil();
	private ZEOTableListener listenerDerivationComptable = new ListenerDerivationComptable();

	private EOCodeMarche currentNiveau1, currentNiveau2;
	private EOCodeExer codeExerCurrentNiveau1, codeExerCurrentNiveau2;
	private EOExercice currentExercice;
	private EOCodeMarcheFour currentFournisseur;
	private EOMapaCe currentSeuil;
	private EOCodeMarchePlanco currentCompte;

	/**
	 * @param globalEc
	 *            Un {@link EOEditingContext}
	 */
	public CodesNomenclaturesCtrl(EOEditingContext globalEc) {
		super();
		ec = globalEc;

		myView = new CodesNomenclaturesView(new JFrame(), true, eodNiveau1, eodNiveau2, eodFournis, eodSeuils, eodDerivationComptable, rendererNiveaux, rendererNiveaux);
		myView.getBtnAddNiveau1().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterCodeNiveau1();
			}
		});

		myView.getBtnAddNiveau2().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterCodeNiveau2();
			}
		});

		myView.getBtnDelNiveau1().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerCodeNiveau1();
			}
		});

		myView.getBtnDelNiveau2().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerCodeNiveau2();
			}
		});

		myView.getBtnAddFournis().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterFournis();
			}
		});

		myView.getBtnDelFournis().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerFournis();
			}
		});

		myView.getBtnAddSeuil().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterSeuil();
			}
		});

		myView.getBtnDelSeuil().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerSeuil();
			}
		});

		myView.getBtnUpdate().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				update();
			}
		});
		
		myView.getCheckCNMasques().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent paramActionEvent) {
				actualiser();
			}
		});
		
		myView.getBtnVisibleNiveau1().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent paramActionEvent) {
				toggleVisibiliteNiveau1();
			}
		});
		
		myView.getBtnVisibleNiveau2().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent paramActionEvent) {
				toggleVisibiliteNiveau2();
			}
		});

		if (!NSApp.hasFonction(ApplicationClient.ID_FONC_NOMENCLATURES)) {
			myView.getBtnAddFournis().setVisible(false);
			myView.getBtnAddNiveau1().setVisible(false);
			myView.getBtnAddNiveau2().setVisible(false);
			myView.getBtnAddSeuil().setVisible(false);
			myView.getBtnDelFournis().setVisible(false);
			myView.getBtnDelNiveau1().setVisible(false);
			myView.getBtnDelNiveau2().setVisible(false);
			myView.getBtnDelSeuil().setVisible(false);
			myView.getBtnUpdate().setVisible(false);
			myView.getBtnVisibleNiveau1().setVisible(false);
			myView.getBtnVisibleNiveau2().setVisible(false);
			myView.getCheckCNMasques().setVisible(false);
		}

		myView.setListeExercices(EOExercice.find(ec));
		myView.getExercices().setSelectedItem(EOExercice.exerciceCourant(ec));
		myView.getExercices().addActionListener(new PopupExerciceListener());

		currentExercice = (EOExercice) myView.getExercices().getSelectedItem();

		myView.getTfTitreCodeMarche().setEditable(false);
		
		myView.getMyEOTable2().addListener(listenerNiveau2);

		myView.getMyEOTableFournis().addListener(listenerFournis);
		myView.getMyEOTableSeuils().addListener(listenerSeuil);
		myView.setTypesCodes(EOTypeCn.find(ec));
		setSaisieEnabled(false);
		myView.getTfFiltreNiveau1().getDocument().addDocumentListener(new DocumentListenerNiveau1());
		myView.getTfFiltreNiveau2().getDocument().addDocumentListener(new DocumentListenerNiveau2());
		
		initDerivationComptableUiBinding();
	}
	
	protected void initDerivationComptableUiBinding() {
	    myView.getBtnAddCompte().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajouterCompte();
            }
        });
	    
	    myView.getBtnDelCompte().addActionListener(new java.awt.event.ActionListener() {
	        public void actionPerformed(java.awt.event.ActionEvent evt) {
	            supprimerCompte();
	        }
	    });
	    
	    myView.getMyEOTableDerivationComptable().addListener(listenerDerivationComptable);
	    
	    if (!NSApp.hasFonction(ApplicationClient.ID_FONC_NOMENCLATURES)) {
	        myView.getBtnAddCompte().setVisible(false);
	        myView.getBtnDelCompte().setVisible(false);
	    }
	}

	/**
	 * 
	 * @param editingContext
	 *            {@link EOEditingContext}
	 * @return L'instance partagée
	 */
	public static CodesNomenclaturesCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new CodesNomenclaturesCtrl(editingContext);
		}
		return sharedInstance;
	}

	/**
	 *
	 */
	private class PopupExerciceListener implements ActionListener {
		public PopupExerciceListener() {
			super();
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent anAction) {
			currentExercice = (EOExercice) myView.getExercices().getSelectedItem();
			actualiser();
		}
	}

	/**
	 * 
	 *
	 */
	private void updateUI() {
//		myView.getBtnUpdate().setEnabled((currentNiveau1 != null && currentNiveau1.isModifiable()) || (currentNiveau2 != null && currentNiveau2.isModifiable()));
		myView.getBtnUpdate().setEnabled(currentNiveau1 != null || currentNiveau2 != null);
		myView.getBtnAddFournis().setEnabled(currentNiveau2 != null);
		myView.getBtnDelFournis().setEnabled(currentFournisseur != null);
		myView.getBtnAddSeuil().setEnabled(currentNiveau2 != null);
		myView.getBtnDelSeuil().setEnabled(currentSeuil != null);
		myView.getBtnDelNiveau1().setEnabled(currentNiveau1 != null && currentNiveau1.isSupprimable());
		myView.getBtnDelNiveau2().setEnabled(currentNiveau2 != null && currentNiveau2.isSupprimable());
		myView.getBtnAddNiveau2().setEnabled(currentNiveau1 != null);
		myView.getBtnVisibleNiveau1().setEnabled(currentNiveau1 != null);
		myView.getBtnVisibleNiveau2().setEnabled(currentNiveau2 != null);
		
		updateUIDerivationComptable();
		
		// Boutons de visibilité 
		if (currentNiveau1 != null) {
			EOCodeExer codeExer = codeExerCurrentNiveau1();
			if ("O".equals(codeExer.ceActif())) {
				myView.getBtnVisibleNiveau1().setIcon(CocktailConstantes.ICON_EYE_CLOSE);
			} else {
				myView.getBtnVisibleNiveau1().setIcon(CocktailConstantes.ICON_EYE_OPEN);
			}
		}
		
		if (currentNiveau2 != null) {
			EOCodeExer codeExer = codeExerCurrentNiveau2();
			if ("O".equals(codeExer.ceActif())) {
				myView.getBtnVisibleNiveau2().setIcon(CocktailConstantes.ICON_EYE_CLOSE);
			} else {
				myView.getBtnVisibleNiveau2().setIcon(CocktailConstantes.ICON_EYE_OPEN);
			}
		}
	}

	private void updateUIDerivationComptable() {
	    myView.getBtnAddCompte().setEnabled(currentNiveau2 != null);
	    myView.getBtnDelCompte().setEnabled(currentCompte != null);
	}
	
	/**
	 * Ouvre la fenêtre
	 */
	public void open() {
		NSApp.setGlassPane(true);
		actualiser();
		myView.setVisible(true);
		NSApp.setGlassPane(false);
	}

	private void clearTextField() {
		myView.getTfMontant().setText("");
		myView.getTfTitreCodeMarche().setText("");
	}

	private void ajouterCodeNiveau1() {
		try {
			EOCodeMarche newCodeMarche = EOCodeMarche.creer(ec, null);
			EOCodeExer newCodeExer = EOCodeExer.creer(ec, currentExercice, newCodeMarche);
			CodeNomenclatureSaisieCtrl.sharedInstance(ec).modifier(newCodeExer);
			actualiser();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void ajouterCodeNiveau2() {
		try {
			EOCodeMarche newCodeMarche = EOCodeMarche.creer(ec, currentNiveau1);
			EOCodeExer newCodeExer = EOCodeExer.creer(ec, currentExercice, newCodeMarche);
			CodeNomenclatureSaisieCtrl.sharedInstance(ec).modifier(newCodeExer);
			listenerNiveau1.onSelectionChanged();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void ajouterFournis() {
		try {
			NSApp.setGlassPane(true);
			EOFournis fournisseur = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();
			NSApp.setGlassPane(false);
			if (fournisseur != null) {
				EOCodeMarcheFour.creer(ec, currentNiveau2, fournisseur, currentExercice);
				ec.saveChanges();
				actualiserFournisseurs();
			}
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}

	private void supprimerCodeNiveau1() {
		if (!EODialogs.runConfirmOperationDialog("Attention", "Confirmez vous la suppression du code " + currentNiveau1.cmCode() + " et de ses fils ?", "OUI", "NON")) {
			return;
		}

		try {
			EOCodeExer codeExer = codeExerCurrentNiveau1();
			codeExer.setCeSuppr("O");
			// currentNiveau1.setCmSuppr("O");
			// On supprime aussi les fils de niveau 2
			NSArray<EOCodeExer> listeFils = EOCodeExer.fetchFils(currentNiveau1, currentExercice);
			for (EOCodeExer ceFils : listeFils) {
				ceFils.setCeSuppr("O");
			}
			ec.saveChanges();
			actualiser();
		} catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}
	}

	private void supprimerCodeNiveau2() {
		if (!EODialogs.runConfirmOperationDialog("Attention", "Etes vous surs de vouloir invalider le code " + currentNiveau2.cmCode() + " ?", "OUI", "NON")) {
			return;
		}

		try {
			EOCodeExer codeExer = codeExerCurrentNiveau2();
			codeExer.setCeSuppr("O");
			// currentNiveau2.setCmSuppr("O");
			ec.saveChanges();
			actualiser();
		} catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}
	}

	private void supprimerFournis() {
		if (!EODialogs.runConfirmOperationDialog("Attention", "Confirmez vous la suppression de cette association ?", "OUI", "NON")) {
			return;
		}

		try {
			ec.deleteObject(currentFournisseur);
			ec.saveChanges();
			actualiserFournisseurs();
		} catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}
	}

	/* SEUILS - Begin */
	private void actualiserSeuils() {
		EOCodeExer codeExer = codeExerCurrentNiveau2();
		eodSeuils.setObjectArray(EOMapaCe.findForCodeExer(ec, codeExer));
		myView.getMyEOTableSeuils().updateData();
	}
	
	private void ajouterSeuil() {
		try {
			EOMapaTrancheExer tranche = MapaTrancheExerSelectCtrl.sharedInstance(ec).getTranche(currentExercice,
					(EOTypeCn) myView.getTypeCode().getSelectedItem());
			if (tranche != null) {
				EOCodeExer codeExer = codeExerCurrentNiveau2();
				EOMapaCe.creer(ec, codeExer, tranche, NSApp.getCurrentUtilisateur());
				ec.saveChanges();
				actualiserSeuils();
			}
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}

	private void supprimerSeuil() {
		if (!EODialogs.runConfirmOperationDialog("Attention", "Confirmez vous la suppression de cette association ?", "OUI", "NON")) {
			return;
		}

		try {
			ec.deleteObject(currentSeuil);
			ec.saveChanges();
			actualiserSeuils();
		} catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}
	}
	/* SEUILS - End */
	
	/* COMPTES - Begin */
	private void actualiserComptes() {
	    EOCodeExer codeExer = codeExerCurrentNiveau2();
	    eodDerivationComptable.setObjectArray(EOCodeMarchePlanco.findForCodeExer(ec, codeExer));
	    myView.getMyEOTableDerivationComptable().updateData();
    }
	
	private void ajouterCompte() {
        try {
            NSApp.setGlassPane(true);
            NSArray comptes = PlanComptableSelectCtrl.sharedInstance(ec).getComptes(currentExercice);
            NSApp.setGlassPane(false);
            if (comptes != null && comptes.count() > 0) {
                NSMutableArray codeMarcheComptesAjoutes = new NSMutableArray();
                EOCodeExer codeExercice = codeExerCurrentNiveau2();
                NSArray codeMarchePlancoExistants = EOCodeMarchePlanco.findForCodeExer(ec, codeExercice);
                for (int idx = 0; idx < comptes.count(); idx++) {
                    EOPlanComptableExer compte = (EOPlanComptableExer) comptes.objectAtIndex(idx);
                    if (!isCompteDejaAjoute(codeMarchePlancoExistants, compte)) {
                        codeMarcheComptesAjoutes.addObject(EOCodeMarchePlanco.creer(ec, codeExercice, compte));
                    }
                }
                
                if (codeMarcheComptesAjoutes.count() > 0) {
                    ec.saveChanges();
                    actualiserComptes();
                }
            }
        } catch (Exception e) {
            ec.revert();
            e.printStackTrace();
        }
	}
	
	private boolean isCompteDejaAjoute(NSArray codeMarchePlancoExistants, EOPlanComptableExer compte) {
	    if (compte == null) {
	        return false;
	    }

	    for (int idx = 0; idx < codeMarchePlancoExistants.count(); idx++) {
	        EOCodeMarchePlanco codeMarcheCompte = (EOCodeMarchePlanco) codeMarchePlancoExistants.objectAtIndex(idx);
            if (compte.equals(codeMarcheCompte.toPlanComptable())) {
                return true;
            }
        }
	    
	    return false;
	}
	
	private void supprimerCompte() {
	    if (currentCompte == null) {
	        return;
	    }
	    
	    if (!EODialogs.runConfirmOperationDialog("Attention", "Confirmez vous la suppression de cette association ?", "OUI", "NON")) {
            return;
        }

        try {
            ec.deleteObject(currentCompte);
            ec.saveChanges();
            actualiserComptes();
        } catch (Exception ex) {
            ec.revert();
            ex.printStackTrace();
        }
	}
	/* COMPTES - End */

	private void update() {
		if (currentNiveau2 != null) {
			CodeNomenclatureSaisieCtrl.sharedInstance(ec).modifier(codeExerCurrentNiveau2());
			actualiserDetailCode(currentNiveau2);
		} else if (currentNiveau1 != null) {
			CodeNomenclatureSaisieCtrl.sharedInstance(ec).modifier(codeExerCurrentNiveau1());
			// FIXME RLY ?
			actualiserDetailCode(currentNiveau1);
		}
	}

	/**
	 * @return
	 */
	private EOCodeExer codeExerCurrentNiveau1() {
		return codeExerCurrentNiveau1;
	}
	
	/**
	 * @return
	 */
	private EOCodeExer codeExerCurrentNiveau2() {
		return codeExerCurrentNiveau2;
	}

	private void actualiser() {
		clearTextField();
		NSArray codesMarches = EOCodeMarche.findForCodeExercideAndNiveau(ec, currentExercice, new Integer(1), showCodesMasques());
		eodNiveau1.setObjectArray(codesMarches);
		myView.getMyEOTable1().updateData();
		myView.getMyEOTable1().addListener(listenerNiveau1);
		listenerNiveau1.onSelectionChanged();
	}

	/**
	 * @return
	 */
	private boolean showCodesMasques() {
		return myView.getCheckCNMasques().isSelected();
	}

	private void actualiserFournisseurs() {
		eodFournis.setObjectArray(EOCodeMarcheFour.findForCodeMarche(ec, currentNiveau2, currentExercice));
		myView.getMyEOTableFournis().updateData();
	}

	/**
	 *
	 */
	private class ListenerNiveau1 implements ZEOTable.ZEOTableListener {
		/** {@inheritDoc} */
		public void onDbClick() {
			CodeNomenclatureSaisieCtrl.sharedInstance(ec).modifier(codeExerCurrentNiveau1());
			listenerNiveau1.onSelectionChanged();
		}

		/** {@inheritDoc} */
		public void onSelectionChanged() {
			CRICursor.setWaitCursor(myView);
			
			myView.getMyEOTable2().removeListener(listenerNiveau2);
			eodNiveau2.setObjectArray(new NSArray());
			myView.getMyEOTable2().updateData();
			eodFournis.setObjectArray(new NSArray());
			myView.getMyEOTableFournis().updateData();
			eodSeuils.setObjectArray(new NSArray());
			myView.getMyEOTableSeuils().updateData();
			viderTableDerivationComptable();
			
			currentNiveau1 = (EOCodeMarche) eodNiveau1.selectedObject();
			
			if (currentNiveau1 != null) {
				codeExerCurrentNiveau1 = EOCodeExer.findForCodeMarcheEtExercice(currentNiveau1, currentExercice);
			} else {
				codeExerCurrentNiveau1 = null;
			}
			currentNiveau2 = null;
			codeExerCurrentNiveau2 = null;

			if (currentNiveau1 != null) {
				actualiserDetailCode(currentNiveau1);
				eodNiveau2.setObjectArray(EOCodeMarche.findForPere(ec, currentExercice, currentNiveau1, myView.getCheckCNMasques().isSelected()));
			}

			myView.getMyEOTable2().updateData();
			myView.getMyTableModel2().fireTableDataChanged();

			updateUI();
			CRICursor.setDefaultCursor(myView);
			myView.getMyEOTable2().addListener(listenerNiveau2);
		}
	}

	/**
	 *
	 */
	private class ListenerNiveau2 implements ZEOTable.ZEOTableListener {
		/** {@inheritDoc} */
		public void onDbClick() {
			CodeNomenclatureSaisieCtrl.sharedInstance(ec).modifier(codeExerCurrentNiveau2());
			listenerNiveau2.onSelectionChanged();
		}

		/** {@inheritDoc} */
		public void onSelectionChanged() {
			CRICursor.setWaitCursor(myView);
			currentNiveau2 = (EOCodeMarche) eodNiveau2.selectedObject();
			if (currentNiveau2 != null) {
				codeExerCurrentNiveau2 = EOCodeExer.findForCodeMarcheEtExercice(currentNiveau2, currentExercice);
			} else {
				codeExerCurrentNiveau2 = null;
			}
			
			// vider les tableaux de second niveaux
			eodFournis.setObjectArray(new NSArray());
			myView.getMyEOTableFournis().updateData();
			
			eodSeuils.setObjectArray(new NSArray());
			myView.getMyEOTableSeuils().updateData();

			viderTableDerivationComptable();
			
			// recharger les tableaux de second niveaux.
			if (currentNiveau2 != null) {
				actualiserDetailCode(currentNiveau2);
				actualiserFournisseurs();
				actualiserSeuils();
				actualiserComptes();
			}

			updateUI();
			CRICursor.setDefaultCursor(myView);
		}
	}
	
	private void viderTableDerivationComptable() {
        eodDerivationComptable.setObjectArray(new NSArray());
        myView.getMyEOTableDerivationComptable().updateData();
	}

	/**
	 *
	 */
	private class ListenerFournis implements ZEOTable.ZEOTableListener {

		/** {@inheritDoc} */
		public void onDbClick() {

		}

		/** {@inheritDoc} */
		public void onSelectionChanged() {
			currentFournisseur = (EOCodeMarcheFour) eodFournis.selectedObject();
		}
	}

	/**
	 *
	 */
	private class ListenerSeuil implements ZEOTable.ZEOTableListener {
		/** {@inheritDoc} */
		public void onDbClick() {
		}

		/** {@inheritDoc} */
		public void onSelectionChanged() {
			currentSeuil = (EOMapaCe) eodSeuils.selectedObject();
		}
	}

   private class ListenerDerivationComptable implements ZEOTable.ZEOTableListener {
       /** {@inheritDoc} */
       public void onDbClick() {
       }

       /** {@inheritDoc} */
       public void onSelectionChanged() {
           currentCompte = (EOCodeMarchePlanco) eodDerivationComptable.selectedObject();
           updateUIDerivationComptable();
       }
   }

	private void setSaisieEnabled(boolean yn) {
		CocktailUtilities.initTextField(myView.getTfMontant(), false, yn);
		myView.getTypeCode().setEnabled(yn);
		myView.getCheck3Cmp().setEnabled(yn);
		myView.getCheckMonopole().setEnabled(yn);
		myView.getCheckAutres().setEnabled(yn);
		myView.getCheckRecherche().setEnabled(yn);
	}

	private void actualiserDetailCode(EOCodeMarche codeMarche) {
		if (codeMarche != null) {
			myView.getTfTitreCodeMarche().setText("Détail du code Nomenclature : " + codeMarche.cmCode() + " - " + codeMarche.cmLib());
			EOCodeExer codeExer = EOCodeExer.findForCodeMarcheEtExercice(codeMarche, currentExercice);
			if (codeExer != null) {
				myView.getTfMontant().setText(codeExer.ceControle().toString());
				myView.getTypeCode().setSelectedItem(codeExer.typeCN());
				myView.getCheck3Cmp().setSelected(codeExer.ce3Cmp().intValue() == 1);
				myView.getCheckAutres().setSelected(codeExer.ceAutres().intValue() == 1);
				myView.getCheckMonopole().setSelected(codeExer.ceMonopole().intValue() == 1);
				myView.getCheckRecherche().setSelected(codeExer.ceRech().equals("O"));
				myView.getCheckRecherche().setSelected(codeExer.ceRech().equals("O"));

			}
		}
	}

	public EOQualifier getFilterQualifier1() {
		NSMutableArray mesQualifiers = new NSMutableArray();
		if (!StringCtrl.chaineVide(myView.getTfFiltreNiveau1().getText())) {
			NSArray args = new NSArray("*" + myView.getTfFiltreNiveau1().getText() + "*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CM_CODE_KEY + " caseInsensitiveLike %@", args));
			args = new NSArray("*" + myView.getTfFiltreNiveau1().getText() + "*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CM_LIB_KEY + " caseInsensitiveLike %@", args));
		}
		return new EOOrQualifier(mesQualifiers);
	}

	public void filter1() {
		eodNiveau1.setQualifier(getFilterQualifier1());
		eodNiveau1.updateDisplayedObjects();
		myView.getMyEOTable1().updateData();
	}

	private class DocumentListenerNiveau1 implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter1();
		}

		public void insertUpdate(DocumentEvent e) {
			filter1();
		}

		public void removeUpdate(DocumentEvent e) {
			filter1();
		}
	}

	public EOQualifier getFilterQualifier2() {
		NSMutableArray mesQualifiers = new NSMutableArray();
		if (!StringCtrl.chaineVide(myView.getTfFiltreNiveau2().getText())) {
			NSArray args = new NSArray("*" + myView.getTfFiltreNiveau2().getText() + "*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CM_CODE_KEY + " caseInsensitiveLike %@", args));

			args = new NSArray("*" + myView.getTfFiltreNiveau2().getText() + "*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CM_LIB_KEY + " caseInsensitiveLike %@", args));
		}
		return new EOOrQualifier(mesQualifiers);
	}

	public void filter2() {
		eodNiveau2.setQualifier(getFilterQualifier2());
		eodNiveau2.updateDisplayedObjects();
		myView.getMyEOTable2().updateData();
	}

	private class DocumentListenerNiveau2 implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter2();
		}

		public void insertUpdate(DocumentEvent e) {
			filter2();
		}

		public void removeUpdate(DocumentEvent e) {
			filter2();
		}
	}

	/**
	 *
	 */
	private class NiveauxRenderer extends ZEOTableCellRenderer {
		private static final long serialVersionUID = -3389880708039630191L;

		/** {@inheritDoc} */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final EOCodeMarche obj = (EOCodeMarche) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
			EOCodeExer codeExer = EOCodeExer.findForCodeMarcheEtExercice(obj, currentExercice);
			
			boolean suppr = "O".equals(codeExer.ceSuppr());
			boolean actif = "O".equals(codeExer.ceActif());
//			Le type code marché est indiqué dans le tableau. A voir si l'on conserve le fond coloré...
//			boolean isNacres = EOTypeCodeMarche.CODE_NACRES.equals(obj.toTypeCodeMarche().tcmCode());
//			boolean isAncien = EOTypeCodeMarche.CODE_ANCIEN.equals(obj.toTypeCodeMarche().tcmCode());
			
			leComposant.setForeground(CocktailConstantes.BG_COLOR_BLACK);
			leComposant.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
			
			if (isSelected) {
				leComposant.setBackground(CocktailConstantes.COLOR_SELECTED_ROW);
			} else if (!actif) {
				leComposant.setBackground(CocktailConstantes.BG_COLOR_LIGHT_GREY);
			} else {
				leComposant.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
			}
			
			if (suppr) {
				leComposant.setForeground(CocktailConstantes.BG_COLOR_RED);
			} else if (!actif) {
				leComposant.setForeground(CocktailConstantes.BG_COLOR_LIGHTER_GREY);
			} else if (isSelected) {
				leComposant.setForeground(CocktailConstantes.BG_COLOR_WHITE);
			} else {
				leComposant.setForeground(CocktailConstantes.BG_COLOR_BLACK);
			}
			
//			if (isNacres){
//				leComposant.setBackground(CocktailConstantes.BG_COLOR_CYAN);
//			} else if (isAncien) {
//				leComposant.setBackground(CocktailConstantes.BG_COLOR_LIGHT_GREY);
//			} else {
//				leComposant.setBackground(CocktailConstantes.BG_COLOR_NUIT);
//			}

			return leComposant;
		}
	}

	private void toggleVisibiliteNiveau1() {
		try {
			boolean isActif = "O".equals(codeExerCurrentNiveau1.ceActif());
			if (isActif) {
				codeExerCurrentNiveau1.setCeActif("N");
				EOCodeMarche.findForPere(ec, currentExercice, currentNiveau1);
				NSArray<EOCodeExer> listeFils = EOCodeExer.fetchFils(currentNiveau1, currentExercice);
				for (EOCodeExer ceFils : listeFils) {
					ceFils.setCeActif("N");
				}
			} else {
				codeExerCurrentNiveau1.setCeActif("O");
			}
			ec.saveChanges();
			actualiser();
		} catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}
	}
	
	private void toggleVisibiliteNiveau2() {
		try {
			boolean isActif = "O".equals(codeExerCurrentNiveau2.ceActif());
			if (isActif) {
				codeExerCurrentNiveau2.setCeActif("N");
			} else {
				codeExerCurrentNiveau2.setCeActif("O");
			}
			ec.saveChanges();
			actualiser();
		} catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}
	}
}
