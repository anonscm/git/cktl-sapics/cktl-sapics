/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client.admin;
//import papayeFwkEof.client.*;

import javax.swing.JFrame;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOParametreMarche;
import org.cocktail.sapics.client.gui.ParametrageView;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;


public class ParametrageCtrl
{
	public static ParametrageCtrl sharedInstance;

    private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
    private EOEditingContext 	ec;
    
    private ParametrageView myView;
    
    private EOParametreMarche currentParametre;
        
    /**
     *
     */
    public ParametrageCtrl(EOEditingContext globalEc) {
        super();

        ec = globalEc;

        myView = new ParametrageView(new JFrame(), true);
        
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		
		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});
				
		myView.getCheckCtrlDatesFalse().setSelected(true);
		
		if ( !NSApp.hasFonction(ApplicationClient.ID_FONC_ADMIN) ) {
			myView.getButtonValider().setVisible(false);
			myView.getButtonAnnuler().setVisible(false);
		}
    }
    
    /**
     * 
     * @param editingContext
     * @return
     */    
    public static ParametrageCtrl sharedInstance(EOEditingContext editingContext)	{
     	if (sharedInstance == null)
     		sharedInstance = new ParametrageCtrl(editingContext);
     	return sharedInstance;
     }
    
        
    
    /**
     * 
     *
     */
    public void open() {
    	
		NSApp.setGlassPane(true);

		actualiser();
    	
    	myView.setVisible(true);
		NSApp.setGlassPane(false);
    	
    }
    
    private void clearTextFields() {
    	
    }
    
    private void actualiser() {
    	
    	clearTextFields();
    	
        String paramCtrlDatesMarche = EOParametreMarche.getValue(ec, EOParametreMarche.ID_CTRL_DATES);
        if (paramCtrlDatesMarche == null)
        	paramCtrlDatesMarche = EOParametreMarche.DEFAULT_VALUE_CTRL_DATES;

        myView.getCheckCtrlDatesTrue().setSelected(paramCtrlDatesMarche != null && "O".equals(paramCtrlDatesMarche));        

        
    }
    
    private void valider() {
    
    	try {
    		
        	currentParametre = EOParametreMarche.find(ec, EOParametreMarche.ID_CTRL_DATES);
        	if (currentParametre == null) {
        		currentParametre = EOParametreMarche.creer(ec, EOParametreMarche.ID_CTRL_DATES, "Affichage des marchés en fonction des dates de début et fin et non de l'exercice (O / N)");
        		ec.insertObject(currentParametre);
        	}
        	currentParametre.setParamValue((myView.getCheckCtrlDatesTrue().isSelected())?"O":"N");

        	
        	ec.saveChanges();
        	
        	EODialogs.runInformationDialog("OK", "Les paramètres ont bien été sauvegardés.");
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	
    	myView.dispose();
    	
    }

    private void annuler() {
    	
    	myView.dispose();
    	
    }
    
}
