package org.cocktail.sapics.client.admin;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.swing.JFormattedTextField.AbstractFormatterFactory;
import javax.swing.JFrame;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOMapaTrancheExer;
import org.cocktail.sapics.client.gui.SaisieTrancheMapaView;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieTrancheMapaCtrl {
	private static SaisieTrancheMapaCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient) ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private EOMapaTrancheExer currentTranche;
	private SaisieTrancheMapaView myView;

	private boolean modeModification;

	/** 
	 *
	 */
	public SaisieTrancheMapaCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new SaisieTrancheMapaView(new JFrame(), true);

		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getTfMontantMax().setFormatterFactory(getMontantFormatterFactory());
		myView.getTfMontantMin().setFormatterFactory(getMontantFormatterFactory());

	}

	private static AbstractFormatterFactory getMontantFormatterFactory() {
		// TODO à factoriser...
		NumberFormatter defaulFmt = new NumberFormatter(new DecimalFormat("###0.00"));
		NumberFormatter displayFmt = new NumberFormatter(new DecimalFormat("#,##0.##"));

		defaulFmt.setValueClass(BigDecimal.class);
		DefaultFormatterFactory fmtf = new DefaultFormatterFactory(defaulFmt, // defaut
				displayFmt // display
		);

		return fmtf;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieTrancheMapaCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new SaisieTrancheMapaCtrl(editingContext);
		}
		return sharedInstance;
	}

	/**
	 * 
	 *
	 */
	private void clearTextFields() {

		myView.getTfLibelle().setText("");

	}

	/**
	 * Selection d'un statut parmi une liste de valeurs
	 * 
	 */
	public boolean modifier(EOMapaTrancheExer tranche) {

		clearTextFields();

		currentTranche = tranche;

		modeModification = true;

		myView.getTfLibelle().setText(currentTranche.mteLibelle());
		myView.getTfType().setText(currentTranche.typeCN().tcnLibelle());
		myView.getTfMontantMin().setValue(currentTranche.mteMin().setScale(2));
		myView.getTfMontantMax().setValue(currentTranche.mteMax().setScale(2));

		myView.setVisible(true);

		return (currentTranche != null);
	}

	/**
	 *
	 */
	private void valider() {

		try {

			if (myView.getTfMontantMin().getText().length() == 0) {
				throw new ValidationException("Vous devez entrer un montant minimum pour cette tranche.");
			}

			if (myView.getTfMontantMax().getText().length() == 0) {
				throw new ValidationException("Vous devez entrer un montant maximum pour cette tranche.");
			}

			BigDecimal montantMin = (BigDecimal) myView.getTfMontantMin().getValue();
			currentTranche.setMteMin(montantMin.setScale(2));

			BigDecimal montantMax = (BigDecimal) myView.getTfMontantMax().getValue();
			currentTranche.setMteMax(montantMax.setScale(2));

		} catch (ValidationException ex) {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		myView.dispose();
	}

	/**
	 *
	 */
	private void annuler() {

		if (!modeModification) {
			ec.deleteObject(currentTranche);
		}

		currentTranche = null;

		myView.dispose();

	}

}