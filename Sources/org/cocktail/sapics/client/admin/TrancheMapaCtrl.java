package org.cocktail.sapics.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.eof.model.EOMapaTrancheExer;
import org.cocktail.sapics.client.eof.model.EOTypeCn;
import org.cocktail.sapics.client.gui.TrancheMapaView;
import org.cocktail.sapics.client.swing.ZEOTable.ZEOTableListener;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSMutableArray;

public class TrancheMapaCtrl {

	private static TrancheMapaCtrl sharedInstance;
	
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private EODisplayGroup eod;
	private TrancheMapaView myView;

	private EOExercice currentExercice;
	private EOMapaTrancheExer currentTranche;
	
	public TrancheMapaCtrl(EOEditingContext editingContext) {
		
		super();

		ec = editingContext;
	
		eod = new EODisplayGroup();
		
		myView = new TrancheMapaView(eod);
		
		myView.getButtonModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});
				
		
		myView.setListeExercices(EOExercice.find(ec));
		myView.getExercices().setSelectedItem(EOExercice.exerciceCourant(ec));
		currentExercice = (EOExercice)myView.getExercices().getSelectedItem();
		myView.getExercices().addActionListener(new PopupExerciceListener());

		myView.getButtonModifier().setVisible(NSApp.hasFonction(ApplicationClient.ID_FONC_NOMENCLATURES));

		NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EOMapaTrancheExer.MTE_LIBELLE_KEY, EOSortOrdering.CompareAscending));
	    mySort.addObject(new EOSortOrdering(EOMapaTrancheExer.TYPE_CN_KEY+"."+EOTypeCn.TCN_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		eod.setSortOrderings(mySort);
		
		myView.getMyEOTable().addListener(new ListenerTranche());

	}

	
	public static TrancheMapaCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new TrancheMapaCtrl(editingContext);
		return sharedInstance;
	}
	
	
	
	/**
	 * 
	 */
	private void actualiser() {
		
		eod.setObjectArray(EOMapaTrancheExer.findForExercice(ec, currentExercice));
		myView.getMyEOTable().updateData();
				
	}
	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			currentExercice = (EOExercice)myView.getExercices().getSelectedItem();
			actualiser();

		}
	}
	
	/**
	 * 
	 */
	public void open() {
		
		actualiser();
		
		myView.setVisible(true);
		
	}
	
	/**
	 * 
	 */
	private void modifier() {
				
		try {
			if (SaisieTrancheMapaCtrl.sharedInstance(ec).modifier( currentTranche)) {
				ec.saveChanges();
				myView.getMyEOTable().updateData();
			}
			else
				ec.revert();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runInformationDialog("ERREUR","Erreur d'enregistrement !");
			ec.revert();
		}
	}	
	
	private class ListenerTranche implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (NSApp.hasFonction(ApplicationClient.ID_FONC_NOMENCLATURES))
				modifier();
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			currentTranche = (EOMapaTrancheExer)eod.selectedObject();
			updateUI();
			
		}
	}
	
	
	private void updateUI() {		
		myView.getButtonModifier().setEnabled(currentTranche != null);
	}
	
}
