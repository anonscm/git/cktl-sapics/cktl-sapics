package org.cocktail.sapics.client.attributions;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.sapics.client.admin.CatalogueSaisieCtrl;
import org.cocktail.sapics.client.admin.CatalogueSelectCtrl;
import org.cocktail.sapics.client.eof.model.EOArticle;
import org.cocktail.sapics.client.eof.model.EOArticleMarche;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOAttributionCatalogue;
import org.cocktail.sapics.client.eof.model.EOCatalogue;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticle;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticleOpt;
import org.cocktail.sapics.client.eof.model.EODevise;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOTypeEtat;
import org.cocktail.sapics.client.gui.AttributionCatalogueView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.swing.ZEOTableCellRenderer;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.CocktailConstantes;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class AttributionCatalogueCtrl {

	public static AttributionCatalogueCtrl sharedInstance;

	private EOEditingContext 	ec;

	private AttributionCatalogueView myView;

	private EODisplayGroup eodCatalogues = new EODisplayGroup(), eodArticles = new EODisplayGroup(), eodOptions = new EODisplayGroup();

	private ListenerCatalogues listenerCatalogue = new ListenerCatalogues();
	private ListenerArticles listenerArticle = new ListenerArticles();

	private CatalogueRenderer	monRendererColor = new CatalogueRenderer();

	private EOAttribution 	currentAttribution;
	private EOAttributionCatalogue currentAttributionCatalogue;
	private EOCatalogue currentCatalogue;
	private EOCatalogueArticle currentCatalogueArticle;
	private EOCatalogueArticleOpt currentOption;

	public AttributionCatalogueCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new AttributionCatalogueView(eodCatalogues, eodArticles, eodOptions, monRendererColor);

		myView.getBtnAddCatalogue().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterCatalogue();
			}
		});

		myView.getBtnUpdateCatalogue1().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierCatalogue();
			}
		});

		myView.getBtnDelCatalogue().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerCatalogue();
			}
		});

		myView.getBtnAddArticle().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterArticle();
			}
		});

		myView.getBtnUpdateArticle().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierArticle();
			}
		});

		myView.getBtnDelArticle().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerArticle();
			}
		});


		myView.getBtnAddOption().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterOption();
			}
		});


		myView.getBtnUpdateOption().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierOption();
			}
		});

		myView.getBtnDelOption().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerOption();
			}
		});

		myView.getBtnSelectCatalogue().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getCatalgogue();
			}
		});

		myView.getBtnImport().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				importer();
			}
		});

		myView.getTfLibelleArticle().getDocument().addDocumentListener(new DocumentListenerArticle());

		myView.getMyEOTableCatalogues().addListener(listenerCatalogue);
		myView.getMyEOTableArticles().addListener(listenerArticle);
		myView.getMyEOTableOptions().addListener(new ListenerOptions());

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOAttributionCatalogue.CATALOGUE_KEY+"."+EOCatalogue.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY, EOSortOrdering.CompareDescending));
		eodCatalogues.setSortOrderings(mySort);
	}


	public static AttributionCatalogueCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new AttributionCatalogueCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}

	public void actualiser(EOAttribution attribution, EOLot lot) {

		CRICursor.setWaitCursor(myView);

		currentAttribution = attribution;

		eodCatalogues.setObjectArray(EOAttributionCatalogue.findForAttribution(ec, currentAttribution));

		myView.getMyEOTableCatalogues().updateData();

		updateUI();

		CRICursor.setDefaultCursor(myView);

	}


	private void ajouterCatalogue() {

		NSArray cataloguesValides = EOAttributionCatalogue.findCataloguesValidesForAttribution(ec, currentAttribution);
		if (cataloguesValides.count() > 0) {
			
			EODialogs.runErrorDialog("ERREUR","Il existe déjà un catalogue valide pour cette attribution !");
			return;
			
		}
		
		EOCatalogue newCatalogue = EOCatalogue.creer(ec);
		newCatalogue.setFournisRelationship(currentAttribution.fournis());

		EOAttributionCatalogue.creer(ec, currentAttribution, newCatalogue);

		newCatalogue.setCatDateDebut(currentAttribution.attDebut());
		newCatalogue.setCatDateFin(currentAttribution.attFin());
		
		CatalogueSaisieCtrl.sharedInstance(ec).modifier(newCatalogue);

		actualiser(currentAttribution, currentAttribution.lot());

	}


	private void modifierCatalogue() {

		CatalogueSaisieCtrl.sharedInstance(ec).modifier(currentAttributionCatalogue.catalogue());

		listenerCatalogue.onSelectionChanged();

	}

	private void supprimerCatalogue() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Désirez vous réellement invalider ce catalogue ?",
				"OUI", "NON"))
			return;

		try {	

			currentCatalogue.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));
			
			//ec.deleteObject(currentAttributionCatalogue);

			ec.saveChanges();

			actualiser (currentAttribution, currentAttribution.lot());
		}
		catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}

	}

	private void getCatalgogue() {

		try {

			EOCatalogue catalogue = CatalogueSelectCtrl.sharedInstance(ec).getCatalogue(currentAttribution.fournis());

			if (catalogue != null) {

				EOAttributionCatalogue.creer(ec, currentAttribution, catalogue);

				ec.saveChanges();

				actualiser(currentAttribution, currentAttribution.lot());

			}
		}
		catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}
	}

	private void ajouterArticle() {


		EOArticle newArticle = EOArticle.creer(ec);
		EOArticleMarche newArticleMarche = EOArticleMarche.creer(ec, newArticle);
		newArticleMarche.setDeviseRelationship(EODevise.findDeviseCourante(ec));

		EOCatalogueArticle newCatalogueArticle = EOCatalogueArticle.creer(ec, currentCatalogue);
		newCatalogueArticle.setArticleRelationship(newArticle);

		ArticleSaisieCtrl.sharedInstance(ec).modifier(currentAttribution.lot(), newCatalogueArticle, newArticleMarche);

		listenerCatalogue.onSelectionChanged();

	}



	private void modifierArticle() {

		EOArticleMarche articleMarche = EOArticleMarche.findForArticle(ec, currentCatalogueArticle.article());

		if (articleMarche == null)
			articleMarche = EOArticleMarche.creer(ec, currentCatalogueArticle.article());


		ArticleSaisieCtrl.sharedInstance(ec).modifier(currentAttribution.lot(), currentCatalogueArticle, articleMarche);

		myView.getMyEOTableArticles().updateUI();

	}





	private void supprimerArticle() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Désirez vous réellement supprimer les articles sélectionnés (et leur options) ?",
				"OUI", "NON"))
			return;

		CRICursor.setWaitCursor(myView);
		
		try {

			for (Enumeration<EOCatalogueArticle> e = eodArticles.selectedObjects().objectEnumerator();e.hasMoreElements();) {

				EOCatalogueArticle catalogueArticle = e.nextElement();
				NSArray options = EOCatalogueArticleOpt.findForArticle(ec, catalogueArticle.article());
				
				for (Enumeration<EOCatalogueArticleOpt> e1 = options.objectEnumerator();e1.hasMoreElements();) {
					EOCatalogueArticleOpt option = e1.nextElement();
					option.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));										
				}
				
				catalogueArticle.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));			
			}

			ec.saveChanges();

			listenerCatalogue.onSelectionChanged();

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();
		}
		
		CRICursor.setDefaultCursor(myView);
		
	}


	private void importer() {

		ImportArticleCtrl.sharedInstance(ec, AttributionCtrl.sharedInstance(ec).getView()).open(currentAttributionCatalogue);

	//	actualiser(currentAttribution, currentAttribution.lot());

		listenerCatalogue.onSelectionChanged();
		myView.getMyEOTableArticles().updateData();
		
	}


	public BigDecimal calculerMontantCatalogue() {

		if (currentCatalogue == null)
			return new BigDecimal(0);

		BigDecimal montantCatalogue = new BigDecimal(0);

		NSArray articles = EOCatalogueArticle.findForCatalogue(ec, currentCatalogue);

		for (int i=0;i<articles.count();i++) {

			EOCatalogueArticle article = (EOCatalogueArticle)articles.objectAtIndex(i);
			EOArticleMarche articleMarche = (EOArticleMarche)article.article().articlesMarche().objectAtIndex(0);

			if ( article != null && articleMarche != null) {

				BigDecimal montantArticle = article.caarPrixTTC().multiply(new BigDecimal(articleMarche.artmQteControle()));

				montantCatalogue = montantCatalogue.add(montantArticle);

				// Ajout des options
				NSArray options = EOCatalogueArticleOpt.findForArticle(ec, article.article());			
				for (int j=0;j<options.count();j++) {

					EOCatalogueArticleOpt option = (EOCatalogueArticleOpt)options.objectAtIndex(j);
					EOArticleMarche articleMarcheOption = (EOArticleMarche)option.article().articlesMarche().objectAtIndex(0);

					BigDecimal montantOption = option.caarPrixTTC().multiply(new BigDecimal(articleMarcheOption.artmQteControle()));

					montantCatalogue = montantCatalogue.add(montantOption);
				}

			}
		}

		return montantCatalogue;

	}


	private void ajouterOption() {


		EOArticle newArticle = EOArticle.creer(ec);
		EOArticleMarche newArticleMarche = EOArticleMarche.creer(ec, newArticle);
		newArticleMarche.setDeviseRelationship(EODevise.findDeviseCourante(ec));

		EOCatalogueArticleOpt newCatalogueArticleOpt = EOCatalogueArticleOpt.creer(ec, currentCatalogue);
		newCatalogueArticleOpt.setArticleRelationship(newArticle);
		newArticle.setPereRelationship(currentCatalogueArticle.article());

		OptionSaisieCtrl.sharedInstance(ec).modifier(currentAttribution.lot(), newCatalogueArticleOpt, newArticleMarche);

		listenerArticle.onSelectionChanged();

	}

	private void modifierOption() {

		EOArticleMarche articleMarche = EOArticleMarche.findForArticle(ec, currentOption.article());

		if (articleMarche == null)
			articleMarche = EOArticleMarche.creer(ec, currentOption.article());

		OptionSaisieCtrl.sharedInstance(ec).modifier(currentAttribution.lot(),currentOption, articleMarche);

		listenerArticle.onSelectionChanged();

	}

	private void supprimerOption() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Désirez vous réellement supprimer cet option ?",
				"OUI", "NON"))
			return;

		try {

			currentOption.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_ANNULE));			

			ec.saveChanges();

			listenerArticle.onSelectionChanged();

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}


	private class ListenerCatalogues implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			modifierCatalogue();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentAttributionCatalogue = (EOAttributionCatalogue)eodCatalogues.selectedObject();

			eodArticles.setObjectArray(new NSArray());
			eodOptions.setObjectArray(new NSArray());

			if (currentAttributionCatalogue != null) {
				currentCatalogue = currentAttributionCatalogue.catalogue();
				eodArticles.setObjectArray(EOCatalogueArticle.findForCatalogue(ec, currentCatalogue));
				filter();
			}

			myView.getMyEOTableArticles().updateData();
			myView.getMyEOTableOptions().updateData();
			updateUI();
		}
	}

	private class ListenerArticles implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			modifierArticle();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentCatalogueArticle = (EOCatalogueArticle)eodArticles.selectedObject();

			eodOptions.setObjectArray(new NSArray());

			if (currentCatalogueArticle != null)
				eodOptions.setObjectArray(EOCatalogueArticleOpt.findForArticle(ec, currentCatalogueArticle.article()));

			myView.getMyEOTableOptions().updateData();
			updateUI();
		}
	}



	private class ListenerOptions implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			modifierOption();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentOption = (EOCatalogueArticleOpt)eodOptions.selectedObject();
			updateUI();
		}

	}


	public EOQualifier getFilterQualifier()	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfLibelleArticle().getText())) {

			NSMutableArray mesOrQualifiers = new NSMutableArray();

			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.ARTICLE_KEY+"."+EOArticle.ART_LIBELLE_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfLibelleArticle().getText()+"*")));
			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.CAAR_REFERENCE_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfLibelleArticle().getText()+"*")));

			mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));
		}

		return new EOAndQualifier(mesQualifiers);        

	}


	public void filter()	{

		CRICursor.setWaitCursor(myView);

		eodArticles.setQualifier(getFilterQualifier());
		eodArticles.updateDisplayedObjects();              

//		BigDecimal montantArticles = new BigDecimal(0);
//		for (int i=0;i<eodArticles.displayedObjects().count();i++) {
//
//			EOCatalogueArticle article = (EOCatalogueArticle)eodArticles.displayedObjects().objectAtIndex(i);
//			EOArticleMarche articleMarche = (EOArticleMarche)article.article().articlesMarche().objectAtIndex(0);
//
//			if (articleMarche != null && articleMarche.artmQteControle() != null) {
//				BigDecimal montantArticle = article.caarPrixTTC().multiply(new BigDecimal(articleMarche.artmQteControle()));
//
//				montantArticles = montantArticles.add(montantArticle);
//			}
//		}

		myView.getLblArticles().setText(eodArticles.displayedObjects().count() + " Articles" );//+ montantArticles.toString() + CocktailConstantes.STRING_EURO);

		myView.getMyEOTableArticles().updateData();

		CRICursor.setDefaultCursor(myView);

	}


	private class DocumentListenerArticle implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


	private void updateUI() {

		myView.getBtnUpdateCatalogue1().setEnabled(currentCatalogue != null && currentCatalogue.isValide());
		myView.getBtnDelCatalogue().setEnabled(currentCatalogue != null && currentCatalogue.isValide());
		myView.getBtnSelectCatalogue().setEnabled(eodCatalogues.displayedObjects().count() == 0);

		myView.getBtnAddArticle().setEnabled(currentAttributionCatalogue != null && currentCatalogue != null && currentCatalogue.isValide());
		myView.getBtnUpdateArticle().setEnabled(currentCatalogueArticle != null && currentCatalogue.isValide());
		myView.getBtnDelArticle().setEnabled(currentCatalogueArticle != null && currentCatalogue.isValide());

		myView.getBtnAddOption().setEnabled(currentCatalogue != null && currentCatalogueArticle != null && currentCatalogue.isValide());
		myView.getBtnUpdateOption().setEnabled(currentOption != null && currentCatalogue.isValide());
		myView.getBtnDelOption().setEnabled(currentOption != null && currentCatalogue.isValide());

		myView.getBtnImport().setEnabled(currentAttributionCatalogue != null && currentCatalogue != null && currentCatalogue.isValide());

	}

	private class CatalogueRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2907930349355563787L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOAttributionCatalogue obj = (EOAttributionCatalogue) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if ( obj.catalogue().typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_VALIDE) )
				leComposant.setForeground(CocktailConstantes.BG_COLOR_BLACK);
			else
				leComposant.setForeground(CocktailConstantes.BG_COLOR_GREY);

			return leComposant;
		}
	}

}
