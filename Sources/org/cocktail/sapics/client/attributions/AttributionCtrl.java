package org.cocktail.sapics.client.attributions;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EORevision;
import org.cocktail.sapics.client.gui.AttributionView;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AttributionCtrl 
{
	private static AttributionCtrl sharedInstance;

	private org.cocktail.sapics.client.ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext	ec;

	private AttributionView myView;

	private OngletChangeListener listenerOnglets = new OngletChangeListener();

	private	EOAttribution currentAttribution;



	/** 
	 *
	 */
	public AttributionCtrl (EOEditingContext globalEc) {

		super();

		myView = new AttributionView(new JFrame(), true);

		ec = globalEc;

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnReviser().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reviser();
			}
		});

		myView.getBtnGetDebut().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDebut();
			}
		});

		myView.getBtnGetFin().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFin();
			}
		});

		myView.getBtnGetMontantCatalogue().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				calculerMontantCatalogue();
			}
		});

		myView.getCheckReponseAcceptee().setSelected(true);
		myView.getCheckValidite().setSelected(true);

		myView.getOnglets().addTab ("Catalogue ",null , AttributionCatalogueCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Execution ",null , AttributionExecutionCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Sous Traitance ", AttributionSousTraitantsCtrl.sharedInstance(ec).getView());

		myView.getOnglets().addChangeListener(listenerOnglets);

		myView.getTfDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDebut()));
		myView.getTfDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDebut()));

		myView.getTfFin().addFocusListener(new FocusListenerDateTextField(myView.getTfFin()));
		myView.getTfFin().addActionListener(new ActionListenerDateTextField(myView.getTfFin()));

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AttributionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AttributionCtrl(editingContext);
		return sharedInstance;
	}



	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

			CRICursor.setWaitCursor(myView);

			switch (myView.getOnglets().getSelectedIndex()) {

			case 0 : AttributionCatalogueCtrl.sharedInstance(ec).actualiser(currentAttribution, currentAttribution.lot());break;
			case 1 : AttributionExecutionCtrl.sharedInstance(ec).actualiser(currentAttribution.lot(), currentAttribution);break;
			case 2 : AttributionSousTraitantsCtrl.sharedInstance(ec).actualiser(currentAttribution);break;

			}

			CRICursor.setDefaultCursor(myView);

		}
	}


	private void getDebut() {

		CocktailUtilities.setMyTextField(myView.getTfDebut());
		CocktailUtilities.showDatePickerPanel(new Dialog(myView));

	}


	public JDialog getView() {
		return myView;
	}


	private void getFin() {

		CocktailUtilities.setMyTextField(myView.getTfFin());
		CocktailUtilities.showDatePickerPanel(new Dialog(myView));

	}


	/**
	 * 
	 *
	 */
	private void updateUI()	{		

	}


	/**
	 * 
	 */
	private void calculerMontantCatalogue() {

		CRICursor.setWaitCursor(myView);

		myView.getTfMontantCatalogue().setText(AttributionCatalogueCtrl.sharedInstance(ec).calculerMontantCatalogue().toString());

		CRICursor.setDefaultCursor(myView);

	}


	public void updateAttribution(EOAttribution attribution)	{

		NSApp.setGlassPane(true);

		if (attribution != null) {

			currentAttribution = attribution;

			myView.getBtnReviser().setEnabled(attribution.attHt() != null && attribution.attHt().floatValue() > 0 );

			NSArray revisions = EORevision.findForAttribution(ec, currentAttribution);
			myView.getBtnReviser().setText("Réviser ("+revisions.count()+")");

			CocktailUtilities.initTextField(myView.getTfMontant(), false, true);

			actualiser();

			myView.setVisible(true);

			NSApp.setGlassPane(false);

		}

	}


	private void clearTextField() {

		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getTfMontant().setText("");



	}

	private void actualiser() {

		clearTextField();

		String texteMarche = "MARCHE : " + currentAttribution.lot().marche().marLibelle();
		myView.getLblMarche().setText(texteMarche);
		myView.getLblMarche().setToolTipText(texteMarche);

		String texteLot = "LOT : " + currentAttribution.lot().lotLibelle();
		myView.getLblLot().setText(texteLot);
		myView.getLblLot().setToolTipText(texteLot);
		
		String texteFournisseur = "Fournisseur : " + currentAttribution.fournis().nom();
		if (currentAttribution.fournis().prenom() != null) {
			texteFournisseur += " "  + currentAttribution.fournis().prenom();
		}
		myView.getLblFournisseur().setText(texteFournisseur);
		myView.getLblFournisseur().setToolTipText(texteFournisseur);

		myView.getTfDebut().setText(DateCtrl.dateToString(currentAttribution.attDebut()));

		myView.getTfFin().setText(DateCtrl.dateToString(currentAttribution.attFin()));

		if (currentAttribution.attHt() != null)
			myView.getTfMontant().setText(currentAttribution.attHt().toString());
		else
			myView.getTfMontant().setText("0");

		myView.getCheckReponseAcceptee().setSelected(currentAttribution.attAcceptee().equals("O"));

		myView.getCheckValidite().setSelected(currentAttribution.attValide().equals("O"));

		listenerOnglets.stateChanged(null);

	}

	private void reviser() {

		if(myView.getTfMontant().getText().equals(""))
			EODialogs.runErrorDialog("ERREUR", "On ne peux pas réviser une attribution dont le montant n'est pas defini !");
		else	{
			AttributionRevisionCtrl.sharedInstance(ec).reviser(currentAttribution);	
			myView.getTfMontant().setText(currentAttribution.attHt().toString());
		}

	}


	/**
	 *
	 */
	private void valider()	{

		try {

			currentAttribution.setAttDebut(DateCtrl.stringToDate(myView.getTfDebut().getText()));
			currentAttribution.setAttFin(DateCtrl.stringToDate(myView.getTfFin().getText()));
			currentAttribution.setAttHt(new BigDecimal(myView.getTfMontant().getText()));
			currentAttribution.setAttAcceptee((myView.getCheckReponseAcceptee().isSelected())?"O":"N");
			currentAttribution.setAttValide((myView.getCheckValidite().isSelected())?"O":"N");

			// Verifier que le montant de toutes les attribution soit inferieur au montant du lot
			NSArray attributions = EOAttribution.findForLot(ec, currentAttribution.lot());
			BigDecimal montantAttributions = CocktailUtilities.computeSumForKey(attributions, EOAttribution.ATT_HT_KEY);
			if (ec.insertedObjects().count() > 0)
				montantAttributions = montantAttributions.add(currentAttribution.attHt());
			if (montantAttributions.compareTo(currentAttribution.lot().lotHT()) > 0)
				throw new ValidationException("Le montant des attributions (" + montantAttributions.toString() + ") ne peut dépasser le montant du lot ("+currentAttribution.attHt().toString()+") !");

			ec.saveChanges();

			myView.dispose();

		}
		catch (ValidationException ex){
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}		
		catch (Exception e){

			e.printStackTrace();
		}


	}



	/**
	 *
	 */
	public void annuler()	{
		ec.revert();
		myView.dispose();
	}


	private class ActionListenerDateTextField implements ActionListener	{
		
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
				updateUI();
			}
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
				updateUI();
			}
		}
	}	



}