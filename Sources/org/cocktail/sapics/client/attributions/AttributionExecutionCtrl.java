package org.cocktail.sapics.client.attributions;

import javax.swing.JPanel;

import org.cocktail.sapics.client.ServerProxy;
import org.cocktail.sapics.client.editions.ReportsJasperCtrl;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOVExecutionGlobale;
import org.cocktail.sapics.client.gui.AttributionExecutionView;
import org.cocktail.sapics.client.utilities.CRICursor;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class AttributionExecutionCtrl {

	public static AttributionExecutionCtrl sharedInstance;

	private EOEditingContext 	ec;

	private AttributionExecutionView myView;

	private EODisplayGroup eodExecution = new EODisplayGroup();

	private EOLot currentLot;
	private EOAttribution currentAttribution;

	public AttributionExecutionCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;
		
		myView = new AttributionExecutionView(eodExecution);

		myView.getBtnImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimer();	}});
	
	}


	public static AttributionExecutionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new AttributionExecutionCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}


	public void imprimer() {
		
		NSMutableDictionary parametres = new NSMutableDictionary();			
		
		Integer lotOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentLot)).objectForKey("lotOrdre");
		Integer marOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentLot.marche())).objectForKey("marOrdre");
		Integer exer = currentLot.marche().exercice().exeExercice();
		Integer fouOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentAttribution.fournis())).objectForKey("fouOrdre");
		
		parametres.setObjectForKey(lotOrdre, "LOTORDRE");
		parametres.setObjectForKey(marOrdre, "MARORDRE");
		parametres.setObjectForKey(exer, "EXEREXEC");
		parametres.setObjectForKey(fouOrdre, "FOUORDRE");

		ReportsJasperCtrl.sharedInstance(ec).printSapicsExecutionLotFouExer(parametres);

		
	}

	public void actualiser(EOLot lot, EOAttribution attribution) {

		CRICursor.setWaitCursor(myView);

		currentLot = lot;
		currentAttribution = attribution;
		
		eodExecution.setObjectArray(new NSArray());

		if (currentLot != null) {

			eodExecution.setObjectArray(EOVExecutionGlobale.findForLot(ec, currentLot));    	

		}

		myView.getMyEOTable().updateData();

		CRICursor.setDefaultCursor(myView);

	}


	
}
