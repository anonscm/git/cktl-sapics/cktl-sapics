
package org.cocktail.sapics.client.attributions;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOAttributionHist;
import org.cocktail.sapics.client.eof.model.EORevision;
import org.cocktail.sapics.client.gui.AttributionRevisionView;
import org.cocktail.sapics.client.marches.MarchesCtrl;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.AskForValeur;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AttributionRevisionCtrl {

	public static AttributionRevisionCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient )ApplicationClient.sharedApplication();
	private EOEditingContext 	ec;

	private AttributionRevisionView myView;
	private EODisplayGroup eod = new EODisplayGroup();

	private EOAttribution currentAttribution;
	private EORevision currentRevision;

	public AttributionRevisionCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new AttributionRevisionView(new JFrame(), true, eod);

		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getBtnUpdRevision().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});

		myView.getBtnDelRevision().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});


		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});


		myView.getMyEOTable().addListener(new ListenerRevision());

		myView.getCheckRevision().setSelected(true);

	}


	public static AttributionRevisionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new AttributionRevisionCtrl(editingContext);
		return sharedInstance;
	}

	private void annuler() {

		ec.revert();
		myView.setVisible(false);
	}


	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Désirez vous réellement supprimer cette révision ?",
				"OUI", "NON"))
			return;

		try {

			BigDecimal montantRevision = currentRevision.revDiff();

			BigDecimal montantAttribution = currentAttribution.attHt().subtract(montantRevision);

			currentAttribution.setAttHt(montantAttribution);

			//EODialogs.runInformationDialog("OK",(myView.getCheckAvenant().isSelected())?"L'avenant a bien été enregistré !":"La révision a bien été enregistrée !");

			myView.getTfMontantAttribution().setText(currentAttribution.attHt().toString());

			ec.deleteObject(currentRevision);

			ec.saveChanges();

			eod.setObjectArray(EORevision.findForAttribution(ec, currentAttribution));
			myView.getMyEOTable().updateData();

		}
		catch (Exception e) {

			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression de la révision.\n" + CocktailUtilities.getErrorDialog(e));

		}

	}


	private void modifier() {

		try {

			// Saisie du montant a sous traiter
			BigDecimal montantAttribution = AskForValeur.sharedInstance().getMontant("Montant Révision ", currentAttribution.attHt());

			if (montantAttribution != null) {

				// Annulation de la révision
				currentAttribution.setAttHt(currentAttribution.attHt().subtract(currentRevision.revDiff()));
				
				BigDecimal difference = montantAttribution.subtract(currentAttribution.attHt());

				currentRevision.setRevDate(new NSTimestamp());
				currentRevision.setRevDiff(difference);

				currentAttribution.setAttHt(montantAttribution);
				
				ec.saveChanges();

				reviser(currentAttribution);

			}

		}
		catch (ValidationException ve) {

			ec.revert();
			ve.printStackTrace();

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}
	}

	private void ajouter() {

		try {

			// TESTS VALIDITE
			if (myView.getTfMontant().getText().length() == 0 ) {

				EODialogs.runErrorDialog("ERREUR","Vous devez saisir un montant pour cette révision !");
				return;
			}

			if (myView.getTfLibelleRevision().getText().length() == 0 ) {

				EODialogs.runErrorDialog("ERREUR","Vous devez saisir un libellé pour cette révision !");
				return;
			}

			myView.getTfMontant().setText((NSArray.componentsSeparatedByString(myView.getTfMontant().getText(),",")).componentsJoinedByString("."));
			
			//BigDecimal montantRevision = new BigDecimal(myView.getTfMontant().getText());
			//			if (montantRevision.floatValue() <= currentAttribution.attHt().floatValue()) {
			//				
			//				EODialogs.runErrorDialog("ERREUR","Le montant de la révision doit être supérieur au montant de l'attribution !");
			//				return;
			//			}

			// Historisation du lot
			EOAttributionHist attHist = EOAttributionHist.creer(ec, currentAttribution);			

			// Ajout d'une revision lot
			EORevision newRevision = EORevision.creer(ec, currentAttribution, NSApp.getCurrentUtilisateur());

			newRevision.setRevLibelle(myView.getTfLibelleRevision().getText());

			BigDecimal difference = new BigDecimal(myView.getTfMontant().getText()).subtract(currentAttribution.attHt());

			newRevision.setRevDiff(difference);

			newRevision.setRevType((myView.getCheckAvenant().isSelected())?"A":"R");

			// Mise a jour du montant du lot

			currentAttribution.setAttHt(new BigDecimal(myView.getTfMontant().getText()));

			NSArray attributions = EOAttribution.findForLot(ec, currentAttribution.lot());
			BigDecimal montantAttributions = CocktailUtilities.computeSumForKey(attributions, EOAttribution.ATT_HT_KEY);

			if (montantAttributions.compareTo(currentAttribution.lot().lotHT()) > 0) {
				
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Le montant des attributions ne peut dépasser le montant du lot !\n "+
						" Désirez vous que le montant du lot soit modifié et passé à " + montantAttributions.toString() + " Euros ?",
						"OUI", "NON")) {
					
					currentAttribution.lot().setLotHT(montantAttributions);
					MarchesCtrl.sharedInstance(ec).rafraichirLot();
				}
			}

			ec.saveChanges();

			EODialogs.runInformationDialog("OK",(myView.getCheckAvenant().isSelected())?"L'avenant a bien été enregistré !":"La révision a bien été enregistrée !");

			myView.getTfMontantAttribution().setText(currentAttribution.attHt().toString());
			eod.setObjectArray(EORevision.findForAttribution(ec, currentAttribution));
			myView.getMyEOTable().updateData();
			
		}
		catch (ValidationException ex) {

			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();

		}
		catch (Exception ex) {

			ec.revert();
			ex.printStackTrace();

		}
		
		updateUI();

	}

	private void clearTextFields() {

		myView.getTfLibelleLot().setText("");
		myView.getTfMontant().setText("0");
		myView.getTfMontantAttribution().setText("");
		myView.getTfLibelleRevision().setText("");

	}

	public void reviser(EOAttribution attribution) {

		currentAttribution = attribution;

		eod.setObjectArray(new NSArray());
		clearTextFields();

		if (currentAttribution != null) {

			myView.getTfMontantAttribution().setText(currentAttribution.attHt().toString());

			eod.setObjectArray(EORevision.findForAttribution(ec, currentAttribution));
			myView.getMyEOTable().updateData();

			myView.getTfTitre().setText("REVISION ATTRIBUTION " + currentAttribution.lot().marche().exercice().exeExercice() + " / "  + currentAttribution.lot().marche().marIndex() + " / " + currentAttribution.lot().lotIndex());

			myView.getTfLibelleLot().setText(currentAttribution.lot().lotLibelle());

		}

		myView.getMyEOTable().updateData();

		updateUI();

		myView.setVisible(true);

	}


	private void updateUI() {

		myView.getBtnUpdRevision().setEnabled(currentRevision != null);
		myView.getBtnDelRevision().setEnabled(currentRevision != null);
		
	}

	private class ListenerRevision implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentRevision = (EORevision)eod.selectedObject();

			updateUI();
			
		}
	}
}
