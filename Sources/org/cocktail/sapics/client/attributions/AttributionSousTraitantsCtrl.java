package org.cocktail.sapics.client.attributions;

import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOFournis;
import org.cocktail.sapics.client.eof.model.EOSousTraitant;
import org.cocktail.sapics.client.eof.model.EOVRib;
import org.cocktail.sapics.client.gui.AttributionSousTraitantsView;
import org.cocktail.sapics.client.lots.FournisseurSelectCtrl;
import org.cocktail.sapics.client.lots.RibSelectCtrl;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.AskForValeur;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class AttributionSousTraitantsCtrl {

	public static AttributionSousTraitantsCtrl sharedInstance;

	private EOEditingContext 	ec;

	private AttributionSousTraitantsView myView;

	private EODisplayGroup eod = new EODisplayGroup();
	private ListenerSousTraitant listenerSousTraitant = new ListenerSousTraitant();

	private EOSousTraitant currentSousTraitant;
	private EOAttribution currentAttribution;


	public AttributionSousTraitantsCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new AttributionSousTraitantsView(eod);

		myView.getBtnAddSousTraitant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getBtnUpdSousTraitant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});

		myView.getBtnDelSousTraitant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});
		myView.getBtnGetRib().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				associerRIB();
			}
		});

		myView.getBtnDelRib().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerRib();
			}
		});

		myView.getMyEOTable().addListener(listenerSousTraitant);


	}


	public static AttributionSousTraitantsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new AttributionSousTraitantsCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}

	public void actualiser(EOAttribution attribution) {

		CRICursor.setWaitCursor(myView);

		currentAttribution = attribution;
		eod.setObjectArray(new NSArray());

		if (currentAttribution != null && currentAttribution.attHt() != null) {

			eod.setObjectArray(EOSousTraitant.findForAttribution(ec, attribution));    	

			try {
				myView.getTfMontantControle().setText(
						(currentAttribution.attHt().subtract(CocktailUtilities.computeSumForKey(eod, EOSousTraitant.ST_MONTANT_KEY))).toString());
			}
			catch (Exception e){
				e.printStackTrace();
			}

		}

		myView.getMyEOTable().updateData();


		updateUI();

		CRICursor.setDefaultCursor(myView);

	}

	private void ajouter() {

		try {

			EOFournis fournisseur = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();

			if (fournisseur != null) {

				// Saisie du montant a sous traiter
				BigDecimal montantSousTraitance = AskForValeur.sharedInstance().getMontant("Montant Sous Traitance", new BigDecimal(0));

				if (montantSousTraitance != null) {

					EOSousTraitant sousTraitant = EOSousTraitant.creer(ec, currentAttribution, fournisseur);
					sousTraitant.setStMontant(montantSousTraitance);

					ec.saveChanges();

					actualiser(currentAttribution);

				}
			}
		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}

	private void modifier() {

		try {
				
				// Saisie du montant a sous traiter
				BigDecimal montantSousTraitance = AskForValeur.sharedInstance().getMontant("Montant Sous Traitance", currentSousTraitant.stMontant());

				if (montantSousTraitance != null) {

					currentSousTraitant.setStMontant(montantSousTraitance);

					ec.saveChanges();

					actualiser(currentAttribution);
					myView.getMyEOTable().updateUI();

			}

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}

	
	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de ce sous traitant ?",
				"OUI", "NON"))
			return;

		try {

			ec.deleteObject(currentSousTraitant);

			ec.saveChanges();

			actualiser(currentAttribution);

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}

	
	private void associerRIB() {
		
		EOVRib rib = RibSelectCtrl.sharedInstance(ec).getRib(currentSousTraitant.fournisseur());
		
		if (rib != null) {
			
			try {
				
				currentSousTraitant.setRibRelationship(rib);
				ec.saveChanges();
				
				listenerSousTraitant.onSelectionChanged();
				
			}
			catch (Exception e) {
				ec.revert();
				
			}			
		}
		
	}
	
	private void supprimerRib() {
		
		try {
			
			currentSousTraitant.setRibRelationship(null);
			ec.saveChanges();
			
			listenerSousTraitant.onSelectionChanged();
			
		}
		catch (Exception e) {
			ec.revert();
			
		}			
		
	}

	

	private class ListenerSousTraitant implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentSousTraitant = (EOSousTraitant)eod.selectedObject();
			myView.getTfRib().setText("");
			
			if (currentSousTraitant!=  null && currentSousTraitant.rib() != null) {

				EOVRib localRib = currentSousTraitant.rib();
				String chaineRib = "";
				
				if (localRib.iban() != null) 
					chaineRib = localRib.iban();
				else
					chaineRib = localRib.ribNum();

				if (localRib.ribTitco() != null)
					chaineRib += " - " + localRib.ribTitco();

				CocktailUtilities.setTextTextField(myView.getTfRib(), chaineRib );

			}
			updateUI();

		}
	}


	private void updateUI()	{		

		myView.getBtnAddSousTraitant().setEnabled(currentAttribution != null);
		myView.getBtnUpdSousTraitant().setEnabled(currentSousTraitant != null);
		myView.getBtnDelSousTraitant().setEnabled(currentSousTraitant != null);

		myView.getBtnGetRib().setEnabled(currentSousTraitant != null);
		myView.getBtnDelRib().setEnabled(currentSousTraitant != null && currentSousTraitant.rib() != null);

	}



}
