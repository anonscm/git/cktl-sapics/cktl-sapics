/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client.attributions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;

import javax.swing.JDialog;
import javax.swing.JFileChooser;

import org.cocktail.sapics.client.eof.model.EOArticle;
import org.cocktail.sapics.client.eof.model.EOArticleMarche;
import org.cocktail.sapics.client.eof.model.EOAttributionCatalogue;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticle;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticleOpt;
import org.cocktail.sapics.client.eof.model.EOCodeMarche;
import org.cocktail.sapics.client.eof.model.EODevise;
import org.cocktail.sapics.client.eof.model.EOLotNomenclature;
import org.cocktail.sapics.client.eof.model.EOTaux;
import org.cocktail.sapics.client.eof.model.EOTypeArticle;
import org.cocktail.sapics.client.eof.model.EOTypeEtat;
import org.cocktail.sapics.client.gui.ImportArticleView;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.CocktailUtilities;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class ImportArticleCtrl {

	private static ImportArticleCtrl sharedInstance;

	private EOEditingContext ec;

	private  	JFileChooser 	fileChooser;
	private	File				fichier;
	private	FileReader			fileReader;
	public	BufferedReader		reader;		
	public	int					indexLigne;
	public	String				rapportErreurs;

	private EOAttributionCatalogue currentAttributionCatalogue;
	private EOArticle articlePere = null;

	private ImportArticleView myView;

	public ImportArticleCtrl(EOEditingContext editingContext, JDialog frameParent) {

		super();

		myView = new ImportArticleView(frameParent, true);

		ec = editingContext;

		// Creation d'un fileChooser pour choisir les fichiers et recuperer les icones
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);

		myView.getBtnGetFile().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFile();
			}
		});

		myView.getBtnImport().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				importer();
			}
		});

		myView.getBtnCancel().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnImport().setEnabled(false);

		myView.setDevises(EODevise.fetchAll(ec));

		myView.setTvas(EOTaux.find(ec));
		myView.getPopupTva().setSelectedIndex(myView.getPopupTva().getItemCount() - 1);

		myView.getTfGarantie().setText("12");
		myView.getTfLivraison().setText("1");

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ImportArticleCtrl sharedInstance(EOEditingContext editingContext, JDialog frameParent)	{
		if (sharedInstance == null)	{
			sharedInstance = new ImportArticleCtrl(editingContext, frameParent);
		}
		return sharedInstance;
	}


	/**
	 * 
	 */
	public void open(EOAttributionCatalogue att) {

		currentAttributionCatalogue = att;
		CocktailUtilities.setTextTextField(myView.getTfCatalogue(), att.catalogue().catLibelle());

		myView.setNomenclatures((NSArray) ((att.attribution().lot().lotNomenclatures()).valueForKey(EOLotNomenclature.CODEMARCHE_KEY)));

		myView.setVisible(true);

	}


	/**
	 * 
	 */
	private void getFile() {

		if (fileChooser.showOpenDialog(myView) == JFileChooser.APPROVE_OPTION) {
			myView.getTfFileName().setText(fileChooser.getSelectedFile().getPath());
			myView.getBtnImport().setEnabled(true);
		}

	}

	private void annuler() {

		myView.setVisible(false);

	}


	private void importer() {

		CRICursor.setWaitCursor(myView);

		try  {
			fichier		= new File(myView.getTfFileName().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR 01", "Impossible de lire le fichier sélectionné !");
			return;
		}

		indexLigne = 1;
		String ligne = "";
		int nbLignes = 0;

		// On compte le nombre de lignes du fichier
		try  {
			while (ligne != null) {
				ligne = reader.readLine();
				nbLignes++;
			}
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR 03", "Impossible de  calculer le nombre de lignes du fichier !");
			e.printStackTrace();

			return;
		}

		System.out.println("ImportArticleCtrl.importer() " + nbLignes);

		// Lecture du fichier termine. On close le reader.
		try {
			reader.close();
			fileReader.close();
		} catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR 04", "Impossible de refermer le fichier ouvert !");
			return;
		}

		try {
			fichier = new File(myView.getTfFileName().getText());
			fileReader = new FileReader(fichier);
			reader = new BufferedReader(fileReader);
		} catch (Exception e) {

			EODialogs.runErrorDialog("ERREUR 05", "Impossible de lire le fichier sélectionné !");

			return;

		}

		try {

			enregistrerLigne();

			EODialogs.runInformationDialog("Import OK", "Import du fichier d'articles terminé !");

		} catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR 10", ex.getMessage());
		}

		try {

			reader.close();
			fileReader.close();

		} catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR 10", "Impossible de fermer le fichier d'articles");
		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * @param nbLignes
	 */
	private void enregistrerLigne() throws Exception {
		String ligne = "";
		indexLigne = 0;
		try {
			while (ligne != null) {
				ligne = reader.readLine();
				indexLigne++;
				traiterLigne(ligne);
				ec.saveChanges();
			}
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
			throw new Exception("Erreur Import - LIGNE " + indexLigne + " !\n\n " + CocktailUtilities.getErrorDialog(e));
		}
	}

	/**
	 * @param ligne
	 * @return
	 */
	private void traiterLigne(String ligne) {
		NSArray champs;
		if (ligne != null && ligne.length() > 0) {
			champs = StringCtrl.componentsSeparatedByString(ligne, ";");
			// Traitement de la ligne
			String champ0 = (String) champs.objectAtIndex(0);
			if (champ0.length() == 1) {
				if (isArticle(champs)) {
					articlePere = creerArticle(champs);
				} else if (isOption(champs)) {
					creerOption(champs, articlePere);
				} else {
					throw new NSValidation.ValidationException("Format inconnu : " + ligne);
				}
			} else {
				articlePere = creerArticle(champs);
			}
		}
	}

	/**
	 * @param champs
	 * @return
	 */
	protected boolean isOption(NSArray champs) {
		return "O".equals(champs.objectAtIndex(0)) && (champs.count() == 6 || champs.count() == 7);
	}

	/**
	 * @param champs
	 * @return
	 */
	protected boolean isArticle(NSArray champs) {
		return "A".equals(champs.objectAtIndex(0)) && champs.count() == 9;
	}

	public EOArticle creerArticle(NSArray lesChamps) {

		// ARTICLE
		EOArticle newArticle = EOArticle.creer(ec);

		newArticle.setTypeArticleRelationship(EOTypeArticle.findForType(ec, "ARTICLE"));

		if (myView.getPopupCollCodeNomenclature().getSelectedIndex() == 0) {
			// Si emplacement du C.N. = * , alors on affecte à tous les articles le code sélectionné dans la fenêtre
			newArticle.setCodeMarcheRelationship((EOCodeMarche) myView.getCodesNomenclature().getSelectedItem());
		} else {
			// Sinon, on recherche le code de chaque article
			String stringCodeMarche = (String) lesChamps.objectAtIndex(myView.getPopupCollCodeNomenclature().getSelectedIndex() - 1);
			EOCodeMarche codeMarche = EOCodeMarche.findForCode(ec, stringCodeMarche);
			newArticle.setCodeMarcheRelationship(codeMarche);
		}

		newArticle.setArtLibelle((String) lesChamps.objectAtIndex(myView.getPopupColLibelle().getSelectedIndex()));

		// ARTICLE MARCHE
		EOArticleMarche newArticleMarche = EOArticleMarche.creer(ec, newArticle);
		newArticleMarche.setDeviseRelationship((EODevise) myView.getDevises().getSelectedItem());

		if (myView.getTfGarantie().getText().length() == 0) {
			newArticleMarche.setArtmGarantie(new Integer((String) lesChamps.objectAtIndex(6)));
		} else {
			newArticleMarche.setArtmGarantie(new Integer(myView.getTfGarantie().getText()));
		}

		if (myView.getTfLivraison().getText().length() == 0) {
			newArticleMarche.setArtmLivraison(new Integer((String) lesChamps.objectAtIndex(7)));
		} else {
			newArticleMarche.setArtmLivraison(new Integer(myView.getTfLivraison().getText()));
		}

		try {
			newArticleMarche.setArtmQteControle(new Integer((String) lesChamps.objectAtIndex(4)));
			newArticleMarche.setArtmQteUnite(new Integer((String) lesChamps.objectAtIndex(8)));
		} catch (Exception e) {
			newArticleMarche.setArtmQteControle(new Integer(1));
			newArticleMarche.setArtmQteUnite(new Integer(1));
		}

		// CATALOGUE ARTICLE

		EOCatalogueArticle newCatalogueArticle = EOCatalogueArticle.creer(ec, currentAttributionCatalogue.catalogue());

		newCatalogueArticle.setArticleRelationship(newArticle);
		newCatalogueArticle.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_VALIDE));

		EOTaux tauxTva = null;
		if (myView.getPopupColTva().getSelectedIndex() == 0) {
			tauxTva = (EOTaux) myView.getPopupTva().getSelectedItem();
		} else {
			tauxTva = EOTaux.find(ec, new BigDecimal((String) lesChamps.objectAtIndex(myView.getPopupColTva().getSelectedIndex() - 1)));
		}

		newCatalogueArticle.setTauxRelationship(tauxTva);

		String prix = (String) lesChamps.objectAtIndex(myView.getPopupColPrix().getSelectedIndex());
		prix = NSArray.componentsSeparatedByString(prix, ",").componentsJoinedByString(".");
		BigDecimal ht = new BigDecimal(prix);
		newCatalogueArticle.setCaarPrixHT(ht.setScale(3, BigDecimal.ROUND_HALF_UP));

		BigDecimal ttc = ht.add((ht.multiply(tauxTva.tvaTaux())).divide(new BigDecimal(100)));
		newCatalogueArticle.setCaarPrixTTC(ttc.setScale(3, BigDecimal.ROUND_HALF_UP));

		newCatalogueArticle.setCaarReference((String) lesChamps.objectAtIndex(myView.getPopupColReference().getSelectedIndex()));

		return newArticle;

	}

	private EOArticle creerOption(NSArray champs, EOArticle articlePere) {

		if (articlePere == null) {
			throw new NSValidation.ValidationException("Option sans article : " + champs);
		}
		
		// ARTICLE
		EOArticle newArticle = EOArticle.creer(ec);

		newArticle.setTypeArticleRelationship(EOTypeArticle.findForType(ec, "OPTION"));

		if (champs.count() == 6) {
			// Pas de code de nomenclature dans le fichier
			newArticle.setCodeMarcheRelationship((EOCodeMarche) myView.getCodesNomenclature().getSelectedItem());
		} else {
			// Code de nomenclature présent
			String stringCodeMarche = (String) champs.objectAtIndex(myView.getPopupCollCodeNomenclature().getSelectedIndex() - 1);
			EOCodeMarche codeMarche = EOCodeMarche.findForCode(ec, stringCodeMarche);
			newArticle.setCodeMarcheRelationship(codeMarche);
		}

		newArticle.setArtLibelle((String) champs.objectAtIndex(2));

		// ARTICLE MARCHE

		EOArticleMarche newArticleMarche = EOArticleMarche.creer(ec, newArticle);

		newArticleMarche.setDeviseRelationship((EODevise) myView.getDevises().getSelectedItem());

		newArticleMarche.setArtmGarantie(((EOArticleMarche) articlePere.articlesMarche().objectAtIndex(0)).artmGarantie());
		newArticleMarche.setArtmLivraison(((EOArticleMarche) articlePere.articlesMarche().objectAtIndex(0)).artmLivraison());
		newArticleMarche.setArtmQteUnite(((EOArticleMarche) articlePere.articlesMarche().objectAtIndex(0)).artmQteUnite());

		newArticleMarche.setArtmQteControle(new Integer((String) champs.objectAtIndex(4))); 

		// CATALOGUE ARTICLE

		EOCatalogueArticleOpt newCatalogueArticleOpt = EOCatalogueArticleOpt.creer(ec, currentAttributionCatalogue.catalogue());
		newCatalogueArticleOpt.setArticleRelationship(newArticle);
		newArticle.setPereRelationship(articlePere);

		newCatalogueArticleOpt.setArticleRelationship(newArticle);
		newCatalogueArticleOpt.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_VALIDE));

		BigDecimal tva = new BigDecimal((String) champs.objectAtIndex(5));
		newCatalogueArticleOpt.setTauxRelationship(EOTaux.find(ec, tva));

		BigDecimal ht = new BigDecimal((String) champs.objectAtIndex(3));
		newCatalogueArticleOpt.setCaarPrixHT(ht.setScale(3, BigDecimal.ROUND_HALF_UP));

		BigDecimal ttc = ht.add((ht.multiply(tva)).divide(new BigDecimal(100)));
		newCatalogueArticleOpt.setCaarPrixTTC(ttc.setScale(3, BigDecimal.ROUND_HALF_UP));

		newCatalogueArticleOpt.setCaarReference((String) champs.objectAtIndex(1));

		return newArticle;
	}
}
