package org.cocktail.sapics.client.attributions;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.sapics.client.eof.model.EOArticle;
import org.cocktail.sapics.client.eof.model.EOArticleMarche;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticleOpt;
import org.cocktail.sapics.client.eof.model.EOCodeMarche;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOLotNomenclature;
import org.cocktail.sapics.client.eof.model.EOTaux;
import org.cocktail.sapics.client.eof.model.EOTypeArticle;
import org.cocktail.sapics.client.gui.ArticleSaisieView;
import org.cocktail.sapics.client.utilities.CocktailUtilities;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class OptionSaisieCtrl 
{
	private static OptionSaisieCtrl sharedInstance;

	private EOEditingContext	ec;

	private ArticleSaisieView myView;

	private EOLot currentLot;
	private	EOCatalogueArticleOpt  currentCatalogueArticle;
	private EOArticle currentArticle;
	private EOArticleMarche currentArticleMarche;


	/** 
	 *
	 */
	public OptionSaisieCtrl (EOEditingContext globalEc) {

		super();

		myView = new ArticleSaisieView(new JFrame(), true);

		ec = globalEc;

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.setTvas(EOTaux.find(ec));
		
		myView.setTypesArticle(EOTypeArticle.fetchAll(ec));
		
		
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static OptionSaisieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new OptionSaisieCtrl(editingContext);
		return sharedInstance;
	}



	public void modifier(EOLot lot, EOCatalogueArticleOpt cat, EOArticleMarche articleMarche)	{

		currentLot = lot;
		currentCatalogueArticle = cat;
		currentArticle = currentCatalogueArticle.article();
		currentArticleMarche = articleMarche;
		
		if (lot != null)
			myView.setCodesNomenclatures((NSArray)(EOLotNomenclature.findForLot(ec, currentLot)).valueForKey(EOLotNomenclature.CODEMARCHE_KEY));

		actualiser();
		
		myView.setVisible(true);

	}
	
	
	private void clearTextField() {
		
		myView.getTfIntitule().setText("");
		myView.getTfReference().setText("");
		myView.getTfConditionnement().setText("");
		myView.getTfPrixUnitaire().setText("");
		myView.getTfQuantiteMax().setText("");

		myView.getTfDelaiLivraison().setText("");
		myView.getTfDureeGarantie().setText("");
		
		myView.getTva().setSelectedIndex(0);
		myView.getCodesNomenclature().setSelectedIndex(0);

	}
	
	private void actualiser() {
		
		clearTextField();
		
		if (currentCatalogueArticle.caarPrixHT() != null)
			myView.getTfPrixUnitaire().setText(currentCatalogueArticle.caarPrixHT().toString());

		if (currentArticle != null && currentArticle.artLibelle() != null)
			myView.getTfIntitule().setText(currentCatalogueArticle.article().artLibelle());

		if (currentCatalogueArticle.caarReference() != null)
			myView.getTfReference().setText(currentCatalogueArticle.caarReference());

		if (currentArticleMarche != null && currentArticleMarche.artmLivraison() != null)
			myView.getTfDelaiLivraison().setText(currentArticleMarche.artmLivraison().toString());

		if (currentArticleMarche != null && currentArticleMarche.artmQteControle() != null)
			myView.getTfQuantiteMax().setText(currentArticleMarche.artmQteControle().toString());

		if (currentArticleMarche != null && currentArticleMarche.artmGarantie() != null)
			myView.getTfDureeGarantie().setText(currentArticleMarche.artmGarantie().toString());

		myView.getTva().setSelectedItem(currentCatalogueArticle.taux());

		if (currentArticle != null && currentArticle.codeMarche() != null)
			myView.getCodesNomenclature().setSelectedItem(currentArticle.codeMarche());

	}

	/**
	 *
	 */
	private void valider()	{

		try {
			
			
			if (myView.getTfIntitule().getText().length() == 0)
				throw new ValidationException("Merci de spécifier un libellé pour cet article !");

			if (myView.getTfReference().getText().length() == 0)
				throw new ValidationException("Vous devez entrer une référence !");

			if (myView.getTfDelaiLivraison().getText().length() == 0)
				throw new ValidationException("Vous devez entrer un délai de livraison !");

			if (myView.getTfDureeGarantie().getText().length() == 0)
				throw new ValidationException("Vous devez entrer une durée de garantie !");

			if (myView.getTva().getSelectedIndex() <= 0)
				throw new ValidationException("Le taux de TVA est obligatoire !");

			if (!CocktailUtilities.isInteger(myView.getTfDelaiLivraison().getText()))
				throw new ValidationException("Vous devez spécifier un nombre ENTIER pour le délai de livraison !");

			if (!CocktailUtilities.isInteger(myView.getTfDureeGarantie().getText()))
				throw new ValidationException("Vous devez spécifier un nombre ENTIER pour la durée de garantie !");

			if (!CocktailUtilities.isInteger(myView.getTfQuantiteMax().getText()))
				throw new ValidationException("Vous devez spécifier un nombre ENTIER pour la quantité !");

			
			
			currentArticle.setArtLibelle(myView.getTfIntitule().getText());			
			currentArticle.setTypeArticleRelationship((EOTypeArticle)myView.getTypesArticle().getSelectedItem());

			if (myView.getCodesNomenclature().getSelectedIndex() > 0)
				currentArticle.setCodeMarcheRelationship((EOCodeMarche)myView.getCodesNomenclature().getSelectedItem());
			else
				currentArticle.setCodeMarcheRelationship(null);
			
			currentArticleMarche.setArtmLivraison(new Integer(myView.getTfDelaiLivraison().getText()));
			currentArticleMarche.setArtmGarantie(new Integer(myView.getTfDureeGarantie().getText()));

			currentCatalogueArticle.setCaarReference(myView.getTfReference().getText());
			currentCatalogueArticle.setTauxRelationship((EOTaux)myView.getTva().getSelectedItem());

			if (myView.getTfPrixUnitaire().getText().length() > 0) {
				String montant = StringCtrl.replace(myView.getTfPrixUnitaire().getText(), ",", ".");
				currentCatalogueArticle.setCaarPrixHT(new BigDecimal(montant));				
			}

			BigDecimal prixTTC = CocktailUtilities.prixTTC(currentCatalogueArticle.caarPrixHT(), ((EOTaux) myView.getTva().getSelectedItem()).tvaTaux(), 2);
			currentCatalogueArticle.setCaarPrixTTC(prixTTC);
						
			if (myView.getTfQuantiteMax().getText().length() > 0) {				
				String quantite = StringCtrl.replace(myView.getTfQuantiteMax().getText(), ",", ".");
				currentArticleMarche.setArtmQteControle(new Integer(quantite));
			}


			ec.saveChanges();
			
			myView.dispose();
			
		}
		catch (ValidationException ex){
			
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
			
		}
		catch (Exception e){
			
			e.printStackTrace();
		}

		
	}
	
	




	/**
	 *
	 */
	public void annuler()	{

		ec.revert();
		myView.dispose();
		
	}


}