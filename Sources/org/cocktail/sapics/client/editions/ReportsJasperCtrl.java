/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client.editions;

import org.cocktail.application.client.tools.ToolsCocktailReports;
import org.cocktail.sapics.client.ApplicationClient;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class ReportsJasperCtrl {

    private final static String JASPER_NAME_EXECUTION = "executions.jasper";
    private final static String PDF_NAME_EXECUTION = "executions";

    private final static String JASPER_NAME_MARCHES_EN_COURS = "listeMarchesEnCours.jasper";
    private final static String PDF_NAME_MARCHES_EN_COURS = "listeMarchesEnCours";

    private final static String JASPER_NAME_MARCHES_EXERCICE = "listeMarches.jasper";
    private final static String PDF_NAME_MARCHES_EXERCICE = "listeMarches";

    private final static String JASPER_NAME_CODES_NOMENCLATURES = "liste_cn.jasper";
    private final static String PDF_NAME_CODES_NOMENCLATURES = "liste_cn";

    private final static String JASPER_NAME_DETAIL_MARCHE = "detailMarche.jasper";
    private final static String PDF_NAME_DETAIL_MARCHE = "detailMarche";

    private final static String JASPER_NAME_SAPICS_EXECUTION_LOT = "Sapics_execution_lot.jasper";
    private final static String PDF_NAME_SAPICS_EXECUTION_LOT = "SapicsExecutionLot";
    private final static String JASPER_NAME_SAPICS_EXECUTION_LOT_FOU_EXER = "Sapics_execution_lot_fouexer.jasper";
    private final static String PDF_NAME_SAPICS_EXECUTION_LOT_FOU_EXER = "SapicsExecutionLotFouExer";

    private static ReportsJasperCtrl sharedInstance;
	
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
    
	public ReportsJasperCtrl(EOEditingContext editingContext) {
		
		super();
						
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ReportsJasperCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ReportsJasperCtrl(editingContext);
		return sharedInstance;
	}	
	
	public void printMarchesExecutions(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_EXECUTION, parametres, PDF_NAME_EXECUTION);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	
	public void printDetailMarche(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_DETAIL_MARCHE, parametres, PDF_NAME_DETAIL_MARCHE);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	
	public void printListeMarches(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_MARCHES_EXERCICE, parametres, PDF_NAME_MARCHES_EXERCICE);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	
	
	public void printListeMarchesEnCours(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_MARCHES_EN_COURS, parametres, PDF_NAME_MARCHES_EN_COURS);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	public void printSapicsExecutionLot(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_SAPICS_EXECUTION_LOT, parametres, PDF_NAME_SAPICS_EXECUTION_LOT);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	public void printSapicsExecutionLotFouExer(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);
			myReport.imprimerReportParametres(JASPER_NAME_SAPICS_EXECUTION_LOT_FOU_EXER, parametres, PDF_NAME_SAPICS_EXECUTION_LOT_FOU_EXER);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	
	public void printListeCodesNomenclatures(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_CODES_NOMENCLATURES, parametres, PDF_NAME_CODES_NOMENCLATURES);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	
}
