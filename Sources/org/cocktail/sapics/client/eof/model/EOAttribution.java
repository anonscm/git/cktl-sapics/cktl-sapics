/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.sapics.client.eof.model;

import org.cocktail.sapics.client.factory.Factory;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOAttribution extends _EOAttribution {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4593867853793557567L;

	public EOAttribution() {
		super();
	}

	public String adresseFournisseur() {

		return fournis().adrAdresse1()+" "+fournis().adrCp()+" "+fournis().adrVille();

	}

	public static EOAttribution findForLotAndTitulaire(EOEditingContext ec, EOTitulaire titulaire, EOLot lot)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.FOURNIS_KEY + " = %@", new NSArray(titulaire.fournisseur())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY + " = %@", new NSArray(lot)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(mesQualifiers), null);

	}


	public static NSArray findForLot(EOEditingContext ec, EOLot lot)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_SUPPR_KEY + " = %@", new NSArray("N")));
		//	mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_VALIDE_KEY + " = %@", new NSArray("O")));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY + " = %@", new NSArray(lot)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers),null, true);
		
	}

	public static NSArray findForTitulaire(EOEditingContext ec, EOLot lot, EOFournis fournis)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_VALIDE_KEY + " = %@", new NSArray("O")));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY + " = %@", new NSArray(lot)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.FOURNIS_KEY + " = %@", new NSArray(fournis)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers),null, true);

	}

	public static EOAttribution creer(EOEditingContext ec, EOLot lot, EOTitulaire titulaire, EOFournis fournisseur, EOUtilisateur utilisateur, Integer index)	{

		try {

			EOAttribution record = (EOAttribution)Factory.instanceForEntity(ec, EOAttribution.ENTITY_NAME);

			record.setLotRelationship(lot);
			record.setFournisRelationship(fournisseur);
			record.setTitulaireRelationship(titulaire);
			record.setUtilisateurRelationship(utilisateur);
			
			
			NSArray ribs = EOVRib.findForFournis(ec, fournisseur);
			
			if (ribs.count() == 1)
				record.setRibRelationship((EOVRib)ribs.objectAtIndex(0));
			
			record.setAttIndex(index);
			record.setAttSuppr("N");
			record.setAttAcceptee("N");
			record.setAttDate(new NSTimestamp());
			record.setAttDebut(lot.lotDebut());
			record.setAttFin(lot.lotFin());
			record.setAttValide("N");
		
			ec.insertObject(record);

			return record;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static void supprimer(EOEditingContext ec, EOAttribution attribution) {
		
		// Suppression des attributionCatalogue
		try {

			// On teste s'il n'existe pas un article utilise sur cette attribution
			//NSArray articles = EOArticle.
			
			// Suppression des Attributions Catalogues
			NSArray attributionsCatalogues = EOAttributionCatalogue.findForAttribution(ec, attribution);
			
			for (int i=0;i<attributionsCatalogues.count();i++)
				ec.deleteObject((EOAttributionCatalogue)attributionsCatalogues.objectAtIndex(i));

			// Suppression des sous traitants
			NSArray sousTraitants = EOSousTraitant.findForAttribution(ec, attribution);
			
			for (int i=0;i<sousTraitants.count();i++)
				ec.deleteObject((EOSousTraitant)sousTraitants.objectAtIndex(i));
			
			ec.deleteObject(attribution);


		}
		catch (Exception e) {

			EODialogs.runErrorDialog("ERREUR", "ERREUR de suppression de l'attribution !\n"+CocktailUtilities.getErrorDialog(e));
			
			ec.revert();
			e.printStackTrace();

		}
	}
	
	
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */    
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
