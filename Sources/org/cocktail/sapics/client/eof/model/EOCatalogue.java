/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.sapics.client.eof.model;

import org.cocktail.application.client.eof.EOTypeApplication;
import org.cocktail.sapics.client.factory.Factory;
import org.cocktail.sapics.client.finder.FinderTypeApplication;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOCatalogue extends _EOCatalogue {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3807633160178003079L;

	public EOCatalogue() {
        super();
    
	}
	
	
	public boolean isValide() {
		
		return typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_VALIDE);
		
	}
	
	public static NSArray find(EOEditingContext ec, EOTypeEtat typeEtat)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOCatalogue.CAT_DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.TYPE_APPLICATION_KEY+"."+EOTypeApplication.TYAP_LIBELLE_KEY + " = %@", new NSArray("MARCHES")));

		if (typeEtat != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.TYPE_ETAT_KEY + " = %@", new NSArray(typeEtat)));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers),mySort, true);
	}

	public static NSArray findForFournis(EOEditingContext ec, EOFournis fournis)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOCatalogue.CAT_DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.FOURNIS_KEY + " = %@", new NSArray(fournis)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.TYPE_APPLICATION_KEY+"."+EOTypeApplication.TYAP_LIBELLE_KEY + " = %@", new NSArray("MARCHES")));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.TYPE_ETAT_KEY + " = %@", new NSArray(EOTypeEtat.find(ec,EOTypeEtat.ETAT_VALIDE))));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers),mySort, true);
		
	}

	public static EOCatalogue creer(EOEditingContext ec)	{

		try {

			EOCatalogue record = (EOCatalogue)Factory.instanceForEntity(ec, EOCatalogue.ENTITY_NAME);	
			
			record.setTypeEtatRelationship(EOTypeEtat.find(ec, EOTypeEtat.ETAT_VALIDE));
			
			record.setTypeApplicationRelationship(FinderTypeApplication.findTypeApplication(ec, "MARCHES"));
			
			ec.insertObject(record);

			return record;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
