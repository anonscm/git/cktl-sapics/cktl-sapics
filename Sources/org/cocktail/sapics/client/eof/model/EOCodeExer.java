/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.sapics.client.eof.model;

import java.math.BigDecimal;

import org.cocktail.sapics.client.factory.Factory;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOCodeExer extends _EOCodeExer {

	private static final EOQualifier QUAL_CE_NON_SUPPR = EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CE_SUPPR_KEY + " = %@", new NSArray("N"));
	private static final long serialVersionUID = 6402257370302872075L;

	public EOCodeExer() {
        super();
    }

	public static NSArray findForNiveau(EOEditingContext ec, EOExercice exercice, Integer niveau) {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(QUAL_CE_NON_SUPPR);
		mesQualifiers.addObject(getQualifierExercice(exercice));
		if (niveau != null) {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODEMARCHE_KEY + "." + EOCodeMarche.CM_NIVEAU_KEY + " = %@", new NSArray(niveau)));
		}
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CE_ACTIF_KEY + " = %@", new NSArray("O")));

		NSArray mySort = new NSArray(new EOSortOrdering(EOCodeExer.CODEMARCHE_KEY + "." + EOCodeMarche.CM_CODE_KEY, EOSortOrdering.CompareAscending));

		EOFetchSpecification fs = new EOFetchSpecification(EOCodeExer.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		return ec.objectsWithFetchSpecification(fs);
	}

	public static EOExercice findLastExercice(EOEditingContext ec) {

		try {

			NSArray mySort = new NSArray(new EOSortOrdering(EOCodeExer.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending));
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(QUAL_CE_NON_SUPPR);


			EOFetchSpecification fs = new EOFetchSpecification(EOCodeExer.ENTITY_NAME,new EOAndQualifier(mesQualifiers), mySort);
			fs.setFetchLimit(1);

			return ((EOCodeExer)(ec.objectsWithFetchSpecification(fs).objectAtIndex(0))).exercice();

		}
		catch (Exception ex) {

			ex.printStackTrace();
			return null;

		}
	}

	public static EOCodeExer codeExerForCodeMarche(EOEditingContext ec, EOExercice exercice, String codeMarche)	{

		try {
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(getQualifierExercice(exercice));

			mesQualifiers.addObject(QUAL_CE_NON_SUPPR);

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODEMARCHE_KEY + "." + EOCodeMarche.CM_CODE_KEY + " = %@", new NSArray(codeMarche)));

			EOFetchSpecification fs = new EOFetchSpecification(EOCodeExer.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

			return (EOCodeExer)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{return null;}

	}

	/**
	 * @deprecated Utiliser {@link #findForCodeMarcheEtExercice(EOCodeMarche, EOExercice)}
	 */
	@Deprecated
	public static EOCodeExer findForCodeMarche(EOEditingContext ec, EOExercice exercice, EOCodeMarche codeMarche)	{

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(QUAL_CE_NON_SUPPR);

			mesQualifiers.addObject(getQualifierExercice(exercice));

			mesQualifiers.addObject(getQualifierCodeMarche(codeMarche));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOCodeExer.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

			return (EOCodeExer)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{e.printStackTrace(); return null;}

	}
	
	public static EOCodeExer findForCodeMarcheEtExercice(EOCodeMarche codeMarche, EOExercice exercice, EOQualifier qualifier) {
		EOCodeExer codeExer = null;
		if (codeMarche != null) {
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(QUAL_CE_NON_SUPPR);
			mesQualifiers.addObject(getQualifierExercice(exercice));
			mesQualifiers.addObject(getQualifierCodeMarche(codeMarche));
			if (qualifier != null) {
				mesQualifiers.addObject(qualifier);
			}
			NSArray<EOCodeExer> liste = codeMarche.codeExers(new EOAndQualifier(mesQualifiers));
			if (!liste.isEmpty()) {
				return liste.objectAtIndex(0);
			}
		}		
		return codeExer;
	}
	
	public static EOCodeExer findForCodeMarcheEtExercice(EOCodeMarche codeMarche, EOExercice exercice) {
		return findForCodeMarcheEtExercice(codeMarche, exercice, null);
	}

	/**
	 * @param codeMarche
	 * @return
	 */
	private static EOQualifier getQualifierCodeMarche(EOCodeMarche codeMarche) {
		return EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODEMARCHE_KEY + " = %@", new NSArray(codeMarche));
	}

	/**
	 * @param exercice
	 * @return
	 */
	private static EOQualifier getQualifierExercice(EOExercice exercice) {
		return new EOKeyValueQualifier(EOCodeExer.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
	}
	
	public static NSArray<EOCodeExer> fetchFils(EOCodeMarche codeMarche, EOExercice exercice) {
		NSMutableArray<EOCodeExer> listeCodeExer = new NSMutableArray<EOCodeExer>();
		NSArray<EOCodeMarche> listeCM = EOCodeMarche.findForPere(codeMarche.editingContext(), exercice, codeMarche);
		for (EOCodeMarche codeMarcheFils : listeCM) {
			EOCodeExer codeExerFils = findForCodeMarcheEtExercice(codeMarcheFils, exercice);
			if (codeExerFils != null) {
				listeCodeExer.addObject(codeExerFils);
			}
		}
		return listeCodeExer.immutableClone();
	}

	private static EOCodeExer fetchPere(EOCodeMarche codeMarche, EOExercice exercice) {
		EOCodeExer codeExerPere = null;
		if (codeMarche.pere() != null) {
			NSArray arr = codeMarche.pere().codeExers(getQualifierExercice(exercice));
			if (arr.count() > 0) {
				codeExerPere = (EOCodeExer) arr.objectAtIndex(0);
			}
		}
		return codeExerPere;
	}

	public static EOCodeExer creer(EOEditingContext ec, EOExercice exercice, EOCodeMarche codeMarche)	{

		try {

			EOCodeExer record = (EOCodeExer) Factory.instanceForEntity(ec, EOCodeExer.ENTITY_NAME);
			
			record.setExerciceRelationship(exercice);
			record.setCodemarcheRelationship(codeMarche);

			record.setCe3Cmp(new Integer(0));
			record.setCeAutres(new Integer(0));
			record.setCeMonopole(new Integer(0));
			record.setCeRech("N");

			EOCodeExer pere = fetchPere(codeMarche, exercice);
			if (pere != null) {
				record.setCe3Cmp(pere.ce3Cmp());
				record.setCeAutres(pere.ceAutres());
				record.setCeMonopole(pere.ceMonopole());
				record.setCeRech(pere.ceRech());
				record.setTypeCNRelationship(pere.typeCN());
			}
			
			record.setCeSuppr("N");
			record.setCeActif("O");

			record.setCeControle(new BigDecimal(0));

			ec.insertObject(record);
			
			return record;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
        
        if (ce3Cmp().intValue() == 1 && ceMonopole().intValue() == 1)
        	throw new ValidationException("Vous ne pouvez pas cocher 3CMP ET Monopole !");

        if (ce3Cmp().intValue() == 1 && ceAutres().intValue() == 1)
        	throw new ValidationException("Vous ne pouvez pas cocher 3CMP ET Autres !");

        if (ceMonopole().intValue() == 1 && ceAutres().intValue() == 1)
        	throw new ValidationException("Vous ne pouvez pas cocher Monopole ET Autres !");

    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
