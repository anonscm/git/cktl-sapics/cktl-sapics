/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.sapics.client.eof.model;

import org.cocktail.sapics.client.factory.Factory;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOCodeMarche extends _EOCodeMarche {

	private static final NSArray SORT_BY_CM_CODE = new NSArray(new EOSortOrdering(EOCodeMarche.CM_CODE_KEY, EOSortOrdering.CompareAscending));
	private static final long serialVersionUID = -468333134534822760L;

	public static EOCodeMarche creer(EOEditingContext ec, EOCodeMarche pere) {
		try {
			EOCodeMarche record = (EOCodeMarche) Factory.instanceForEntity(ec, EOCodeMarche.ENTITY_NAME);
			if (pere != null) {
				record.setPereRelationship(pere);
			}
			record.setCmSuppr("N");
			record.setToTypeCodeMarcheRelationship(EOTypeCodeMarche.getTypeLocal(ec));
			ec.insertObject(record);
			return record;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOCodeMarche findForCode(EOEditingContext ec, String code)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CM_SUPPR_KEY + " = %@", new NSArray("N")));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CM_CODE_KEY + " = %@", new NSArray(code)));
		
		return fetchFirstByQualifier(ec, new EOAndQualifier(mesQualifiers), null);
	}

	@Deprecated
	public static NSArray findForCodeExercideAndNiveau(EOEditingContext ec, EOExercice exercice , Integer niveau)	{
		return findForCodeExercideAndNiveau(ec, exercice, niveau, false);
	}
	
	public static NSArray findForCodeExercideAndNiveau(EOEditingContext ec, EOExercice exercice , Integer niveau, boolean showMasques)	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CODE_EXERS_KEY + "." + EOCodeExer.CE_SUPPR_KEY + " = %@",
				new NSArray("N")));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CODE_EXERS_KEY + "." + EOCodeExer.EXERCICE_KEY + " = %@", new NSArray(
				exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CM_NIVEAU_KEY + " = %@", new NSArray(niveau)));
		if (!showMasques) {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CODE_EXERS_KEY + "." + EOCodeExer.CE_ACTIF_KEY + " = %@",
					new NSArray("O")));
		}
		return fetchAll(ec, new EOAndQualifier(mesQualifiers), SORT_BY_CM_CODE, true);
	}

	public static NSArray findForPere(EOEditingContext ec, EOExercice exercice, EOCodeMarche pere) {
		return findForPere(ec, exercice, pere, false);
	}

	public static NSArray findForPere(EOEditingContext ec, EOExercice exercice, EOCodeMarche pere, boolean showMasques)	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CODE_EXERS_KEY + "." + EOCodeExer.EXERCICE_KEY + " = %@", new NSArray(
				exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CODE_EXERS_KEY + "." + EOCodeExer.CE_SUPPR_KEY + " = %@",
				new NSArray("N")));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.PERE_KEY + " = %@", new NSArray(pere)));
		if (!showMasques) {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeMarche.CODE_EXERS_KEY + "." + EOCodeExer.CE_ACTIF_KEY + " = %@",
					new NSArray("O")));
		}
		return fetchAll(ec, new EOAndQualifier(mesQualifiers), SORT_BY_CM_CODE, true);
	}

	public EOCodeMarche() {
		super();
	}

	public String toString() {
		
		return cmCode();
		
	}

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

	/**
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 * @throws ValidationException
	 *             Si paramètres manquants ou trop longs
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		if (cmNiveau() == null) {
			throw new ValidationException("Le niveau du code nomenclature doit être renseigné !");
		}

		if (cmLib() == null) {
			throw new ValidationException("Le libellé du code nomenclature est obligatoire !");
		}

		EODistributedClassDescription description = (EODistributedClassDescription) EOClassDescription.classDescriptionForEntityName(ENTITY_NAME);
		int widthMax = description.attributeNamed(CM_LIB_KEY).width();
		if (cmLib().length() > widthMax) {
			throw new ValidationException("Le libellé ne doit pas comporter plus de " + widthMax + " caractères !");
		}

		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    /**
     * ce code marche est-il supprimable
     *
     */
    public boolean isSupprimable() {
		return !EOTypeCodeMarche.CODE_NACRES.equals(this.toTypeCodeMarche().tcmCode());
	}
    
    /**
     * ce code marche est-il modifiable
     *
     */
    public boolean isModifiable() {
		return !EOTypeCodeMarche.CODE_NACRES.equals(this.toTypeCodeMarche().tcmCode());
	}

}
