/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.sapics.client.eof.model;

import org.cocktail.sapics.client.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOParametreMarche extends _EOParametreMarche {

	
	public static final String ID_CTRL_DATES = "CTRL_DATES_MARCHE";
	public static final String DEFAULT_VALUE_CTRL_DATES = "N";

	
	
	
	/**
	 * 
	 * @param ec
	 * @param key
	 * @return
	 */
		public static String getValue(EOEditingContext ec, String key)	{
			try {
				return fetchFirstByQualifier(ec, 
						EOQualifier.qualifierWithQualifierFormat(PARAM_KEY_KEY + " = %@", new NSArray(key))).paramValue();
			}
			catch (Exception e)	{
				return null;
			}
		}
	
/**
 * 
 * @param ec
 * @param key
 * @return
 */
	public static EOParametreMarche find(EOEditingContext ec, String key)	{
		try {
			return fetchFirstByQualifier(ec, 
					EOQualifier.qualifierWithQualifierFormat(PARAM_KEY_KEY + " = %@", new NSArray(key)));
		}
		catch (Exception e)	{
			return null;
		}
	}
/**
 * Creation d un nouvel objet parametreMarche
 * @param ec
 * @param code
 * @param commentaire
 * @return
 */
	public static EOParametreMarche creer (EOEditingContext ec, String code, String commentaire)	{
		
		EOParametreMarche param = (EOParametreMarche)Factory.instanceForEntity(ec, EOParametreMarche.ENTITY_NAME);

		param.setParamKey(code);
		param.setParamCommentaires(commentaire);
		
		ec.insertObject(param);
		
		return param;
	}

    /**
	 * 
	 */
	private static final long serialVersionUID = -5310707577023422807L;

	public EOParametreMarche() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
