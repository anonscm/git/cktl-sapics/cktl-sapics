/*
 * Copyright Cocktail, 2001-2013 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.sapics.client.eof.model;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class EOTypeCodeMarche extends _EOTypeCodeMarche {

	private static final long serialVersionUID = 1L;

	public static final String CODE_NACRES = "N";
	public static final String CODE_ANCIEN = "A";
	public static final String CODE_LOCAL = "L";

	/**
	 * Défini le type de code marché :<br>
	 * <ul>
	 * <li>NACRES</li>
	 * <li>ANCIEN</li>
	 * <li>LOCAL</li>
	 * </ul>
	 */
	public EOTypeCodeMarche() {
		super();
	}

	/**
	 * @param ec
	 *            Un {@link EOEditingContext}
	 * @return Le type NACRES
	 */
	public static EOTypeCodeMarche getTypeNacres(EOEditingContext ec) {
		return getTypeByCode(ec, CODE_NACRES);
	}

	/**
	 * @param ec
	 *            Un {@link EOEditingContext}
	 * @return Le type ANCIEN
	 */
	public static EOTypeCodeMarche getTypeAncien(EOEditingContext ec) {
		return getTypeByCode(ec, CODE_ANCIEN);
	}

	/**
	 * @param ec
	 *            Un {@link EOEditingContext}
	 * @return Le type LOCAL
	 */
	public static EOTypeCodeMarche getTypeLocal(EOEditingContext ec) {
		return getTypeByCode(ec, CODE_LOCAL);
	}

	/**
	 * @param ec
	 *            Un {@link EOEditingContext}
	 * @param code
	 *            Le code recherché
	 * @return Un type de code marché
	 */
	private static EOTypeCodeMarche getTypeByCode(EOEditingContext ec, String code) {
		return fetchRequiredByKeyValue(ec, TCM_CODE_KEY, code);
	}

	/** {@inheritDoc} */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/** {@inheritDoc} */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/** {@inheritDoc} */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/** {@inheritDoc} */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 *             Si erreur de validation de l'objet
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	}

}
