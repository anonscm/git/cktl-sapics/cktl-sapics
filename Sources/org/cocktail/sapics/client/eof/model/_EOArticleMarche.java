// _EOArticleMarche.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOArticleMarche.java instead.
package org.cocktail.sapics.client.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOArticleMarche extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "ArticleMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_CATALOGUE.ARTICLE_MARCHE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "artmId";

	public static final String ARTM_GARANTIE_KEY = "artmGarantie";
	public static final String ARTM_LIVRAISON_KEY = "artmLivraison";
	public static final String ARTM_QTE_CONTROLE_KEY = "artmQteControle";
	public static final String ARTM_QTE_UNITE_KEY = "artmQteUnite";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String ARTM_ID_KEY = "artmId";
	public static final String DEV_ID_KEY = "devId";

//Colonnes dans la base de donnees
	public static final String ARTM_GARANTIE_COLKEY = "ARTM_GARANTIE";
	public static final String ARTM_LIVRAISON_COLKEY = "ARTM_LIVRAISON";
	public static final String ARTM_QTE_CONTROLE_COLKEY = "ARTM_QTE_CONTROLE";
	public static final String ARTM_QTE_UNITE_COLKEY = "ARTM_QTE_UNITE";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String ARTM_ID_COLKEY = "ARTM_ID";
	public static final String DEV_ID_COLKEY = "DEV_ID";


	// Relationships
	public static final String ARTICLE_KEY = "article";
	public static final String DEVISE_KEY = "devise";



	// Accessors methods
  public Integer artmGarantie() {
    return (Integer) storedValueForKey(ARTM_GARANTIE_KEY);
  }

  public void setArtmGarantie(Integer value) {
    takeStoredValueForKey(value, ARTM_GARANTIE_KEY);
  }

  public Integer artmLivraison() {
    return (Integer) storedValueForKey(ARTM_LIVRAISON_KEY);
  }

  public void setArtmLivraison(Integer value) {
    takeStoredValueForKey(value, ARTM_LIVRAISON_KEY);
  }

  public Integer artmQteControle() {
    return (Integer) storedValueForKey(ARTM_QTE_CONTROLE_KEY);
  }

  public void setArtmQteControle(Integer value) {
    takeStoredValueForKey(value, ARTM_QTE_CONTROLE_KEY);
  }

  public Integer artmQteUnite() {
    return (Integer) storedValueForKey(ARTM_QTE_UNITE_KEY);
  }

  public void setArtmQteUnite(Integer value) {
    takeStoredValueForKey(value, ARTM_QTE_UNITE_KEY);
  }

  public org.cocktail.sapics.client.eof.model.EOArticle article() {
    return (org.cocktail.sapics.client.eof.model.EOArticle)storedValueForKey(ARTICLE_KEY);
  }

  public void setArticleRelationship(org.cocktail.sapics.client.eof.model.EOArticle value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOArticle oldValue = article();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EODevise devise() {
    return (org.cocktail.sapics.client.eof.model.EODevise)storedValueForKey(DEVISE_KEY);
  }

  public void setDeviseRelationship(org.cocktail.sapics.client.eof.model.EODevise value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EODevise oldValue = devise();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEVISE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEVISE_KEY);
    }
  }
  

  public static EOArticleMarche createArticleMarche(EOEditingContext editingContext, Integer artmGarantie
, Integer artmLivraison
, org.cocktail.sapics.client.eof.model.EOArticle article, org.cocktail.sapics.client.eof.model.EODevise devise) {
    EOArticleMarche eo = (EOArticleMarche) createAndInsertInstance(editingContext, _EOArticleMarche.ENTITY_NAME);    
		eo.setArtmGarantie(artmGarantie);
		eo.setArtmLivraison(artmLivraison);
    eo.setArticleRelationship(article);
    eo.setDeviseRelationship(devise);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOArticleMarche.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOArticleMarche.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOArticleMarche localInstanceIn(EOEditingContext editingContext) {
	  		return (EOArticleMarche)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOArticleMarche localInstanceIn(EOEditingContext editingContext, EOArticleMarche eo) {
    EOArticleMarche localInstance = (eo == null) ? null : (EOArticleMarche)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOArticleMarche#localInstanceIn a la place.
   */
	public static EOArticleMarche localInstanceOf(EOEditingContext editingContext, EOArticleMarche eo) {
		return EOArticleMarche.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOArticleMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOArticleMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOArticleMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOArticleMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOArticleMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOArticleMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOArticleMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOArticleMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOArticleMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOArticleMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOArticleMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOArticleMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
