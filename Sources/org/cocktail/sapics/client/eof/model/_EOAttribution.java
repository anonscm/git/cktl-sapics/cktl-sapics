// _EOAttribution.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAttribution.java instead.
package org.cocktail.sapics.client.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAttribution extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Attribution";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.ATTRIBUTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "attOrdre";

	public static final String ATT_ACCEPTEE_KEY = "attAcceptee";
	public static final String ATT_DATE_KEY = "attDate";
	public static final String ATT_DEBUT_KEY = "attDebut";
	public static final String ATT_FIN_KEY = "attFin";
	public static final String ATT_HT_KEY = "attHt";
	public static final String ATT_INDEX_KEY = "attIndex";
	public static final String ATT_SUPPR_KEY = "attSuppr";
	public static final String ATT_TYPE_CONTROLE_KEY = "attTypeControle";
	public static final String ATT_VALIDE_KEY = "attValide";

// Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String RIBORDRE_KEY = "ribordre";
	public static final String TIT_ORDRE_KEY = "titOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ATT_ACCEPTEE_COLKEY = "ATT_ACCEPTEE";
	public static final String ATT_DATE_COLKEY = "ATT_DATE";
	public static final String ATT_DEBUT_COLKEY = "ATT_DEBUT";
	public static final String ATT_FIN_COLKEY = "ATT_FIN";
	public static final String ATT_HT_COLKEY = "ATT_HT";
	public static final String ATT_INDEX_COLKEY = "ATT_INDEX";
	public static final String ATT_SUPPR_COLKEY = "ATT_SUPPR";
	public static final String ATT_TYPE_CONTROLE_COLKEY = "ATT_TYPE_CONTROLE";
	public static final String ATT_VALIDE_COLKEY = "ATT_VALIDE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String RIBORDRE_COLKEY = "RIB_ORDRE";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String FOURNIS_KEY = "fournis";
	public static final String LOT_KEY = "lot";
	public static final String RIB_KEY = "rib";
	public static final String TITULAIRE_KEY = "titulaire";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String attAcceptee() {
    return (String) storedValueForKey(ATT_ACCEPTEE_KEY);
  }

  public void setAttAcceptee(String value) {
    takeStoredValueForKey(value, ATT_ACCEPTEE_KEY);
  }

  public NSTimestamp attDate() {
    return (NSTimestamp) storedValueForKey(ATT_DATE_KEY);
  }

  public void setAttDate(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_DATE_KEY);
  }

  public NSTimestamp attDebut() {
    return (NSTimestamp) storedValueForKey(ATT_DEBUT_KEY);
  }

  public void setAttDebut(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_DEBUT_KEY);
  }

  public NSTimestamp attFin() {
    return (NSTimestamp) storedValueForKey(ATT_FIN_KEY);
  }

  public void setAttFin(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_FIN_KEY);
  }

  public java.math.BigDecimal attHt() {
    return (java.math.BigDecimal) storedValueForKey(ATT_HT_KEY);
  }

  public void setAttHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ATT_HT_KEY);
  }

  public Integer attIndex() {
    return (Integer) storedValueForKey(ATT_INDEX_KEY);
  }

  public void setAttIndex(Integer value) {
    takeStoredValueForKey(value, ATT_INDEX_KEY);
  }

  public String attSuppr() {
    return (String) storedValueForKey(ATT_SUPPR_KEY);
  }

  public void setAttSuppr(String value) {
    takeStoredValueForKey(value, ATT_SUPPR_KEY);
  }

  public String attTypeControle() {
    return (String) storedValueForKey(ATT_TYPE_CONTROLE_KEY);
  }

  public void setAttTypeControle(String value) {
    takeStoredValueForKey(value, ATT_TYPE_CONTROLE_KEY);
  }

  public String attValide() {
    return (String) storedValueForKey(ATT_VALIDE_KEY);
  }

  public void setAttValide(String value) {
    takeStoredValueForKey(value, ATT_VALIDE_KEY);
  }

  public org.cocktail.sapics.client.eof.model.EOFournis fournis() {
    return (org.cocktail.sapics.client.eof.model.EOFournis)storedValueForKey(FOURNIS_KEY);
  }

  public void setFournisRelationship(org.cocktail.sapics.client.eof.model.EOFournis value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOLot lot() {
    return (org.cocktail.sapics.client.eof.model.EOLot)storedValueForKey(LOT_KEY);
  }

  public void setLotRelationship(org.cocktail.sapics.client.eof.model.EOLot value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOLot oldValue = lot();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOVRib rib() {
    return (org.cocktail.sapics.client.eof.model.EOVRib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.sapics.client.eof.model.EOVRib value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOVRib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOTitulaire titulaire() {
    return (org.cocktail.sapics.client.eof.model.EOTitulaire)storedValueForKey(TITULAIRE_KEY);
  }

  public void setTitulaireRelationship(org.cocktail.sapics.client.eof.model.EOTitulaire value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOTitulaire oldValue = titulaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITULAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITULAIRE_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOUtilisateur utilisateur() {
    return (org.cocktail.sapics.client.eof.model.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.sapics.client.eof.model.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOAttribution createAttribution(EOEditingContext editingContext, org.cocktail.sapics.client.eof.model.EOFournis fournis, org.cocktail.sapics.client.eof.model.EOLot lot) {
    EOAttribution eo = (EOAttribution) createAndInsertInstance(editingContext, _EOAttribution.ENTITY_NAME);    
    eo.setFournisRelationship(fournis);
    eo.setLotRelationship(lot);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOAttribution.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOAttribution.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOAttribution localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAttribution)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOAttribution localInstanceIn(EOEditingContext editingContext, EOAttribution eo) {
    EOAttribution localInstance = (eo == null) ? null : (EOAttribution)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOAttribution#localInstanceIn a la place.
   */
	public static EOAttribution localInstanceOf(EOEditingContext editingContext, EOAttribution eo) {
		return EOAttribution.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOAttribution fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOAttribution fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOAttribution fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAttribution eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAttribution ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAttribution fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
