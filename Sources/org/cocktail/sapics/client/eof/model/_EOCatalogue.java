// _EOCatalogue.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCatalogue.java instead.
package org.cocktail.sapics.client.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCatalogue extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Catalogue";
	public static final String ENTITY_TABLE_NAME = "JEFY_CATALOGUE.CATALOGUE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "catId";

	public static final String CAT_COMMENTAIRE_KEY = "catCommentaire";
	public static final String CAT_DATE_DEBUT_KEY = "catDateDebut";
	public static final String CAT_DATE_FIN_KEY = "catDateFin";
	public static final String CAT_LIBELLE_KEY = "catLibelle";

// Attributs non visibles
	public static final String CAT_ID_KEY = "catId";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String CAT_COMMENTAIRE_COLKEY = "CAT_COMMENTAIRE";
	public static final String CAT_DATE_DEBUT_COLKEY = "CAT_DATE_DEBUT";
	public static final String CAT_DATE_FIN_COLKEY = "CAT_DATE_FIN";
	public static final String CAT_LIBELLE_COLKEY = "CAT_LIBELLE";

	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String FOURNIS_KEY = "fournis";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public String catCommentaire() {
    return (String) storedValueForKey(CAT_COMMENTAIRE_KEY);
  }

  public void setCatCommentaire(String value) {
    takeStoredValueForKey(value, CAT_COMMENTAIRE_KEY);
  }

  public NSTimestamp catDateDebut() {
    return (NSTimestamp) storedValueForKey(CAT_DATE_DEBUT_KEY);
  }

  public void setCatDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, CAT_DATE_DEBUT_KEY);
  }

  public NSTimestamp catDateFin() {
    return (NSTimestamp) storedValueForKey(CAT_DATE_FIN_KEY);
  }

  public void setCatDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, CAT_DATE_FIN_KEY);
  }

  public String catLibelle() {
    return (String) storedValueForKey(CAT_LIBELLE_KEY);
  }

  public void setCatLibelle(String value) {
    takeStoredValueForKey(value, CAT_LIBELLE_KEY);
  }

  public org.cocktail.sapics.client.eof.model.EOFournis fournis() {
    return (org.cocktail.sapics.client.eof.model.EOFournis)storedValueForKey(FOURNIS_KEY);
  }

  public void setFournisRelationship(org.cocktail.sapics.client.eof.model.EOFournis value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeApplication typeApplication() {
    return (org.cocktail.application.client.eof.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.application.client.eof.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOTypeEtat typeEtat() {
    return (org.cocktail.sapics.client.eof.model.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.sapics.client.eof.model.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  

  public static EOCatalogue createCatalogue(EOEditingContext editingContext, NSTimestamp catDateDebut
, String catLibelle
, org.cocktail.application.client.eof.EOTypeApplication typeApplication, org.cocktail.sapics.client.eof.model.EOTypeEtat typeEtat) {
    EOCatalogue eo = (EOCatalogue) createAndInsertInstance(editingContext, _EOCatalogue.ENTITY_NAME);    
		eo.setCatDateDebut(catDateDebut);
		eo.setCatLibelle(catLibelle);
    eo.setTypeApplicationRelationship(typeApplication);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCatalogue.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCatalogue.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCatalogue localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCatalogue)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCatalogue localInstanceIn(EOEditingContext editingContext, EOCatalogue eo) {
    EOCatalogue localInstance = (eo == null) ? null : (EOCatalogue)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCatalogue#localInstanceIn a la place.
   */
	public static EOCatalogue localInstanceOf(EOEditingContext editingContext, EOCatalogue eo) {
		return EOCatalogue.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCatalogue fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCatalogue fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCatalogue eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCatalogue)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCatalogue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCatalogue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCatalogue eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCatalogue)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCatalogue fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCatalogue eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCatalogue ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCatalogue fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
