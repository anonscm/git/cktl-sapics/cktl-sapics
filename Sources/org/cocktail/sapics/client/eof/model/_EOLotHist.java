// _EOLotHist.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLotHist.java instead.
package org.cocktail.sapics.client.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOLotHist extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "LotHist";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.LOT_HIST";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "lhOrdre";

	public static final String LH_CATALOGUE_KEY = "lhCatalogue";
	public static final String LH_DATE_KEY = "lhDate";
	public static final String LH_DEBUT_KEY = "lhDebut";
	public static final String LH_FIN_KEY = "lhFin";
	public static final String LH_HT_KEY = "lhHT";
	public static final String LH_INDEX_KEY = "lhIndex";
	public static final String LH_LIBELLE_KEY = "lhLibelle";
	public static final String LH_MONNAIE_KEY = "lhMonnaie";
	public static final String LH_SUPPR_KEY = "lhSuppr";
	public static final String LH_VALIDE_KEY = "lhValide";

// Attributs non visibles
	public static final String LH_ORDRE_KEY = "lhOrdre";
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String MAR_ORDRE_KEY = "marOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String LH_CATALOGUE_COLKEY = "LH_CATALOGUE";
	public static final String LH_DATE_COLKEY = "LH_DATE";
	public static final String LH_DEBUT_COLKEY = "LH_DEBUT";
	public static final String LH_FIN_COLKEY = "LH_FIN";
	public static final String LH_HT_COLKEY = "LH_HT";
	public static final String LH_INDEX_COLKEY = "LH_INDEX";
	public static final String LH_LIBELLE_COLKEY = "LH_LIBELLE";
	public static final String LH_MONNAIE_COLKEY = "LH_MONNAIE";
	public static final String LH_SUPPR_COLKEY = "LH_SUPPR";
	public static final String LH_VALIDE_COLKEY = "LH_VALIDE";

	public static final String LH_ORDRE_COLKEY = "LH_ORDRE";
	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String MAR_ORDRE_COLKEY = "MAR_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String LOT_KEY = "lot";
	public static final String MARCHE_KEY = "marche";



	// Accessors methods
  public String lhCatalogue() {
    return (String) storedValueForKey(LH_CATALOGUE_KEY);
  }

  public void setLhCatalogue(String value) {
    takeStoredValueForKey(value, LH_CATALOGUE_KEY);
  }

  public NSTimestamp lhDate() {
    return (NSTimestamp) storedValueForKey(LH_DATE_KEY);
  }

  public void setLhDate(NSTimestamp value) {
    takeStoredValueForKey(value, LH_DATE_KEY);
  }

  public NSTimestamp lhDebut() {
    return (NSTimestamp) storedValueForKey(LH_DEBUT_KEY);
  }

  public void setLhDebut(NSTimestamp value) {
    takeStoredValueForKey(value, LH_DEBUT_KEY);
  }

  public NSTimestamp lhFin() {
    return (NSTimestamp) storedValueForKey(LH_FIN_KEY);
  }

  public void setLhFin(NSTimestamp value) {
    takeStoredValueForKey(value, LH_FIN_KEY);
  }

  public java.math.BigDecimal lhHT() {
    return (java.math.BigDecimal) storedValueForKey(LH_HT_KEY);
  }

  public void setLhHT(java.math.BigDecimal value) {
    takeStoredValueForKey(value, LH_HT_KEY);
  }

  public String lhIndex() {
    return (String) storedValueForKey(LH_INDEX_KEY);
  }

  public void setLhIndex(String value) {
    takeStoredValueForKey(value, LH_INDEX_KEY);
  }

  public String lhLibelle() {
    return (String) storedValueForKey(LH_LIBELLE_KEY);
  }

  public void setLhLibelle(String value) {
    takeStoredValueForKey(value, LH_LIBELLE_KEY);
  }

  public String lhMonnaie() {
    return (String) storedValueForKey(LH_MONNAIE_KEY);
  }

  public void setLhMonnaie(String value) {
    takeStoredValueForKey(value, LH_MONNAIE_KEY);
  }

  public String lhSuppr() {
    return (String) storedValueForKey(LH_SUPPR_KEY);
  }

  public void setLhSuppr(String value) {
    takeStoredValueForKey(value, LH_SUPPR_KEY);
  }

  public String lhValide() {
    return (String) storedValueForKey(LH_VALIDE_KEY);
  }

  public void setLhValide(String value) {
    takeStoredValueForKey(value, LH_VALIDE_KEY);
  }

  public org.cocktail.sapics.client.eof.model.EOLot lot() {
    return (org.cocktail.sapics.client.eof.model.EOLot)storedValueForKey(LOT_KEY);
  }

  public void setLotRelationship(org.cocktail.sapics.client.eof.model.EOLot value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOLot oldValue = lot();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOMarche marche() {
    return (org.cocktail.sapics.client.eof.model.EOMarche)storedValueForKey(MARCHE_KEY);
  }

  public void setMarcheRelationship(org.cocktail.sapics.client.eof.model.EOMarche value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOMarche oldValue = marche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MARCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MARCHE_KEY);
    }
  }
  

  public static EOLotHist createLotHist(EOEditingContext editingContext, org.cocktail.sapics.client.eof.model.EOLot lot, org.cocktail.sapics.client.eof.model.EOMarche marche) {
    EOLotHist eo = (EOLotHist) createAndInsertInstance(editingContext, _EOLotHist.ENTITY_NAME);    
    eo.setLotRelationship(lot);
    eo.setMarcheRelationship(marche);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOLotHist.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOLotHist.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOLotHist localInstanceIn(EOEditingContext editingContext) {
	  		return (EOLotHist)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOLotHist localInstanceIn(EOEditingContext editingContext, EOLotHist eo) {
    EOLotHist localInstance = (eo == null) ? null : (EOLotHist)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOLotHist#localInstanceIn a la place.
   */
	public static EOLotHist localInstanceOf(EOEditingContext editingContext, EOLotHist eo) {
		return EOLotHist.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOLotHist fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOLotHist fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLotHist eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLotHist)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLotHist fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLotHist fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLotHist eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLotHist)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOLotHist fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLotHist eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLotHist ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLotHist fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
