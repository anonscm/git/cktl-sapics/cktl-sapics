// _EOMapaTrancheExer.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMapaTrancheExer.java instead.
package org.cocktail.sapics.client.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMapaTrancheExer extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "MapaTrancheExer";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.MAPA_TRANCHE_EXER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "mteId";

	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String DATE_VOTE_CA_KEY = "dateVoteCA";
	public static final String MTE_ID_KEY = "mteId";
	public static final String MTE_LIBELLE_KEY = "mteLibelle";
	public static final String MTE_MAX_KEY = "mteMax";
	public static final String MTE_MIN_KEY = "mteMin";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TCN_ID_KEY = "tcnId";

//Colonnes dans la base de donnees
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String DATE_VOTE_CA_COLKEY = "DATE_VOTE_CA";
	public static final String MTE_ID_COLKEY = "MTE_ID";
	public static final String MTE_LIBELLE_COLKEY = "MTE_LIBELLE";
	public static final String MTE_MAX_COLKEY = "MTE_MAX";
	public static final String MTE_MIN_COLKEY = "MTE_MIN";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TCN_ID_COLKEY = "TCN_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_CN_KEY = "typeCN";



	// Accessors methods
  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public NSTimestamp dateVoteCA() {
    return (NSTimestamp) storedValueForKey(DATE_VOTE_CA_KEY);
  }

  public void setDateVoteCA(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_VOTE_CA_KEY);
  }

  public Integer mteId() {
    return (Integer) storedValueForKey(MTE_ID_KEY);
  }

  public void setMteId(Integer value) {
    takeStoredValueForKey(value, MTE_ID_KEY);
  }

  public String mteLibelle() {
    return (String) storedValueForKey(MTE_LIBELLE_KEY);
  }

  public void setMteLibelle(String value) {
    takeStoredValueForKey(value, MTE_LIBELLE_KEY);
  }

  public java.math.BigDecimal mteMax() {
    return (java.math.BigDecimal) storedValueForKey(MTE_MAX_KEY);
  }

  public void setMteMax(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MTE_MAX_KEY);
  }

  public java.math.BigDecimal mteMin() {
    return (java.math.BigDecimal) storedValueForKey(MTE_MIN_KEY);
  }

  public void setMteMin(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MTE_MIN_KEY);
  }

  public org.cocktail.sapics.client.eof.model.EOExercice exercice() {
    return (org.cocktail.sapics.client.eof.model.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.sapics.client.eof.model.EOExercice value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOTypeCn typeCN() {
    return (org.cocktail.sapics.client.eof.model.EOTypeCn)storedValueForKey(TYPE_CN_KEY);
  }

  public void setTypeCNRelationship(org.cocktail.sapics.client.eof.model.EOTypeCn value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOTypeCn oldValue = typeCN();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CN_KEY);
    }
  }
  

  public static EOMapaTrancheExer createMapaTrancheExer(EOEditingContext editingContext, NSTimestamp dateVoteCA
, Integer mteId
, org.cocktail.sapics.client.eof.model.EOExercice exercice, org.cocktail.sapics.client.eof.model.EOTypeCn typeCN) {
    EOMapaTrancheExer eo = (EOMapaTrancheExer) createAndInsertInstance(editingContext, _EOMapaTrancheExer.ENTITY_NAME);    
		eo.setDateVoteCA(dateVoteCA);
		eo.setMteId(mteId);
    eo.setExerciceRelationship(exercice);
    eo.setTypeCNRelationship(typeCN);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOMapaTrancheExer.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOMapaTrancheExer.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOMapaTrancheExer localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMapaTrancheExer)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOMapaTrancheExer localInstanceIn(EOEditingContext editingContext, EOMapaTrancheExer eo) {
    EOMapaTrancheExer localInstance = (eo == null) ? null : (EOMapaTrancheExer)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOMapaTrancheExer#localInstanceIn a la place.
   */
	public static EOMapaTrancheExer localInstanceOf(EOEditingContext editingContext, EOMapaTrancheExer eo) {
		return EOMapaTrancheExer.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOMapaTrancheExer fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOMapaTrancheExer fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMapaTrancheExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMapaTrancheExer)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMapaTrancheExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMapaTrancheExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMapaTrancheExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMapaTrancheExer)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOMapaTrancheExer fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMapaTrancheExer eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMapaTrancheExer ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMapaTrancheExer fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
