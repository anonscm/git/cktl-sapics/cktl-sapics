// _EOSeuil.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSeuil.java instead.
package org.cocktail.sapics.client.eof.model;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOSeuil extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Seuil";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.SEUIL";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "seuilOrdre";

	public static final String DATE_EFFET_DEB_KEY = "dateEffetDeb";
	public static final String DATE_EFFET_FIN_KEY = "dateEffetFin";
	public static final String MONTANT_KEY = "montant";

// Attributs non visibles
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String SEUIL_ORDRE_KEY = "seuilOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_EFFET_DEB_COLKEY = "DATEEFFETDEB";
	public static final String DATE_EFFET_FIN_COLKEY = "DATEEFFETFIN";
	public static final String MONTANT_COLKEY = "MONTANT";

	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String SEUIL_ORDRE_COLKEY = "SEUILORDRE";


	// Relationships
	public static final String CODEMARCHE_KEY = "codemarche";



	// Accessors methods
  public NSTimestamp dateEffetDeb() {
    return (NSTimestamp) storedValueForKey(DATE_EFFET_DEB_KEY);
  }

  public void setDateEffetDeb(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_EFFET_DEB_KEY);
  }

  public NSTimestamp dateEffetFin() {
    return (NSTimestamp) storedValueForKey(DATE_EFFET_FIN_KEY);
  }

  public void setDateEffetFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_EFFET_FIN_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public org.cocktail.sapics.client.eof.model.EOCodeMarche codemarche() {
    return (org.cocktail.sapics.client.eof.model.EOCodeMarche)storedValueForKey(CODEMARCHE_KEY);
  }

  public void setCodemarcheRelationship(org.cocktail.sapics.client.eof.model.EOCodeMarche value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOCodeMarche oldValue = codemarche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODEMARCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODEMARCHE_KEY);
    }
  }
  

  public static EOSeuil createSeuil(EOEditingContext editingContext, NSTimestamp dateEffetDeb
) {
    EOSeuil eo = (EOSeuil) createAndInsertInstance(editingContext, _EOSeuil.ENTITY_NAME);    
		eo.setDateEffetDeb(dateEffetDeb);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSeuil.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSeuil.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOSeuil localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSeuil)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSeuil localInstanceIn(EOEditingContext editingContext, EOSeuil eo) {
    EOSeuil localInstance = (eo == null) ? null : (EOSeuil)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSeuil#localInstanceIn a la place.
   */
	public static EOSeuil localInstanceOf(EOEditingContext editingContext, EOSeuil eo) {
		return EOSeuil.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSeuil fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSeuil fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSeuil eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSeuil)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSeuil fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSeuil fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSeuil eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSeuil)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSeuil fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSeuil eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSeuil ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSeuil fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
