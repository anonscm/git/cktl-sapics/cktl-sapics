// _EOVDetailAttGlobale.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVDetailAttGlobale.java instead.
package org.cocktail.sapics.client.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVDetailAttGlobale extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "VDetailAttGlobale";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.V_DETAIL_ATT_GLOBALE";



	// Attributes


	public static final String AEE_ENG_HT_KEY = "aeeEngHt";
	public static final String AEE_EXECUTION_KEY = "aeeExecution";
	public static final String AEE_LIQ_HT_KEY = "aeeLiqHt";
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String CDE_LIB_KEY = "cdeLib";
	public static final String CDE_ORDRE_KEY = "cdeOrdre";
	public static final String DATE_ENG_KEY = "dateEng";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String AEE_ENG_HT_COLKEY = "AEE_ENG_HT";
	public static final String AEE_EXECUTION_COLKEY = "AEE_EXECUTION";
	public static final String AEE_LIQ_HT_COLKEY = "AEE_LIQ_HT";
	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String CDE_LIB_COLKEY = "CDE_LIB";
	public static final String CDE_ORDRE_COLKEY = "CDE_ORDRE";
	public static final String DATE_ENG_COLKEY = "DATE_ENG";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";



	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public java.math.BigDecimal aeeEngHt() {
    return (java.math.BigDecimal) storedValueForKey(AEE_ENG_HT_KEY);
  }

  public void setAeeEngHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AEE_ENG_HT_KEY);
  }

  public java.math.BigDecimal aeeExecution() {
    return (java.math.BigDecimal) storedValueForKey(AEE_EXECUTION_KEY);
  }

  public void setAeeExecution(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AEE_EXECUTION_KEY);
  }

  public java.math.BigDecimal aeeLiqHt() {
    return (java.math.BigDecimal) storedValueForKey(AEE_LIQ_HT_KEY);
  }

  public void setAeeLiqHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AEE_LIQ_HT_KEY);
  }

  public Integer attOrdre() {
    return (Integer) storedValueForKey(ATT_ORDRE_KEY);
  }

  public void setAttOrdre(Integer value) {
    takeStoredValueForKey(value, ATT_ORDRE_KEY);
  }

  public String cdeLib() {
    return (String) storedValueForKey(CDE_LIB_KEY);
  }

  public void setCdeLib(String value) {
    takeStoredValueForKey(value, CDE_LIB_KEY);
  }

  public Integer cdeOrdre() {
    return (Integer) storedValueForKey(CDE_ORDRE_KEY);
  }

  public void setCdeOrdre(Integer value) {
    takeStoredValueForKey(value, CDE_ORDRE_KEY);
  }

  public NSTimestamp dateEng() {
    return (NSTimestamp) storedValueForKey(DATE_ENG_KEY);
  }

  public void setDateEng(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ENG_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public org.cocktail.sapics.client.eof.model.EOAttribution attribution() {
    return (org.cocktail.sapics.client.eof.model.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
  }

  public void setAttributionRelationship(org.cocktail.sapics.client.eof.model.EOAttribution value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOAttribution oldValue = attribution();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
    }
  }
  
  public org.cocktail.sapics.client.eof.model.EOExercice exercice() {
    return (org.cocktail.sapics.client.eof.model.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.sapics.client.eof.model.EOExercice value) {
    if (value == null) {
    	org.cocktail.sapics.client.eof.model.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

  public static EOVDetailAttGlobale createVDetailAttGlobale(EOEditingContext editingContext, java.math.BigDecimal aeeEngHt
, java.math.BigDecimal aeeExecution
, java.math.BigDecimal aeeLiqHt
, Integer attOrdre
, String cdeLib
, Integer cdeOrdre
, NSTimestamp dateEng
, Integer exeOrdre
, org.cocktail.sapics.client.eof.model.EOAttribution attribution, org.cocktail.sapics.client.eof.model.EOExercice exercice) {
    EOVDetailAttGlobale eo = (EOVDetailAttGlobale) createAndInsertInstance(editingContext, _EOVDetailAttGlobale.ENTITY_NAME);    
		eo.setAeeEngHt(aeeEngHt);
		eo.setAeeExecution(aeeExecution);
		eo.setAeeLiqHt(aeeLiqHt);
		eo.setAttOrdre(attOrdre);
		eo.setCdeLib(cdeLib);
		eo.setCdeOrdre(cdeOrdre);
		eo.setDateEng(dateEng);
		eo.setExeOrdre(exeOrdre);
    eo.setAttributionRelationship(attribution);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVDetailAttGlobale.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVDetailAttGlobale.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOVDetailAttGlobale localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVDetailAttGlobale)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVDetailAttGlobale localInstanceIn(EOEditingContext editingContext, EOVDetailAttGlobale eo) {
    EOVDetailAttGlobale localInstance = (eo == null) ? null : (EOVDetailAttGlobale)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVDetailAttGlobale#localInstanceIn a la place.
   */
	public static EOVDetailAttGlobale localInstanceOf(EOEditingContext editingContext, EOVDetailAttGlobale eo) {
		return EOVDetailAttGlobale.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVDetailAttGlobale fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVDetailAttGlobale fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVDetailAttGlobale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVDetailAttGlobale)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVDetailAttGlobale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVDetailAttGlobale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVDetailAttGlobale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVDetailAttGlobale)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVDetailAttGlobale fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVDetailAttGlobale eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVDetailAttGlobale ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVDetailAttGlobale fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
