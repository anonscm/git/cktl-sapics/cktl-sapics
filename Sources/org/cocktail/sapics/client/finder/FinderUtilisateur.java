package org.cocktail.sapics.client.finder;

import org.cocktail.sapics.client.eof.model.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderUtilisateur {

	
	public static NSArray findForQualifier(EOEditingContext ec, EOQualifier qualifier)	{

		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateur.ENTITY_NAME,qualifier,null);
		return ec.objectsWithFetchSpecification(fs);
	}

	
	
	public static EOUtilisateur findForPersId(EOEditingContext ec, Number persId)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.PERS_ID_KEY + " = %@", new NSArray(persId)));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateur.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		return (EOUtilisateur)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		
	}
	
}
