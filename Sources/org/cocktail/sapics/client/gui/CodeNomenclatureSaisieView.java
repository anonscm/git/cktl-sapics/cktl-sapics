/*
 * TemplateJDialog.java
 *
 * Created on 1 octobre 2008, 08:26
 */

package org.cocktail.sapics.client.gui;

import org.cocktail.sapics.client.utilities.CocktailConstantes;

import com.webobjects.foundation.NSArray;

/**
 *
 * @author  cpinsard
 */
public class CodeNomenclatureSaisieView extends javax.swing.JDialog {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -4458916728144792149L;






	/** Creates new form TemplateJDialog */
    public CodeNomenclatureSaisieView(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAnnuler = new javax.swing.JButton();
        btnValider = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaDetail = new javax.swing.JTextArea();
        jLabel16 = new javax.swing.JLabel();
        typeCode = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        tfMontant = new javax.swing.JTextField();
        tfTitre = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        tfLibelle = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        checkRecherche = new javax.swing.JCheckBox();
        check3Cmp = new javax.swing.JCheckBox();
        checkMonopole = new javax.swing.JCheckBox();
        checkAutres = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        tfDomaine = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        tfFamille = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Saisie/Modification d'un Code Nomenclature");
        setMinimumSize(new java.awt.Dimension(550, 580));

        btnAnnuler.setText("Annuler");

        btnValider.setText("Valider");

        areaDetail.setColumns(15);
        areaDetail.setLineWrap(true);
        areaDetail.setRows(5);
        jScrollPane1.setViewportView(areaDetail);

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Type code");

        typeCode.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Montant");

        tfMontant.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tfMontant.setPreferredSize(new java.awt.Dimension(96, 28));

        tfTitre.setEditable(false);
        tfTitre.setBackground(new java.awt.Color(153, 153, 255));
        tfTitre.setForeground(new java.awt.Color(255, 255, 255));
        tfTitre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitre.setText("Saisie d'un nouveau code nomenclature");
        tfTitre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfTitreActionPerformed(evt);
            }
        });

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Libellé");

        tfLibelle.setMaximumSize(new java.awt.Dimension(413, 28));
        tfLibelle.setPreferredSize(new java.awt.Dimension(413, 28));
        tfLibelle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfLibelleActionPerformed(evt);
            }
        });

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel22.setText("Détail");

        checkRecherche.setText("Recherche");

        check3Cmp.setText("3 CMP");

        checkMonopole.setText("Monopole");

        checkAutres.setText("Autres");

        jPanel2.setMaximumSize(new java.awt.Dimension(100, 70));
        jPanel2.setMinimumSize(new java.awt.Dimension(100, 70));

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("Domaine");

        tfDomaine.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDomaine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDomaineActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tfDomaine, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel18)))
        );

        jPanel2Layout.linkSize(new java.awt.Component[] {jLabel18, tfDomaine}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel18)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tfDomaine, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel2);

        jPanel3.setMaximumSize(new java.awt.Dimension(100, 70));
        jPanel3.setMinimumSize(new java.awt.Dimension(100, 70));

        tfFamille.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfFamille.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfFamilleActionPerformed(evt);
            }
        });

        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("Famille");
        jLabel19.setMaximumSize(new java.awt.Dimension(56, 16));
        jLabel19.setMinimumSize(new java.awt.Dimension(56, 16));
        jLabel19.setPreferredSize(new java.awt.Dimension(56, 16));

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFamille, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tfFamille, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0))
        );

        jPanel1.add(jPanel3);

        jLabel1.setText("Type achat");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(tfTitre)
            .add(layout.createSequentialGroup()
                .add(6, 6, 6)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel21)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel22)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel17)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel1)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel16))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(typeCode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 181, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfMontant, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(checkRecherche)
                        .add(18, 18, 18)
                        .add(check3Cmp)
                        .add(18, 18, 18)
                        .add(checkMonopole)
                        .add(18, 18, 18)
                        .add(checkAutres))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(layout.createSequentialGroup()
                            .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(jScrollPane1)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, tfLibelle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(28, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(tfTitre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel21)
                    .add(tfLibelle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel16)
                    .add(typeCode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel17)
                    .add(tfMontant, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(checkRecherche)
                    .add(check3Cmp)
                    .add(checkMonopole)
                    .add(checkAutres)
                    .add(jLabel1))
                .add(21, 21, 21)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel22)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnValider)
                    .add(btnAnnuler))
                .addContainerGap())
        );

        pack();
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        java.awt.Dimension dialogSize = getSize();
        setLocation((screenSize.width-dialogSize.width)/2,(screenSize.height-dialogSize.height)/2);
    }// </editor-fold>//GEN-END:initComponents

    private void tfTitreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfTitreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfTitreActionPerformed

    private void tfDomaineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfDomaineActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfDomaineActionPerformed

    private void tfFamilleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfFamilleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfFamilleActionPerformed

    private void tfLibelleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfLibelleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfLibelleActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	MarchesSaisieView dialog = new MarchesSaisieView(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
	public void setTypesCodes (NSArray liste) {

		typeCode.removeAllItems();
		for (int i=0;i<liste.count();i++)
			typeCode.addItem(liste.objectAtIndex(i));
	}


    public javax.swing.JTextArea getAreaDetail() {
		return areaDetail;
	}

	public void setAreaDetail(javax.swing.JTextArea areaDetail) {
		this.areaDetail = areaDetail;
	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public javax.swing.JButton getBtnValider() {
		return btnValider;
	}

	public void setBtnValider(javax.swing.JButton btnValider) {
		this.btnValider = btnValider;
	}

	public javax.swing.JCheckBox getCheck3Cmp() {
		return check3Cmp;
	}

	public void setCheck3Cmp(javax.swing.JCheckBox check3Cmp) {
		this.check3Cmp = check3Cmp;
	}

	public javax.swing.JCheckBox getCheckAutres() {
		return checkAutres;
	}

	public void setCheckAutres(javax.swing.JCheckBox checkAutres) {
		this.checkAutres = checkAutres;
	}

	public javax.swing.JCheckBox getCheckMonopole() {
		return checkMonopole;
	}

	public void setCheckMonopole(javax.swing.JCheckBox checkMonopole) {
		this.checkMonopole = checkMonopole;
	}

	public javax.swing.JCheckBox getCheckRecherche() {
		return checkRecherche;
	}

	public void setCheckRecherche(javax.swing.JCheckBox checkRecherche) {
		this.checkRecherche = checkRecherche;
	}

	public javax.swing.JTextField getTfDomaine() {
		return tfDomaine;
	}

	public void setTfDomaine(javax.swing.JTextField tfDomaine) {
		this.tfDomaine = tfDomaine;
	}

	public javax.swing.JTextField getTfFamille() {
		return tfFamille;
	}

	public void setTfFamille(javax.swing.JTextField tfFamille) {
		this.tfFamille = tfFamille;
	}

	public javax.swing.JTextField getTfLibelle() {
		return tfLibelle;
	}

	public void setTfLibelle(javax.swing.JTextField tfLibelle) {
		this.tfLibelle = tfLibelle;
	}

	public javax.swing.JTextField getTfMontant() {
		return tfMontant;
	}

	public void setTfMontant(javax.swing.JTextField tfMontant) {
		this.tfMontant = tfMontant;
	}

	public javax.swing.JTextField getTfTitre() {
		return tfTitre;
	}

	public void setTfTitre(javax.swing.JTextField tfTitre) {
		this.tfTitre = tfTitre;
	}

	public javax.swing.JComboBox getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(javax.swing.JComboBox typeCode) {
		this.typeCode = typeCode;
	}



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaDetail;
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnValider;
    private javax.swing.JCheckBox check3Cmp;
    private javax.swing.JCheckBox checkAutres;
    private javax.swing.JCheckBox checkMonopole;
    private javax.swing.JCheckBox checkRecherche;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField tfDomaine;
    private javax.swing.JTextField tfFamille;
    private javax.swing.JTextField tfLibelle;
    private javax.swing.JTextField tfMontant;
    private javax.swing.JTextField tfTitre;
    private javax.swing.JComboBox typeCode;
    // End of variables declaration//GEN-END:variables
    
    
    private void initGui() {
            	
    	btnValider.setIcon(CocktailConstantes.ICON_VALID);
        btnAnnuler.setIcon(CocktailConstantes.ICON_CANCEL);
		
    }

}
