/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sapics.client.gui;

import javax.swing.JDialog;

import org.cocktail.sapics.client.utilities.CocktailConstantes;

import com.webobjects.foundation.NSArray;

/**
 *
 * @author  cpinsard
 */
public class ImportArticleView extends javax.swing.JDialog {

     	/**
	 * 
	 */
	private static final long serialVersionUID = -5018356734761178648L;

    /** Creates new form ImportKXView */
    public ImportArticleView(JDialog parent, boolean modal) {
        
        super(parent, modal);
        
        initComponents();
        
        initGui();
    }

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfFileName = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnGetFile = new javax.swing.JButton();
        btnImport = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        codesNomenclature = new javax.swing.JComboBox();
        btnCancel = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        tfCatalogue = new javax.swing.JTextField();
        devises = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        popupTva = new javax.swing.JComboBox();
        tfGarantie = new javax.swing.JTextField();
        tfLivraison = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        popupColPrix = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        popupColLibelle = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        popupColReference = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        popupColTva = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        popupCollCodeNomenclature = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Import d'articles");

        tfFileName.setEditable(false);
        tfFileName.setEnabled(false);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Fichier d'articles/options à importer :");

        btnGetFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGetFileActionPerformed(evt);
            }
        });

        btnImport.setText("Importer");
        btnImport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportActionPerformed(evt);
            }
        });

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Code Nomenclature :");

        codesNomenclature.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnCancel.setText("Annuler");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("CATALOGUE :");

        tfCatalogue.setEditable(false);
        tfCatalogue.setEnabled(false);

        devises.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Devise :");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("TVA :");

        popupTva.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        tfLivraison.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfLivraisonActionPerformed(evt);
            }
        });

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Garantie :");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Livraison :");

        jLabel4.setText("Emplacement des données dans le fichier : ");

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Prix : ");

        popupColPrix.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Libellé :");

        popupColLibelle.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Référence :");

        popupColReference.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        popupColTva.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("TVA");

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("C.N.");

        popupCollCodeNomenclature.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jSeparator1)
                    .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                                    .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 173, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(tfCatalogue))
                                .add(layout.createSequentialGroup()
                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(jLabel7)
                                        .add(jLabel1)
                                        .add(jLabel5)
                                        .add(jLabel6))
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(layout.createSequentialGroup()
                                            .add(1, 1, 1)
                                            .add(devises, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                            .add(layout.createSequentialGroup()
                                                .add(tfFileName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 335, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                .add(btnGetFile, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                            .add(layout.createSequentialGroup()
                                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                    .add(layout.createSequentialGroup()
                                                        .add(popupTva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .add(jLabel8))
                                                    .add(layout.createSequentialGroup()
                                                        .add(codesNomenclature, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .add(jLabel3)))
                                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                                    .add(tfLivraison)
                                                    .add(tfGarantie, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)))))))
                            .add(layout.createSequentialGroup()
                                .add(jLabel10)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(popupColLibelle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jLabel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 69, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(popupColReference, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jLabel9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 36, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(popupColPrix, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jLabel12)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(popupColTva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jLabel13)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(popupCollCodeNomenclature, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 70, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                                .add(btnCancel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 101, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnImport, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 101, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel2)
                    .add(tfCatalogue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel1)
                    .add(tfFileName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnGetFile, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(popupTva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel7)
                    .add(tfGarantie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel8))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel5)
                    .add(codesNomenclature, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel3)
                    .add(tfLivraison, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(devises, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel4)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel10)
                    .add(popupColLibelle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel11)
                    .add(popupColReference, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel9)
                    .add(popupColPrix, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel12)
                    .add(popupColTva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel13)
                    .add(popupCollCodeNomenclature, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnImport)
                    .add(btnCancel))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-646)/2, (screenSize.height-383)/2, 646, 383);
    }// </editor-fold>//GEN-END:initComponents

private void btnGetFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGetFileActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_btnGetFileActionPerformed

private void btnImportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportActionPerformed

    dispose();
    // TODO add your handling code here:
}//GEN-LAST:event_btnImportActionPerformed

private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
    // TODO add your handling code here:
}//GEN-LAST:event_btnCancelActionPerformed

private void tfLivraisonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfLivraisonActionPerformed
    // TODO add your handling code here:
}//GEN-LAST:event_tfLivraisonActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ImportArticleView dialog = new ImportArticleView(new javax.swing.JDialog(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    protected javax.swing.JButton btnGetFile;
    private javax.swing.JButton btnImport;
    private javax.swing.JComboBox codesNomenclature;
    private javax.swing.JComboBox devises;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JComboBox popupColLibelle;
    private javax.swing.JComboBox popupColPrix;
    private javax.swing.JComboBox popupColReference;
    private javax.swing.JComboBox popupColTva;
    private javax.swing.JComboBox popupCollCodeNomenclature;
    private javax.swing.JComboBox popupTva;
    protected javax.swing.JTextField tfCatalogue;
    protected javax.swing.JTextField tfFileName;
    private javax.swing.JTextField tfGarantie;
    private javax.swing.JTextField tfLivraison;
    // End of variables declaration//GEN-END:variables

    
	public void setTvas(NSArray liste) {

		popupTva.removeAllItems();
		//popupTva.addItem("Dans le fichier");
		for (int i=0;i<liste.count();i++)
			popupTva.addItem(liste.objectAtIndex(i));
		
	}

	public void setNomenclatures(NSArray liste) {

		codesNomenclature.removeAllItems();
		for (int i=0;i<liste.count();i++)
			codesNomenclature.addItem(liste.objectAtIndex(i));
		
	}

    private void initGui() {
        
        btnImport.setIcon(CocktailConstantes.ICON_DOWNLOAD_16);
        btnGetFile.setIcon(CocktailConstantes.ICON_SELECT_16);
        btnCancel.setIcon(CocktailConstantes.ICON_CANCEL);

        popupColLibelle.removeAllItems();
        popupColPrix.removeAllItems();
        popupColReference.removeAllItems();
        popupColTva.removeAllItems();
        popupCollCodeNomenclature.removeAllItems();
        
        popupColTva.addItem("*");
        popupCollCodeNomenclature.addItem("*");
        
        for (int i=0;i<10;i++) {
            popupColLibelle.addItem(String.valueOf(i));
            popupColReference.addItem(String.valueOf(i));
            popupColPrix.addItem(String.valueOf(i));      	
            popupColTva.addItem(String.valueOf(i));
            popupCollCodeNomenclature.addItem(String.valueOf(i));
        }
        
    }

	public javax.swing.JButton getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(javax.swing.JButton btnCancel) {
		this.btnCancel = btnCancel;
	}

	
	
	public javax.swing.JTextField getTfCatalogue() {
		return tfCatalogue;
	}

	public void setTfCatalogue(javax.swing.JTextField tfCatalogue) {
		this.tfCatalogue = tfCatalogue;
	}

	public javax.swing.JTextField getTfFileName() {
		return tfFileName;
	}

	public void setTfFileName(javax.swing.JTextField tfFileName) {
		this.tfFileName = tfFileName;
	}

	public javax.swing.JButton getBtnGetFile() {
		return btnGetFile;
	}

	public void setBtnGetFile(javax.swing.JButton btnGetFile) {
		this.btnGetFile = btnGetFile;
	}

	public javax.swing.JButton getBtnImport() {
		return btnImport;
	}

	
	
	public javax.swing.JTextField getTfGarantie() {
		return tfGarantie;
	}

	public void setTfGarantie(javax.swing.JTextField tfGarantie) {
		this.tfGarantie = tfGarantie;
	}

	public javax.swing.JTextField getTfLivraison() {
		return tfLivraison;
	}

	public void setTfLivraison(javax.swing.JTextField tfLivraison) {
		this.tfLivraison = tfLivraison;
	}

	public javax.swing.JComboBox getPopupTva() {
		return popupTva;
	}

	public void setPopupTva(javax.swing.JComboBox popupTva) {
		this.popupTva = popupTva;
	}

	public javax.swing.JComboBox getDevises() {
		return devises;
	}

	public void setDevises(javax.swing.JComboBox devises) {
		this.devises = devises;
	}

	public void setBtnImport(javax.swing.JButton btnImport) {
		this.btnImport = btnImport;
	}
	public javax.swing.JComboBox getCodesNomenclature() {
		return codesNomenclature;
	}

	public javax.swing.JComboBox getPopupColLibelle() {
		return popupColLibelle;
	}

	public void setPopupColLibelle(javax.swing.JComboBox popupColLibelle) {
		this.popupColLibelle = popupColLibelle;
	}

	public javax.swing.JComboBox getPopupColPrix() {
		return popupColPrix;
	}

	public void setPopupColPrix(javax.swing.JComboBox popupColPrix) {
		this.popupColPrix = popupColPrix;
	}

	public javax.swing.JComboBox getPopupColTva() {
		return popupColTva;
	}

	public void setPopupColTva(javax.swing.JComboBox popupColTva) {
		this.popupColTva = popupColTva;
	}

	public javax.swing.JComboBox getPopupCollCodeNomenclature() {
		return popupCollCodeNomenclature;
	}

	public void setPopupCollCodeNomenclature(
			javax.swing.JComboBox popupCollCodeNomenclature) {
		this.popupCollCodeNomenclature = popupCollCodeNomenclature;
	}

	public javax.swing.JComboBox getPopupColReference() {
		return popupColReference;
	}

	public void setPopupColReference(javax.swing.JComboBox popupColReference) {
		this.popupColReference = popupColReference;
	}

	public void setCodesNomenclature(javax.swing.JComboBox codesNomenclature) {
		this.codesNomenclature = codesNomenclature;
	}
   
	public void setDevises(NSArray liste) {

		devises.removeAllItems();
		for (int i=0;i<liste.count();i++)
			devises.addItem(liste.objectAtIndex(i));
		
	}
	
	
}
