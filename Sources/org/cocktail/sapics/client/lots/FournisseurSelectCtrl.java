package org.cocktail.sapics.client.lots;

import javax.swing.JFrame;

import org.cocktail.sapics.client.eof.model.EOFournis;
import org.cocktail.sapics.client.gui.FournisseurSelectView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FournisseurSelectCtrl 
{
	private static FournisseurSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	
	private EODisplayGroup eod;
		
	private FournisseurSelectView myView;
	
	/**
	 * 
	 * @param globalEc
	 */
	public FournisseurSelectCtrl (EOEditingContext globalEc)	{
		super();

		ec = globalEc;

		eod = new EODisplayGroup();

		myView = new FournisseurSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getButtonFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfFiltreCode().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfFiltreNom().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});
		
		myView.getMyEOTable().addListener(new ListenerFournis());
		
//		myView.getCheckPersonneMorale().setSelected(true);

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static FournisseurSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new FournisseurSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	/**
	 * Selection d'un engagement
	 */
	public EOFournis getFournisseur()	{
		
		myView.setVisible(true);
		
		if (eod.selectedObjects().count() > 0)
			return (EOFournis) eod.selectedObject();			
		
		return null;
	}

	
	/** 
	 * Recherche de tous les fournisseurs correspondant aux criteres entres
	 */
	public void rechercher()	{
		
		if (StringCtrl.chaineVide(myView.getTfFiltreNom().getText()) && StringCtrl.chaineVide(myView.getTfFiltreCode().getText()))	{
			EODialogs.runInformationDialog("ERREUR","Veuillez saisir un nom ou un code.");
			return;
		}
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_VALIDE_KEY + " = 'O'", null));
/*
		if (!myView.getCheckAllFournis().isSelected()) {
			
			if (myView.getCheckPersonneMorale().isSelected())
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.ADR_CIVILITE_KEY + " = %@", new NSArray("STR")));
			else
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.ADR_CIVILITE_KEY + " != %@", new NSArray("STR")));
		}
*/		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.ADR_CIVILITE_KEY + " = %@", new NSArray("STR")));
		if (!StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.NOM_KEY + " caseInsensitiveLike %@", new NSArray("*" + myView.getTfFiltreNom().getText()+"*")));

		if (!StringCtrl.chaineVide(myView.getTfFiltreCode().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_CODE_KEY + " = %@",new NSArray(myView.getTfFiltreCode().getText().toUpperCase())));

		EOFetchSpecification fs = new EOFetchSpecification(EOFournis.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				
		eod.setObjectArray(ec.objectsWithFetchSpecification(fs));	
		
		myView.getMyEOTable().updateData();
		
	}
		
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}  	
	
	private class ListenerFournis implements ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
						
		}
	}
	
}