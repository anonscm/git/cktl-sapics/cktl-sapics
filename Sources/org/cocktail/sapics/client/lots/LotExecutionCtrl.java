
package org.cocktail.sapics.client.lots;

import javax.swing.JPanel;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.ServerProxy;
import org.cocktail.sapics.client.editions.ReportsJasperCtrl;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOVExecutionGlobale;
import org.cocktail.sapics.client.gui.LotExecutionView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.CRICursor;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LotExecutionCtrl {

	public static LotExecutionCtrl sharedInstance;

	private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
	private EOEditingContext 	ec;

	private LotExecutionView myView;

	private EODisplayGroup eodExecution = new EODisplayGroup();

	private EOVExecutionGlobale currentExecution;
	private EOLot currentLot;


	public LotExecutionCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new LotExecutionView(eodExecution);

		myView.getBtnDetail().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {afficherDetail();	}});

		myView.getBtnImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerExecution();	}});

		myView.getMyEOTable().addListener(new ListenerExecution());

	}


	public static LotExecutionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new LotExecutionCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}



	public void actualiser(EOLot lot) {

		CRICursor.setWaitCursor(myView);

		currentLot = lot;
		eodExecution.setObjectArray(new NSArray());

		if (currentLot != null) {

			eodExecution.setObjectArray(EOVExecutionGlobale.findForLot(ec, currentLot));    	

		}

		myView.getMyEOTable().updateData();

		updateUI();
		
		CRICursor.setDefaultCursor(myView);

	}
	
	
	private void afficherDetail() {
		
		NSApp.setGlassPane(true);
		LotExecutionDetailCtrl.sharedInstance(ec).open(currentExecution);
		NSApp.setGlassPane(false);
	}
	
	private void imprimerExecution() {
	
		NSMutableDictionary parametres = new NSMutableDictionary();			
		
		Integer lotOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentLot)).objectForKey("lotOrdre");
		Integer marOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentLot.marche())).objectForKey("marOrdre");
		
		parametres.setObjectForKey(lotOrdre, "LOTORDRE");
		parametres.setObjectForKey(marOrdre, "MARORDRE");
		
		ReportsJasperCtrl.sharedInstance(ec).printSapicsExecutionLot(parametres);
		
	}
	
	private void updateUI() {
		
		myView.getBtnDetail().setEnabled(currentLot != null && currentExecution != null);
		myView.getBtnImprimer().setEnabled(currentLot != null);
		
	}

	private class ListenerExecution implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		 public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		 public void onSelectionChanged() {
			 
			 currentExecution = (EOVExecutionGlobale)eodExecution.selectedObject();
			
		 }
	}
}
