package org.cocktail.sapics.client.lots;

import javax.swing.JFrame;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.ServerProxy;
import org.cocktail.sapics.client.editions.ReportsJasperCtrl;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.eof.model.EOVDetailAttGlobale;
import org.cocktail.sapics.client.eof.model.EOVDetailFactGlobale;
import org.cocktail.sapics.client.eof.model.EOVExecutionGlobale;
import org.cocktail.sapics.client.gui.LotExecutionDetailView;
import org.cocktail.sapics.client.swing.ZEOTable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LotExecutionDetailCtrl 
{
	private static LotExecutionDetailCtrl sharedInstance;

	private org.cocktail.sapics.client.ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext	ec;

	private LotExecutionDetailView myView;

	private EODisplayGroup eodCommandes = new EODisplayGroup(), eodDepenses = new EODisplayGroup();

	private	EOAttribution currentAttribution;
	private EOVDetailAttGlobale currentCommande;
	private Integer currentExeOrdre;

	/** 
	 *
	 */
	public LotExecutionDetailCtrl (EOEditingContext globalEc) {

		super();

		myView = new LotExecutionDetailView(new JFrame(), true, eodCommandes, eodDepenses);

		ec = globalEc;

		myView.getMyEOTableCommandes().addListener(new ListenerCommandes());
		myView.getBtnImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimer();	}});

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LotExecutionDetailCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LotExecutionDetailCtrl(editingContext);
		return sharedInstance;
	}



	private void clean() {
		
		eodCommandes.setObjectArray(new NSArray());
		eodDepenses.setObjectArray(new NSArray());

		myView.getMyEOTableCommandes().updateData();
		myView.getMyEOTableDepenses().updateData();

	}
	

	public void open(EOVExecutionGlobale execution)	{

		NSApp.setGlassPane(true);

		if (execution == null) {
			return;
		}
		currentAttribution = execution.attribution();
		currentExeOrdre = execution.exeOrdre();
		clean();
		
		eodCommandes.setObjectArray(EOVDetailAttGlobale.findForAttribution(ec, EOExercice.find(ec,execution.exeOrdre()), currentAttribution));

		myView.getMyEOTableCommandes().updateData();

		myView.setVisible(true);

		NSApp.setGlassPane(false);

	}

	
	public void imprimer() {
		
		NSMutableDictionary parametres = new NSMutableDictionary();			
		
		Integer lotOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentAttribution.lot())).objectForKey("lotOrdre");
		Integer marOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentAttribution.lot().marche())).objectForKey("marOrdre");
		Integer exer = currentExeOrdre;
		Integer fouOrdre = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentAttribution.fournis())).objectForKey("fouOrdre");
		
		parametres.setObjectForKey(lotOrdre, "LOTORDRE");
		parametres.setObjectForKey(marOrdre, "MARORDRE");
		parametres.setObjectForKey(exer, "EXEREXEC");
		parametres.setObjectForKey(fouOrdre, "FOUORDRE");

		ReportsJasperCtrl.sharedInstance(ec).printSapicsExecutionLotFouExer(parametres);
		
	}

	
	
	private class ListenerCommandes implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentCommande = (EOVDetailAttGlobale)eodCommandes.selectedObject();

			if (currentCommande != null) {

				eodDepenses.setObjectArray(EOVDetailFactGlobale.findForAttribution(ec, currentAttribution, currentCommande.cdeOrdre() ));
				myView.getMyEOTableDepenses().updateData();

			}

		}
	}

}