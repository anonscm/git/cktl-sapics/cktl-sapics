package org.cocktail.sapics.client.lots;

import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.attributions.AttributionCtrl;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOFournis;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOMarche;
import org.cocktail.sapics.client.eof.model.EOTitulaire;
import org.cocktail.sapics.client.eof.model.EOUtilisateur;
import org.cocktail.sapics.client.eof.model.EOVRib;
import org.cocktail.sapics.client.finder.FinderUtilisateur;
import org.cocktail.sapics.client.gui.LotFournisseursView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class LotFournisseursCtrl {

	public static LotFournisseursCtrl sharedInstance;

	private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
	private EOEditingContext 	ec;

	private LotFournisseursView myView;

	private ListenerTitulaire listenerTitulatire = new ListenerTitulaire();
	private ListenerAttribution listenerAttribution = new ListenerAttribution();

	private EODisplayGroup eodTitulaires = new EODisplayGroup(), eodAttributions = new EODisplayGroup(), eodCoTraitants = new EODisplayGroup();

	private EOLot currentLot;
	private EOTitulaire currentCoTraitant;
	private EOTitulaire currentTitulaire;
	private EOAttribution currentAttribution;


	public LotFournisseursCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new LotFournisseursView(eodTitulaires, eodAttributions, eodCoTraitants);

		myView.getBtnAddTitulaire().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterTitulaire();
			}
		});

		myView.getBtnDelTitulaire().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerTitulaire();
			}
		});

		
		myView.getBtnAddCoTraitant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterCoTraitant();
			}
		});

		myView.getBtnDelCoTraitant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerCoTraitant();
			}
		});


		myView.getBtnUpdateAttribution().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateAttribution();
			}
		});


		myView.getBtnDelAttribution().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerAttribution();
			}
		});


		myView.getBtnSetAttribution().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				setAttributionTitulaire();
			}
		});

		myView.getBtnSetAttributionCoTraitant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				setAttributionCoTraitant();
			}
		});

		myView.getBtnShowRib().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				associerRIB();
			}
		});

		myView.getBtnDelRib().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delRib();
			}
		});

		if ( !NSApp.hasFonction(ApplicationClient.ID_FONC_ATTRIBUTIONS) ) {
			
			myView.getBtnAddTitulaire().setVisible(false);
			myView.getBtnAddCoTraitant().setVisible(false);
			myView.getBtnDelTitulaire().setVisible(false);
			myView.getBtnDelCoTraitant().setVisible(false);
			myView.getBtnDelAttribution().setVisible(false);
			myView.getBtnSetAttribution().setVisible(false);
			myView.getBtnUpdateAttribution().setVisible(false);
			myView.getBtnSetAttributionCoTraitant().setVisible(false);
			myView.getBtnShowRib().setVisible(false);
			myView.getBtnDelRib().setVisible(false);
			
		}

		myView.getMyEOTableTitulaires().addListener(listenerTitulatire);
		myView.getMyEOTableAttributions().addListener(new ListenerAttribution());
		myView.getMyEOTableCoTraitants().addListener(new ListenerCoTraitant());

		updateUI();
		
	}

	public static LotFournisseursCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new LotFournisseursCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}

	public void actualiser(EOLot lot) {

		CRICursor.setWaitCursor(myView);

		currentLot = lot;
		currentTitulaire = null;
		currentAttribution = null;
		currentCoTraitant = null;
		myView.getTfRib().setText("");

		eodTitulaires.setObjectArray(new NSArray());
		eodAttributions.setObjectArray(new NSArray());
		eodCoTraitants.setObjectArray(new NSArray());

		if (currentLot != null) {

			eodTitulaires.setObjectArray(EOTitulaire.findForLot(ec, currentLot));    	
			eodAttributions.setObjectArray(EOAttribution.findForLot(ec, currentLot));    	

		}

		myView.getMyEOTableAttributions().updateData();
		myView.getMyEOTableTitulaires().updateData();
		myView.getMyEOTableCoTraitants().updateData();

		updateUI();

		CRICursor.setDefaultCursor(myView);

	}


	private void updateAttribution() {
		
		AttributionCtrl.sharedInstance(ec).updateAttribution(currentAttribution);

		actualiser(currentLot);

	}


	private void supprimerAttribution() {
		if (!EODialogs.runConfirmOperationDialog("Attention", "Confirmez vous la suppression de cette attribution", "OUI", "NON")) {
			return;
		}

		try {
			EOAttribution.supprimer(ec, currentAttribution);
			ec.saveChanges();
		} catch (Exception e) {
			String errMessage = CocktailUtilities.getErrorDialog(e);
			if (errMessage.indexOf("JEFY_DEPENSE.FK_ARTICLE_ATT_ORDRE") > 0) {
				errMessage = "Des dépenses ont été saisies sur cette attribution.";
			}
			EODialogs.runErrorDialog("ERREUR", "ERREUR de suppression de l'attribution !\n" + errMessage);
			ec.revert();
		}
		actualiser(currentLot);
	}

	private void setAttributionTitulaire() {

		try {

//			// On verifie que l'attribution n'y soit pas déjà
//			EOAttribution att = EOAttribution.findForLotAndTitulaire(ec, currentTitulaire, currentLot);
//			if (att != null) {
//				Ethrow new ValidationException("ERREUR","Une attribution est déjà saisie pour ce titulaire !");
//				return;
//			}
			
			Integer index = new Integer(eodAttributions.displayedObjects().count());

			EOUtilisateur myUtilisateur = FinderUtilisateur.findForPersId(ec, NSApp.getCurrentUtilisateur().persId());	

			EOAttribution newAttribution = EOAttribution.creer(ec, currentLot, currentTitulaire, currentTitulaire.fournisseur(), myUtilisateur, index);

			if (currentLot.lotDebut() != null)
				newAttribution.setAttDebut(currentLot.lotDebut());
			
			if (currentLot.lotFin() != null)
				newAttribution.setAttFin(currentLot.lotFin());
			
			// Montant de l'attribution calcule
			BigDecimal totalAttributions = CocktailUtilities.computeSumForKey(eodAttributions, EOAttribution.ATT_HT_KEY);

			if (currentLot.lotHT() != null) {
				newAttribution.setAttHt(currentLot.lotHT().subtract(totalAttributions));
			}
			else {
				newAttribution.setAttHt(new BigDecimal(0));
			}
			
			ec.saveChanges();
			
			currentAttribution = newAttribution;			
			updateAttribution();

			actualiser(currentLot);

		}
		catch (ValidationException e) {
			EODialogs.runErrorDialog("ERREUR",e.getMessage());			
		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();
		}

	}


	private void setAttributionCoTraitant() {

		try {

			EOAttribution att = EOAttribution.findForLotAndTitulaire(ec, currentCoTraitant, currentLot);
			if (att != null) {
				EODialogs.runErrorDialog("ERREUR","Une attribution est déjà saisie pour ce co-traitant !");
				return;
			}

			Integer index = new Integer(eodAttributions.displayedObjects().count());

			EOUtilisateur myUtilisateur = FinderUtilisateur.findForPersId(ec, NSApp.getCurrentUtilisateur().persId());	

			EOAttribution newAttribution = EOAttribution.creer(ec, currentLot, currentCoTraitant, currentCoTraitant.fournisseur(), myUtilisateur, index);

			ec.saveChanges();

			AttributionCtrl.sharedInstance(ec).updateAttribution(newAttribution);

			actualiser(currentLot);

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();
		}

	}
	
	private void ajouterTitulaire() {

		try {

			// Selection d'un utilisateur
			NSApp.setGlassPane(true);
			EOFournis fournisseur = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();
			NSApp.setGlassPane(false);

			if (fournisseur != null) {

				EOTitulaire titulaire = EOTitulaire.findForLot(ec, fournisseur, currentLot);
				
				if (titulaire != null) {
					
					EODialogs.runErrorDialog("ERREUR","Ce fournisseur est déjà associé à ce lot !");
					return;
					
				}
				
				EOTitulaire.creer(ec, currentLot, fournisseur);

				ec.saveChanges();

				actualiser(currentLot);

			}
		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}
	}

	private void associerRIB() {
		
		EOVRib rib = RibSelectCtrl.sharedInstance(ec).getRib(currentAttribution.fournis());
		
		if (rib != null) {
			
			try {
				
				currentAttribution.setRibRelationship(rib);
				ec.saveChanges();
				
				listenerAttribution.onSelectionChanged();
				
			}
			catch (Exception e) {
				ec.revert();
				
			}			
		}
		
	}
	
	private void delRib() {
		
		try {
			
			currentAttribution.setRibRelationship(null);

			ec.saveChanges();
			
			listenerAttribution.onSelectionChanged();
			
		}
		catch (Exception e) {
			ec.revert();
			
		}			
		
	}

	private void ajouterCoTraitant() {

		try {

			// Selection d'un utilisateur
			NSApp.setGlassPane(true);
			EOFournis fournisseur = FournisseurSelectCtrl.sharedInstance(ec).getFournisseur();
			NSApp.setGlassPane(false);

			if (fournisseur != null) {

				EOTitulaire.creerCoTraitant(ec, fournisseur, currentTitulaire);

				ec.saveChanges();

				listenerTitulatire.onSelectionChanged();

			}
		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}


	private void supprimerTitulaire() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de ce titulaire ?\n Les attributions correspondantes seront annulées.",
				"OUI", "NON"))
			return;

		try {

			// Suppression des attributions
			NSArray attributions = EOAttribution.findForTitulaire(ec, currentLot, currentTitulaire.fournisseur());
			for (int i=0;i< attributions.count();i++)
				EOAttribution.supprimer(ec,(EOAttribution)attributions.objectAtIndex(i));

			// Suppression des co-Traitants
			NSArray coTraitants = currentTitulaire.coTraitants();
			for (int i=0;i< coTraitants.count();i++)
				ec.deleteObject((EOTitulaire)coTraitants.objectAtIndex(i));

			ec.deleteObject(currentTitulaire);

			ec.saveChanges();

			actualiser(currentLot);

		}
		catch (Exception e) {

			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression du Titulaire.\n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();

		}

	}

	private void supprimerCoTraitant() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de ce co-traitant ?\n Les attributions correspondantes seront annulées.",
				"OUI", "NON"))
			return;

		try {

			// Suppression des attributions
			NSArray attributions = EOAttribution.findForTitulaire(ec, currentLot, currentCoTraitant.fournisseur());
			for (int i=0;i< attributions.count();i++)
				EOAttribution.supprimer(ec,(EOAttribution)attributions.objectAtIndex(i));

			ec.deleteObject(currentCoTraitant);

			ec.saveChanges();

			actualiser(currentLot);

		}
		catch (Exception e) {
		
			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression du co-traitant.\n"+CocktailUtilities.getErrorDialog(e));
			e.printStackTrace();

		}

	}

	

	private void updateUI() {

		myView.getBtnAddTitulaire().setEnabled(currentLot != null);

		myView.getBtnDelTitulaire().setEnabled(currentLot != null && currentTitulaire != null);


		myView.getBtnUpdateAttribution().setEnabled(currentAttribution != null);

		myView.getBtnDelAttribution().setEnabled(currentAttribution != null);

		myView.getBtnSetAttribution().setEnabled(currentLot != null && currentTitulaire != null);

		myView.getBtnShowRib().setEnabled(currentAttribution != null);
		
		myView.getBtnDelRib().setEnabled(currentAttribution != null && currentAttribution.rib() != null);

		myView.getBtnAddCoTraitant().setEnabled(currentTitulaire != null);

		myView.getBtnDelCoTraitant().setEnabled(currentLot != null && currentCoTraitant != null);

		myView.getBtnSetAttributionCoTraitant().setEnabled(currentLot != null && currentCoTraitant != null);

	}


	private class ListenerTitulaire implements ZEOTable.ZEOTableListener {

		public void onDbClick() {

		}

		public void onSelectionChanged() {

			currentTitulaire = (EOTitulaire)eodTitulaires.selectedObject();

			if (currentTitulaire != null) {
				
				eodCoTraitants.setObjectArray(currentTitulaire.coTraitants());
				myView.getMyEOTableCoTraitants().updateData();
			}

			updateUI();

		}
	}

	private class ListenerCoTraitant implements ZEOTable.ZEOTableListener {

		public void onDbClick() {

		}

		public void onSelectionChanged() {

			currentCoTraitant = (EOTitulaire)eodCoTraitants.selectedObject();

			updateUI();

		}
	}

	private class ListenerAttribution implements ZEOTable.ZEOTableListener {

		public void onDbClick() {

			if ( NSApp.hasFonction(ApplicationClient.ID_FONC_ATTRIBUTIONS) )
				 updateAttribution();
		}

		public void onSelectionChanged() {

			currentAttribution = (EOAttribution)eodAttributions.selectedObject();
			myView.getTfRib().setText("");
			
			if (currentAttribution!=  null && currentAttribution.rib() != null) {

				EOVRib localRib = currentAttribution.rib();
				String chaineRib = "";
				
				try {
				if (localRib.iban() != null) 
					chaineRib = localRib.iban();
				else
					chaineRib = localRib.ribNum();

				if (localRib.ribTitco() != null)
					chaineRib += " - " + localRib.ribTitco();

				CocktailUtilities.setTextTextField(myView.getTfRib(), chaineRib );
				} catch (Exception e) {
					EOMarche marche = currentAttribution.lot().marche();
					System.out.println("ERREUR RIB :: Marché : " + marche.exercice().exeExercice() + "-" + marche.marIndex() + " :: " + e.getMessage());
				}

			}
			
			updateUI();

		}
	}


}
