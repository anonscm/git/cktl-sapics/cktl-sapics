package org.cocktail.sapics.client.lots;

import javax.swing.JPanel;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOArticle;
import org.cocktail.sapics.client.eof.model.EOCatalogueArticle;
import org.cocktail.sapics.client.eof.model.EOCodeExer;
import org.cocktail.sapics.client.eof.model.EOCodeMarche;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOLotNomenclature;
import org.cocktail.sapics.client.gui.LotCodeMarcheView;
import org.cocktail.sapics.client.marches.MarchesCtrl;
import org.cocktail.sapics.client.select.CodeMarcheListeSelectCtrl;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.CRICursor;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class LotNomenclatureCtrl {

	public static LotNomenclatureCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext 	ec;

	private LotCodeMarcheView myView;

	private EODisplayGroup eodCodesMarche = new EODisplayGroup(), eodArticles = new EODisplayGroup();

	private EOLot currentLot;
	private EOLotNomenclature currentLotNomenclature;


	public LotNomenclatureCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new LotCodeMarcheView(eodCodesMarche, eodArticles);

		myView.getBtnAddCodeMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addCodeNomenclature();
			}
		});

		myView.getBtnDelCodeMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delCodeNomenclature();
			}
		});

		if ( !NSApp.hasFonction(ApplicationClient.ID_FONC_NOMENCLATURES) ) {
			
			myView.getBtnAddCodeMarche().setVisible(false);
			myView.getBtnDelCodeMarche().setVisible(false);
		
		}
		
		myView.getMyEOTableCodeMarche().addListener(new ListenerCodeMarche());

		eodCodesMarche.setSortOrderings(new NSArray(new EOSortOrdering(EOLotNomenclature.CODEMARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY, EOSortOrdering.CompareAscending)));
		
		eodArticles.setSortOrderings(new NSArray(new EOSortOrdering(EOCatalogueArticle.ARTICLE_KEY+"."+EOArticle.ART_LIBELLE_KEY, EOSortOrdering.CompareAscending)));
		
	}


	public static LotNomenclatureCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new LotNomenclatureCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}



	public void actualiser(EOLot lot) {

		CRICursor.setWaitCursor(myView);

		currentLot = lot;
		eodCodesMarche.setObjectArray(new NSArray());
		eodArticles.setObjectArray(new NSArray());

		if (currentLot != null) {

			eodCodesMarche.setObjectArray(EOLotNomenclature.findForLot(ec, currentLot));    	

		}

		myView.getMyEOTableCodeMarche().updateData();

		updateUI();

		CRICursor.setDefaultCursor(myView);

	}

	private void addCodeNomenclature() {

		try {

			NSApp.setGlassPane(true);
			CRICursor.setWaitCursor(myView);
			NSArray codes = CodeMarcheListeSelectCtrl.sharedInstance(ec).getCodeMarches(MarchesCtrl.sharedInstance(ec).getExercice(), new Integer(2));

			if (codes != null) {
				for(Object code : codes) {
					if (code != null) {

						NSArray records = EOLotNomenclature.findForLotAndCode(ec, currentLot, (EOCodeExer)code);

						if (records.count() == 0) {

							EOLotNomenclature.creer(ec, currentLot, ((EOCodeExer)code).codemarche());

							ec.saveChanges();

						}

						actualiser(currentLot);
					}
				}
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			ec.revert();
		}

		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

	}

	private void delCodeNomenclature() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de ce code nomenclature ?",
				"OUI", "NON"))
			return;

		try {

			ec.deleteObject(currentLotNomenclature);

			ec.saveChanges();

			actualiser(currentLot);

		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}



	private void updateUI() {

		myView.getBtnAddCodeMarche().setEnabled(currentLot != null);
		myView.getBtnDelCodeMarche().setEnabled(currentLotNomenclature != null);

	}

	private class ListenerCodeMarche implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentLotNomenclature = null;
			eodArticles.setObjectArray(new NSArray());

			if (eodCodesMarche.selectedObjects().count() > 0) {

				currentLotNomenclature = (EOLotNomenclature)eodCodesMarche.selectedObject();

				try {

					eodArticles.setObjectArray(EOCatalogueArticle.findForCodeMarche(ec, currentLot, currentLotNomenclature.codemarche(), "ARTICLE"));

				}
				catch (Exception e){e.printStackTrace();}

			}

			myView.getMyEOTableArticle().updateData();

		}
	}

}
