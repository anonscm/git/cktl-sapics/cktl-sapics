package org.cocktail.sapics.client.lots;

import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOLotOrgan;
import org.cocktail.sapics.client.gui.LotOrganView;
import org.cocktail.sapics.client.select.OrganListeSelectCtrl;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.AskForValeur;
import org.cocktail.sapics.client.utilities.CRICursor;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class LotOrganCtrl {

	public static LotOrganCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext 	ec;

	private LotOrganView myView;

	private EODisplayGroup eodLotOrgan = new EODisplayGroup();
	private ListenerLotOrgan listenerLotOrgan = new ListenerLotOrgan();

	private EOLot currentLot;
	private EOLotOrgan currentLotOrgan;


	public LotOrganCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new LotOrganView(eodLotOrgan);

		myView.getBtnAddOrgan().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getBtnDelOrgan().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});


		myView.getMyEOTableOrgan().addListener(listenerLotOrgan);

		if ( !NSApp.hasFonction(ApplicationClient.ID_FONC_LBUDS) ) {
			
			myView.getBtnAddOrgan().setVisible(false);
			myView.getBtnDelOrgan().setVisible(false);
		
		}

	}


	public static LotOrganCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new LotOrganCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}



	public void actualiser(EOLot lot) {

		CRICursor.setWaitCursor(myView);

		currentLot = lot;
		currentLotOrgan = null;
		eodLotOrgan.setObjectArray(new NSArray());

		if (currentLot != null) {

			eodLotOrgan.setObjectArray(EOLotOrgan.findForLot(ec, currentLot));    	

		}

		myView.getMyEOTableOrgan().updateData();
		updateUI();

		CRICursor.setDefaultCursor(myView);

	}


	private void ajouter() {

		try {

			EOOrgan organ = OrganListeSelectCtrl.sharedInstance(ec).getOrgan((NSArray)(null), null);

			if (organ != null) {

				BigDecimal montant = AskForValeur.sharedInstance().getMontant("Montant Charge : ", new BigDecimal(0));

				if (montant != null) {
					EOLotOrgan newLotOrgan = EOLotOrgan.creer(ec, currentLot, organ);

					newLotOrgan.setLoMontant(montant);

					ec.saveChanges();

				}

				actualiser(currentLot);

			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			ec.revert();
		}
	}


	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de cette ligne budgétaire ?",
				"OUI", "NON"))
			return;

		try {

			ec.deleteObject(currentLotOrgan);

			ec.saveChanges();

			actualiser(currentLot);

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}

	private void updateUI() {

		myView.getBtnAddOrgan().setEnabled(currentLot != null);

		myView.getBtnDelOrgan().setEnabled(currentLot != null && currentLotOrgan != null);
	}



	private class ListenerLotOrgan implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			try {

				BigDecimal montant = AskForValeur.sharedInstance().getMontant("Montant Charge : ", currentLotOrgan.loMontant());

				currentLotOrgan.setLoMontant(montant);

				ec.saveChanges();

				myView.getMyEOTableOrgan().updateUI();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentLotOrgan = (EOLotOrgan)eodLotOrgan.selectedObject();
			updateUI();

		}
	}


}
