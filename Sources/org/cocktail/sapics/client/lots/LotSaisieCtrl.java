package org.cocktail.sapics.client.lots;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOLotHist;
import org.cocktail.sapics.client.eof.model.EORevisionLot;
import org.cocktail.sapics.client.gui.LotSaisieView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.AskForValeur;
import org.cocktail.sapics.client.utilities.CocktailUtilities;
import org.cocktail.sapics.client.utilities.DateCtrl;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class LotSaisieCtrl 
{
	private static LotSaisieCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext	ec;

	private LotSaisieView myView;

	private EODisplayGroup eod = new EODisplayGroup();;
	private ListenerRevision listenerRevision = new ListenerRevision();

	private boolean modeCreation;
	private	EOLot currentLot;
	private EORevisionLot currentRevision;
	private boolean modeModification;

	/** 
	 *
	 */
	public LotSaisieCtrl (EOEditingContext globalEc) {

		super();

		myView = new LotSaisieView(new JFrame(), true, eod);

		ec = globalEc;

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnGetDebut().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDebut();
			}
		});

		myView.getBtnGetFin().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFin();
			}
		});

		myView.getBtnAddRevision().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterRevision();
			}
		});
		myView.getBtnModifierRevision().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierRevision();
			}
		});
		myView.getBtnSupprimerRevision().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerRevision();
			}
		});

		myView.getBtnValiderRevision().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				validerRevision();
			}
		});

		myView.getBtnAnnulerRevision().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annulerRevision();
			}
		});

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ec.revert();
				myView.setVisible(false);
			}
		});

		setSaisieRevisionEnabled(false);

		myView.getMyEOTable().addListener(listenerRevision);

		myView.getCheckRevision().setSelected(true);

		myView.getCheckCatalogue().setSelected(false);

		myView.getTfDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDebut()));
		myView.getTfDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDebut()));

		myView.getTfFin().addFocusListener(new FocusListenerDateTextField(myView.getTfFin()));
		myView.getTfFin().addActionListener(new ActionListenerDateTextField(myView.getTfFin()));

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LotSaisieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LotSaisieCtrl(editingContext);
		return sharedInstance;
	}

	public void ajouter(EOLot lot) {

		myView.setTitle("Ajout d'un nouveau LOT");
		clearTextField();
		
		modeCreation = true;
		currentLot = lot;

		myView.getTfTitreLot().setText("LOT " + currentLot.marche().exercice().exeExercice() + " - " + currentLot.lotIndex().toString());

		myView.getBtnAddRevision().setEnabled(false);
		CocktailUtilities.initTextField(myView.getTfMontant(), false, true);

		myView.getTfDebut().setText(DateCtrl.dateToString(lot.marche().marDebut()));
		myView.getTfFin().setText(DateCtrl.dateToString(lot.marche().marFin()));
		
		myView.getCheckValide().setSelected(true);
		myView.getCheckInvalide().setSelected(false);
		myView.setVisible(true);

	}

	public void modifier(EOLot lot)	{

		myView.setTitle("Révision / Avenant d'un LOT");

		modeCreation = false;
		currentLot = lot;

		myView.getTfTitreLot().setText("LOT " + currentLot.marche().exercice().exeExercice() + " - " + currentLot.lotIndex().toString());

		myView.getBtnAddRevision().setEnabled(true);

		actualiser();

		myView.setVisible(true);

	}


	private void getDebut() {

		CocktailUtilities.setMyTextField(myView.getTfDebut());
		CocktailUtilities.showDatePickerPanel(myView);

	}

	private void getFin(){

		CocktailUtilities.setMyTextField(myView.getTfFin());
		CocktailUtilities.showDatePickerPanel(myView);

	}


	private void clearTextField() {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getAreaIntitule().setText("");
		myView.getTfMontant().setText("");

		myView.getTfMontantRevision().setText("");
		myView.getTfLibelleRevision().setText("");

	}

	private void actualiser() {

		clearTextField();

		myView.getTfDebut().setText(DateCtrl.dateToString(currentLot.lotDebut()));

		myView.getTfFin().setText(DateCtrl.dateToString(currentLot.lotFin()));

		if (currentLot.lotHT() != null)
			myView.getTfMontant().setText(currentLot.lotHT().toString());

		if (currentLot.lotLibelle() != null)
			myView.getAreaIntitule().setText(currentLot.lotLibelle().toString());

		if (currentLot.lotCatalogue() != null)
			myView.getCheckCatalogue().setSelected(currentLot.lotCatalogue().equals("O"));

		myView.getCheckValide().setSelected(currentLot.lotValide().equals("O"));

		if (currentLot.lotIndex() != null) {

			eod.setObjectArray(EORevisionLot.findForLot(ec, currentLot));			
			myView.getMyEOTable().updateData();

		}

		updateUI();

	}

	/**
	 *
	 */
	private void valider()	{

		try {

			if (myView.getTfDebut().getText().length() == 0)
				throw new ValidationException("La date de début est obligatoire !");

			if (myView.getTfFin().getText().length() == 0)
				throw new ValidationException("La date de fin est obligatoire !");

			if (myView.getAreaIntitule().getText().length() == 0)
				throw new ValidationException("Le titre du lot est obligatoire !");

			if (myView.getTfMontant().getText().length() == 0)
				throw new ValidationException("Le montant Hors Taxe est obligatoire !");

			currentLot.setLotDebut(DateCtrl.stringToDate(myView.getTfDebut().getText()));

			currentLot.setLotFin(DateCtrl.stringToDate(myView.getTfFin().getText()));

			String montant = StringCtrl.replace(myView.getTfMontant().getText(), ",", ".");			
			currentLot.setLotHT(new BigDecimal(montant));

			// Controle du montant du lot. Il ne doit pas etre inferieur au montant des attributions
			if (!modeCreation) {
				NSArray attributions = EOAttribution.findForLot(ec, currentLot);
				BigDecimal montantAttributions = CocktailUtilities.computeSumForKey(attributions, EOAttribution.ATT_HT_KEY);
				System.out.println("LotSaisieCtrl.valider() " + montantAttributions + " , compare : " + currentLot.lotHT().compareTo(montantAttributions));
				if (currentLot.lotHT().compareTo(montantAttributions) < 0)
					throw new ValidationException("Le montant du lot doit être supérieur au montant des attributions !");
			}

			currentLot.setLotLibelle(myView.getAreaIntitule().getText());

			currentLot.setLotCatalogue((myView.getCheckCatalogue().isSelected())?"O":"N");

			currentLot.setLotValide((myView.getCheckValide().isSelected())?"O":"N");

			ec.saveChanges();

			myView.dispose();

		}
		catch (ValidationException ex){

			EODialogs.runInformationDialog("ATTENTION", ex.getMessage());

		}
		catch (Exception e){

			e.printStackTrace();
		}


	}


	private void updateUI() {

		myView.getBtnSupprimerRevision().setEnabled(currentRevision != null);
		myView.getBtnModifierRevision().setEnabled(currentRevision != null);
		
		CocktailUtilities.initTextField(myView.getTfMontant(), false, eod.displayedObjects().count() == 0);

	}


	/**
	 *
	 */
	public void annuler()	{

		ec.revert();
		myView.dispose();

	}

	// TRAITEMENT DES REVISIONS
	private void ajouterRevision() {

		currentRevision = EORevisionLot.creer(ec, currentLot, NSApp.getCurrentUtilisateur());
		myView.getTfMontantRevision().setText("");
		myView.getTfLibelleRevision().setText("");
		modeModification = false;
		setSaisieRevisionEnabled(true);

	}

	private void modifierRevision() {

		try {
			// Saisie du montant a sous traiter
			BigDecimal montantRevision = AskForValeur.sharedInstance().getMontant("Nouveau montant du lot : ", currentLot.lotHT());

			if (montantRevision != null) {

				// Annulation de la révision
				currentLot.setLotHT(currentLot.lotHT().subtract(currentRevision.rlDiff()));

				BigDecimal difference = montantRevision.subtract(currentLot.lotHT());

				currentRevision.setRlDate(new NSTimestamp());
				currentRevision.setRlDiff(difference);

				currentLot.setLotHT(montantRevision);

				ec.saveChanges();

				myView.getTfMontant().setText(currentLot.lotHT().toString());
				myView.getMyEOTable().updateData();
				myView.getMyEOTable().updateUI();

			}

		}
		catch (ValidationException ve) {

			ec.revert();
			ve.printStackTrace();

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}
	private void supprimerRevision() {

		try {

			if (!EODialogs.runConfirmOperationDialog("Attention",
					"Désirez-vous réellement supprimer " + 
					((myView.getCheckAvenant().isSelected())?"cet Avenant ?":"cette Révision ?"),
					"OUI", "NON"))
				return;

			currentLot.setLotHT(currentLot.lotHT().subtract(currentRevision.rlDiff()));			
			currentRevision.setRlSuppr("O");

			ec.saveChanges();
			actualiser();

		}
		catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		}

	}


	private void validerRevision() {

		try {

			// TESTS VALIDITE
			if (myView.getTfMontant().getText().length() == 0 )
				throw new ValidationException("Vous devez saisir un montant pour cette révision !");

			if (myView.getTfLibelleRevision().getText().length() == 0 )				
				throw new ValidationException("Vous devez saisir un libellé pour cette révision !");

			String montant = StringCtrl.replace(myView.getTfMontantRevision().getText(), ",", ".");
			BigDecimal montantRevision = new BigDecimal(montant);

			if (montantRevision.floatValue() <= 0)				
				throw new ValidationException("Le montant de la révision doit être positif ET supérieur à 0!");


			// Historisation du lot
			EOLotHist.creer(ec, currentLot);			

			currentRevision.setRlLibelle(myView.getTfLibelleRevision().getText());

			BigDecimal difference = montantRevision.subtract(currentLot.lotHT());	
			currentRevision.setRlDiff(difference);	
			currentRevision.setRlType((myView.getCheckAvenant().isSelected())?"A":"R");

			// Mise a jour du montant du lot
			currentLot.setLotHT(montantRevision);

			ec.saveChanges();

			EODialogs.runInformationDialog("OK",(myView.getCheckAvenant().isSelected())?"L'avenant a bien été enregistré !":"La révision a bien été enregistrée !");

			myView.getTfMontant().setText(currentLot.lotHT().toString());
			eod.setObjectArray(EORevisionLot.findForLot(ec, currentLot));
			myView.getMyEOTable().updateData();

			setSaisieRevisionEnabled(false);
			updateUI();
		}
		catch (ValidationException e) {
			ec.revert();
			EODialogs.runInformationDialog("ERREUR", e.getMessage());			
		}
		catch (Exception ex) {
			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur d'enregistrement de la révision.\n"+CocktailUtilities.getErrorDialog(ex));
			ex.printStackTrace();

		}
	}

	private void annulerRevision() {

		ec.revert();
		actualiser();

		setSaisieRevisionEnabled(false);

	}

	private void setSaisieRevisionEnabled(boolean yn) {

		myView.getBtnAddRevision().setVisible(!yn);
		myView.getBtnModifierRevision().setVisible(!yn);
		myView.getBtnSupprimerRevision().setVisible(!yn);

		myView.getBtnValiderRevision().setVisible(yn);
		myView.getBtnAnnulerRevision().setVisible(yn);

		myView.getCheckAvenant().setEnabled(yn);
		myView.getCheckRevision().setEnabled(yn);
		CocktailUtilities.initTextField(myView.getTfMontantRevision(), false, yn);
		CocktailUtilities.initTextField(myView.getTfLibelleRevision(), false, yn);

		myView.getAreaIntitule().setEnabled(!yn);
		myView.getBtnGetDebut().setEnabled(!yn);
		myView.getBtnGetFin().setEnabled(!yn);
		CocktailUtilities.initTextField(myView.getTfDebut(), false, !yn);
		CocktailUtilities.initTextField(myView.getTfFin(), false, !yn);
		myView.getCheckCatalogue().setEnabled(!yn);

		myView.getBtnValider().setEnabled(!yn);
		myView.getBtnAnnuler().setEnabled(!yn);
		myView.getBtnFermer().setEnabled(!yn);

	}


	private class ActionListenerDateTextField implements ActionListener	{
		
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myView.getTfDebut().selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else
				myTextField.setText(myDate);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else
				myTextField.setText(myDate);
		}
	}	


	private class ListenerRevision implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			if ( NSApp.hasFonction(ApplicationClient.ID_FONC_MARCHES) ) 	
				modifierRevision();

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentRevision = (EORevisionLot)eod.selectedObject();
			if (currentRevision != null) {
				myView.getTfMontantRevision().setText(currentRevision.rlDiff().toString());
			}
			if (currentRevision != null) {
				myView.getTfLibelleRevision().setText(currentRevision.rlLibelle());
			}

			updateUI();
		}
	}


}