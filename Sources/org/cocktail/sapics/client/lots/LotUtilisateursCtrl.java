package org.cocktail.sapics.client.lots;

import javax.swing.JPanel;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOLotUtilisateur;
import org.cocktail.sapics.client.eof.model.EOUtilisateur;
import org.cocktail.sapics.client.gui.LotUtilisateursView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.CRICursor;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class LotUtilisateursCtrl {

	public static LotUtilisateursCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext 	ec;

	private LotUtilisateursView myView;

	private EODisplayGroup eodUtilisateurs = new EODisplayGroup();

	private EOLot currentLot;
	private EOLotUtilisateur  currentLotUtilisateur;

	public LotUtilisateursCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new LotUtilisateursView(eodUtilisateurs);

		myView.getBtnAddUtilisateur().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouter();
			}
		});

		myView.getBtnDelUtilisateur().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});

		myView.getMyEOTableUtilisateur().addListener(new ListenerUtilisateur());

		if ( !NSApp.hasFonction(ApplicationClient.ID_FONC_MARCHES) ) {
			
			myView.getBtnAddUtilisateur().setVisible(false);
			myView.getBtnDelUtilisateur().setVisible(false);
		
		}

	}


	public static LotUtilisateursCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new LotUtilisateursCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}



	public void actualiser(EOLot lot) {

		CRICursor.setWaitCursor(myView);

		currentLot = lot;
		currentLotUtilisateur = null;
		eodUtilisateurs.setObjectArray(new NSArray());

		if (currentLot != null) {

			eodUtilisateurs.setObjectArray(EOLotUtilisateur.findForLot(ec, currentLot));    	

		}

		myView.getMyEOTableUtilisateur().updateData();
		updateUI();

		CRICursor.setDefaultCursor(myView);

	}


	private void ajouter() {

		try {

			EOUtilisateur utilisateur = UtilisateurSelectCtrl.sharedInstance(ec).getUtilisateur();

			if (utilisateur != null) {

				EOLotUtilisateur.creer(ec, currentLot, utilisateur);

				ec.saveChanges();

				actualiser(currentLot);

			}

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}


	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression de cet utilisateur ?",
				"OUI", "NON"))
			return;

		try {

			ec.deleteObject(currentLotUtilisateur);

			ec.saveChanges();

			actualiser(currentLot);

		}
		catch (Exception e) {

			ec.revert();
			e.printStackTrace();

		}

	}


	private void updateUI() {
		
		myView.getBtnAddUtilisateur().setEnabled(currentLot != null);
		
		myView.getBtnDelUtilisateur().setEnabled(currentLot != null && currentLotUtilisateur != null);
	}

	private class ListenerUtilisateur implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentLotUtilisateur = (EOLotUtilisateur)eodUtilisateurs.selectedObject();
			updateUI();

		}
	}


}
