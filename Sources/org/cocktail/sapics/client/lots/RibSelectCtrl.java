package org.cocktail.sapics.client.lots;

import javax.swing.JFrame;

import org.cocktail.sapics.client.eof.model.EOFournis;
import org.cocktail.sapics.client.eof.model.EOVRib;
import org.cocktail.sapics.client.gui.RibSelectView;
import org.cocktail.sapics.client.swing.ZEOTable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class RibSelectCtrl 
{
	private static RibSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	
	private EODisplayGroup eod;
		
	private RibSelectView myView;
	private EOVRib currentRib;
	
	/**
	 * 
	 * @param globalEc
	 */
	public RibSelectCtrl (EOEditingContext globalEc)	{
		super();

		ec = globalEc;

		eod = new EODisplayGroup();

		myView = new RibSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});
		
		myView.getMyEOTable().addListener(new ListenerRib());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static RibSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new RibSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	/**
	 * Selection d'un engagement
	 */
	public EOVRib getRib(EOFournis fournis)	{
		
		eod.setObjectArray(EOVRib.findForFournis(ec, fournis));
		myView.getMyEOTable().updateData();

		myView.setVisible(true);
		
		if (eod.selectedObjects().count() > 0)
			return (EOVRib) eod.selectedObject();			
		
		return null;
	}

			
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}  	
	
	private class ListenerRib implements ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentRib = (EOVRib)eod.selectedObject();
		}
	}
	
}