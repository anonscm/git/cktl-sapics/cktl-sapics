package org.cocktail.sapics.client.lots;

import javax.swing.JFrame;

import org.cocktail.sapics.client.eof.model.EOIndividu;
import org.cocktail.sapics.client.eof.model.EOUtilisateur;
import org.cocktail.sapics.client.gui.UtilisateurSelectView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class UtilisateurSelectCtrl 
{
	private static UtilisateurSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	
	private EODisplayGroup eod;
		
	private UtilisateurSelectView myView;
	
	/**
	 * 
	 * @param globalEc
	 */
	public UtilisateurSelectCtrl (EOEditingContext globalEc)	{
		super();

		ec = globalEc;

		eod = new EODisplayGroup();

		myView = new UtilisateurSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getButtonFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfNom().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});
		
		myView.getMyEOTable().addListener(new ListenerUtilisateur());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static UtilisateurSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new UtilisateurSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	/**
	 * Selection d'un engagement
	 */
	public EOUtilisateur getUtilisateur()	{
		
		myView.setVisible(true);
		
		if (eod.selectedObjects().count() > 0)
			return (EOUtilisateur) eod.selectedObject();			
		
		return null;
	}

	
	/** 
	 * Recherche de tous les fournisseurs correspondant aux criteres entres
	 */
	public void rechercher()	{
		
		if (StringCtrl.chaineVide(myView.getTfNom().getText()) && StringCtrl.chaineVide(myView.getTfNom().getText()))	{
			EODialogs.runInformationDialog("ERREUR","Veuillez saisir un nom pour la recherche.");
			return;
		}
		
		NSMutableArray mesQualifiers = new NSMutableArray();
				
		if (!StringCtrl.chaineVide(myView.getTfNom().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike '*" + myView.getTfNom().getText() + "*' and " + EOUtilisateur.INDIVIDU_KEY+"."+EOIndividu.PRENOM_KEY + " != nil", null));

		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateur.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				
		eod.setObjectArray(ec.objectsWithFetchSpecification(fs));	
		
		myView.getMyEOTable().updateData();
		
	}
		
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}  	
	
	private class ListenerUtilisateur implements ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
		}
	}
	
}