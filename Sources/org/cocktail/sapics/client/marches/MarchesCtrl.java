package org.cocktail.sapics.client.marches;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.client.eof.EOCodeMarche;
import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.ServerProxy;
import org.cocktail.sapics.client.editions.ReportsJasperCtrl;
import org.cocktail.sapics.client.eof.model.EODevise;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOLotNomenclature;
import org.cocktail.sapics.client.eof.model.EOMarche;
import org.cocktail.sapics.client.eof.model.EOParametreMarche;
import org.cocktail.sapics.client.eof.model.EOPassationMarche;
import org.cocktail.sapics.client.gui.MarchesView;
import org.cocktail.sapics.client.lots.LotExecutionCtrl;
import org.cocktail.sapics.client.lots.LotFournisseursCtrl;
import org.cocktail.sapics.client.lots.LotNomenclatureCtrl;
import org.cocktail.sapics.client.lots.LotOrganCtrl;
import org.cocktail.sapics.client.lots.LotSaisieCtrl;
import org.cocktail.sapics.client.lots.LotUtilisateursCtrl;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.CRICursor;
import org.cocktail.sapics.client.utilities.CocktailConstantes;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class MarchesCtrl {

	public static MarchesCtrl sharedInstance;

	private	ApplicationClient	NSApp = (ApplicationClient) ApplicationClient.sharedApplication();	
	private EOEditingContext 	ec;

	private MarchesView myView;

	private EODisplayGroup eodMarches = new EODisplayGroup(), eodLots = new EODisplayGroup();

	private ListenerMarche listenerMarche = new ListenerMarche();

	private OngletChangeListener listenerOnglets = new OngletChangeListener();

	private EOMarche currentMarche;
	private EOLot currentLot;
	private String paramCtrlDatesMarche;

	public MarchesCtrl(EOEditingContext globalEc) {

		super();

		ec = globalEc;

		myView = new MarchesView(eodMarches, eodLots);

		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getBtnAddMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterMarche();
			}
		});

		myView.getBtnUpdateMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierMarche();
			}
		});
		
		myView.getBtnAnnulerMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annulerMarche();
			}
		});


		myView.getBtnDelMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerMarche();
			}
		});

		myView.getBtnAddLot().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterLot();
			}
		});


		myView.getBtnUpdateLot().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierLot();
			}
		});

		myView.getBtnAnnulerLot().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annulerLot();
			}
		});

		myView.getBtnSupprimerLot().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerLot();
			}
		});

		myView.getBtnPrintMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		
		if (!NSApp.hasFonction(ApplicationClient.ID_FONC_MARCHES)) {

			myView.getBtnAddMarche().setVisible(false);
			myView.getBtnDelMarche().setVisible(false);
			myView.getBtnUpdateMarche().setVisible(false);
			myView.getBtnAnnulerMarche().setVisible(false);

			myView.getBtnAddLot().setVisible(false);
			myView.getBtnUpdateLot().setVisible(false);
			myView.getBtnAnnulerLot().setVisible(false);
			myView.getBtnSupprimerLot().setVisible(false);
			
		}
		
		myView.getOnglets().addTab("Codes Nomenclatures ", null, LotNomenclatureCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab("Attributions ", null, LotFournisseursCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab("Lignes Budgétaires ", null, LotOrganCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab("Utilisateurs ", null, LotUtilisateursCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab("Exécution ", null, LotExecutionCtrl.sharedInstance(ec).getView());

		myView.setListeExercices(EOExercice.find(ec));
		myView.getExercices().setSelectedItem(EOExercice.exerciceCourant(ec));
		myView.getExercices().addActionListener(new PopupExerciceListener());

		myView.getMyEOTableMarches().addListener(listenerMarche);
		myView.getMyEOTableLots().addListener(new ListenerLot());

		myView.getValidite().addActionListener(new PopupsListener());
		myView.getTypesMarche().addActionListener(new PopupsListener());

		myView.getOnglets().setSelectedIndex(1);
		myView.getOnglets().addChangeListener(listenerOnglets);

		myView.getTfFiltreMarche().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfFiltreLot().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfFiltreCn().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		EOSortOrdering sort_index = new EOSortOrdering(EOMarche.MAR_INDEX_KEY, EOSortOrdering.CompareDescending);
		EOSortOrdering sort_exer = new EOSortOrdering(EOMarche.EXERCICE_KEY, EOSortOrdering.CompareDescending);
		NSArray mySort = new NSArray(new Object[] {sort_exer, sort_index});
		eodMarches.setSortOrderings(mySort);
		
		paramCtrlDatesMarche = EOParametreMarche.getValue(ec, EOParametreMarche.ID_CTRL_DATES);
		if (paramCtrlDatesMarche == null) {
			paramCtrlDatesMarche = EOParametreMarche.DEFAULT_VALUE_CTRL_DATES;
		}

		rechercher();		
		
	}


	public static MarchesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new MarchesCtrl(editingContext);
		return sharedInstance;
	}


	public EOExercice getExercice() {

		return (EOExercice)myView.getExercices().getSelectedItem();

	}

	public JPanel getView() {
		return myView;
	}

	private EOQualifier getFilterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (paramCtrlDatesMarche.equals("N"))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.EXERCICE_KEY+" = %@", new NSArray(myView.getExercices().getSelectedItem())));
		else {

			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.MAR_FIN_KEY+" >= %@", new NSArray(EOExercice.debutExercice((EOExercice)myView.getExercices().getSelectedItem()))));
			
			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));

		}
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.MAR_SUPPR_KEY  + " = %@", new NSArray("N")));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.MAR_VALIDE_KEY + " = %@", new NSArray((myView.getValidite().getSelectedIndex() == 0)?"O":"N")));    		

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.PASSATIONMARCHE_KEY + "." + EOPassationMarche.PAS_TYPE_KEY + " = %@", new NSArray((myView.getTypesMarche().getSelectedIndex() == 0)?"MARCHE":"INTERNE")));    		

		if (!StringCtrl.chaineVide(myView.getTfFiltreMarche().getText())) {

			NSMutableArray mesOrQualifiers = new NSMutableArray();

			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.MAR_LIBELLE_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreMarche().getText()+"*")));
			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.MAR_INDEX_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreMarche().getText()+"*")));
			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.MAR_REF_EXTERNE_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreMarche().getText()+"*")));

			mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltreLot().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.LOTS_KEY+"."+ EOLot.LOT_LIBELLE_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreLot().getText()+"*")));

		if (!StringCtrl.chaineVide(myView.getTfFiltreCn().getText())) {

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMarche.LOTS_KEY+"."+ EOLot.LOT_NOMENCLATURES_KEY + "."+
					EOLotNomenclature.CODEMARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreCn().getText()+"*")));

		}

		return new EOAndQualifier(mesQualifiers);
	}

	
	public void rafraichirLot() {
		
		myView.getMyEOTableLots().updateUI();
		
	}
	
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		
		eodMarches.setObjectArray(EOMarche.fetchAll(ec, getFilterQualifier(), null, true));	
		myView.getMyEOTableMarches().updateData();

		updateUI();

		CRICursor.setDefaultCursor(myView);

	}

	private void ajouterLot() {

		NSApp.setGlassPane(true);

		currentLot = EOLot.creer(ec, currentMarche, EODevise.findDeviseCourante(ec));
		currentLot.setLotIndex(StringCtrl.stringCompletion(String.valueOf(eodLots.displayedObjects().count()+1), 3, "0", "G"));

		LotSaisieCtrl.sharedInstance(ec).ajouter(currentLot);

		NSApp.setGlassPane(false);
		listenerMarche.onSelectionChanged();
		updateUI();

	}
	private void modifierLot () {

		LotSaisieCtrl.sharedInstance(ec).modifier(currentLot);
		myView.getMyEOTableLots().updateUI();

	}



	private void ajouterMarche() {

		NSApp.setGlassPane(true);

		currentMarche = EOMarche.creer(ec, (EOExercice)myView.getExercices().getSelectedItem());
		
		currentMarche.setMarIndex(EOMarche.getNouvelIndex(ec, (EOExercice)myView.getExercices().getSelectedItem()));

		MarchesSaisieCtrl.sharedInstance(ec).updateMarche(currentMarche);

		NSApp.setGlassPane(false);

		rechercher();

		updateUI();

	}


	private void modifierMarche () {

		MarchesSaisieCtrl.sharedInstance(ec).updateMarche(currentMarche);
		myView.getMyEOTableMarches().updateUI();

	}



	private void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();			

		Number marOrdre = (Number)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentMarche)).objectForKey("marOrdre");

		parametres.setObjectForKey(marOrdre, "m_ordre");
		ReportsJasperCtrl.sharedInstance(ec).printDetailMarche(parametres);

	}

	private void annulerMarche () {

		String message = "";
		if (currentMarche.marValide().equals("O"))
			message = "Confirmez vous l'annulation de ce Marché ?";
		else
			message = "Voulez-vous revalider ce Marché ?";

		if (!EODialogs.runConfirmOperationDialog("Attention",
				message,
				"OUI", "NON"))
			return;

		try {
			currentMarche.setMarValide((currentMarche.marValide().equals("O"))?"N":"O");    		
			ec.saveChanges();
			rechercher();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}

	private void supprimerMarche () {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression définitive de ce Marché ?",
				"OUI", "NON"))
			return;

		try {
			currentMarche.setMarSuppr("O");    		
			ec.saveChanges();
			rechercher();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}

	private void annulerLot () {

		String message = "";
		if (currentLot.lotValide().equals("O"))
			message = "Confirmez vous l'annulation de ce Lot ?";
		else
			message = "Voulez-vous revalider ce Lot ?";

		if (!EODialogs.runConfirmOperationDialog("Attention",
				message,
				"OUI", "NON"))
			return;

		try {
			currentLot.setLotValide((currentLot.lotValide().equals("O"))?"N":"O");    		
			ec.saveChanges();
			listenerMarche.onSelectionChanged();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}


	private void supprimerLot () {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez vous la suppression définitive de ce Lot ?",
				"OUI", "NON"))
			return;

		try {
			currentLot.setLotSuppr("O");    		
			ec.saveChanges();
			listenerMarche.onSelectionChanged();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

	}


	private class ListenerMarche implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			 if ( NSApp.hasFonction(ApplicationClient.ID_FONC_MARCHES) ) 	
					 modifierMarche();
			 
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);

			eodLots.setObjectArray(new NSArray());
			currentMarche = (EOMarche)eodMarches.selectedObject();

			if (currentMarche != null) {

				if (currentMarche.marValide().equals("N")) {
					myView.getBtnAnnulerMarche().setToolTipText("Revalidation du marché");
					myView.getBtnAnnulerMarche().setIcon(CocktailConstantes.ICON_VALID);
				}				
				else {
					myView.getBtnAnnulerMarche().setToolTipText("Annulation du marché");
					myView.getBtnAnnulerMarche().setIcon(CocktailConstantes.ICON_CANCEL);				
				}

				eodLots.setObjectArray(EOLot.findForMarche(ec, currentMarche));

			}

			myView.getMyEOTableLots().updateData();
			updateUI();
			CRICursor.setDefaultCursor(myView);

		}
	}


	private void updateUI() {

		myView.getBtnUpdateMarche().setEnabled(currentMarche != null);
		myView.getBtnDelMarche().setEnabled(currentMarche != null);

		myView.getBtnPrintMarche().setEnabled(currentMarche != null);

		myView.getBtnAddLot().setEnabled(currentMarche != null);
		myView.getBtnUpdateLot().setEnabled(currentMarche != null && currentLot != null);
		myView.getBtnAnnulerLot().setEnabled(currentMarche != null && currentLot != null);
		myView.getBtnSupprimerLot().setEnabled(currentMarche != null && currentLot != null);

	}


	private class ListenerLot implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			 if ( NSApp.hasFonction(ApplicationClient.ID_FONC_MARCHES) ) 	
				 modifierLot();
			 
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentLot = (EOLot)eodLots.selectedObject();
			listenerOnglets.stateChanged(null);

			if (currentLot != null) {

				if (currentLot.lotValide().equals("N")) {
					myView.getBtnAnnulerLot().setToolTipText("Revalidation du lot");
					myView.getBtnAnnulerLot().setIcon(CocktailConstantes.ICON_VALID);
				}				
				else {
					myView.getBtnAnnulerLot().setToolTipText("Annulation du lot");
					myView.getBtnAnnulerLot().setIcon(CocktailConstantes.ICON_CANCEL);				
				}
			}


			//			LotNomenclatureCtrl.sharedInstance(ec).actualiser(currentLot);
			//			LotFournisseursCtrl.sharedInstance(ec).actualiser(currentLot);
			//			LotOrganCtrl.sharedInstance(ec).actualiser(currentLot);
			//			LotUtilisateursCtrl.sharedInstance(ec).actualiser(currentLot);
			//			LotExecutionCtrl.sharedInstance(ec).actualiser(currentLot);

		}
	}

	private class PopupsListener implements ActionListener	{
		public PopupsListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			rechercher();

		}
	}


	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			rechercher();

		}
	}


	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

			CRICursor.setWaitCursor(myView);

			switch (myView.getOnglets().getSelectedIndex()) {

			case 0 : LotNomenclatureCtrl.sharedInstance(ec).actualiser(currentLot);break;
			case 1 : LotFournisseursCtrl.sharedInstance(ec).actualiser(currentLot);break;
			case 2 : LotOrganCtrl.sharedInstance(ec).actualiser(currentLot);break;
			case 3 : LotUtilisateursCtrl.sharedInstance(ec).actualiser(currentLot);break;
			case 4 : LotExecutionCtrl.sharedInstance(ec).actualiser(currentLot);break;

			}

			CRICursor.setDefaultCursor(myView);

		}
	}


}
