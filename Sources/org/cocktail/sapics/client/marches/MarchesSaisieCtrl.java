package org.cocktail.sapics.client.marches;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOAttribution;
import org.cocktail.sapics.client.eof.model.EOLot;
import org.cocktail.sapics.client.eof.model.EOMarche;
import org.cocktail.sapics.client.eof.model.EOPassationMarche;
import org.cocktail.sapics.client.gui.MarchesSaisieView;
import org.cocktail.sapics.client.utilities.CocktailUtilities;
import org.cocktail.sapics.client.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class MarchesSaisieCtrl 
{
	private static MarchesSaisieCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext	ec;

	private MarchesSaisieView myView;

	private	EOMarche currentMarche;


	/** 
	 *
	 */
	public MarchesSaisieCtrl (EOEditingContext globalEc) {

		super();

		myView = new MarchesSaisieView(new JFrame(), true);

		ec = globalEc;

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnGetDebut().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getDebut();
			}
		});

		myView.getBtnGetFin().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFin();
			}
		});

		myView.getCheckExterne().setSelected(true);

		myView.getCheckExterne().addItemListener(new TypeExterneListener());
		myView.getCheckInterne().addItemListener(new TypeInterneListener());
		
		myView.getTfDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDebut()));
		myView.getTfDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDebut()));

		myView.getTfFin().addFocusListener(new FocusListenerDateTextField(myView.getTfFin()));
		myView.getTfFin().addActionListener(new ActionListenerDateTextField(myView.getTfFin()));

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static MarchesSaisieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new MarchesSaisieCtrl(editingContext);
		return sharedInstance;
	}


	private void getDebut() {
		CocktailUtilities.setMyTextField(myView.getTfDebut());
		CocktailUtilities.showDatePickerPanel(myView);
	}

	private void getFin(){
		CocktailUtilities.setMyTextField(myView.getTfFin());
		CocktailUtilities.showDatePickerPanel(myView);
	}


	public void updateMarche(EOMarche marche)	{

		NSApp.setGlassPane(true);

		currentMarche = marche;

		actualiser();

		myView.setVisible(true);

		NSApp.setGlassPane(false);

	}


	private void clearTextField() {

		myView.getCheckExterne().setSelected(true);
		myView.getTfReferenceExterne().setText("");
		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getAreaIntitule().setText("");
		myView.getAreaClauses().setText("");

	}

	private void actualiser() {

		clearTextField();

		myView.getTfDebut().setText(DateCtrl.dateToString(currentMarche.marDebut()));

		myView.getTfFin().setText(DateCtrl.dateToString(currentMarche.marFin()));

		if (currentMarche.marRefExterne() != null)
			myView.getTfReferenceExterne().setText(currentMarche.marRefExterne());
		//		else
		//			myView.getTfReferenceExterne().setText(currentMarche.marIndex());

		if (currentMarche.marLibelle() != null)
			myView.getAreaIntitule().setText(currentMarche.marLibelle().toString());

		if (currentMarche.marClauses() != null)
			myView.getAreaClauses().setText(currentMarche.marClauses());

		if (currentMarche.passationmarche() != null) {
			myView.getCheckExterne().setSelected(currentMarche.passationmarche().pasLibelle().equals(EOPassationMarche.TYPE_PASSATION_MARCHE));
			myView.getCheckInterne().setSelected(currentMarche.passationmarche().pasLibelle().equals(EOPassationMarche.TYPE_PASSATION_INTERNE));

			myView.setPassations(EOPassationMarche.findForType(ec, (myView.getCheckExterne().isSelected())?EOPassationMarche.TYPE_PASSATION_MARCHE:EOPassationMarche.TYPE_PASSATION_INTERNE));

			myView.getPassations().setSelectedItem(currentMarche.passationmarche());
		}
		else
			myView.setPassations(EOPassationMarche.findForType(ec, (myView.getCheckExterne().isSelected())?EOPassationMarche.TYPE_PASSATION_MARCHE:EOPassationMarche.TYPE_PASSATION_INTERNE));

	}

	/**
	 *
	 */
	private void valider()	{

		try {

			boolean datesModifiees;

			if (myView.getTfReferenceExterne().getText().length() > 20)
				throw new ValidationException("La référence externe ne doit pas excéder 20 caractères !");

			if (myView.getTfDebut().getText().length() ==0)
				throw new ValidationException("La date de début de marché est obligatoire !");

			if (myView.getTfFin().getText().length() ==0)
				throw new ValidationException("La date de fin de marché est obligatoire !");

			if (myView.getAreaIntitule().getText().length() ==0)
				throw new ValidationException("Le libellé du  marché est obligatoire !");

			currentMarche.setPassationmarcheRelationship((EOPassationMarche)myView.getPassations().getSelectedItem());

			NSTimestamp dateDebut = DateCtrl.stringToDate(myView.getTfDebut().getText()+ " 00:00", "%d/%m/%Y %H:%M");
			NSTimestamp dateFin = DateCtrl.stringToDate(myView.getTfFin().getText()+ " 00:00", "%d/%m/%Y %H:%M");

			if (currentMarche.marDebut() != null 
					&& (!DateCtrl.isSameDay(currentMarche.marDebut(), dateDebut)
							|| !DateCtrl.isSameDay(currentMarche.marFin(), dateFin) ) ) {

				if (currentMarche.lots() != null && currentMarche.lots().count() == 1) {

					if (EODialogs.runConfirmOperationDialog("DATES", "Les dates du marché ont été modifiées, voulez-vous répercuter les modifications au lot et à l'attribution associée ?", "OUI", "NON")) {

						for (Enumeration<EOLot> e = currentMarche.lots().objectEnumerator();e.hasMoreElements();) {							
							EOLot lot = e.nextElement();
								lot.setLotDebut(dateDebut);
								lot.setLotFin(dateFin);								
								
							if (lot.attribution() != null && lot.attribution().count() == 1) {
								
								for (Enumeration<EOAttribution> e1 = lot.attribution().objectEnumerator();e1.hasMoreElements();) {							
										EOAttribution attribution = e1.nextElement();
										attribution.setAttDebut(dateDebut);
										attribution.setAttFin(dateFin);								
								}								
							}
								
						}

					}
				}

			}

			currentMarche.setMarDebut(dateDebut);
			currentMarche.setMarFin(dateFin);

			currentMarche.setMarLibelle(myView.getAreaIntitule().getText());

			if (myView.getTfReferenceExterne().getText().length() > 0)
				currentMarche.setMarRefExterne(myView.getTfReferenceExterne().getText());
			else
				currentMarche.setMarRefExterne(null);

			currentMarche.setMarClauses(myView.getAreaClauses().getText());

			ec.saveChanges();

			myView.dispose();

		}
		catch (ValidationException ex){

			EODialogs.runInformationDialog("ATTENTION", ex.getMessage());

		}
		catch (Exception e){
			e.printStackTrace();
		}


	}



	/**
	 *
	 */
	public void annuler()	{

		ec.revert();
		myView.dispose();

	}

	private class TypeExterneListener implements ItemListener 	{

		public void itemStateChanged(ItemEvent e)		{

			if (e.getStateChange() == ItemEvent.SELECTED)
				myView.setPassations(EOPassationMarche.findForType(ec, EOPassationMarche.TYPE_PASSATION_MARCHE));
		}
	} 

	private class TypeInterneListener implements ItemListener 	{

		public void itemStateChanged(ItemEvent e)		{

			if (e.getStateChange() == ItemEvent.SELECTED)
				myView.setPassations(EOPassationMarche.findForType(ec, EOPassationMarche.TYPE_PASSATION_INTERNE));
		}
	} 


	private class ActionListenerDateTextField implements ActionListener	{
		
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else
				myTextField.setText(myDate);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else
				myTextField.setText(myDate);
		}
	}	


//	private class ActionListenerDateFin implements ActionListener	{
//		public void actionPerformed(ActionEvent e)	{
//			if ("".equals(myView.getTfFin().getText()))	return;
//
//			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
//			if ("".equals(myDate))	{
//				myView.getTfFin().selectAll();
//				EODialogs.runInformationDialog("Date non valide","La date de fin n'est pas valide !");
//			}
//			else
//				myView.getTfFin().setText(myDate);
//		}
//	}
//	private class ListenerTextFieldDateFin implements FocusListener	{
//		public void focusGained(FocusEvent e) 	{}
//
//		public void focusLost(FocusEvent e)	{
//			if ("".equals(myView.getTfFin().getText()))	return;
//
//			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
//			if ("".equals(myDate))	{
//				EODialogs.runInformationDialog("Date Invalide","La date de fin n'est pas valide !");
//				myView.getTfFin().selectAll();
//			}
//			else
//				myView.getTfFin().setText(myDate);
//		}
//	}	
//
//


}