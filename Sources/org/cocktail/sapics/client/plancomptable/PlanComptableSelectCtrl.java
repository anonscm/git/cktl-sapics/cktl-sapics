package org.cocktail.sapics.client.plancomptable;

import javax.swing.JFrame;

import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.client.metier.EOPlancoCredit;
import org.cocktail.fwkcktlcompta.client.metier.finders.EOPlanComptableExerFinder;
import org.cocktail.fwkcktlcompta.client.metier.finders.PlanComptablePourTypeCreditSpecification;
import org.cocktail.fwkcktlcompta.client.util.ClientCktlEOControlUtilities;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;
import org.cocktail.sapics.client.ApplicationClient;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.gui.PlanComptableSelectView;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class PlanComptableSelectCtrl {

    private static PlanComptableSelectCtrl sharedInstance = null;
    
    private PlanComptableSelectView myView;
    private ApplicationClient NSApp;
    private EOEditingContext ec;
    private EODisplayGroup eod;
    private NSArray currentComptes;
    private EOJefyAdminExercice currentExercice;
    
    public static PlanComptableSelectCtrl sharedInstance(EOEditingContext editingContext) {
        if (sharedInstance == null) {
            sharedInstance = new PlanComptableSelectCtrl(editingContext);
        }
        
        return sharedInstance;
    }
    
    private PlanComptableSelectCtrl(EOEditingContext editingContext)    {
        super();

        NSApp = (ApplicationClient) ApplicationClient.sharedApplication();
        ec = editingContext;
        eod = new EODisplayGroup();
        initView();
    }
    
    private void initView() {
        myView = new PlanComptableSelectView(new JFrame(), true, eod);

        myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerAction();
            }
        });
        
        myView.getButtonFind().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rechercherAction();
            }
        });
        
        myView.getjTextFieldSrchNumeroCompte().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rechercherAction();
            }
        });

        myView.getjTextFieldSrchLibelleCompte().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rechercherAction();
            }
        });

        myView.getMyEOTable().addListener(new ListenerPlanComptable());
    }
    
    public NSArray getComptes(EOExercice exercice) {
        currentExercice = chargerExercice(exercice);
        myView.getjTextFieldSrchNumeroCompte().setText("");
        myView.getjTextFieldSrchLibelleCompte().setText("");
        eod.setObjectArray(new NSArray());
        myView.getMyEOTable().updateData();
        
        myView.setVisible(true);

        if (eod.selectedObjects().count() == 0) {
            return null;
        }
        return eod.selectedObjects();            
    }

    protected EOJefyAdminExercice chargerExercice(EOExercice exercice) {
        Object pkValue = ClientCktlEOControlUtilities.primaryKeyObjectForObject(exercice);
        return EOJefyAdminExercice.fetchByKeyValue(ec, EOJefyAdminExercice.ENTITY_PRIMARY_KEY, pkValue);
    }
    
    protected void annulerAction() {
        eod.setSelectionIndexes(new NSArray());
        myView.dispose();
    }
    
    protected void rechercherAction()    {
        if (StringCtrl.chaineVide(myView.getjTextFieldSrchNumeroCompte().getText())
            && StringCtrl.chaineVide(myView.getjTextFieldSrchLibelleCompte().getText())) {
            EODialogs.runInformationDialog("ERREUR", "Veuillez saisir un numéro ou un libellé de compte.");
            return;
        }
        
        eod.setObjectArray(
            rechercher(
                myView.getjTextFieldSrchNumeroCompte().getText(), 
                myView.getjTextFieldSrchLibelleCompte().getText()));   
        myView.getMyEOTable().updateData();
    }
    
    protected NSArray rechercher(String numero, String libelle) {
        String compteValide = EOPlanComptableExer.etatValide;
        String compteTypeRelationValide = EOPlancoCredit.etatValide;
        EOJefyAdminExercice exercice = currentExercice;
        String compteTyperelationNature = EOPlanComptableExer.pcoNatureDepense;
        String typeCredit = EOTypeCredit.TCD_TYPE_DEPENSE;
        String compteNumeroPartiel = null;
        String compteLibellePartiel = null;
        
        if (!StringCtrl.chaineVide(numero)) {
            compteNumeroPartiel = numero;
        }
        
        if (!StringCtrl.chaineVide(libelle)) {
            compteLibellePartiel = libelle;
        }
        
        PlanComptablePourTypeCreditSpecification spec = new PlanComptablePourTypeCreditSpecification(
            compteValide, compteTypeRelationValide, exercice, compteTyperelationNature, typeCredit, compteLibellePartiel, compteNumeroPartiel);
        
        return EOPlanComptableExerFinder.findPlanComptableExerForTypeCredit(ec, spec);
    }
    
    private class ListenerPlanComptable implements ZEOTable.ZEOTableListener {
        public void onDbClick() {
            myView.dispose();
        }

        public void onSelectionChanged() {
            currentComptes = eod.selectedObjects();
        }
    }
}
