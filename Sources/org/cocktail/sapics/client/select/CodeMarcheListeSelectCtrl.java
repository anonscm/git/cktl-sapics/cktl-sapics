/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.sapics.client.eof.model.EOCodeExer;
import org.cocktail.sapics.client.eof.model.EOCodeMarche;
import org.cocktail.sapics.client.eof.model.EOExercice;
import org.cocktail.sapics.client.swing.ZEOTable;
import org.cocktail.sapics.client.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CodeMarcheListeSelectCtrl  {


	private static CodeMarcheListeSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private CodeMarcheListeSelectView myView;

	private EODisplayGroup eod;
	private EOExercice currentExercice;
	
	// currenCodeExer est un NSArray d'éléments de type EOCodeExer
	private NSArray currentCodeExer;

	/**
	 * 
	 *
	 */
	public CodeMarcheListeSelectCtrl(EOEditingContext editingContext)	{
		super();

		ec = editingContext;

		eod = new EODisplayGroup();
		
		myView = new CodeMarcheListeSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});
		
		myView.getMyEOTable().addListener(new ListenerCodeNomenclature());

		myView.getCheckNiveau2().setSelected(true);
		myView.getCheckNiveau3().setEnabled(false);
		
		myView.getCheckNiveau2().addActionListener(new CheckNiveauListener());
		myView.getCheckNiveau3().addActionListener(new CheckNiveauListener());

		myView.getTfFiltreLibelle().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreCode().getDocument().addDocumentListener(new ADocumentListener());
		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CodeMarcheListeSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CodeMarcheListeSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * Cette méthode affiche la fenêtre de sélection des codes marchés (cette fenêtre s'ouvre lors du clic sur le "+" vert
	 * dans l'onglet "Codes Nomenclatures" de la fenêtre principale).
	 * 
	 * L'instruction myView.setVisible(true) met le programme en attente tant qu'aucune sélection n'est faite.
	 * L'appel d'une instruction dispose() permet de fermer la fenêtre.
	 * 
	 * Voir aussi la méthode onSelectionChanged() ci-dessous.
	 * 
	 * @return Renvoie un NSArray d'éléments de type EOCodeExer, correspondant aux codes marchés sélectionnés par l'utilisateur à la souris
	 */
	public NSArray getCodeMarches(EOExercice exercice, Integer niveau)	{
		
		myView.setTitle("Codes Marchés " + exercice.exeExercice());
		
		if (eod.displayedObjects().count() == 0 || currentExercice == null || currentExercice.exeExercice().intValue() != exercice.exeExercice().intValue() )
			eod.setObjectArray(EOCodeExer.findForNiveau(ec, exercice, niveau));							

		currentExercice = exercice;

		filter();

		myView.setVisible(true);

		return currentCodeExer;

	}


	/**
	 * 
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getCheckNiveau2().isSelected())
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODEMARCHE_KEY+"."+EOCodeMarche.CM_NIVEAU_KEY + " = 2", null));
		else
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODEMARCHE_KEY+"."+EOCodeMarche.CM_NIVEAU_KEY + " = 3", null));

		if (!StringCtrl.chaineVide(myView.getTfFiltreCode().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreCode().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODEMARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltreLibelle().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreLibelle().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODEMARCHE_KEY+"."+EOCodeMarche.CM_LIB_KEY + " caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);        
	}

	/** 
	 *
	 */
	public void filter()	{

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}


	public void annuler() {
		
		currentCodeExer = null;
		myView.dispose();
		
	}  


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerCodeNomenclature implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			
			currentCodeExer =eod.selectedObjects();
			
		}
	}

    private class CheckNiveauListener implements ActionListener	{
        public CheckNiveauListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	filter();

        }
    }

    
	/**
	 * Permet de définir un listener sur le contenu du champ texte qui sert  a filtrer la table. 
	 * Dés que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


}
