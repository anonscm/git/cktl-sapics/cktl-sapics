/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client.utilities;

import java.awt.Color;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.ImageIcon;

import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class CocktailConstantes extends EOModalDialogController {

	protected static final EOClientResourceBundle resourceBundle = new EOClientResourceBundle();

	public static final String[] LISTE_MOIS = new String[]{"JANVIER", "FEVRIER", "MARS", "AVRIL", "MAI", "JUIN",
		"JUILLET", "AOUT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE"};
	public static final String[] LISTE_ANNEES = new String[]{"2004", "2005", "2006", "2007", "2008", "2009"};
	public static final String STRING_EURO = " \u20ac";
	
	// COULEURS

    public static final Color COLOR_REMUN    = new Color(255, 207, 213);
    public static final Color COLOR_SALARIAL = new Color(218, 221, 255);
    public static final Color COLOR_PATRONAL = new Color(205, 188, 255);

	public static final Color BG_COLOR_WHITE        = new Color(255, 255, 255);
	public static final Color BG_COLOR_BLACK        = new Color(0, 0, 0);
	public static final Color BG_COLOR_RED          = new Color(255, 0, 0);
	public static final Color BG_COLOR_GREEN        = new Color(0, 255, 0);
	public static final Color BG_COLOR_BLUE         = new Color(0, 0, 255);
	public static final Color BG_COLOR_YELLOW       = new Color(255, 255, 0);
	public static final Color BG_COLOR_GREY         = new Color(100, 100, 100);
	public static final Color BG_COLOR_LIGHT_GREY   = new Color(153, 153, 153);
	public static final Color BG_COLOR_LIGHTER_GREY = new Color(220, 220, 220);
	public static final Color BG_COLOR_DARK_GREY    = new Color(51, 51, 51);
	public static final Color BG_COLOR_CYAN         = new Color(224, 255, 255);
	public static final Color BG_COLOR_NUIT         = new Color(153, 153, 255);

	public static final Color COULEUR_FOND_INACTIF = new Color(222, 222, 222);
	public static final Color COULEUR_FOND_ACTIF   = BG_COLOR_WHITE;
	 
	public static final Color COLOR_INACTIVE_BACKGROUND = COULEUR_FOND_INACTIF;
	public static final Color COLOR_ACTIVE_BACKGROUND   = BG_COLOR_BLACK;

    public static final Color COLOR_BKG_TABLE_VIEW = new Color(230, 230, 230);
    public static final Color COLOR_SELECTED_ROW   = new Color(127, 155, 165);

    public static final Color COLOR_SELECT_NOMENCLATURES  = BG_COLOR_GREY;
    public static final Color COLOR_FOND_NOMENCLATURES    = COULEUR_FOND_INACTIF;
    public static final Color COLOR_FILTRES_NOMENCLATURES = new Color(240, 240, 240);
    
    public static final Color COLOR_LABEL_NUIT   = new Color(204, 204, 255);
    public static final Color COLOR_LABEL_SAUMON = new Color(255, 204, 204);

	// ICONES 
    // Icones communes aux differentes applications

	public static final ImageIcon ICON_CHIFFRE_1 		= (ImageIcon) resourceBundle.getObject("chiffre_1_16");
	public static final ImageIcon ICON_CHIFFRE_2 		= (ImageIcon) resourceBundle.getObject("chiffre_2_16");
	public static final ImageIcon ICON_CHIFFRE_3 		= (ImageIcon) resourceBundle.getObject("chiffre_3_16");
	public static final ImageIcon ICON_CHIFFRE_4 		= (ImageIcon) resourceBundle.getObject("chiffre_4_16");
	public static final ImageIcon ICON_CHIFFRE_5 		= (ImageIcon) resourceBundle.getObject("chiffre_5_16");
	public static final ImageIcon ICON_CHIFFRE_6 		= (ImageIcon) resourceBundle.getObject("chiffre_6_16");
	public static final ImageIcon ICON_CHIFFRE_7 		= (ImageIcon) resourceBundle.getObject("chiffre_7_16");
    
	public static final ImageIcon ICON_APP_LOGO 		= (ImageIcon) resourceBundle.getObject("cktl_logo_app_16");
	public static final ImageIcon ICON_ADD 				= (ImageIcon) resourceBundle.getObject("cktl_add_16");
	public static final ImageIcon ICON_ADD_22			= (ImageIcon) resourceBundle.getObject("cktl_plus_22");
	public static final ImageIcon ICON_UPDATE			= (ImageIcon) resourceBundle.getObject("cktl_update_16");
	public static final ImageIcon ICON_UPDATE_22	 	= (ImageIcon) resourceBundle.getObject("cktl_refresh_22");
	public static final ImageIcon ICON_DELETE 			= (ImageIcon) resourceBundle.getObject("cktl_delete_16");
	public static final ImageIcon ICON_DELETE_22 		= (ImageIcon) resourceBundle.getObject("cktl_delete_22");
	public static final ImageIcon ICON_VALID 			= (ImageIcon) resourceBundle.getObject("cktl_valid_16");
	public static final ImageIcon ICON_CANCEL 			= (ImageIcon) resourceBundle.getObject("cktl_cancel_16");
	public static final ImageIcon ICON_OK 				= (ImageIcon) resourceBundle.getObject("cktl_coche_16");	
	public static final ImageIcon ICON_CLOSE 			= (ImageIcon) resourceBundle.getObject("cktl_close_16");
	public static final ImageIcon ICON_PARAMS_16 		= (ImageIcon) resourceBundle.getObject("cktl_params_16");
	public static final ImageIcon ICON_PRINTER_16 		= (ImageIcon) resourceBundle.getObject("cktl_printer_16");
	public static final ImageIcon ICON_PRINTER_22 		= (ImageIcon) resourceBundle.getObject("cktl_printer_22");
    public static final ImageIcon ICON_EXCEL_16 		= (ImageIcon) resourceBundle.getObject("cktl_excel_16");
    public static final ImageIcon ICON_EXCEL_22 		= (ImageIcon) resourceBundle.getObject("cktl_excel_22");
	public static final ImageIcon ICON_ALERT_INFO 		= (ImageIcon) resourceBundle.getObject("alert-exclam");
	public static final ImageIcon ICON_ALERT_ERROR 		= (ImageIcon) resourceBundle.getObject("alert-error");
	public static final ImageIcon ICON_ALERT_OK 		= (ImageIcon) resourceBundle.getObject("alert-message");
    public static final ImageIcon ICON_OUTILS_16 		= (ImageIcon) resourceBundle.getObject("cktl_outils_16");
    public static final ImageIcon ICON_OUTILS_22 		= (ImageIcon) resourceBundle.getObject("cktl_outils_22");
    public static final ImageIcon ICON_DOWNLOAD_16 		= (ImageIcon) resourceBundle.getObject("cktl_download_16");
    public static final ImageIcon ICON_DOWNLOAD_22		= (ImageIcon) resourceBundle.getObject("cktl_download_22");
	public static final ImageIcon ICON_EURO_16 			= (ImageIcon) resourceBundle.getObject("cktl_euro_16");
	public static final ImageIcon ICON_LOUPE_16 		= (ImageIcon) resourceBundle.getObject("cktl_loupe_16");
	public static final ImageIcon ICON_LOUPE_22 		= (ImageIcon) resourceBundle.getObject("cktl_loupe_22");
	public static final ImageIcon ICON_SELECT_16 		= (ImageIcon) resourceBundle.getObject("cktl_why_16");
	public static final ImageIcon ICON_CALCULATE_16 	= (ImageIcon) resourceBundle.getObject("cktl_calculate_16");
	public static final ImageIcon ICON_EXIT_16 			= (ImageIcon) resourceBundle.getObject("cktl_exit_16");
	public static final ImageIcon ICON_WIZARD_22 		= (ImageIcon) resourceBundle.getObject("cktl_wizard_22");
	public static final ImageIcon ICON_INSPECTEUR_16 	= (ImageIcon) resourceBundle.getObject("cktl_inspecteur_16");
	public static final ImageIcon ICON_INFOS_16 		= (ImageIcon) resourceBundle.getObject("cktl_infos_16");
	public static final ImageIcon ICON_DOSSIER_22 		= (ImageIcon) resourceBundle.getObject("cktl_dossier_22");
	public static final ImageIcon ICON_CALENDAR 		= (ImageIcon) resourceBundle.getObject("cktl_calendar_16");

	public static final ImageIcon ICON_REFRESH_22	 		= (ImageIcon) resourceBundle.getObject("cktl_refresh_22");
	public static final ImageIcon ICON_REFRESH_32	 		= (ImageIcon) resourceBundle.getObject("cktl_refresh_32");
	public static final ImageIcon ICON_MAIL_22 				= (ImageIcon) resourceBundle.getObject("cktl_mail_22");
	public static final ImageIcon ICON_MAIL_32 				= (ImageIcon) resourceBundle.getObject("cktl_mail_32");
	public static final ImageIcon ICON_CORBEILLE_VIDE_32 	= (ImageIcon) resourceBundle.getObject("cktl_corbeille_vide_32");
	
	public static final ImageIcon ICON_BOMBE_16	 		= (ImageIcon) resourceBundle.getObject("cktl_bombe_16");
	public static final ImageIcon ICON_BOMBE_22	 		= (ImageIcon) resourceBundle.getObject("cktl_bombe_22");
    public static final ImageIcon ICON_FLECHE_D_32 		= (ImageIcon) resourceBundle.getObject("cktl_fleche_d_32");

    public static final ImageIcon ICON_EYE_OPEN 		= (ImageIcon) resourceBundle.getObject("eye_open");
    public static final ImageIcon ICON_EYE_CLOSE 		= (ImageIcon) resourceBundle.getObject("eye_close");

	// FORMATS 
    public static final Format FORMAT_EUROS     = NumberFormat.getCurrencyInstance(Locale.FRANCE);
    public static final Format FORMAT_NUMBER    = NumberFormat.getNumberInstance();
	public static final Format FORMAT_DECIMAL   = new DecimalFormat("#,##0.00");
    public static final Format FORMAT_DATESHORT = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);

    // Booleans
    /** valeur vraie d'un champ de type booleen */
    public static final String VRAI = "O";
    /** valeur fausse d'un champ de type booleen */
    public static final String FAUX = "N";

}