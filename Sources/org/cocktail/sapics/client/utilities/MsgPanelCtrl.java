/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.client.utilities;

import java.awt.Color;

import javax.swing.JFrame;

public class MsgPanelCtrl 
{
	private static MsgPanelCtrl sharedInstance;
		
	private MsgPanelView myView;
	    
    /**
     * 
     *
     */
    public MsgPanelCtrl() {

    	super();

    	myView = new MsgPanelView(new JFrame(), true);
    	
		myView.getBtnOk().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ok();
			}
		});
    	
    }
       
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static MsgPanelCtrl sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new MsgPanelCtrl();
		return sharedInstance;
	}
	
		
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runConfirmationDialog(String titre, String message) {

    	myView.getImage().setIcon(CocktailConstantes.ICON_ALERT_OK);
    	
    	myView.setTitle(titre);
    	myView.getConsole().setBackground(new Color(204,255,204));
    	myView.getConsole().setText(message);
        myView.setVisible(true);
    }
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runInformationDialog(String titre, String message) {

    	myView.getImage().setIcon(CocktailConstantes.ICON_ALERT_INFO);
    	
    	myView.setTitle(titre);
    	myView.getConsole().setBackground(new Color(251,236,208));

    	myView.getConsole().setText(message);
        myView.setVisible(true);
    }
    
    
    /**
     * 
     * @param titre
     * @param message
     */
    public void runErrorDialog(String titre, String message) {

    	myView.getImage().setIcon(CocktailConstantes.ICON_ALERT_ERROR);

    	myView.setTitle(titre);

    	myView.getConsole().setBackground(new Color(238, 153, 138));
    	myView.getConsole().setText(message);
    	
        myView.setVisible(true);
    }
    
    /**
     * 
     * @param sender
     */
    public void ok() {
    	myView.dispose();
    }
       
}