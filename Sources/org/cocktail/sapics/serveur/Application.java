/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sapics.serveur;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.TimeZone;

import org.cocktail.application.serveur.CocktailApplication;
import org.cocktail.application.serveur.eof.EOGrhumParametres;
import org.cocktail.application.serveur.eof.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.sapics.serveur.components.Main;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.foundation._NSUtilities;
import com.woinject.WOInject;

public class Application extends CocktailApplication {

	
	private static final String ADMIN_MAIL_KEY = "ADMIN_MAIL";
	public static final boolean SHOWSQLLOGS    = false;
	private	static final int LOG_OUT_TAILLE_MAXI = 100000;

	private	NSMutableArray utilisateursConnectes = new NSMutableArray();
	private MyByteArrayOutputStream	redirectedOutStream, redirectedErrStream; // Nouvelles sorties

	private NSMutableDictionary appParametres;

	public static void main(String argv[]) {
//		ERXApplication.main(argv, Application.class);
		NSLegacyBundleMonkeyPatch.apply();
        WOInject.init("org.cocktail.sapics.serveur.Application", argv);
	}

	public Application() {
		super();
		Properties prop = System.getProperties();
		addLogMessage("INFORMATION", "WOOutputPath = "
				+ prop.getProperty("WOOutputPath"));
		addLogMessage("INFORMATION", "WOPort = " + prop.getProperty("WOPort"));
		
		
		if (showSqlLogs()) {
		//	NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(-1);
		}
		else {
			System.out.println("Application.Application() - Affichage des logs SQL : inactif");
		}
		
		initTimeZones(getParam("DEFAULT_TIME_ZONE"));
		
		// redirection des logs
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));

		((NSLog.PrintStreamLogger)NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.err).setPrintStream(System.err);
		
		if (StringCtrl.isEmpty(getParam("REP_BASE_JASPER_PATH_LOCAL"))) {
			addLogMessage("ATTENTION", "Le paramètre REP_BASE_JASPER_PATH_LOCAL n'est pas défini, cela peut entrainer des erreurs pour certaines éditions !");
		}
		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}
		fixClassLoading();
	}

	private void fixClassLoading() {
		_NSUtilities.setClassForName(Main.class, Main.class.getSimpleName());
	}
	/**
	 * Initialise le TimeZone a utiliser pour l'application.
	 */
	private void initTimeZones(String timezone) {

		System.out.println("Application.initTimeZones() " + timezone);
		NSTimeZone theTimezone = NSTimeZone.timeZoneWithName("GMT", false); // Valeur par defaut
		if (timezone != null && !timezone.trim().equals("")) {
			NSTimeZone ntz = NSTimeZone.timeZoneWithName(timezone, false);
			if (ntz == null) {
				System.out.println("Le Timezone defini n'est pas valide (" + timezone + ")");
			}
			else {
				theTimezone = ntz;
			}
		}
		
		TimeZone.setDefault(theTimezone);
		NSTimeZone.setDefaultTimeZone(theTimezone);

		System.out.println("NSTimeZone par defaut utilise dans l'application : " + NSTimeZone.defaultTimeZone());
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		System.out.println("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone " + ntf.defaultParseTimeZone());
	}

	
	private String parametresTableName() {
		return EOGrhumParametres.ENTITY_NAME;
	}
	

	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (EOSharedEditingContext.defaultSharedEditingContext() == null || parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
			EOGenericRecord vTmpRec;
			String previousParamKey = null;
			NSMutableArray a = null;
			for (java.util.Enumeration<EOGenericRecord> enumerator = vParam.objectEnumerator();enumerator.hasMoreElements();) {
				vTmpRec = enumerator.nextElement();
				if (vTmpRec.valueForKey("paramKey") == null || ((String) vTmpRec.valueForKey("paramKey")).equals("")
						|| vTmpRec.valueForKey("paramValue") == null) {
					continue;
				}
				if (!((String) vTmpRec.valueForKey("paramKey")).equalsIgnoreCase(previousParamKey)) {
					if (a != null && a.count() > 0) {
						appParametres.setObjectForKey(a, previousParamKey);
					}
					previousParamKey = (String) vTmpRec.valueForKey("paramKey");
					a = new NSMutableArray();
				}
				if (vTmpRec.valueForKey("paramValue") != null) {
					a.addObject((String) vTmpRec.valueForKey("paramValue"));
				}
			}
			if (a != null && a.count() > 0) {
				appParametres.setObjectForKey(a, previousParamKey);
			}
			EOSharedEditingContext.defaultSharedEditingContext().invalidateAllObjects();
		}
		return appParametres;
	}

	public String getParam(String paramKey) {

		String res = config().stringForKey(paramKey);
		// recherche dans le configFileName()/configTableName()
		if (res == null) {
			NSArray a = (NSArray) appParametres().valueForKey(paramKey);
			if (a != null)
				res = (String) a.objectAtIndex(0);
		}

		return res;
	}
	
    public static String parametrePourCle(EOEditingContext ec,String cle) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + "=%@",new NSArray(cle));
			EOFetchSpecification fs = new EOFetchSpecification(EOGrhumParametres.ENTITY_NAME, qualifier, null);
			NSArray params = ec.objectsWithFetchSpecification(fs);
			return ((EOGrhumParametres)params.objectAtIndex(0)).paramValue();
		} catch(Exception e) {
			return null;
		}
    }

    /* (non-Javadoc)
	 * @see fr.univlr.cri.webapp.CRIWebApplication#getSessionTimeoutPage(com.webobjects.appserver.WOContext)
	 */
	public WOComponent getSessionTimeoutPage(WOContext arg0) {
		// TODO Auto-generated method stub
		return null;
		//return super.getSessionTimeoutPage(arg0);
	}
	
	
	protected boolean showSqlLogs() {
		if     (config().valueForKey("SHOWSQLLOGS") == null) {
			return SHOWSQLLOGS ;
		}
		return config().valueForKey("SHOWSQLLOGS").equals("1");
	} 
	
	public void cleanLogs()	{
		redirectedErrStream.reset();
		redirectedOutStream.reset();		
	}
	public String outLogs()	{
		return redirectedOutStream.toString();
	}

	public String errLogs()	{
		return redirectedErrStream.toString();
	}
	
	public String version() {
		return new VersionSapics().txtAppliVersion();
//		return versionApplication().txtAppliVersion();
		// return "1";//versionApplication().txtAppliVersion();
	}

	public String getMailHost() {
		return getMandatoryParameterFoKeyString("HOST_MAIL");
	}

	public String configFileName() {
		return "Sapics.config";
	}

	public String getVersionApp() {
		return version();
	}

	public String configTableName() {
		return "FwkCktlWebApp_GrhumParametres";
	}

	public String mainModelName() {
		return "Sapics";
	}
	
	private HashMap mySessions = new HashMap();

	public HashMap getMySessions() {
		return mySessions;
	}

	public void setMySessions(final HashMap mySessions) {
		this.mySessions = mySessions;
	}

	public boolean sendMail( String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) {
		return mailBus().sendMail(mailFrom, mailTo, "", mailSubject, mailBody);
	}
	
	/**
	 * Ecrit la liste des utilisateurs connetces dans la console.
	 */
	public final void listConnectedUsers() {

		for (Iterator iter = mySessions.values().iterator(); iter.hasNext();) {
			final Session element = (Session) iter.next();
			System.out.println("Application.listConnectedUsers()" + element.getInfoConnectedUser());
		}
	}
	public NSArray utilisateursConnectes()	{
		return utilisateursConnectes;
	}

	public void addToUtilisateurs(EOUtilisateur value)	{
		utilisateursConnectes.addObject(value);
	}

	public void removeFromUtilisateurs(EOUtilisateur value)	{
		utilisateursConnectes.removeObject(value);
	}

	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;
		public MyByteArrayOutputStream() {
			this(System.out);
		}
		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}
		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}
		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);	    			    		
			out.write(b, off, len);

			if (count > LOG_OUT_TAILLE_MAXI)	{
				String logServerOut = redirectedOutStream.toString();
				logServerOut = logServerOut.substring((LOG_OUT_TAILLE_MAXI / 2), logServerOut.length());

				// On reaffecte cette valeur a la variable redirectOutStream
				redirectedOutStream.reset();
				try {
					redirectedOutStream.write(logServerOut.getBytes());
				}
				catch (Exception e)	{}
			}

		}
	}

	public String getAdminMail() {
		return config().stringForKey(ADMIN_MAIL_KEY);
	}
}
