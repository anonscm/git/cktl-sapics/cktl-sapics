/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.serveur;
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frédéric (frederic.rivalland@univ-paris5.fr)



import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.cocktail.application.serveur.CocktailSession;
import org.cocktail.application.serveur.eof.EOUtilisateur;
import org.cocktail.sapics.serveur.eof.model.EOExercice;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class Session extends CocktailSession {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2376451171081890097L;
	Application	myApp;
	public EOEditingContext defaultEC;

	private String	login;

    public SessionBudget remoteCallBudget;

	public Session() {
		super();
			
		myApp = (Application)Application.application();

		defaultEC = defaultEditingContext();

		remoteCallBudget = new SessionBudget(myApp,defaultEC);

        connectedUser.setDateConnection(new NSTimestamp());
        
        myApp.getMySessions().put(sessionID(), this);

	}

	public void clientSideRequestBasculeExercice(NSDictionary parametres)  throws Exception	{
		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Number) parametres.objectForKey(EOExercice.EXE_EXERCICE_KEY), EOExercice.EXE_ORDRE_KEY);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "basculerCN", dicoProc);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "basculerSeuils", dicoProc);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "basculer3CMP", dicoProc);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "basculerMonopoles", dicoProc);
		} catch (Exception ex) {
			throw ex;
		}
	}

	 public boolean distributionContextShouldFollowKeyPath(EODistributionContext distributionContext, String path) {
		  return (path.startsWith("session"));
		    }

		/** verifie le login/mot de passe avec CRIPassword */
		public void clientSideRequestSetLoginParametres(String setlogin, String setIp) {

			login = setlogin;
			connectedUser.setIp(setIp);
			
		}

	public String clientSideRequestBdConnexionName() { return ((Application)WOApplication.application()).bdConnexionName();}
	public String clientSideRequestAppVersion() { return VersionMe.TXTVERSION;}

	
	
	/**
	 * 
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public final NSArray clientSideRequestSqlQuery(final String sql) throws Exception {
		try {
			return EOUtilities.rawRowsForSQL(defaultEditingContext(),myApp.mainModelName(), sql , null);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	} 
	
	
	/**  
	 * Recuperation d'une cle primaire 
	 *
	 * @param eo Objet pour lequel on veut recuperer la cle primaire
	 *
	 * @return Retourne un dictionnaire contenant les cles primaires et leur valeur
	 */
	public  NSDictionary clientSideRequestPrimaryKeyForObject(EOEnterpriseObject eo) 	{
		NSDictionary myResult = EOUtilities.primaryKeyForObject(defaultEditingContext(),eo);
		return myResult;
	}
	
	
	/**
	 * Permet d'envoyer un mail a partir du client.
	 * 
	 * @param ec
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) throws Exception {
		try {
			if (!myApp.sendMail(mailFrom,mailTo,mailCC, mailSubject,mailBody)) {
				throw new Exception ("Erreur lors de l'envoi du mail.");  
			}
		} catch (Throwable e) {
			throw new Exception (e.getMessage());
		}

	}

	public String clientSideRequestGetParam(String paramKey) {
		return myApp.getParam(paramKey);
	}
	
	/****************************************************************************************	
	  GESTION DES LOGS 
	****************************************************************************************/	

	public void clientSideRequestCleanLogs() throws Exception	{
		myApp.cleanLogs();
	}

	public String clientSideRequestOutLog() throws Exception	{
		return myApp.outLogs();
	}

	public String clientSideRequestErrLog() throws Exception	{
		return myApp.errLogs();
	}
	
	/****************************************************************************************/
	
	
/****************************************************************************************	
  GESTION DES UTILISATEURS CONNECTES
****************************************************************************************/	
	private final ConnectedUser connectedUser = new ConnectedUser();


	public String getInfoConnectedUser() {
		return connectedUser.toString();
	}

	public NSArray clientSideRequestGetConnectedUsers() {
		NSMutableArray t = new NSMutableArray();
		for (Iterator iter = myApp.getMySessions().values().iterator(); iter.hasNext();) {
			Session element = (Session) iter.next();
			t.addObject(element.connectedUser.toNSDictionary());
		}
		return t;
	}

	/**
	 * 
	 */
	 public void terminate() {

		myApp.getMySessions().remove(sessionID());
		super.terminate();
	 }


	 /**
	  * Represente un client connecte.
	  */
	 public final class ConnectedUser {
		 public Date getDateConnection() {
			 return dateConnection;
		 }

		 public void setDateConnection(Date dateConnection) {
			 this.dateConnection = dateConnection;
		 }

		 public String getIp() {
			 return ip;
		 }

		 public void setIp(String ip) {
			 this.ip = ip;
		 }

		 public String getLogin() {
			 return login;
		 }

		 private Date dateConnection;
		 private Date dateLastHeartBeat;

		 private String ip;
		 private String sessionID;

		 /**
		  * @see java.lang.Object#toString()
		  */
		  public String toString() {
			 return getLogin() + " connecte depuis le " + dateConnection + "(IP:" + getIp() + ")";
		  }

		  public String getSessionID() {
			  return sessionID;
		  }

		  public void setSessionID(String sessionID) {
			  this.sessionID = sessionID;
		  }

		  public Date getDateLastHeartBeat() {
			  return dateLastHeartBeat;
		  }

		  public void setDateLastHeartBeat(Date dateLastHeartBeat) {
			  this.dateLastHeartBeat = dateLastHeartBeat;
		  }

		  public HashMap toHashMap() {
			  HashMap t = new HashMap();
			  t.put("dateConnection", dateConnection);
			  t.put("sessionID", sessionID);
			  t.put("dateLastHeartBeat", dateLastHeartBeat);
			  t.put("ip", getIp());
			  t.put("login", getLogin());
			  return t;
		  }

		  public NSDictionary toNSDictionary() {
			  NSMutableDictionary t = new NSMutableDictionary();
			  t.takeValueForKey(dateConnection, "dateConnection");
			  t.takeValueForKey(sessionID, "sessionID");
			  t.takeValueForKey(dateLastHeartBeat, "dateLastHeartBeat");
			  t.takeValueForKey(getIp(), "ip");
			  t.takeValueForKey(getLogin(), "login");
			  return t;
		  }

	 }
	 
		/**
		 * 
		 * @return
		 * @throws Exception
		 */
		public NSArray clientSideRequestUtilisateursConnectes() throws Exception	{
			return myApp.utilisateursConnectes();
		}

		/**
		 * 
		 * @param idUtilisateur
		 * @throws Exception
		 */
		public void clientSideRequestAddToUtilisateursConnectes(NSDictionary parametres) throws Exception	{

			EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");
			System.out.println("SESSION - AJOUT UTILISATEUR : " + utilisateur.individu().nomUsuel());
			myApp.addToUtilisateurs(utilisateur);
		}

		/**
		 * 
		 * @param idUtilisateur
		 * @throws Exception
		 */
		public void removeFromUtilisateursConnectes(NSDictionary parametres) throws Exception	{

			EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");
			System.out.println("SESSION - REMOVE UTILISATEUR : " + utilisateur.individu().nomUsuel());
			myApp.removeFromUtilisateurs(utilisateur);
		}

/********************************************************************************************/	 
	
}
