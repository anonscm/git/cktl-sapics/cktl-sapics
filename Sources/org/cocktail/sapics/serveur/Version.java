package org.cocktail.sapics.serveur;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleServer;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWonder;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class Version extends A_CktlVersion {

	/** Version du frmk FwkCktlWebApp */
	private static final String CKTLWEBAPP_VERSION_MIN = "4.0.1";
	private static final String CKTLWEBAPP_VERSION_MAX = null;

	/** Version de WebObjects */
	private static final String WO_VERSION_MIN = "5.4.3";
	private static final String WO_VERSION_MAX = null;

	/** Version du JRE */
	private static final String JRE_VERSION_MIN = "1.5";
	private static final String JRE_VERSION_MAX = null;

	/** Version d'ORACLE */
	private static final String ORACLE_VERSION_MIN = "9.0";
	private static final String ORACLE_VERSION_MAX = null;

	/** Version de WONDER */
	private static final String WONDER_VERSION_MIN = "5.0";
	private static final String WONDER_VERSION_MAX = null;

	/** Version du framework JAVACLIENT */
	private static final String JAVA_CLIENT_VERSION_MIN = "2.2.1";
	private static final String JAVA_CLIENT_VERSION_MAX = null;

	/** {@inheritDoc} */
	@Override
	public String name() {
		return VersionMe.APPLICATIONFINALNAME;
	}

	/** {@inheritDoc} */
	@Override
	public int versionNumMaj() {
		return VersionMe.VERSIONNUMMAJ;
	}

	/** {@inheritDoc} */
	@Override
	public int versionNumMin() {
		return VersionMe.VERSIONNUMMIN;
	}

	/** {@inheritDoc} */
	@Override
	public int versionNumPatch() {
		return VersionMe.VERSIONNUMPATCH;
	}

	/** {@inheritDoc} */
	@Override
	public int versionNumBuild() {
		return VersionMe.VERSIONNUMBUILD;
	}

	/** {@inheritDoc} */
	public String date() {
		return VersionMe.VERSIONDATE;
	}

	/** {@inheritDoc} */
	@Override
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {new CktlVersionRequirements(new CktlVersionWebObjects(), WO_VERSION_MIN, WO_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionWonder(), WONDER_VERSION_MIN, WONDER_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionJava(), JRE_VERSION_MIN, JRE_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionOracleServer(), ORACLE_VERSION_MIN, ORACLE_VERSION_MAX, false),
				new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.Version(), CKTLWEBAPP_VERSION_MIN, CKTLWEBAPP_VERSION_MAX, true),
				new CktlVersionRequirements(new org.cocktail.application.serveur.Version(), JAVA_CLIENT_VERSION_MIN, JAVA_CLIENT_VERSION_MAX, true)};
	}

}
