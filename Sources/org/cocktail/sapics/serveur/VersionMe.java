package org.cocktail.sapics.serveur;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;

/**
 * Versions du serveur, mini du framework, mini de la base, nom de l'application
 */
public class VersionMe extends Object {
	// Nom de l'appli
	public static final String APPLICATIONFINALNAME = "Sapics - Gestion des Marchés";
	public static final String APPLICATIONINTERNALNAME = "Sapics";
	public static final String APPLICATION_STRID = "SAPICS";

	// Version minimum de la base de donnees (USER) necessaire pour fonctionner avec cette version
	public static final String MINDBVERSION = "2.1.0";

	// Version appli
	public static final int VERSIONNUMMAJ;
	public static final int VERSIONNUMMIN;
	public static final int VERSIONNUMPATCH;
	public static final int VERSIONNUMBUILD;

	public static final String VERSIONDATE;
	public static final String COMMENT = null;

	static {
		VersionApp versionApp = Application.application().injector().getInstance(VersionApp.class);
		VERSIONNUMMAJ = versionApp.majVersion();
		VERSIONNUMMIN = versionApp.minVersion();
		VERSIONNUMPATCH = versionApp.patchVersion();
		VERSIONNUMBUILD = versionApp.buildVersion();
		VERSIONDATE = versionApp.dateVersion();
	}

	public static final String VERSIONNUM = VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD;



	public static final String HTMLVERSION = "<font color=\"#FF0000\"><b>Version " + VERSIONNUM + " du  " + VERSIONDATE
			+ "</b></font>";
	public static final String TXTVERSION = "Version " + VERSIONNUM + " du " + VERSIONDATE;
	public static final String COPYRIGHT = "(c) " + VERSIONDATE.substring(VERSIONDATE.lastIndexOf("/") + 1)
			+ " Association Cocktail";

	/**
	 * Affiche le numéro de version formaté
	 * 
	 * @param args
	 *            Args
	 */
	public static void main(String[] args) {
		System.out.println("" + VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD);
	}
}
