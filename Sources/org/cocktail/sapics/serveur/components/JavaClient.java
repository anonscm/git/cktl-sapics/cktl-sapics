/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.serveur.components;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

//		Texte complet de la licence a l'url suivante :
//		http://www.cecill.info/licences/Licence_CeCILL_V1-fr.html

//		Developpement dans le cadre des activités du Consortium Cocktail : http://www.cocktail.org
//		Auteur : Rivalland Frédéric (frederic.rivalland@univ-paris5.fr)


//
//  JavaClient.java
//  Abricot
//
//  Created by Frederic Rivalland on Mon Oct 23 2006.
//  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
//




public class JavaClient extends WOComponent {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8640932240607841971L;

	/** @TypeInfo java.lang.String */

    public JavaClient(WOContext context) {
        super(context);
    }
}
