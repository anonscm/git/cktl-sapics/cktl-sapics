// _EOAttributionHist.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAttributionHist.java instead.
package org.cocktail.sapics.serveur.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAttributionHist extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "AttributionHist";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.ATTRIBUTION_HIST";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ahOrdre";

	public static final String AH_ACCEPTEE_KEY = "ahAcceptee";
	public static final String AH_DATE_KEY = "ahDate";
	public static final String AH_DEBUT_KEY = "ahDebut";
	public static final String AH_FIN_KEY = "ahFin";
	public static final String AH_HT_KEY = "ahHt";
	public static final String AH_LIBELLE_KEY = "ahLibelle";
	public static final String AH_SUPPR_KEY = "ahSuppr";
	public static final String AH_TYPE_CONTROLE_KEY = "ahTypeControle";
	public static final String AH_VALIDE_KEY = "ahValide";

// Attributs non visibles
	public static final String AH_ORDRE_KEY = "ahOrdre";
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String AH_ACCEPTEE_COLKEY = "AH_ACCEPTEE";
	public static final String AH_DATE_COLKEY = "AH_DATE";
	public static final String AH_DEBUT_COLKEY = "AH_DEBUT";
	public static final String AH_FIN_COLKEY = "AH_FIN";
	public static final String AH_HT_COLKEY = "AH_HT";
	public static final String AH_LIBELLE_COLKEY = "AH_LIBELLE";
	public static final String AH_SUPPR_COLKEY = "AH_SUPPR";
	public static final String AH_TYPE_CONTROLE_COLKEY = "AH_TYPE_CONTROLE";
	public static final String AH_VALIDE_COLKEY = "AH_VALIDE";

	public static final String AH_ORDRE_COLKEY = "AH_ORDRE";
	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String FOURNIS_KEY = "fournis";
	public static final String LOT_KEY = "lot";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String ahAcceptee() {
    return (String) storedValueForKey(AH_ACCEPTEE_KEY);
  }

  public void setAhAcceptee(String value) {
    takeStoredValueForKey(value, AH_ACCEPTEE_KEY);
  }

  public NSTimestamp ahDate() {
    return (NSTimestamp) storedValueForKey(AH_DATE_KEY);
  }

  public void setAhDate(NSTimestamp value) {
    takeStoredValueForKey(value, AH_DATE_KEY);
  }

  public NSTimestamp ahDebut() {
    return (NSTimestamp) storedValueForKey(AH_DEBUT_KEY);
  }

  public void setAhDebut(NSTimestamp value) {
    takeStoredValueForKey(value, AH_DEBUT_KEY);
  }

  public NSTimestamp ahFin() {
    return (NSTimestamp) storedValueForKey(AH_FIN_KEY);
  }

  public void setAhFin(NSTimestamp value) {
    takeStoredValueForKey(value, AH_FIN_KEY);
  }

  public java.math.BigDecimal ahHt() {
    return (java.math.BigDecimal) storedValueForKey(AH_HT_KEY);
  }

  public void setAhHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AH_HT_KEY);
  }

  public String ahLibelle() {
    return (String) storedValueForKey(AH_LIBELLE_KEY);
  }

  public void setAhLibelle(String value) {
    takeStoredValueForKey(value, AH_LIBELLE_KEY);
  }

  public String ahSuppr() {
    return (String) storedValueForKey(AH_SUPPR_KEY);
  }

  public void setAhSuppr(String value) {
    takeStoredValueForKey(value, AH_SUPPR_KEY);
  }

  public String ahTypeControle() {
    return (String) storedValueForKey(AH_TYPE_CONTROLE_KEY);
  }

  public void setAhTypeControle(String value) {
    takeStoredValueForKey(value, AH_TYPE_CONTROLE_KEY);
  }

  public String ahValide() {
    return (String) storedValueForKey(AH_VALIDE_KEY);
  }

  public void setAhValide(String value) {
    takeStoredValueForKey(value, AH_VALIDE_KEY);
  }

  public org.cocktail.sapics.serveur.eof.model.EOAttribution attribution() {
    return (org.cocktail.sapics.serveur.eof.model.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
  }

  public void setAttributionRelationship(org.cocktail.sapics.serveur.eof.model.EOAttribution value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOAttribution oldValue = attribution();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
    }
  }
  
  public org.cocktail.sapics.serveur.eof.model.EOFournis fournis() {
    return (org.cocktail.sapics.serveur.eof.model.EOFournis)storedValueForKey(FOURNIS_KEY);
  }

  public void setFournisRelationship(org.cocktail.sapics.serveur.eof.model.EOFournis value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOFournis oldValue = fournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_KEY);
    }
  }
  
  public org.cocktail.sapics.serveur.eof.model.EOLot lot() {
    return (org.cocktail.sapics.serveur.eof.model.EOLot)storedValueForKey(LOT_KEY);
  }

  public void setLotRelationship(org.cocktail.sapics.serveur.eof.model.EOLot value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOLot oldValue = lot();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
    }
  }
  

/**
 * Créer une instance de EOAttributionHist avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAttributionHist createEOAttributionHist(EOEditingContext editingContext, org.cocktail.sapics.serveur.eof.model.EOAttribution attribution, org.cocktail.sapics.serveur.eof.model.EOFournis fournis, org.cocktail.sapics.serveur.eof.model.EOLot lot			) {
    EOAttributionHist eo = (EOAttributionHist) createAndInsertInstance(editingContext, _EOAttributionHist.ENTITY_NAME);    
    eo.setAttributionRelationship(attribution);
    eo.setFournisRelationship(fournis);
    eo.setLotRelationship(lot);
    return eo;
  }

  
	  public EOAttributionHist localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAttributionHist)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAttributionHist creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAttributionHist creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAttributionHist object = (EOAttributionHist)createAndInsertInstance(editingContext, _EOAttributionHist.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAttributionHist localInstanceIn(EOEditingContext editingContext, EOAttributionHist eo) {
    EOAttributionHist localInstance = (eo == null) ? null : (EOAttributionHist)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAttributionHist#localInstanceIn a la place.
   */
	public static EOAttributionHist localInstanceOf(EOEditingContext editingContext, EOAttributionHist eo) {
		return EOAttributionHist.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAttributionHist fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAttributionHist fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAttributionHist eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAttributionHist)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAttributionHist fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAttributionHist fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAttributionHist eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAttributionHist)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAttributionHist fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAttributionHist eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAttributionHist ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAttributionHist fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
