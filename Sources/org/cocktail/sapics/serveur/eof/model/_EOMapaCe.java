// _EOMapaCe.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMapaCe.java instead.
package org.cocktail.sapics.serveur.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMapaCe extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "MapaCe";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.MAPA_CE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "mcId";

	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String MC_SUPP_KEY = "mcSupp";

// Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String MC_ID_KEY = "mcId";
	public static final String MTE_ID_KEY = "mteId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String MC_SUPP_COLKEY = "MC_SUPP";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String MC_ID_COLKEY = "MC_ID";
	public static final String MTE_ID_COLKEY = "MTE_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String MAPA_TRANCHE_EXER_KEY = "mapaTrancheExer";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public String mcSupp() {
    return (String) storedValueForKey(MC_SUPP_KEY);
  }

  public void setMcSupp(String value) {
    takeStoredValueForKey(value, MC_SUPP_KEY);
  }

  public org.cocktail.sapics.serveur.eof.model.EOCodeExer codeExer() {
    return (org.cocktail.sapics.serveur.eof.model.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
  }

  public void setCodeExerRelationship(org.cocktail.sapics.serveur.eof.model.EOCodeExer value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOCodeExer oldValue = codeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
    }
  }
  
  public org.cocktail.sapics.serveur.eof.model.EOMapaTrancheExer mapaTrancheExer() {
    return (org.cocktail.sapics.serveur.eof.model.EOMapaTrancheExer)storedValueForKey(MAPA_TRANCHE_EXER_KEY);
  }

  public void setMapaTrancheExerRelationship(org.cocktail.sapics.serveur.eof.model.EOMapaTrancheExer value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOMapaTrancheExer oldValue = mapaTrancheExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MAPA_TRANCHE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MAPA_TRANCHE_EXER_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOMapaCe avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMapaCe createEOMapaCe(EOEditingContext editingContext, String mcSupp
, org.cocktail.sapics.serveur.eof.model.EOCodeExer codeExer, org.cocktail.sapics.serveur.eof.model.EOMapaTrancheExer mapaTrancheExer, org.cocktail.application.serveur.eof.EOUtilisateur utilisateur			) {
    EOMapaCe eo = (EOMapaCe) createAndInsertInstance(editingContext, _EOMapaCe.ENTITY_NAME);    
		eo.setMcSupp(mcSupp);
    eo.setCodeExerRelationship(codeExer);
    eo.setMapaTrancheExerRelationship(mapaTrancheExer);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOMapaCe localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMapaCe)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMapaCe creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMapaCe creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOMapaCe object = (EOMapaCe)createAndInsertInstance(editingContext, _EOMapaCe.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMapaCe localInstanceIn(EOEditingContext editingContext, EOMapaCe eo) {
    EOMapaCe localInstance = (eo == null) ? null : (EOMapaCe)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMapaCe#localInstanceIn a la place.
   */
	public static EOMapaCe localInstanceOf(EOEditingContext editingContext, EOMapaCe eo) {
		return EOMapaCe.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMapaCe fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMapaCe fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMapaCe eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMapaCe)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMapaCe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMapaCe fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMapaCe eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMapaCe)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMapaCe fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMapaCe eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMapaCe ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMapaCe fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
