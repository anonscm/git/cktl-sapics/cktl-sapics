// _EORevisionLot.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORevisionLot.java instead.
package org.cocktail.sapics.serveur.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORevisionLot extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "RevisionLot";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.REVISION_LOT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rlOrdre";

	public static final String RL_DATE_KEY = "rlDate";
	public static final String RL_DIFF_KEY = "rlDiff";
	public static final String RL_LIBELLE_KEY = "rlLibelle";
	public static final String RL_SUPPR_KEY = "rlSuppr";
	public static final String RL_TYPE_KEY = "rlType";

// Attributs non visibles
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String RL_ORDRE_KEY = "rlOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RL_DATE_COLKEY = "RL_DATE";
	public static final String RL_DIFF_COLKEY = "RL_DIFF";
	public static final String RL_LIBELLE_COLKEY = "RL_LIBELLE";
	public static final String RL_SUPPR_COLKEY = "RL_SUPPR";
	public static final String RL_TYPE_COLKEY = "RL_TYPE";

	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String RL_ORDRE_COLKEY = "RL_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String LOT_KEY = "lot";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp rlDate() {
    return (NSTimestamp) storedValueForKey(RL_DATE_KEY);
  }

  public void setRlDate(NSTimestamp value) {
    takeStoredValueForKey(value, RL_DATE_KEY);
  }

  public java.math.BigDecimal rlDiff() {
    return (java.math.BigDecimal) storedValueForKey(RL_DIFF_KEY);
  }

  public void setRlDiff(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RL_DIFF_KEY);
  }

  public String rlLibelle() {
    return (String) storedValueForKey(RL_LIBELLE_KEY);
  }

  public void setRlLibelle(String value) {
    takeStoredValueForKey(value, RL_LIBELLE_KEY);
  }

  public String rlSuppr() {
    return (String) storedValueForKey(RL_SUPPR_KEY);
  }

  public void setRlSuppr(String value) {
    takeStoredValueForKey(value, RL_SUPPR_KEY);
  }

  public String rlType() {
    return (String) storedValueForKey(RL_TYPE_KEY);
  }

  public void setRlType(String value) {
    takeStoredValueForKey(value, RL_TYPE_KEY);
  }

  public org.cocktail.sapics.serveur.eof.model.EOLot lot() {
    return (org.cocktail.sapics.serveur.eof.model.EOLot)storedValueForKey(LOT_KEY);
  }

  public void setLotRelationship(org.cocktail.sapics.serveur.eof.model.EOLot value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOLot oldValue = lot();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EORevisionLot avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORevisionLot createEORevisionLot(EOEditingContext editingContext, NSTimestamp rlDate
, java.math.BigDecimal rlDiff
, String rlLibelle
, String rlSuppr
, String rlType
, org.cocktail.sapics.serveur.eof.model.EOLot lot, org.cocktail.application.serveur.eof.EOUtilisateur utilisateur			) {
    EORevisionLot eo = (EORevisionLot) createAndInsertInstance(editingContext, _EORevisionLot.ENTITY_NAME);    
		eo.setRlDate(rlDate);
		eo.setRlDiff(rlDiff);
		eo.setRlLibelle(rlLibelle);
		eo.setRlSuppr(rlSuppr);
		eo.setRlType(rlType);
    eo.setLotRelationship(lot);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EORevisionLot localInstanceIn(EOEditingContext editingContext) {
	  		return (EORevisionLot)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORevisionLot creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORevisionLot creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORevisionLot object = (EORevisionLot)createAndInsertInstance(editingContext, _EORevisionLot.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORevisionLot localInstanceIn(EOEditingContext editingContext, EORevisionLot eo) {
    EORevisionLot localInstance = (eo == null) ? null : (EORevisionLot)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORevisionLot#localInstanceIn a la place.
   */
	public static EORevisionLot localInstanceOf(EOEditingContext editingContext, EORevisionLot eo) {
		return EORevisionLot.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORevisionLot fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORevisionLot fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORevisionLot eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORevisionLot)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORevisionLot fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORevisionLot fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORevisionLot eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORevisionLot)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORevisionLot fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORevisionLot eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORevisionLot ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORevisionLot fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
