// _EOSousTraitant.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSousTraitant.java instead.
package org.cocktail.sapics.serveur.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSousTraitant extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "SousTraitant";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.SOUS_TRAITANT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "stOrdre";

	public static final String ST_MONTANT_KEY = "stMontant";

// Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String RIBORDRE_KEY = "ribordre";
	public static final String ST_ORDRE_KEY = "stOrdre";

//Colonnes dans la base de donnees
	public static final String ST_MONTANT_COLKEY = "ST_MONTANT";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String RIBORDRE_COLKEY = "RIB_ORDRE";
	public static final String ST_ORDRE_COLKEY = "ST_ORDRE";


	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String RIB_KEY = "rib";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal stMontant() {
    return (java.math.BigDecimal) storedValueForKey(ST_MONTANT_KEY);
  }

  public void setStMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ST_MONTANT_KEY);
  }

  public org.cocktail.sapics.serveur.eof.model.EOAttribution attribution() {
    return (org.cocktail.sapics.serveur.eof.model.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
  }

  public void setAttributionRelationship(org.cocktail.sapics.serveur.eof.model.EOAttribution value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOAttribution oldValue = attribution();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
    }
  }
  
  public org.cocktail.sapics.serveur.eof.model.EOFournis fournisseur() {
    return (org.cocktail.sapics.serveur.eof.model.EOFournis)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.sapics.serveur.eof.model.EOFournis value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOFournis oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.sapics.serveur.eof.model.EOVRib rib() {
    return (org.cocktail.sapics.serveur.eof.model.EOVRib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.sapics.serveur.eof.model.EOVRib value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOVRib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  

/**
 * Créer une instance de EOSousTraitant avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSousTraitant createEOSousTraitant(EOEditingContext editingContext, java.math.BigDecimal stMontant
, org.cocktail.sapics.serveur.eof.model.EOAttribution attribution, org.cocktail.sapics.serveur.eof.model.EOFournis fournisseur			) {
    EOSousTraitant eo = (EOSousTraitant) createAndInsertInstance(editingContext, _EOSousTraitant.ENTITY_NAME);    
		eo.setStMontant(stMontant);
    eo.setAttributionRelationship(attribution);
    eo.setFournisseurRelationship(fournisseur);
    return eo;
  }

  
	  public EOSousTraitant localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSousTraitant)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSousTraitant creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSousTraitant creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOSousTraitant object = (EOSousTraitant)createAndInsertInstance(editingContext, _EOSousTraitant.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSousTraitant localInstanceIn(EOEditingContext editingContext, EOSousTraitant eo) {
    EOSousTraitant localInstance = (eo == null) ? null : (EOSousTraitant)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSousTraitant#localInstanceIn a la place.
   */
	public static EOSousTraitant localInstanceOf(EOEditingContext editingContext, EOSousTraitant eo) {
		return EOSousTraitant.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSousTraitant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSousTraitant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSousTraitant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSousTraitant)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSousTraitant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSousTraitant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSousTraitant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSousTraitant)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSousTraitant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSousTraitant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSousTraitant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSousTraitant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
