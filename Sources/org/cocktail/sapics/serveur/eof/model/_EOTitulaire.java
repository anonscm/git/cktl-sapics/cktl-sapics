// _EOTitulaire.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitulaire.java instead.
package org.cocktail.sapics.serveur.eof.model;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTitulaire extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Titulaire";
	public static final String ENTITY_TABLE_NAME = "JEFY_MARCHES.TITULAIRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "titOrdre";


// Attributs non visibles
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String TIT_ORDRE_KEY = "titOrdre";
	public static final String TIT_PERE_KEY = "titPere";

//Colonnes dans la base de donnees

	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";
	public static final String TIT_PERE_COLKEY = "TIT_PERE";


	// Relationships
	public static final String ATTRIBUTIONS_KEY = "attributions";
	public static final String CO_TRAITANTS_KEY = "coTraitants";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String LOT_KEY = "lot";
	public static final String TIT_PER_KEY = "titPer";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public org.cocktail.sapics.serveur.eof.model.EOFournis fournisseur() {
    return (org.cocktail.sapics.serveur.eof.model.EOFournis)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.sapics.serveur.eof.model.EOFournis value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOFournis oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.sapics.serveur.eof.model.EOLot lot() {
    return (org.cocktail.sapics.serveur.eof.model.EOLot)storedValueForKey(LOT_KEY);
  }

  public void setLotRelationship(org.cocktail.sapics.serveur.eof.model.EOLot value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOLot oldValue = lot();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
    }
  }
  
  public org.cocktail.sapics.serveur.eof.model.EOTitulaire titPer() {
    return (org.cocktail.sapics.serveur.eof.model.EOTitulaire)storedValueForKey(TIT_PER_KEY);
  }

  public void setTitPerRelationship(org.cocktail.sapics.serveur.eof.model.EOTitulaire value) {
    if (value == null) {
    	org.cocktail.sapics.serveur.eof.model.EOTitulaire oldValue = titPer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TIT_PER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TIT_PER_KEY);
    }
  }
  
  public NSArray attributions() {
    return (NSArray)storedValueForKey(ATTRIBUTIONS_KEY);
  }

  public NSArray attributions(EOQualifier qualifier) {
    return attributions(qualifier, null, false);
  }

  public NSArray attributions(EOQualifier qualifier, boolean fetch) {
    return attributions(qualifier, null, fetch);
  }

  public NSArray attributions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.sapics.serveur.eof.model.EOAttribution.TITULAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.sapics.serveur.eof.model.EOAttribution.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = attributions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAttributionsRelationship(org.cocktail.sapics.serveur.eof.model.EOAttribution object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ATTRIBUTIONS_KEY);
  }

  public void removeFromAttributionsRelationship(org.cocktail.sapics.serveur.eof.model.EOAttribution object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ATTRIBUTIONS_KEY);
  }

  public org.cocktail.sapics.serveur.eof.model.EOAttribution createAttributionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Attribution");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ATTRIBUTIONS_KEY);
    return (org.cocktail.sapics.serveur.eof.model.EOAttribution) eo;
  }

  public void deleteAttributionsRelationship(org.cocktail.sapics.serveur.eof.model.EOAttribution object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ATTRIBUTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAttributionsRelationships() {
    Enumeration objects = attributions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAttributionsRelationship((org.cocktail.sapics.serveur.eof.model.EOAttribution)objects.nextElement());
    }
  }

  public NSArray coTraitants() {
    return (NSArray)storedValueForKey(CO_TRAITANTS_KEY);
  }

  public NSArray coTraitants(EOQualifier qualifier) {
    return coTraitants(qualifier, null, false);
  }

  public NSArray coTraitants(EOQualifier qualifier, boolean fetch) {
    return coTraitants(qualifier, null, fetch);
  }

  public NSArray coTraitants(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.sapics.serveur.eof.model.EOTitulaire.TIT_PER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.sapics.serveur.eof.model.EOTitulaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = coTraitants();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCoTraitantsRelationship(org.cocktail.sapics.serveur.eof.model.EOTitulaire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CO_TRAITANTS_KEY);
  }

  public void removeFromCoTraitantsRelationship(org.cocktail.sapics.serveur.eof.model.EOTitulaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CO_TRAITANTS_KEY);
  }

  public org.cocktail.sapics.serveur.eof.model.EOTitulaire createCoTraitantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Titulaire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CO_TRAITANTS_KEY);
    return (org.cocktail.sapics.serveur.eof.model.EOTitulaire) eo;
  }

  public void deleteCoTraitantsRelationship(org.cocktail.sapics.serveur.eof.model.EOTitulaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CO_TRAITANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCoTraitantsRelationships() {
    Enumeration objects = coTraitants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCoTraitantsRelationship((org.cocktail.sapics.serveur.eof.model.EOTitulaire)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOTitulaire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTitulaire createEOTitulaire(EOEditingContext editingContext, org.cocktail.sapics.serveur.eof.model.EOFournis fournisseur, org.cocktail.sapics.serveur.eof.model.EOLot lot			) {
    EOTitulaire eo = (EOTitulaire) createAndInsertInstance(editingContext, _EOTitulaire.ENTITY_NAME);    
    eo.setFournisseurRelationship(fournisseur);
    eo.setLotRelationship(lot);
    return eo;
  }

  
	  public EOTitulaire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTitulaire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitulaire creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitulaire creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTitulaire object = (EOTitulaire)createAndInsertInstance(editingContext, _EOTitulaire.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTitulaire localInstanceIn(EOEditingContext editingContext, EOTitulaire eo) {
    EOTitulaire localInstance = (eo == null) ? null : (EOTitulaire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTitulaire#localInstanceIn a la place.
   */
	public static EOTitulaire localInstanceOf(EOEditingContext editingContext, EOTitulaire eo) {
		return EOTitulaire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTitulaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTitulaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitulaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitulaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitulaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitulaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitulaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitulaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTitulaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitulaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitulaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitulaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
