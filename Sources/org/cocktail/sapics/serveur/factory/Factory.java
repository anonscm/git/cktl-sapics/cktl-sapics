/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.sapics.serveur.factory;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Factory {
	
	/**
	 * 
	 * Creation d'une nouvelle instance d'objet dans l'editingContext ec
	 * 
	 * @param ec
	 * @param entity
	 * @return
	 */
	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {
		
		EOClassDescription description = (EOClassDescription) EOClassDescription.classDescriptionForEntityName(entity);
		
		EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);
		
		return object;
		
	}
	
	public static final NSArray fetchArray(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings, boolean refreshObjects) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		final EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}
	
	
	public static final NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qual, NSArray sortOrderings, boolean refreshObjects ){
		final EOFetchSpecification spec = new EOFetchSpecification(entityName,qual,null,true,true,null);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}
	
	
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings, boolean refreshObjects) {
		NSArray res = fetchArray(ec, entityName, conditionStr, params, sortOrderings, refreshObjects);
		if ((res == null) || (res.count() == 0)) {
			return null;
		} else {
			return (EOEnterpriseObject) res.objectAtIndex(0);
		}
	}
	
	
	public float computeSumForKey(NSArray eo, String key) {
		
		float total = 0;
		int i = 0;
		while (i < eo.count()) {
			total = total + ((Number) ((EOEnterpriseObject) eo.objectAtIndex(i)).valueForKey(key)).floatValue();
			i = i + 1;
		}
		return (float) total;
	}
	
	protected final void executeStoredProcedure(EOEditingContext ed, String laProcedure, NSDictionary lesParametres) {
		System.out.println("Factory.executeStoredProcedure() - Execution proc = " + laProcedure);
		System.out.println("Factory.executeStoredProcedure() - Params = " + lesParametres);
		EOUtilities.executeStoredProcedureNamed(ed, laProcedure, lesParametres);
		
	}
	
	/**
	 * 
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sortOrderings, boolean refresh)	{
		
		try {
			EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sortOrderings);
			fs.setRefreshesRefetchedObjects(refresh);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String stringQualifier, NSArray sortOrderings, boolean refresh)	{
		
		try {
			
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, null);
			
			EOFetchSpecification fs = new EOFetchSpecification(entityName, myQualifier, sortOrderings);
			fs.setRefreshesRefetchedObjects(refresh);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{
		
		try {
			EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
			fs.setRefreshesRefetchedObjects(true);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{
		
		EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
		
	}

}
