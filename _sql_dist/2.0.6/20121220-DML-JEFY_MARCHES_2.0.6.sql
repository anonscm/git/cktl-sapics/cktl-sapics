--
-- Patch DML de JEFY_MARCHES du 20/12/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2
-- Type : DML
-- Schema : JEFY_MARCHES
-- Numero de version : 2.0.6
-- Date de publication : 20/12/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
INSERT INTO jefy_marches.DB_VERSION VALUES(jefy_marches.DB_VERSION_SEQ.NEXTVAL, '2.0.6', sysdate,NULL,'Arret de la bascule des codes marches niveau 3');

declare
begin
JEFY_ADMIN.Api_Application.REG_PREPARE_EXERCICE ( 13,  'jefy_marches.bascule_marches.bascule_exercice', 60 );
end;
/
commit;

--
-- DB_VERSION
--
UPDATE JEFY_MARCHES.DB_VERSION SET DBV_INSTALL = SYSDATE WHERE DBV_LIBELLE='2.0.6';


COMMIT;