--
-- Patch DDL de JEFY_MARCHES du 28/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : JEFY_MARCHES
-- Numero de version : 2.0.8.0
-- Date de publication : 28/10/2013
-- Auteur(s) : Julien Blandineau

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


------------------------------------------------------------------------------------------------------------------------
-- Supprime les modifications effectuées par ce script si déjà passé
------------------------------------------------------------------------------------------------------------------------
/*******
begin 
  execute immediate 'DROP TABLE JEFY_MARCHES.LOG_UTILS'; exception when others then null; 
end; 
/
begin 
  execute immediate 'DROP TABLE JEFY_MARCHES.REFERENCE_NACRES'; exception when others then null; 
end;  
/
begin 
  execute immediate 'ALTER TABLE JEFY_MARCHES.code_marche DROP CONSTRAINT FK_TCM_ID'; exception when others then null; 
end;  
/
begin
  execute immediate 'ALTER TABLE JEFY_MARCHES.code_marche DROP COLUMN TCM_ID'; exception when others then null; 
end;
/
begin 
  execute immediate 'DROP TABLE JEFY_MARCHES.TYPE_CODE_MARCHE'; exception when others then null; 
end;  
/
begin 
  execute immediate 'DROP TABLE JEFY_MARCHES.CORRESPONDANCE_CN'; exception when others then null; 
end;  
/
begin 
  execute immediate 'DROP SEQUENCE JEFY_MARCHES.log_utils_seq'; exception when others then null; 
end;  
/
begin 
  execute immediate 'DROP SEQUENCE JEFY_MARCHES.reference_nacres_seq'; exception when others then null; 
end;  
/
*******/
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
-- Création d'une table de logs
CREATE TABLE JEFY_MARCHES.LOG_UTILS
(
  LOG_ID       NUMBER NOT NULL,
  LOG_DATE     DATE   NOT NULL,
  LOG_DESC    VARCHAR2(2000),

  CONSTRAINT PK_LOG_UTILS PRIMARY KEY (LOG_ID)
);
COMMENT ON COLUMN JEFY_MARCHES.LOG_UTILS.LOG_ID    IS 'Identifiant';
COMMENT ON COLUMN JEFY_MARCHES.LOG_UTILS.LOG_DATE  IS 'Date de création';
COMMENT ON COLUMN JEFY_MARCHES.LOG_UTILS.LOG_DESC  IS 'Contenu du log';

CREATE SEQUENCE JEFY_MARCHES.log_utils_seq
  START WITH 1000 INCREMENT BY 1 
  NOCACHE  NOORDER  NOCYCLE;

------------------------------------------------------------------------------------------------------------------------
-- Création de la table de référence de NACRES
CREATE TABLE JEFY_MARCHES.REFERENCE_NACRES
(
  REFN_ID       NUMBER         NOT NULL,
  REFN_CODE     VARCHAR2(5)    NOT NULL,
  REFN_LIBELLE  VARCHAR2(150)  NOT NULL,
  REFN_CE_TYPE  VARCHAR2(1)            ,

  CONSTRAINT PK_REFN_ID PRIMARY KEY (REFN_ID)
);

COMMENT ON TABLE  JEFY_MARCHES.REFERENCE_NACRES              IS 'Table de référence des codes NACRES';
COMMENT ON COLUMN JEFY_MARCHES.REFERENCE_NACRES.REFN_ID      IS 'Identifiant';
COMMENT ON COLUMN JEFY_MARCHES.REFERENCE_NACRES.REFN_CODE    IS 'Code NACRES';
COMMENT ON COLUMN JEFY_MARCHES.REFERENCE_NACRES.REFN_LIBELLE IS 'Libellé du code NACRE';
COMMENT ON COLUMN JEFY_MARCHES.REFERENCE_NACRES.REFN_CE_TYPE IS 'Référence vers le code de la table TYPE_CN ';

CREATE SEQUENCE JEFY_MARCHES.reference_nacres_seq
  START WITH 1 INCREMENT BY 1 
  NOCACHE  NOORDER  NOCYCLE;

  
------------------------------------------------------------------------------------------------------------------------
-- Création d'une table de correspondance entre les anciennes nomenclatures et les nomenclatures NACRES
CREATE TABLE JEFY_MARCHES.CORRESPONDANCE_CN
(
  ANCIEN_CN    VARCHAR2(10)    NOT NULL,
  NOUVEAU_CN   VARCHAR2(5)     NOT NULL,

  CONSTRAINT PK_CORRESPONDANCE_CN PRIMARY KEY (ANCIEN_CN, NOUVEAU_CN)
);

COMMENT ON TABLE  JEFY_MARCHES.CORRESPONDANCE_CN            IS 'Table de correspondance entre les anciens codes de nomenclature et la nomenclature NACRES';
COMMENT ON COLUMN JEFY_MARCHES.CORRESPONDANCE_CN.ANCIEN_CN  IS 'Code nomenclature 2013 (CM_CODE de la table JEFY_MARCHES.CODE_MARCHE)';
COMMENT ON COLUMN JEFY_MARCHES.CORRESPONDANCE_CN.NOUVEAU_CN IS 'Code nomenclature NACRES ou LOCAL correspondant';

------------------------------------------------------------------------------------------------------------------------
-- Permet de différencier les anciens codes de nomenclatures avec ceux de NACRES et ceux propres à l'établissement 
create table JEFY_MARCHES.TYPE_CODE_MARCHE
(
  TCM_ID      NUMBER        NOT NULL,
  TCM_CODE    VARCHAR2(1)   NOT NULL,
  TCM_LIBELLE VARCHAR2(25)  NOT NULL,

  PRIMARY KEY (TCM_ID)
);

COMMENT ON TABLE  JEFY_MARCHES.TYPE_CODE_MARCHE             IS 'Définition des types de nomenclatures';
COMMENT ON COLUMN JEFY_MARCHES.TYPE_CODE_MARCHE.TCM_ID      IS 'Clé primaire';
COMMENT ON COLUMN JEFY_MARCHES.TYPE_CODE_MARCHE.TCM_CODE    IS 'Code du type';
COMMENT ON COLUMN JEFY_MARCHES.TYPE_CODE_MARCHE.TCM_LIBELLE IS 'Libellé du type';

------------------------------------------------------------------------------------------------------------------------
-- Mise à jour de la table CODE_MARCHE : on ajoute le type de nomenclature, par défaut ANCIEN
ALTER TABLE JEFY_MARCHES.CODE_MARCHE
ADD
(
  TCM_ID  NUMBER DEFAULT '0' NOT NULL
);

COMMENT ON COLUMN JEFY_MARCHES.CODE_MARCHE.TCM_ID IS 'Identifiant du type de nomenclature';

-- Ajout de la contrainte de référence, désactivée
ALTER TABLE JEFY_MARCHES.CODE_MARCHE
ADD CONSTRAINT FK_TCM_ID
   FOREIGN KEY (TCM_ID)
   REFERENCES JEFY_MARCHES.TYPE_CODE_MARCHE (TCM_ID) DISABLE
;

ALTER TABLE JEFY_MARCHES.CODE_MARCHE  
MODIFY 
(
  -- Augmentation de la taille du libellé
  CM_LIB VARCHAR2(150 BYTE), 
  -- Tous les nouveau codes seront de type LOCAL
  TCM_ID  NUMBER DEFAULT '2' 
);

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- Liste des code_marche utilisés en 2013
create or replace view JEFY_MARCHES.V_CODE_MARCHE_2013 (cm_code, cm_niveau, cm_lib, tcn_libelle) as
select cm.cm_code,
       cm.cm_niveau,
       cm.cm_lib,
       tcn.tcn_libelle
from JEFY_MARCHES.code_marche             cm,
     JEFY_MARCHES.code_exer               ce
     left outer join JEFY_MARCHES.type_cn tcn
      on ce.tcn_id = tcn.tcn_id
where cm.cm_ordre  = ce.cm_ordre
  and ce.exe_ordre = 2013
  and ce.ce_suppr  = 'N'
  and cm.cm_suppr  = 'N'
  and cm.cm_niveau in (1, 2)
group by cm.cm_code, cm.cm_niveau, cm.cm_lib, cm.cm_lib_court, tcn.tcn_libelle
order by cm.cm_code
;

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- Liste des code_marche utilisés en 2013
create or replace view JEFY_MARCHES.V_CODE_MARCHE_ALL (cm_code, cm_niveau, cm_lib, tcn_libelle, nb_annees_utilise, derniere_annee) as
select cm.cm_code,
       cm.cm_niveau,
       cm.cm_lib,
       tcn.tcn_libelle,
       count(*),
       max(ce.exe_ordre)
from 
  JEFY_MARCHES.code_marche             cm,
  JEFY_MARCHES.code_exer               ce
  left outer join JEFY_MARCHES.type_cn tcn 
    on ce.tcn_id = tcn.tcn_id
where cm.cm_ordre  = ce.cm_ordre
  and ce.ce_suppr  = 'N'
  and cm.cm_suppr  = 'N'  
  and cm.cm_niveau in (1, 2)
group by cm.cm_code, cm.cm_niveau, cm.cm_lib, cm.cm_lib_court, tcn.tcn_libelle
order by cm.cm_code
;

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-- ------------------------------ --
-- Procédure d'insertion des logs --
-- ------------------------------ --
create or replace procedure JEFY_MARCHES.add_log(
  logDesc VARCHAR2
) is
begin
  INSERT INTO LOG_UTILS (LOG_ID, LOG_DATE, LOG_DESC)
      VALUES (JEFY_MARCHES.log_utils_seq.nextval, sysdate, logDesc);
end add_log;
/


------------------------------------------ --------------------------------- ------------------------------------------
------------------------------------------ Package NACRES_MIGRATION - HEADER ------------------------------------------
------------------------------------------ --------------------------------- ------------------------------------------

create or replace PACKAGE JEFY_MARCHES.NACRES_MIGRATION IS
  -- Remplissage de la table de référence
  procedure insert_ref_nacres (
    cmCode VARCHAR2,
    cmLib  VARCHAR2,
    ceType VARCHAR2
  ) ;
  -- Création des code_marche de NACRES
  procedure create_all_cm_nacres;
  -- Création d'un nouveau code_marche, ou mise à jour s'il existe
  procedure insert_code_marche (
    cmCode VARCHAR2,
    cmLib  VARCHAR2
  ) ;
  -- Vérification de la cohérence de la table de correspondance
  procedure check_correspondance;
  -- Supprime les codes basculés de 2013 à 2014
  procedure supprimer_anciens_cm_sur_2014;
  -- Met à jour les codes basculés de 2013 à 2014 en type local pour une utilisation hors NACRES
  procedure utiliser_anciens_cm_sur_2014;
  
END; 
/

------------------------------------------- ------------------------------- -------------------------------------------
------------------------------------------- Package NACRES_MIGRATION - BODY -------------------------------------------
------------------------------------------- ------------------------------- -------------------------------------------

create or replace PACKAGE BODY JEFY_MARCHES.NACRES_MIGRATION IS

  -- ---------------------------------------------- --  
  -- Permet le remplissage de la table de référence --
  -- ---------------------------------------------- --  
  procedure insert_ref_nacres (
    cmCode  VARCHAR2,
    cmLib   VARCHAR2,
    ceType  VARCHAR2
  ) 
  is
    cpt     NUMBER;
  begin  
    SELECT count(*) INTO cpt FROM REFERENCE_NACRES WHERE REFN_CODE = cmCode;
    if (cpt = 0) then	
      INSERT INTO REFERENCE_NACRES (REFN_ID, REFN_CODE, REFN_LIBELLE, REFN_CE_TYPE)
        VALUES (REFERENCE_NACRES_SEQ.nextval, cmCode, cmLib, ceType);
    end if;	
  end insert_ref_nacres;

  -- --------------------------------------- --
  -- Créé tous les codes NACRES de référence --
  -- dans la table CODE_MARCHE               --
  -- --------------------------------------- --
  procedure create_all_cm_nacres
  is
    CURSOR C1 IS select * from REFERENCE_NACRES;
    rn        REFERENCE_NACRES%rowtype;
    nbErr     NUMBER;
    strErr    VARCHAR2(2000);

    insertion_exception EXCEPTION;
    PRAGMA EXCEPTION_INIT(insertion_exception, -20001);
  begin
    nbErr  := 0;
    strErr := '';
  
    open C1;
    loop
      fetch C1 into rn;
      exit when C1%notfound;
        begin
          insert_code_marche(rn.REFN_CODE, rn.REFN_LIBELLE);
        EXCEPTION
          WHEN insertion_exception THEN
            nbErr  := nbErr + 1;
            strErr := strErr || SQLERRM || ' ' || chr(13);
        end;
    end loop;
    close C1;
    
    -- Gestion des erreurs
    if (nbErr > 0) then
      JEFY_MARCHES.add_log('create_all_cm_nacres : ' || nbErr || ' erreur(s) : ' || chr(13) || strErr);
      commit;
      raise_application_error(-20001, 'Des erreurs sont apparues. Voir la table JEFY_MARCHES.LOG_UTILS');
    end if;
  end create_all_cm_nacres;
  
  
  -- ----------------------------------------------------- --
  -- Procédure permettant d'insérer un nouveau code_marche --
  -- ou de le mettre à jour s'il existe déjà               --
  -- ----------------------------------------------------- --
  procedure insert_code_marche (
    cmCode VARCHAR2,
    cmLib  VARCHAR2
  )
  is
    nb          NUMBER;
    cmNiveau    NUMBER;
    cmPere      NUMBER;
    cmCodePere  VARCHAR2(10);
    tcmIdNacres NUMBER;
    tcmIdLocal  NUMBER;
    tcmIdNewCm  NUMBER;
    cmExistant  code_marche%rowtype;
    cmRefLib    VARCHAR2(150);
  begin
    
    -- On vérifie que le code est fourni. Le libellé n'est pas obligatoire si le code_marche existe déjà  
    if (cmCode is null) then
      RAISE_APPLICATION_ERROR (-20001,'Parametre cmCode est null ');
    end if;
    
    -- On initialise de type de code : a-t-on à faire à un code NACRES ?
    select tcm_id into tcmIdNacres from jefy_marches.type_code_marche where tcm_code = 'N';
    select tcm_id into tcmIdLocal  from jefy_marches.type_code_marche where tcm_code = 'L';
    
    select count(*) into nb from reference_nacres where refn_code = cmCode;
    if (nb > 0) then
      tcmIdNewCm := tcmIdNacres;
    else
      tcmIdNewCm := tcmIdLocal;
      select refn_libelle into cmRefLib from reference_nacres where refn_code = cmCode;
    end if;
    ---------------------------------------------------------------------------------------
    
    select count(*) into nb from jefy_marches.code_marche where cm_code = cmCode;
    if (nb = 0) then
      -- Nouveau code, on vérifie alors que le libellé a été fourni
      if (cmLib is null) then
        RAISE_APPLICATION_ERROR (-20001,'Parametre cmLib est null ');
      end if;
      
      -- Recherche du cmPere
      if (length(cmCode) = 2) then 
        -- Code de niveau 1 : pas de père
        cmPere   := null;
        cmNiveau := 1;
      else 
        -- Code de niveau 2 : on recherche le père
        cmCodePere := substr(cmCode, 1, 2);
        cmNiveau   := 2;
        
        -- Vérification qu'un seul père existe
        select count(*) into nb from jefy_marches.code_marche where cm_code = cmCodePere;
        if (nb = 0) then
          raise_application_error(-20001, 'Aucun cmPere '''|| cmCodePere ||''' trouvé pour le code '''|| cmCode ||'''. ');
        elsif (nb > 1) then
          raise_application_error(-20001, 'Plusieurs cmPere '''|| cmCodePere ||''' trouvés pour le code '''|| cmCode ||'''. ');
        end if;
        
        -- Le père a été trouvé
        select cm_ordre into cmPere from jefy_marches.code_marche where cm_code = cmCodePere;
      end if;
      
      INSERT INTO JEFY_MARCHES.code_marche 
              (cm_ordre               , cm_code, cm_lib, cm_niveau, cm_suppr, cm_pere, tcm_id)
      VALUES  (CODE_MARCHE_SEQ.NEXTVAL, cmCode , cmLib , cmNiveau , 'N'     , cmPere , tcmIdNewCm);
      
    else
      -- le code existe, on fait des verifs
      select * into cmExistant from jefy_marches.code_marche where cm_code = cmCode;
      -- Le type du code existant ne correspond pas à la table de référence 
      if (tcmIdNewCm = tcmIdNacres and cmExistant.tcm_id <> tcmIdNacres) or
         (tcmIdNewCm = tcmIdLocal  and cmExistant.tcm_id =  tcmIdNacres) then
        -- Code NACRES non identifié comme tel dans CODE_MARCHE
        -- ou Code Local identifié comme un code NACRES dans CODE_MARCHE
        UPDATE CODE_MARCHE
          SET tcm_id = tcmIdNewCm
          WHERE cm_ordre = cmExistant.cm_ordre;
      end if;
      -- Le libellé du code NACRES est différent de celui dans la table de référence
      if (tcmIdNewCm = tcmIdNacres and cmRefLib <> cmExistant.cm_lib) then
        UPDATE CODE_MARCHE
          SET   cm_lib   = cmRefLib
          WHERE cm_ordre = cmExistant.cm_ordre;
      end if;
    end if;
  end insert_code_marche;


  -- ---------------------------------------------------------- --
  -- Vérification de la cohérence de la table de correspondance --
  -- ---------------------------------------------------------- --
  procedure check_correspondance
  is
    cursor C1 is select * from JEFY_MARCHES.correspondance_cn;
    corr       JEFY_MARCHES.correspondance_cn%rowtype;
    cpt        NUMBER;
    nbErr      NUMBER;
    strErr     VARCHAR2(2000);
    ancienCm   JEFY_MARCHES.code_marche%rowtype;
    cmCodePere VARCHAR2(2);
  BEGIN
    nbErr  := 0;
    strErr := '';
    
    open C1;
    loop
      fetch C1 into corr;
      exit when C1%notfound;

      -- On vérifie que l'ancien_cm existe
      select count(*) into cpt from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.type_code_marche tcm, JEFY_MARCHES.code_exer ce
        where cm.cm_code   = corr.ancien_cn
        and   tcm.tcm_id   = cm.tcm_id
        and   tcm.tcm_code = 'A'
        and   cm.cm_ordre  = ce.cm_ordre
        and   ce.exe_ordre = 2013;
      if (cpt = 0) then
        nbErr  := nbErr +1;
        strErr := strErr || 'L''ancien CM ''' || corr.ancien_cn || ''' n''existe pas. ' || chr(13);
      else 
        dbms_output.put_line('corr.ancien_cn : ' || corr.ancien_cn);
        select cm.* into ancienCm from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.type_code_marche tcm, JEFY_MARCHES.code_exer ce
        where cm.cm_code   = corr.ancien_cn
        and   tcm.tcm_id   = cm.tcm_id
        and   tcm.tcm_code = 'A'
        and   cm.cm_ordre  = ce.cm_ordre
        and   ce.exe_ordre = 2013
        ;
        
        if (ancienCm.cm_pere is not null) then
          select count(*) into cpt from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.correspondance_cn ccn 
            where cm.cm_ordre = ancienCm.cm_pere
            and   cm.cm_code  = ccn.ancien_cn;
          if (cpt = 0) then 
            nbErr  := nbErr +1;
            strErr := strErr || 'Le père de l''ancien CM ''' || corr.ancien_cn || ''' n''est pas présent dans la table de correspondance. ' || chr(13);
          end if;
        end if;
      end if;

      -- On vérifie que le CM NACRES existe (ou est local)
      select count(*) into cpt from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.type_code_marche tcm 
        where tcm.tcm_id = cm.tcm_id and cm_code = corr.nouveau_cn and tcm.tcm_code in ('N', 'L');
        
      if (cpt = 0) then
        nbErr  := nbErr +1;
        strErr := strErr || 'Le CM NACRES ''' || corr.nouveau_cn || ''' n''existe pas. ' || chr(13);
      end if;
      
      -- On vérifie que les codes de niveau deux ont un père
      if (length(corr.nouveau_cn) > 2) then
        cmCodePere := substr(corr.nouveau_cn, 1, 2);
        
        select count(*) into cpt from jefy_marches.code_marche cm, JEFY_MARCHES.type_code_marche tcm 
          where tcm.tcm_id = cm.tcm_id and cm_code = cmCodePere and tcm.tcm_code in ('N', 'L');
        
        if (cpt = 0) then
          nbErr  := nbErr +1;
          strErr := strErr || 'Aucun cmPere '''|| cmCodePere ||''' trouvé pour le code '''|| corr.nouveau_cn ||'''. ';
        elsif (cpt > 1) then
          nbErr  := nbErr +1;
          strErr := strErr || 'Plusieurs cmPere '''|| cmCodePere ||''' trouvés pour le code '''|| corr.nouveau_cn ||'''. ';
        end if;
      end if;
    end loop;
    close C1;

    if (nbErr > 0) then
      JEFY_MARCHES.add_log(nbErr || ' erreur(s) : ' || chr(13) || strErr);
      commit;
      raise_application_error(-20001, 'Des erreurs sont apparues. Voir la table JEFY_MARCHES.LOG_UTILS');
    end if;
  END check_correspondance;



  -- ------------------------------------------------------ --
  -- Procédure de suppression de la bascule faite pour 2014 --
  -- ------------------------------------------------------ --
  procedure supprimer_anciens_cm_sur_2014
  is
    cpt             NUMBER;
    nb_cmf_deleted  NUMBER;
    nb_mc_deleted   NUMBER;
    nb_ce_deleted   NUMBER;
    str             VARCHAR(200);
  begin
  
    select count(*) into cpt from code_marche cm, code_exer ce, type_code_marche tcm
       where cm.cm_ordre  = ce.cm_ordre
       and   cm.tcm_id    = tcm.tcm_id
       and   ce.exe_ordre = 2014
       and   tcm.tcm_code = 'A';
    if (cpt = 0) then
      -- Pas de données à supprimer
      add_log('supprimer_anciens_cm_sur_2014 : aucune donnée à supprimer.');
      dbms_output.put_line('supprimer_anciens_cm_sur_2014 : Aucune donnée à supprimer.');
      return;
    end if; 

	  nb_cmf_deleted := 0;
    nb_mc_deleted  := 0;
    nb_ce_deleted  := 0;
	  
    -- On supprime les code_marche_four
    delete from code_marche_four
      where cm_ordre in (select distinct cm.cm_ordre
                         from code_marche cm, type_code_marche tcm
                         where cm.tcm_id    = tcm.tcm_id
                         and   tcm.tcm_code = 'A')
     and exe_ordre = 2014;
    nb_cmf_deleted := SQL%ROWCOUNT;
  
      
    -- On supprime les mapa_ce
    delete from mapa_ce
      where ce_ordre in (select distinct ce.ce_ordre
                         from code_marche cm, code_exer ce, type_code_marche tcm
                         where cm.cm_ordre  = ce.cm_ordre
                         and   cm.tcm_id    = tcm.tcm_id
                         and   ce.exe_ordre = 2014
                         and   tcm.tcm_code = 'A');
    nb_mc_deleted := SQL%ROWCOUNT;
    
    -- On supprime les code_exer
    delete from code_exer
      where ce_ordre in (select distinct ce.ce_ordre
                         from code_marche cm, code_exer ce, type_code_marche tcm
                         where cm.cm_ordre  = ce.cm_ordre
                         and   cm.tcm_id    = tcm.tcm_id
                         and   ce.exe_ordre = 2014
                         and   tcm.tcm_code = 'A');
    nb_ce_deleted := SQL%ROWCOUNT;
    
    str := 'supprimer_anciens_cm_sur_2014 : ' || chr(13);
    str := str || ' code_marche_four supprimés : ' || nb_cmf_deleted || chr(13);
    str := str || ' mapa_ce supprimés : ' || nb_mc_deleted || chr(13);
    str := str || ' code_exer supprimés : ' || nb_ce_deleted;
    add_log(str);
    
    dbms_output.put_line('supprimer_anciens_cm_sur_2014 : Suppression terminée. Voir la table LOG_UTILS pour un récapitulatif des suppressions.');
    
  end supprimer_anciens_cm_sur_2014 ;

  -- --------------------------------------------------- --
  -- Procédure de mise à jour des codes basculés en 2014 -- 
  -- pour une utilisation sans nomenclature NACRES       --
  -- --------------------------------------------------- --
  procedure utiliser_anciens_cm_sur_2014
  is
    cpt           NUMBER;
    tcmIdLocal    NUMBER;
    nb_cm_updated NUMBER;
  begin
    select count(*) into cpt from code_marche cm, code_exer ce, type_code_marche tcm
      where cm.cm_ordre  = ce.cm_ordre
      and   cm.tcm_id    = tcm.tcm_id
      and   ce.exe_ordre = 2014
      and   tcm.tcm_code = 'A' and cm_code like 'ZZ%';
    
    if (cpt = 0) then
      -- Pas de données à mettre à jour
      add_log('maj_anciens_cm_sur_2014_apres_bascule : aucune donnée à mettre à jour.');
      dbms_output.put_line('utiliser_anciens_cm_sur_2014 : Aucune donnée à mettre à jour.');
      return;
    end if;
    
    select tcm_id into tcmIdLocal from type_code_marche where tcm_code = 'L';
    -- On met à jour les codes 
    update CODE_MARCHE
      set tcm_id = tcmIdLocal
      where cm_ordre in ( 
        select cm.cm_ordre from code_marche cm, code_exer ce, type_code_marche tcm
        where cm.cm_ordre  = ce.cm_ordre
        and   cm.tcm_id    = tcm.tcm_id
        and   ce.exe_ordre = 2014
        and   tcm.tcm_code = 'A' and cm_code like 'ZZ%');
    nb_cm_updated := SQL%ROWCOUNT;
    
    add_log('maj_anciens_cm_sur_2014_apres_bascule : ' || nb_cm_updated || ' code(s) marché mis à jour.');
    dbms_output.put_line('utiliser_anciens_cm_sur_2014 : Mise à jour terminée. Voir la table LOG_UTILS pour un récapitulatif.');
    
  end utiliser_anciens_cm_sur_2014;


END NACRES_MIGRATION;
/

