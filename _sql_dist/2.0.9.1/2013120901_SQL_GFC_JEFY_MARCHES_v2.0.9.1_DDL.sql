--
-- Patch DDL de JEFY_MARCHES du 09/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : JEFY_MARCHES
-- Numero de version : 2.0.9.1
-- Date de publication : 09/12/2013
-- Auteur(s) : Julien Blandineau
--
--
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
reqVersion VARCHAR2(15);
newVersion VARCHAR2(15);
begin
  reqVersion := '2.0.8.3';
  newVersion := '2.0.9.1';
	
  select count(*) into cpt from JEFY_MARCHES.db_version where dbv_libelle = reqVersion;
    if cpt = 0 then
      raise_application_error(-20000,'Le user JEFY_MARCHES n''est pas à jour pour passer ce patch ! Version requise : ' || reqVersion);
    end if;

  select count(*) into cpt from JEFY_MARCHES.db_version where dbv_libelle = newVersion;
    if cpt > 0 then
      raise_application_error(-20000,'Le patch ' || newVersion || ' a déjà été passé !');
  end if;
end;
/

-------------------------------------------- ---------------------------- ----------------------------------------------
-------------------------------------------- DROP de procédures obsolètes ----------------------------------------------
-------------------------------------------- ---------------------------- ----------------------------------------------
declare
  is_proc_exists int;
begin
  select count(*) into is_proc_exists from all_procedures where owner = 'JEFY_MARCHES' and object_type = 'PROCEDURE' and object_name = 'BASCULE_CODE_MARCHE_FOUR';
  if (is_proc_exists  = 1) then
    execute immediate 'DROP PROCEDURE JEFY_MARCHES.BASCULE_CODE_MARCHE_FOUR';
    dbms_output.put_line('DROP PROCEDURE JEFY_MARCHES.BASCULE_CODE_MARCHE_FOUR');
  end if;
   
  select count(*) into is_proc_exists from all_procedures where owner = 'JEFY_MARCHES' and object_type = 'PROCEDURE' and object_name = 'BASCULE_CODE_MARCHE_3CMP';
  if (is_proc_exists  = 1) then
    execute immediate 'DROP PROCEDURE JEFY_MARCHES.BASCULE_CODE_MARCHE_3CMP';
    dbms_output.put_line('DROP PROCEDURE JEFY_MARCHES.BASCULE_CODE_MARCHE_3CMP');
  end if;
  commit;
end;
/

--------------------------------------------------- ---------------- ---------------------------------------------------
--------------------------------------------------- code_marche_four ---------------------------------------------------
--------------------------------------------------- ---------------- ---------------------------------------------------
declare
  is_col_exists int;
  sql_1 VARCHAR2(255);
begin
  select count(*) into is_col_exists from all_tab_cols where owner = 'JEFY_MARCHES' and upper(table_name) = 'CODE_MARCHE_FOUR' and (upper(column_name) = 'EXE_ORDRE' OR upper(column_name) = 'CM_ORDRE');
  if (is_col_exists = 2) then
    -- Mise à jour du code_exer
    sql_1 := 'SELECT ce_ordre FROM (SELECT ce_ordre, ce.cm_ordre, ce.exe_ordre FROM JEFY_MARCHES.code_exer ce ORDER BY ce.ce_suppr) WHERE cm_ordre = cmf.cm_ordre AND exe_ordre = cmf.exe_ordre AND rownum = 1';
    execute immediate 'UPDATE JEFY_MARCHES.code_marche_four cmf SET ce_ordre = nvl((' || sql_1 || '), -1)';
    dbms_output.put_line('UPDATE JEFY_MARCHES.code_marche_four cmf SET ce_ordre = nvl((' || sql_1 || '), -1)');
  end if;
    -- Suppression des colonnes inutilisées
  select count(*) into is_col_exists from all_tab_cols where owner = 'JEFY_MARCHES' and upper(table_name) = 'CODE_MARCHE_FOUR' and upper(column_name) = 'EXE_ORDRE';
  if (is_col_exists = 1) then
    execute immediate 'ALTER TABLE jefy_marches.code_marche_four DROP column exe_ordre';
    dbms_output.put_line('ALTER TABLE jefy_marches.code_marche_four DROP column exe_ordre');
  end if;
  select count(*) into is_col_exists from all_tab_cols where owner = 'JEFY_MARCHES' and upper(table_name) = 'CODE_MARCHE_FOUR' and upper(column_name) = 'CM_ORDRE';
  if (is_col_exists = 1) then
    execute immediate 'ALTER TABLE jefy_marches.code_marche_four DROP column cm_ordre';
    dbms_output.put_line('ALTER TABLE jefy_marches.code_marche_four DROP column cm_ordre');
  end if;
end;
/


-- Suppression des lignes non utilisées
DELETE FROM JEFY_MARCHES.code_marche_four
WHERE ce_ordre = -1;

-- Ajout de la contrainte FK vers CODE_EXER, si elle n'existe pas
declare
  is_constraint_exists int;
begin
  select count(*) into is_constraint_exists from all_constraints where upper(owner) = 'JEFY_MARCHES' and upper(table_name) = 'CODE_MARCHE_FOUR' and constraint_name = 'FK_CE_ORDRE';
  if (is_constraint_exists = 0) then
    execute immediate 'ALTER TABLE JEFY_MARCHES.code_marche_four ADD CONSTRAINT FK_CE_ORDRE FOREIGN KEY (ce_ordre) REFERENCES JEFY_MARCHES.code_exer (ce_ordre)';
    dbms_output.put_line('ALTER TABLE JEFY_MARCHES.code_marche_four ADD CONSTRAINT FK_CE_ORDRE FOREIGN KEY (ce_ordre) REFERENCES JEFY_MARCHES.code_exer (ce_ordre)');
  end if;
  
  commit;
end;
/

------------------------------------ ---------------------------------------------- ------------------------------------
------------------------------------ PROCEDURE JEFY_MARCHES.creerCodeMarcheFourFils ------------------------------------
------------------------------------ ---------------------------------------------- ------------------------------------
create or replace PROCEDURE JEFY_MARCHES.creerCodeMarcheFourFils(
  cmOrdrePere NUMBER,
  fouOrdre    NUMBER,
  origine     VARCHAR2,
  exeOrdre    NUMBER
)
IS
  CURSOR C1 is 
    select cm_ordre from code_marche where cm_pere = cmOrdrePere;
  cm_ordre_courant  NUMBER;
  ce_ordre_courant  NUMBER;
  cpt               NUMBER;
  origineUp         VARCHAR2(25);
  cmfSeq            NUMBER;
  
BEGIN
  -- verifier que les parametres sont bien remplis -----------------------------------
  IF (cmOrdrePere IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre ceOrdre est null ');
  END IF;
  IF (fouOrdre IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre fouOrdre est null ');
  END IF;
  IF (origine IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre origine est null ');
  END IF;
  
  select count(*) into cpt from code_marche where cm_ordre = cmOrdrePere;
  IF (cpt=0 ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le cmOrdrePere n''existe pas ');
  END IF;
  
  select count(*) into cpt from GRHUM.FOURNIS_ULR where fou_ordre = fouOrdre;
  IF (cpt=0 ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le fournisseur n''existe pas ');
  END IF;
  -------------------------------------------------------------------------------------
  origineUp := UPPER(origine);
  OPEN C1;
  LOOP
    fetch C1 into cm_ordre_courant;
    exit when C1%notfound;
    
    -- dbms_output.put_line('DEBUG :: DEBUT FOUR FILS : ' || cm_ordre_courant);
    -- Le code_exer existe ?
    select count(*) into cpt from code_exer where exe_ordre = exeOrdre and cm_ordre = cm_ordre_courant;
    IF cpt > 0 THEN
      select ce_ordre into ce_ordre_courant from code_exer 
      where exe_ordre = exeOrdre 
        and cm_ordre  = cm_ordre_courant;
  
      -- dbms_output.put_line('DEBUG :: ce_ordre_courant : ' || ce_ordre_courant);
      -- dbms_output.put_line('DEBUG :: INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR (CMF_ORDRE, CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE ) 
      --   VALUES ('||cmfSeq||', '||origine||', '||'''N'', '||fouOrdre||', '||ce_ordre_courant||');');
      INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR 
             (CMF_ORDRE,                    CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE)
      VALUES (code_marche_four_seq.nextval, origine,     'N',       fouOrdre,  ce_ordre_courant);
  
      -- Récursivité
      -- dbms_output.put_line('DEBUG :: Appel fils');      
      creerCodeMarcheFourFils(cm_ordre_courant, fouOrdre, origine, exeOrdre);
    END IF;
  END LOOP;
  CLOSE C1;
  
  -- dbms_output.put_line('DEBUG :: FIN FOUR FILS');

 END;
/

-------------------------------------- ------------------------------------------ --------------------------------------
-------------------------------------- PROCEDURE JEFY_MARCHES.creerCodeMarcheFour --------------------------------------
-------------------------------------- ------------------------------------------ --------------------------------------
create or replace PROCEDURE JEFY_MARCHES.creerCodeMarcheFour(
  fouOrdre NUMBER,
  ceOrdre  NUMBER,
  origine  VARCHAR2
)
IS
  cm_ordre_courant  NUMBER;
  exe_ordre_courant NUMBER;
  cpt               NUMBER;
  origineUp         VARCHAR2(25);
BEGIN
  -- verifier que les parametres sont bien remplis -----------------------------------
  IF (fouOrdre IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre fouOrdre est null ');
  END IF;
  IF (ceOrdre IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre ceOrdre est null ');
  END IF;
  IF (origine IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre origine est null ');
  END IF;
  
  select count(*) into cpt from GRHUM.FOURNIS_ULR where fou_ordre = fouOrdre;
  IF (cpt = 0) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le fournisseur n''existe pas (fou_ordre = ' || fouOrdre || ')');
  END IF;
  
  select count(*) into cpt from code_exer where ce_ordre = ceOrdre;
  IF (cpt = 0) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le code_exer n''existe pas (ce_ordre = ' || ceOrdre || ')');
  END IF;
  -------------------------------------------------------------------------------------
  
  -- On récupère le CM_ORDRE et EXE_ORDRE correspondant au ceOrdre
  select cm_ordre, exe_ordre into cm_ordre_courant, exe_ordre_courant from code_exer where ce_ordre = ceOrdre;
  -- Un CODE_MARCHE_FOUR correspondant ?
  select count(*) into cpt from code_marche_four where ce_ordre = ceOrdre;
  IF cpt = 0 THEN
    origineUp := UPPER(origine);

    -- dbms_output.put_line('DEBUG :: INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR (CMF_ORDRE, CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE ) 
    -- VALUES ('||cmfSeq||', '||origine||', '||'''N'', '||fouOrdre||', '||ceOrdre||');');

    INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR 
              (CMF_ORDRE,                    CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE) 
      VALUES  (code_marche_four_seq.nextval, origineUp,   'N',       fouOrdre,  ceOrdre);
      
    -- Récursivité
    -- dbms_output.put_line('DEBUG :: creerCodeMarcheFourFils_test('||cm_ordre_pere||', '||fouOrdre||', '||origine||', '||exe_ordre_courant||');');
    creerCodeMarcheFourFils(cm_ordre_courant, fouOrdre, origineUp, exe_ordre_courant);
  END IF;
END;
 
/


------------------------------------------ --------------------------------- ------------------------------------------
------------------------------------------ Package NACRES_MIGRATION - HEADER ------------------------------------------
------------------------------------------ --------------------------------- ------------------------------------------

create or replace PACKAGE JEFY_MARCHES.NACRES_MIGRATION IS
 
  -- Procédure principale de création
  procedure create_all_2014;
  -- Création des code_marche de NACRES
  procedure create_all_cm_nacres;
  -- Créé les code_exer de type Nacres ou Local sur 2014
  procedure create_code_exer_2014;
  -- Création des code_marche_four sur 2014 en fonction de la table de correspondance
  procedure create_code_marche_four_2014;
  -- Création des lot_nomenclature sur 2014 en fonction de la table de correspondance
  procedure create_lot_nomenclature_2014;
  -- Recopie des droits de SA_CONTROLE_CODE_MARCHE
  procedure create_sa_controle_cm_2014;
  -- Recopie des MAPA_CE sur 2014
  procedure create_mapa_ce_2014;
  
  -- Vérification de la cohérence de la table de correspondance
  procedure check_correspondance;
  -- Supprime les codes basculés de 2013 à 2014
  procedure supprimer_anciens_cm_sur_2014;
  -- Met à jour les codes basculés de 2013 à 2014 en type local pour une utilisation hors NACRES
  procedure utiliser_anciens_cm_sur_2014;
  
  -- Remplissage de la table de référence
  procedure insert_ref_nacres (
    cmCode VARCHAR2,
    cmLib  VARCHAR2,
    ceType VARCHAR2
  );
  -- Création d'un nouveau code_marche, ou mise à jour s'il existe
  procedure insert_code_marche (
    cmCode VARCHAR2,
    cmLib  VARCHAR2
  );
  
END; 
/

------------------------------------------- ------------------------------- -------------------------------------------
------------------------------------------- Package NACRES_MIGRATION - BODY -------------------------------------------
------------------------------------------- ------------------------------- -------------------------------------------

create or replace PACKAGE BODY JEFY_MARCHES.NACRES_MIGRATION IS

  -- ---------------------------------------------- --  
  -- Permet le remplissage de la table de référence --
  -- ---------------------------------------------- --  
  procedure insert_ref_nacres (
    cmCode  VARCHAR2,
    cmLib   VARCHAR2,
    ceType  VARCHAR2
  ) 
  is
    cpt     NUMBER;
  begin  
    SELECT count(*) INTO cpt FROM REFERENCE_NACRES WHERE REFN_CODE = cmCode;
    if (cpt = 0) then	
      INSERT INTO REFERENCE_NACRES (REFN_ID, REFN_CODE, REFN_LIBELLE, REFN_CE_TYPE)
        VALUES (REFERENCE_NACRES_SEQ.nextval, cmCode, cmLib, ceType);
    end if;	
  end insert_ref_nacres;


  -- -------------------------------- --
  -- Procédure de création principale --
  -- -------------------------------- --
  procedure create_all_2014
  is
  begin

    create_code_exer_2014;
    create_code_marche_four_2014;
    create_lot_nomenclature_2014;
    create_sa_controle_cm_2014;
    create_mapa_ce_2014;
    
  end create_all_2014;

  -- --------------------------------------- --
  -- Créé tous les codes NACRES de référence --
  -- dans la table CODE_MARCHE               --
  -- --------------------------------------- --
  procedure create_all_cm_nacres
  is
    CURSOR C1 IS select * from REFERENCE_NACRES order by refn_code;
    rn        REFERENCE_NACRES%rowtype;
    nbErr     NUMBER;
    strErr    CLOB;

    insertion_exception EXCEPTION;
    PRAGMA EXCEPTION_INIT(insertion_exception, -20001);
  begin
    nbErr  := 0;
    strErr := '';
  
    open C1;
    loop
      fetch C1 into rn;
      exit when C1%notfound;
        begin
          insert_code_marche(rn.REFN_CODE, rn.REFN_LIBELLE);
        EXCEPTION
          WHEN insertion_exception THEN
            nbErr  := nbErr + 1;
            strErr := strErr || SQLERRM || ' ' || chr(13);
        end;
    end loop;
    close C1;
    
    -- Gestion des erreurs
    if (nbErr > 0) then
      JEFY_MARCHES.add_log('create_all_cm_nacres : ' || nbErr || ' erreur(s) : ' || chr(13) || strErr);
      commit;
      raise_application_error(-20001, 'Des erreurs sont apparues. Voir la table JEFY_MARCHES.LOG_UTILS');
    end if;
  end create_all_cm_nacres;
  
  
  -- ----------------------------------------------------- --
  -- Procédure permettant d'insérer un nouveau code_marche --
  -- ou de le mettre à jour s'il existe déjà               --
  -- ----------------------------------------------------- --
  procedure insert_code_marche (
    cmCode VARCHAR2,
    cmLib  VARCHAR2
  )
  is
    nb          NUMBER;
    cmNiveau    NUMBER;
    cmPere      NUMBER;
    cmCodePere  VARCHAR2(10);
    tcmIdNacres NUMBER;
    tcmIdLocal  NUMBER;
    tcmIdNewCm  NUMBER;
    cmExistant  code_marche%rowtype;
    cmRefLib    VARCHAR2(150);
  begin
    
    -- On vérifie que le code est fourni. Le libellé n'est pas obligatoire si le code_marche existe déjà  
    if (cmCode is null) then
      RAISE_APPLICATION_ERROR (-20001,'Parametre cmCode est null ');
    end if;
    
    -- On initialise de type de code : a-t-on à faire à un code NACRES ?
    select tcm_id into tcmIdNacres from jefy_marches.type_code_marche where tcm_code = 'N';
    select tcm_id into tcmIdLocal  from jefy_marches.type_code_marche where tcm_code = 'L';
    
    select count(*) into nb from reference_nacres where refn_code = cmCode;
    if (nb > 0) then
      tcmIdNewCm := tcmIdNacres;
      select refn_libelle into cmRefLib from reference_nacres where refn_code = cmCode;
    else
      tcmIdNewCm := tcmIdLocal;
    end if;
    ---------------------------------------------------------------------------------------
    
    select count(*) into nb from jefy_marches.code_marche where cm_code = cmCode;
    if (nb = 0) then
      -- Nouveau code, on vérifie alors que le libellé a été fourni
      if (cmLib is null) then
        RAISE_APPLICATION_ERROR (-20001,'Parametre cmLib est null ');
      end if;
      
      -- Recherche du cmPere
      if (length(cmCode) = 2) then 
        -- Code de niveau 1 : pas de père
        cmPere   := null;
        cmNiveau := 1;
      else 
        -- Code de niveau 2 : on recherche le père
        cmCodePere := substr(cmCode, 1, 2);
        cmNiveau   := 2;
        
        -- Vérification qu'un seul père existe
        select count(*) into nb from jefy_marches.code_marche where cm_code = cmCodePere;
        if (nb = 0) then
          raise_application_error(-20001, 'Aucun cmPere '''|| cmCodePere ||''' trouvé pour le code '''|| cmCode ||'''. ');
        elsif (nb > 1) then
          raise_application_error(-20001, 'Plusieurs cmPere '''|| cmCodePere ||''' trouvés pour le code '''|| cmCode ||'''. ');
        end if;
        
        -- Le père a été trouvé
        select cm_ordre into cmPere from jefy_marches.code_marche where cm_code = cmCodePere;
      end if;
      
      INSERT INTO JEFY_MARCHES.code_marche 
              (cm_ordre               , cm_code, cm_lib, cm_niveau, cm_suppr, cm_pere, tcm_id)
      VALUES  (CODE_MARCHE_SEQ.NEXTVAL, cmCode , cmLib , cmNiveau , 'N'     , cmPere , tcmIdNewCm);
      
    else
      -- le code existe, on fait des verifs
      select * into cmExistant from jefy_marches.code_marche where cm_code = cmCode;
      -- Le type du code existant ne correspond pas à la table de référence 
      if (tcmIdNewCm = tcmIdNacres and cmExistant.tcm_id <> tcmIdNacres) or
         (tcmIdNewCm = tcmIdLocal  and cmExistant.tcm_id =  tcmIdNacres) then
        -- Code NACRES non identifié comme tel dans CODE_MARCHE
        -- ou Code Local identifié comme un code NACRES dans CODE_MARCHE
        UPDATE CODE_MARCHE
          SET tcm_id = tcmIdNewCm
          WHERE cm_ordre = cmExistant.cm_ordre;
      end if;
      -- Le libellé du code NACRES est différent de celui dans la table de référence
      if (tcmIdNewCm = tcmIdNacres and cmRefLib <> cmExistant.cm_lib) then
        UPDATE CODE_MARCHE
          SET   cm_lib   = cmRefLib
          WHERE cm_ordre = cmExistant.cm_ordre;
      end if;
    end if;
  end insert_code_marche;


  -- ---------------------------------------------------------- --
  -- Vérification de la cohérence de la table de correspondance --
  -- ---------------------------------------------------------- --
  procedure check_correspondance
  is
    cursor C1 is select * from JEFY_MARCHES.correspondance_cn;
    corr       JEFY_MARCHES.correspondance_cn%rowtype;
    cpt        NUMBER;
    nbErr      NUMBER;
    strErr     CLOB;
    ancienCm   JEFY_MARCHES.code_marche%rowtype;
    cmCodePere VARCHAR2(2);
  BEGIN
    nbErr  := 0;
    strErr := '';
    
    open C1;
    loop
      fetch C1 into corr;
      exit when C1%notfound;

      -- On vérifie que l'ancien_cm existe
      select count(*) into cpt from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.type_code_marche tcm, JEFY_MARCHES.code_exer ce
        where cm.cm_code   = corr.ancien_cn
        and   tcm.tcm_id   = cm.tcm_id
        and   tcm.tcm_code = 'A'
        and   cm.cm_ordre  = ce.cm_ordre
        and   ce.exe_ordre = 2013;
      if (cpt = 0) then
        nbErr  := nbErr +1;
        strErr := strErr || 'L''ancien CM ''' || corr.ancien_cn || ''' n''existe pas. ' || chr(13);
      else 
        dbms_output.put_line('corr.ancien_cn : ' || corr.ancien_cn);
        select cm.* into ancienCm from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.type_code_marche tcm, JEFY_MARCHES.code_exer ce
        where cm.cm_code   = corr.ancien_cn
        and   tcm.tcm_id   = cm.tcm_id
        and   tcm.tcm_code = 'A'
        and   cm.cm_ordre  = ce.cm_ordre
        and   ce.exe_ordre = 2013
        ;
        
        if (ancienCm.cm_pere is not null) then
          select count(*) into cpt from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.correspondance_cn ccn 
            where cm.cm_ordre = ancienCm.cm_pere
            and   cm.cm_code  = ccn.ancien_cn;
          if (cpt = 0) then 
            nbErr  := nbErr +1;
            strErr := strErr || 'Le père de l''ancien CM ''' || corr.ancien_cn || ''' n''est pas présent dans la table de correspondance. ' || chr(13);
          end if;
        end if;
      end if;

      -- On vérifie que le CM NACRES existe (ou est local)
      select count(*) into cpt from JEFY_MARCHES.code_marche cm, JEFY_MARCHES.type_code_marche tcm 
        where tcm.tcm_id = cm.tcm_id and cm_code = corr.nouveau_cn and tcm.tcm_code in ('N', 'L');
        
      if (cpt = 0) then
        nbErr  := nbErr +1;
        strErr := strErr || 'Le CM NACRES ''' || corr.nouveau_cn || ''' n''existe pas. ' || chr(13);
      end if;
      
      -- On vérifie que les codes de niveau deux ont un père
      if (length(corr.nouveau_cn) > 2) then
        cmCodePere := substr(corr.nouveau_cn, 1, 2);
        
        select count(*) into cpt from jefy_marches.code_marche cm, JEFY_MARCHES.type_code_marche tcm 
          where tcm.tcm_id = cm.tcm_id and cm_code = cmCodePere and tcm.tcm_code in ('N', 'L');
        
        if (cpt = 0) then
          nbErr  := nbErr +1;
          strErr := strErr || 'Aucun cmPere '''|| cmCodePere ||''' trouvé pour le code '''|| corr.nouveau_cn ||'''. ';
        elsif (cpt > 1) then
          nbErr  := nbErr +1;
          strErr := strErr || 'Plusieurs cmPere '''|| cmCodePere ||''' trouvés pour le code '''|| corr.nouveau_cn ||'''. ';
        end if;
      end if;
    end loop;
    close C1;

    if (nbErr > 0) then
      JEFY_MARCHES.add_log(nbErr || ' erreur(s) : ' || chr(13) || strErr);
      commit;
      raise_application_error(-20001, 'Des erreurs sont apparues. Voir la table JEFY_MARCHES.LOG_UTILS');
    end if;
  END check_correspondance;



  -- ------------------------------------------------------ --
  -- Procédure de suppression de la bascule faite pour 2014 --
  -- ------------------------------------------------------ --
  procedure supprimer_anciens_cm_sur_2014
  is
    cpt             NUMBER;
    nb_cmf_deleted  NUMBER;
    nb_mc_deleted   NUMBER;
    nb_ce_deleted   NUMBER;
    str             CLOB;
  begin
  
    select count(*) into cpt from code_marche cm, code_exer ce, type_code_marche tcm
       where cm.cm_ordre  = ce.cm_ordre
       and   cm.tcm_id    = tcm.tcm_id
       and   ce.exe_ordre = 2014
       and   tcm.tcm_code = 'A';
    if (cpt = 0) then
      -- Pas de données à supprimer
      add_log('supprimer_anciens_cm_sur_2014 : aucune donnée à supprimer.');
      dbms_output.put_line('supprimer_anciens_cm_sur_2014 : Aucune donnée à supprimer.');
      return;
    end if; 

	  nb_cmf_deleted := 0;
    nb_mc_deleted  := 0;
    nb_ce_deleted  := 0;
	  
    -- On supprime les code_marche_four
    delete from code_marche_four
      where ce_ordre in (select distinct ce.ce_ordre
                         from code_marche cm, type_code_marche tcm, code_exer ce
                         where cm.tcm_id    = tcm.tcm_id
                         and   cm.cm_ordre  = ce.cm_ordre
                         and   tcm.tcm_code = 'A'
                         and   ce.exe_ordre = 2014)
     ;
    nb_cmf_deleted := SQL%ROWCOUNT;
  
      
    -- On supprime les mapa_ce
    delete from mapa_ce
      where ce_ordre in (select distinct ce.ce_ordre
                         from code_marche cm, code_exer ce, type_code_marche tcm
                         where cm.cm_ordre  = ce.cm_ordre
                         and   cm.tcm_id    = tcm.tcm_id
                         and   ce.exe_ordre = 2014
                         and   tcm.tcm_code = 'A');
    nb_mc_deleted := SQL%ROWCOUNT;
    
    -- On supprime les code_exer
    delete from code_exer
      where ce_ordre in (select distinct ce.ce_ordre
                         from code_marche cm, code_exer ce, type_code_marche tcm
                         where cm.cm_ordre  = ce.cm_ordre
                         and   cm.tcm_id    = tcm.tcm_id
                         and   ce.exe_ordre = 2014
                         and   tcm.tcm_code = 'A');
    nb_ce_deleted := SQL%ROWCOUNT;
    
    str := 'supprimer_anciens_cm_sur_2014 : ' || chr(13);
    str := str || ' code_marche_four supprimés : ' || nb_cmf_deleted || chr(13);
    str := str || ' mapa_ce supprimés : ' || nb_mc_deleted || chr(13);
    str := str || ' code_exer supprimés : ' || nb_ce_deleted;
    add_log(str);
    
    dbms_output.put_line('supprimer_anciens_cm_sur_2014 : Suppression terminée. Voir la table LOG_UTILS pour un récapitulatif des suppressions.');
    
  end supprimer_anciens_cm_sur_2014 ;

  -- --------------------------------------------------- --
  -- Procédure de mise à jour des codes basculés en 2014 -- 
  -- pour une utilisation sans nomenclature NACRES       --
  -- --------------------------------------------------- --
  procedure utiliser_anciens_cm_sur_2014
  is
    cpt           NUMBER;
    tcmIdLocal    NUMBER;
    nb_cm_updated NUMBER;
  begin
    select count(*) into cpt from code_marche cm, code_exer ce, type_code_marche tcm
      where cm.cm_ordre  = ce.cm_ordre
      and   cm.tcm_id    = tcm.tcm_id
      and   ce.exe_ordre = 2014
      and   tcm.tcm_code = 'A';
    
    if (cpt = 0) then
      -- Pas de données à mettre à jour
      add_log('maj_anciens_cm_sur_2014_apres_bascule : aucune donnée à mettre à jour.');
      dbms_output.put_line('utiliser_anciens_cm_sur_2014 : Aucune donnée à mettre à jour.');
      return;
    end if;
    
    select tcm_id into tcmIdLocal from type_code_marche where tcm_code = 'L';
    -- On met à jour les codes 
    update CODE_MARCHE
      set tcm_id = tcmIdLocal
      where cm_ordre in ( 
        select cm.cm_ordre from code_marche cm, code_exer ce, type_code_marche tcm
        where cm.cm_ordre  = ce.cm_ordre
        and   cm.tcm_id    = tcm.tcm_id
        and   ce.exe_ordre = 2014
        and   tcm.tcm_code = 'A');
    nb_cm_updated := SQL%ROWCOUNT;
    
    add_log('maj_anciens_cm_sur_2014_apres_bascule : ' || nb_cm_updated || ' code(s) marché mis à jour.');
    dbms_output.put_line('utiliser_anciens_cm_sur_2014 : Mise à jour terminée. Voir la table LOG_UTILS pour un récapitulatif.');
    
  end utiliser_anciens_cm_sur_2014;

  -- --------------------------------------------------------------- --
  -- Procédure de création des codes_exer locaux et nacres sur 2014  --
  -- --------------------------------------------------------------- --
  PROCEDURE create_code_exer_2014
  IS
    CURSOR C1
      IS SELECT CM.*
        FROM JEFY_MARCHES.CODE_MARCHE CM INNER JOIN JEFY_MARCHES.TYPE_CODE_MARCHE TCM ON CM.TCM_ID = TCM.TCM_ID
        WHERE TCM.TCM_CODE in ('N', 'L') AND CM.CM_NIVEAU < 3 ORDER BY CM.CM_CODE;
    
    currentCM     JEFY_MARCHES.CODE_MARCHE%ROWTYPE;
    CPT           INTEGER;
    oldCodeExer   JEFY_MARCHES.CODE_EXER%ROWTYPE;
    tcnId         NUMBER;
    strLog                    CLOB;
    strLogExists              CLOB;
    strLogTypeVide            CLOB;
    strLogMultiCorrespondance CLOB;
    
  BEGIN
    strLog := 'Lancement NACRES_MIGRATION.create_code_exer_2014...' || chr(13);
    strLogExists := '';
    strLogTypeVide := '';
    strLogMultiCorrespondance := '';
    
    OPEN C1;
      LOOP
        FETCH C1 INTO currentCM;
        EXIT WHEN C1%NOTFOUND;
          
          -- VÉRIFICATION DE L'EXISTENCE D'UNE ENTRÉE DANS CODE_EXER
          SELECT COUNT(*) INTO CPT FROM JEFY_MARCHES.CODE_EXER CE WHERE CE.CM_ORDRE = currentCM.CM_ORDRE AND CE.EXE_ORDRE = '2014';
          
          IF(CPT>0) THEN
              IF (length(strLogExists) > 0) THEN strLogExists := strLogExists || ', '; END IF;
              strLogExists := strLogExists || currentCM.CM_CODE;
          ELSE
            -- On tente de récupérerle TCN_ID si c'est un code NACRE de référence
            SELECT nvl(
              (SELECT t.tcn_id FROM JEFY_MARCHES.REFERENCE_NACRES r, JEFY_MARCHES.TYPE_CN t WHERE r.refn_ce_type = t.tcn_code AND refn_code = currentCM.CM_CODE)
              , null) INTO tcnId
            FROM DUAL;     
            
            IF (tcnId is null) THEN
              -- on tente de récupérer le type dans le CODE_EXER de 2013 du premier ancien code            
              SELECT nvl(
                (SELECT ce.tcn_id FROM JEFY_MARCHES.CORRESPONDANCE_CN corr, JEFY_MARCHES.CODE_MARCHE cm, JEFY_MARCHES.CODE_EXER ce 
                  WHERE corr.NOUVEAU_CN = currentCM.CM_CODE AND corr.ANCIEN_CN = cm.CM_CODE AND cm.CM_ORDRE = ce.CM_ORDRE AND ce.EXE_ORDRE = 2013
                  AND rownum < 2
                ), null) INTO tcnId
              FROM DUAL;
            END IF;
    
            SELECT count(*) INTO CPT FROM JEFY_MARCHES.CORRESPONDANCE_CN WHERE NOUVEAU_CN = currentCM.CM_CODE;
    
            IF (tcnId is null) 
              THEN 
              -- strLog || 'ATTENTION : Le Type Code (A, F, S ou T) en 2014 pour le CM_CODE ' || currentCM.CM_CODE || ' n''a pas pu être déterminé.' || chr(13);
              IF (length(strLogTypeVide) > 0) THEN strLogTypeVide := strLogTypeVide || ', '; END IF;
              strLogTypeVide := strLogTypeVide || currentCM.CM_CODE;
            ELSIF (CPT > 1) 
              THEN 
                -- strLogMultiCorrespondance := strLogMultiCorrespondance || 'ATTENTION : Le Type Code (A, F, S ou T) en 2014 pour le CM_CODE ' || currentCM.CM_CODE || ' est à vérifier (plusieurs anciens codes correspondants).' || chr(13);
                IF (length(strLogMultiCorrespondance) > 0) THEN strLogMultiCorrespondance := strLogMultiCorrespondance || ', '; END IF;
                strLogMultiCorrespondance := strLogMultiCorrespondance || currentCM.CM_CODE;
            END IF;
            
            -- On créé le code_exer
            INSERT INTO JEFY_MARCHES.CODE_EXER (CE_ORDRE, CM_ORDRE, EXE_ORDRE, CE_3CMP, CE_AUTRES, CE_CONTROLE, CE_MONOPOLE, CE_RECH, CE_SUPPR, TCN_ID)
              VALUES (JEFY_MARCHES.CODE_EXER_SEQ.NEXTVAL, currentCM.CM_ORDRE, '2014', 0, 0, 0, 0, 'N', 'N', tcnId);
            
          END IF;
      END LOOP;
    CLOSE C1;
    
    -- Gestion des logs
    IF (length(strLogExists) > 0) 
      THEN strLog := strLog || 'Le CODE_EXER des codes suivants existe déjà pour 2014 : ' || chr(13) || strLogExists || chr(13); 
    END IF;
    IF (length(strLogTypeVide) > 0) 
      THEN strLog := strLog || 'Le Type (A, F, S ou T) du code_exer en 2014 des codes suivants n''a pas pu être déterminé : ' || chr(13) || strLogTypeVide || chr(13); 
    END IF;
    IF (length(strLogMultiCorrespondance) > 0) 
      THEN strLog := strLog || 'Le Type (A, F, S ou T) du code_exer en 2014 des codes suivants est à vérifier (plusieurs anciens codes correspondants) : ' || chr(13) || strLogMultiCorrespondance || chr(13); 
    END IF;
    strLog := strLog || '... create_code_exer_2014 terminé.';
    add_log(strLog);
    
  END create_code_exer_2014;


  -- --------------------------------------------------------------------- --
  -- Procédure de création des code_marche_four locaux et nacres sur 2014  --
  -- en s'appuyant sur la table de correspondance.                         --
  -- Ne recréé pas les code_marche_four existants sur 2014                 --
  -- --------------------------------------------------------------------- --
  PROCEDURE create_code_marche_four_2014 
  IS 
    CURSOR C1 IS
    SELECT cmf.CMF_ORIGINE, 
           cmf.CMF_SUPPR, 
           cmf.FOU_ORDRE, 
           ce2014.ce_ordre, 
           cm2014.cm_code,
           cm2013.cm_code cm_code_orig
    FROM JEFY_MARCHES.CODE_MARCHE_FOUR   cmf, 
         JEFY_MARCHES.CODE_EXER          ce2013, 
         JEFY_MARCHES.CODE_MARCHE        cm2013,
         JEFY_MARCHES.correspondance_cn  corr,
         JEFY_MARCHES.CODE_MARCHE        cm2014,
         JEFY_MARCHES.CODE_EXER          ce2014
    WHERE cmf.CE_ORDRE      = ce2013.CE_ORDRE 
      AND ce2013.CM_ORDRE   = cm2013.CM_ORDRE
      AND ce2013.EXE_ORDRE  = 2013 
      AND ce2013.CE_SUPPR   = 'N'
      AND NOT EXISTS (
        SELECT 1 FROM JEFY_MARCHES.CODE_MARCHE_FOUR cmftemp, 
                      JEFY_MARCHES.CODE_EXER        cetemp
        WHERE cmftemp.ce_ordre  = cetemp.ce_ordre
          AND cmftemp.FOU_ORDRE = cmf.FOU_ORDRE 
          AND cmftemp.CE_ORDRE  = cetemp.CE_ORDRE
          AND cetemp.exe_ordre  = 2014
          AND cetemp.cm_ordre   = ce2013.cm_ordre
      )    
      AND cm2013.cm_code  = corr.ancien_cn (+)
      AND corr.nouveau_cn = cm2014.cm_code (+)
      AND cm2014.cm_ordre = ce2014.cm_ordre (+)
    ;
      
    strLog          CLOB;
    strLogCreate    CLOB;
    strLogNotfound  CLOB;
    
  BEGIN
    strLog          := 'Lancement NACRES_MIGRATION.create_code_marche_four_2014...' || chr(13);
    strLogCreate    := '';
    strLogNotfound  := '';

    FOR myRes IN C1 LOOP
      IF (myRes.ce_ordre IS NULL) THEN
        -- Pas de correspondance
        IF (length(strLogNotfound) > 0) THEN strLogNotfound := strLogNotfound || ', '; END IF;
        strLogNotFound := strLogNotFound || myRes.cm_code_orig;
      ELSE
        -- Correspondance, on créé le code_marche_four
        INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR (CMF_ORDRE, CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE)
          VALUES (CODE_MARCHE_FOUR_SEQ.NEXTVAL, myRes.CMF_ORIGINE, myRes.CMF_SUPPR, myRes.FOU_ORDRE, myRes.ce_ordre) ;
        -- DBMS_OUTPUT.PUT_LINE('('||CODE_MARCHE_FOUR_SEQ.NEXTVAL||'|, '||myRes.CMF_ORIGINE||', '||myRes.CMF_SUPPR||', '||myRes.FOU_ORDRE||', '||myRes.ce_ordre||')');
        -- Logs   
        IF (length(strLogCreate) > 0) THEN strLogCreate := strLogCreate || ', '; END IF;
        strLogCreate := strLogCreate || '{' || myRes.cm_code || ',' || myRes.fou_ordre || '}';
      END IF;
    END LOOP;
    
    -- Logs
    IF (length(strLogCreate) > 0) THEN strLog := strLog || ' Création de code_marche_four pour les couples {CM_CODE, FOU_ORDRE} suivants : ' || strLogCreate || chr(13); END IF;
    IF (length(strLogNotfound) > 0) THEN strLog := strLog || 'Pas de correspondance pour le(s) cm_code suivant(s) : ' || strLogNotfound || chr(13); END IF;
    strLog := strLog || '... create_code_marche_four_2014 terminé.';
    add_log(strLog);
      
  END create_code_marche_four_2014;

  -- ----------------------------------------------------- --
  -- Procédure de création des lot_nomenclature 2014       --
  -- en s'appuyant sur la table de correspondance          --
  -- Permet d'associer un lot à des codes marchés          --
  -- Ne recréé pas les lot_nomenclature existants sur 2014 --
  -- ----------------------------------------------------- --
  PROCEDURE create_lot_nomenclature_2014
  IS

    CURSOR C1
    IS 
      SELECT  cm2014.cm_ordre cm_ordre_2014, 
              cm2014.cm_code  cm_code_2014, 
              cm2013.cm_code  cm_code_2013,  
              lo.lot_ordre
      FROM  JEFY_MARCHES.LOT LO 
            INNER JOIN JEFY_MARCHES.LOT_NOMENCLATURE LN 
              ON lo.lot_ordre = ln.lot_ordre
            INNER JOIN JEFY_MARCHES.CODE_MARCHE cm2013
              ON ln.cm_ordre = cm2013.cm_ordre
            LEFT OUTER JOIN JEFY_MARCHES.CORRESPONDANCE_CN corr
              ON cm2013.cm_code = corr.ancien_cn
            LEFT OUTER JOIN JEFY_MARCHES.CODE_MARCHE cm2014
              ON corr.nouveau_cn = cm2014.cm_code
      WHERE LO.LOT_FIN > '31/12/2013' 
        AND LO.LOT_VALIDE = 'O'
        AND NOT EXISTS (
            SELECT 1 FROM JEFY_MARCHES.LOT_NOMENCLATURE lntmp 
            WHERE lntmp.cm_ordre  = cm2014.cm_ordre 
              AND lntmp.lot_ordre = lo.lot_ordre
        )
      ;
    
    strLog          CLOB;
    strLogCreate    CLOB;
    strLogNotfound  CLOB;
    
  BEGIN
      strLog          := 'Lancement NACRES_MIGRATION.create_lot_nomenclature_2014...' || chr(13);
      strLogCreate    := '';
      strLogNotfound  := '';
      
    FOR myRes IN C1
    LOOP
      IF (myRes.cm_ordre_2014 IS NULL) THEN
        -- Pas de correspondance
        IF (length(strLogNotfound) > 0) THEN strLogNotfound := strLogNotfound || ', '; END IF;
        strLogNotFound := strLogNotFound || '{' || myRes.cm_code_2013 || ', ' || myRes.lot_ordre || '}';
      ELSE
        -- Correspondance, on créé le LOT_NOMENCLATURE en faisant attention à ne pas faire de doublon
        INSERT INTO JEFY_MARCHES.LOT_NOMENCLATURE (CM_ORDRE, LOT_ORDRE, LN_ID)
          SELECT myRes.cm_ordre_2014, myRes.lot_ordre, LOT_NOMENCLATURE_SEQ.NEXTVAL FROM DUAL
            WHERE NOT EXISTS (SELECT 1 FROM JEFY_MARCHES.LOT_NOMENCLATURE 
                              WHERE CM_ORDRE = myRes.cm_ordre_2014 
                              AND LOT_ORDRE =  myRes.lot_ordre)
          ;
          -- VALUES (myRes.cm_ordre_2014, myRes.lot_ordre, LOT_NOMENCLATURE_SEQ.NEXTVAL) ;
        -- DBMS_OUTPUT.PUT_LINE(myRes.cm_ordre_2014||', '||myRes.lot_ordre);
        -- DBMS_OUTPUT.PUT_LINE('('||myRes.cm_ordre_2014||', '||myRes.lot_ordre||', '||LOT_NOMENCLATURE_SEQ.NEXTVAL||')');
        -- Logs   
        IF (length(strLogCreate) > 0) THEN strLogCreate := strLogCreate || ', '; END IF;
        strLogCreate := strLogCreate || '{' || myRes.cm_code_2014 || ',' || myRes.lot_ordre || '}';
      END IF;
    END LOOP;
     
    -- Logs
    IF (length(strLogCreate) > 0) THEN strLog := strLog || 'Création de lot_nomenclature pour les couples {CM_CODE, LOT_ORDRE} suivants : ' || strLogCreate || chr(13); END IF;
    IF (length(strLogNotfound) > 0) THEN strLog := strLog || 'Pas de correspondance pour le(s) couple(s) {CM_CODE, LOT_ORDRE} suivant(s) : ' || strLogNotfound || chr(13); END IF;
    strLog := strLog || '... create_lot_nomenclature_2014 terminé.';
    add_log(strLog);
    
  END create_lot_nomenclature_2014;

  -- ---------------------------------------------------------- --
  -- Procédure de création des SA_CONTROLE_CODE_MARCHE sur 2014 --
  -- en s'appuyant sur la table de correspondance.              --
  -- Permet de recopier les droits                              --
  -- ---------------------------------------------------------- --
  PROCEDURE create_sa_controle_cm_2014
  IS
    CURSOR C1
    IS 
      SELECT
        sacm.cm_ordre   cm_ordre_2013, 
        sacm.svac_id, 
        cm2013.cm_code  cm_code_2013, 
        cm2014.cm_code  cm_code_2014, 
        cm2014.cm_ordre cm_ordre_2014
      FROM
        JEFY_MARCHES.SA_CONTROLE_CODE_MARCHE sacm
        INNER JOIN JEFY_MARCHES.CODE_MARCHE cm2013
          ON sacm.cm_ordre = cm2013.cm_ordre
        LEFT OUTER JOIN JEFY_MARCHES.CORRESPONDANCE_CN corr
          ON cm2013.cm_code = corr.ancien_cn
        LEFT OUTER JOIN JEFY_MARCHES.CODE_MARCHE cm2014
          ON corr.nouveau_cn = cm2014.cm_code
      WHERE NOT EXISTS (
              SELECT 1 FROM JEFY_MARCHES.SA_CONTROLE_CODE_MARCHE sacmtemp 
              WHERE sacmtemp.cm_ordre  = cm2014.cm_ordre 
                AND sacmtemp.svac_id = sacm.svac_id
      );
      
    strLog          CLOB;
    strLogCreate    CLOB;
    strLogNotfound  CLOB;      
  BEGIN
    strLog          := 'Lancement NACRES_MIGRATION.create_sa_controle_cm_2014...' || chr(13);
    strLogCreate    := '';
    strLogNotfound  := '';  
    
    FOR myRes IN C1 
    LOOP
      IF (myRes.cm_ordre_2014 IS NULL) THEN
        -- Pas de correspondance
        IF (length(strLogNotfound) > 0) THEN strLogNotfound := strLogNotfound || ', '; END IF;
        strLogNotFound := strLogNotFound || myRes.cm_code_2013 || ', ' || myRes.svac_id;
      ELSE
      
        INSERT INTO JEFY_MARCHES.SA_CONTROLE_CODE_MARCHE (CCMA_DATE, CCMA_ID, CM_ORDRE, SVAC_ID)
          VALUES (sysdate, SA_CONTROLE_CODE_MARCHE_SEQ.nextval, myRes.cm_ordre_2014, myRes.svac_id);
      
        IF (length(strLogCreate) > 0) THEN strLogCreate := strLogCreate || ', '; END IF;
        strLogCreate := strLogCreate || '{' || myRes.cm_code_2014 || ',' || myRes.svac_id || '}';
      END IF;
    END LOOP;
    
    -- Logs
    IF (length(strLogCreate) > 0) THEN strLog := strLog || 'Création de SA_CONTROLE_CODE_MARCHE pour les couples {CM_CODE, SVAC_ID} suivants : ' || strLogCreate || chr(13); END IF;
    IF (length(strLogNotfound) > 0) THEN strLog := strLog || 'Pas de correspondance pour le(s) couple(s) {CM_CODE, SVAC_ID} suivant(s) : ' || strLogNotfound || chr(13); END IF;
    strLog := strLog || '... create_sa_controle_cm_2014 terminé.';
    add_log(strLog);
    
  END create_sa_controle_cm_2014;


  -- --------------------------------------------- --
  -- Procédure de création des MAPA_CE sur 2014    --
  -- en s'appuyant sur la table de correspondance. --
  -- --------------------------------------------- --
  PROCEDURE create_mapa_ce_2014
  IS
    CURSOR C1
    IS 
      SELECT cm2014.cm_ordre  cm_ordre_2014,
             cm2014.cm_code   cm_code_2014,
             ce2014.ce_ordre  ce_ordre_2014,
             cm2013.cm_code   cm_code_2013,
             mce.utl_ordre,
             mce.mc_supp,
             mte2014.mte_id   mte_id_2014
             
      FROM  JEFY_MARCHES.MAPA_CE                            mce
            INNER JOIN JEFY_MARCHES.mapa_tranche_exer       mte2013
              ON mce.mte_id = mte2013.mte_id
            INNER JOIN JEFY_MARCHES.mapa_tranche_exer       mte2014
              ON mte2013.mod_id = mte2014.mod_id              
            INNER JOIN JEFY_MARCHES.CODE_EXER               ce2013
              ON mce.ce_ordre = ce2013.ce_ordre
            INNER JOIN JEFY_MARCHES.CODE_MARCHE             cm2013
              ON ce2013.cm_ordre = cm2013.cm_ordre
            LEFT OUTER JOIN JEFY_MARCHES.CORRESPONDANCE_CN  corr
            ON cm2013.cm_code = corr.ancien_cn
            LEFT OUTER JOIN JEFY_MARCHES.CODE_MARCHE        cm2014
            ON corr.nouveau_cn = cm2014.cm_code
            LEFT OUTER JOIN JEFY_MARCHES.CODE_EXER          ce2014
            ON cm2014.cm_ordre = ce2014.cm_ordre
      WHERE mte2014.exe_ordre = 2014
        AND ce2013.exe_ordre  = 2013 
        AND ce2013.ce_suppr   = 'N'
      AND NOT EXISTS (
        SELECT 1 FROM JEFY_MARCHES.MAPA_CE mcetemp 
        WHERE mcetemp.mte_id    = mte2014.mte_id 
          AND mcetemp.ce_ordre  = ce2014.ce_ordre
      );
      
    strLog          CLOB;
    strLogCreate    CLOB;
    strLogNotfound  CLOB;      
  BEGIN
    strLog          := 'Lancement NACRES_MIGRATION.create_mapa_ce_2014...' || chr(13);
    strLogCreate    := '';
    strLogNotfound  := '';  
    
    -- INSERTING into MAPA_TRANCHE_EXER
    INSERT INTO MAPA_TRANCHE_EXER 
      (MTE_ID, TCN_ID    , DATE_VOTE_CA    , EXE_ORDRE, MTE_LIBELLE    , MTE_MAX    , MTE_MIN    , UTL_ORDRE    , DATE_MODIFICATION, MOD_ID    ) 
      SELECT mapa_tranche_exer_seq.nextval , mte.TCN_ID, mte.DATE_VOTE_CA, 2014 , mte.MTE_LIBELLE, mte.MTE_MAX, mte.MTE_MIN, mte.UTL_ORDRE, SYSDATE          , mte.MOD_ID
      FROM mapa_tranche_exer mte 
      WHERE exe_ordre = 2013
      AND NOT EXISTS (
        SELECT 1 FROM mapa_tranche_exer mtetemp 
        WHERE mtetemp.TCN_ID = mte.TCN_ID 
        AND exe_ordre = 2014 
        AND mtetemp.mod_id = mte.mod_id
    );
    
    FOR myRes IN C1 
    LOOP
      IF (myRes.cm_ordre_2014 IS NULL) THEN
        -- Pas de correspondance
        IF (length(strLogNotfound) > 0) THEN strLogNotfound := strLogNotfound || ', '; END IF;
        strLogNotFound := strLogNotFound || myRes.cm_code_2013;
      ELSE
      
        -- On créé un nouveau MAPA_CE si le couple (ce_ordre, mte_id) n'est pas déjà présent
        INSERT INTO JEFY_MARCHES.MAPA_CE (MC_ID, CE_ORDRE, MTE_ID, DATE_MODIFICATION, UTL_ORDRE, MC_SUPP)
          SELECT MAPA_CE_SEQ.NEXTVAL, myRes.ce_ordre_2014, myRes.mte_id_2014, sysdate, myRes.utl_ordre, myRes.mc_supp
          FROM DUAL 
          WHERE NOT EXISTS (
            SELECT 1 FROM JEFY_MARCHES.MAPA_CE 
            WHERE mte_id = myRes.mte_id_2014 AND ce_ordre = myRes.ce_ordre_2014
          );
      
        IF (length(strLogCreate) > 0) THEN strLogCreate := strLogCreate || ', '; END IF;
        strLogCreate := strLogCreate || '{' || myRes.cm_code_2014 || ',' || myRes.mte_id_2014 || '}';
      END IF;
    END LOOP;
    
    -- Logs
    IF (length(strLogCreate) > 0) THEN strLog := strLog || 'Création de MAPA_CE pour les couples {CM_CODE, MTE_ID} suivants : ' || strLogCreate || chr(13); END IF;
    IF (length(strLogNotfound) > 0) THEN strLog := strLog || 'Pas de correspondance pour le(s) code(s) suivant(s) : ' || strLogNotfound || chr(13); END IF;
    strLog := strLog || '... create_mapa_ce_2014 terminé.';
    add_log(strLog);
    
  END create_mapa_ce_2014;

END NACRES_MIGRATION;
/


----------------------------------------------- ----------------------- -----------------------------------------------
----------------------------------------------- Package BASCULE_MARCHES -----------------------------------------------
----------------------------------------------- ----------------------- -----------------------------------------------
-- HEADER
CREATE OR REPLACE PACKAGE JEFY_MARCHES.BASCULE_MARCHES IS
   
PROCEDURE bascule_exercice(exeOrdre NUMBER);


   PROCEDURE bascule_code_exer(
    exeOrdre NUMBER
   ) ;
   
   PROCEDURE bascule_mapace(
    exeOrdre NUMBER
   ) ;
 
   PROCEDURE bascule_3CMP(
       exeOrdre NUMBER
   )   ;
   
   PROCEDURE bascule_monopole(
       exeOrdre NUMBER
   )   ;
END; 

/
-- BODY
create or replace PACKAGE BODY JEFY_MARCHES.BASCULE_MARCHES IS

-- -------------------- --
-- Procédure principale --
-- -------------------- --
PROCEDURE bascule_exercice(exeOrdre NUMBER)   
IS
begin

    bascule_code_exer(exeOrdre);
    bascule_mapace(exeOrdre);
    bascule_3cmp(exeOrdre);
    bascule_monopole(exeOrdre);

end;

-- --------------------- --
-- Bascule des CODE_EXER --
-- --------------------- --
 PROCEDURE bascule_code_exer(
    exeOrdre NUMBER
   )   IS


  cursor c1 is select ce.* 
               from   code_exer ce, code_marche cm
               where  ce.exe_ordre = exeordre 
               and    ce.cm_ordre  = cm.cm_ordre 
               and    cm.cm_niveau > 1 
               and    ce_suppr     = 'N';

   current_code_exer   code_exer%ROWTYPE;
   current_code_marche code_marche%ROWTYPE;
   cpt integer;

   BEGIN

     -- A-t-on deja passe cette procedure ?    
     select count(*) into cpt from jefy_marches.code_exer where exe_ordre=exeOrdre;
     if(cpt>0) THEN
            RAISE_APPLICATION_ERROR (-20001,'La bascule des codes nomenclatures a deja ete faite');
     end if;
 
    -- Insertion des codes marches pour l'annee exeordre a partir de exeordre - 1 
    -- 19/12/2012 Rod: on ne recupere que les niveaux 1 et 2
    -- 08/10/2013 JBL: on ne bascule que les codes NACRES et LOCAUX.
    insert into jefy_marches.code_exer 
              (CE_3CMP, CE_AUTRES, CE_CONTROLE, CE_MONOPOLE, CE_ORDRE , 
               CE_RECH, CE_SUPPR , CE_TYPE    , CM_ORDRE   , EXE_ORDRE, TCN_ID)
        (
          select ce.ce_3cmp, ce.ce_autres, ce.ce_controle, ce.ce_monopole, code_exer_seq.nextval, 
                 ce.ce_rech, ce.ce_suppr , ce.ce_type    , ce.cm_ordre   , exeOrdre             , ce.tcn_id 
          from jefy_marches.code_exer ce 
               inner join jefy_marches.code_marche      cm  on (ce.CM_ORDRE = cm.cm_ordre)
               inner join jefy_marches.type_code_marche tcm on (tcm.tcm_id  = cm.tcm_id  )
          where ce.exe_ordre = (exeOrdre - 1) and cm.CM_NIVEAU <= 2 and tcm.tcm_code in ('N', 'L')
        );
    
      open C1;
        loop
        fetch C1 into current_code_exer;
        exit when C1%notfound;

            select * into current_code_marche from code_marche where cm_ordre = current_code_exer.cm_ordre;
                      
            if (current_code_marche.cm_pere is null)
            then
                raise_application_error(-20001, 'Pas de code MARCHE pere defini pour le code ' ||current_code_marche.cm_code|| ' , CM_ORDRE : '||current_code_marche.cm_ordre||')');
            end if;
            

            select count(*) into cpt from code_exer where exe_ordre = exeordre and cm_ordre = current_code_marche.cm_pere and ce_suppr = 'N';
            if (cpt = 0)
            then
                raise_application_error(-20001, 'Pas de code EXER pere defini pour le code ' ||current_code_marche.cm_code|| ' , CM_ORDRE_PERE : '||current_code_marche.cm_pere||')');
            end if;

            if (cpt > 1)
            then
                raise_application_error(-20001, 'Plusieurs occurences dans CODE_EXER pour le cm_ordre '||current_code_marche.cm_pere||' !');
            end if;

            -- 08/10/2013 JBL: Plus besoin de ce_pere
--            select ce_ordre into ceordrepere from code_exer where exe_ordre = exeordre and cm_ordre = current_code_marche.cm_pere and ce_suppr = 'N';
--            update code_exer set ce_pere = ceordrepere where ce_ordre = current_code_exer.ce_ordre;

        end loop;
        close C1;
    commit;
   END;

-- ------------------- --
-- Bascule des MAPA_CE --
-- ------------------- --
 PROCEDURE bascule_mapace(
    exeOrdre NUMBER
   )   IS
   
    CURSOR C1     is select m.* 
                     from   mapa_ce m 
                     where  ce_ordre in (
                            select ce.ce_ordre 
                            from   code_exer ce, code_marche cm, type_code_marche tcm
                            where  ce.cm_ordre  = cm.cm_ordre
                            and    cm.tcm_id    = tcm.tcm_id
                            and    ce.exe_ordre = (exeOrdre-1) 
                            and    ce.ce_suppr  = 'N'
                            and    cm.cm_niveau <= 2
                            and    tcm.tcm_code in ('N', 'L')
                            )
    ;
    CURSOR C2     is select m.* from mapa_tranche_exer m where exe_ordre = (exeOrdre-1);
    
    cmOrdre       NUMBER;
    ceOrdreBascul NUMBER;
    mce           mapa_ce%rowtype;
    mte           mapa_tranche_exer%rowtype;
    cnt           Integer;
    mteId         Integer;
 
 BEGIN
   
  select count(*) into cnt from jefy_marches.code_exer where exe_ordre=exeOrdre;
  if(cnt=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'Veuillez commencer par basculer les codes nomenclatures');
  end if;

  select count(*) into cnt from jefy_marches.MAPA_CE where ce_ordre in (select ce_ordre from jefy_marches.code_exer where exe_ordre=exeOrdre);
  if(cnt>0) THEN
         RAISE_APPLICATION_ERROR (-20001,'La bascule des seuils MaPA a deja ete faite');
  end if;

  
  open C1;
    loop
      fetch C1 into mce;
      exit when C1%notfound;
   
      select cm_ordre into cmOrdre       from code_exer where ce_ordre  = mce.ce_ordre;
      select ce_ordre into ceOrdreBascul from code_exer where exe_ordre = exeOrdre      and cm_ordre = cmOrdre;
      
      if(ceOrdreBascul IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001,'Un code nomenclature n a pas ete bascule sur le nouvel exercice');
      end if;
      
      INSERT INTO jefy_marches.MAPA_CE values (mapa_ce_seq.nextval,ceOrdreBascul,mce.MTE_ID, SYSDATE, mce.UTL_ORDRE, mce.MC_SUPP);
   
    end loop;
  close C1;
    
  open C2;
    loop
      fetch C2 into mte;
      exit when C2%notfound;
  
      select mapa_tranche_exer_seq.nextval into mteId from dual;
      -- INSERTING into MAPA_TRANCHE_EXER
      Insert into MAPA_TRANCHE_EXER 
             (MTE_ID, TCN_ID    , DATE_VOTE_CA    , EXE_ORDRE, MTE_LIBELLE    , MTE_MAX    , MTE_MIN    , UTL_ORDRE    , DATE_MODIFICATION, MOD_ID    ) 
      values (mteId , mte.TCN_ID, mte.DATE_VOTE_CA, exeOrdre , mte.MTE_LIBELLE, mte.MTE_MAX, mte.MTE_MIN, mte.UTL_ORDRE, SYSDATE          , mte.MOD_ID);
      
      update mapa_ce 
      set    mte_id = mteId 
      where  mte_id = mte.mte_id 
      and    ce_ordre in (
               select ce_ordre from code_exer where exe_ordre = exeOrdre
      );
      
    end loop;
  close C2;
  
  commit;
 END;


-- ------------------------------------------ --
-- Bascule des CODE_MARCHE_FOUR lié à un CM   --
-- de l'article 3 du Code des Marchés Publics --
-- ------------------------------------------ --
 PROCEDURE bascule_3CMP(
    exeOrdre NUMBER
   )   IS
   
   CURSOR C1 is select cmf.* 
                from   code_marche_four cmf,
                       code_exer ce,
                       code_marche cm,
                       type_code_marche tcm
                where  cmf.ce_ordre = ce.ce_ordre
                and    ce.cm_ordre  = cm.cm_ordre
                and    cm.tcm_id    = tcm.tcm_id
                and    ce.exe_ordre = (exeOrdre-1)
                and    ce.ce_3cmp   = 1                  
                and    not exists (select * from code_marche_four f, code_exer e
                                   where    f.fou_ordre = cmf.fou_ordre 
                                   and      f.ce_ordre  = e.ce_ordre
                                   and      e.exe_ordre = exeOrdre
                       )
                and    cm.cm_niveau <= 2
                and    tcm.tcm_code in ('N', 'L')
                order  by cmf.cmf_ordre
                ;

   cmf     code_marche_four%rowtype;
   ceOrdre NUMBER;
       
   BEGIN
   
  open C1;
  loop
   fetch C1 into cmf;
   exit when C1%notfound;

     select ce.ce_ordre into ceOrdre 
     from   code_exer ce
     where  ce.exe_ordre = exeOrdre
     and    ce.cm_ordre = (
              select e.cm_ordre 
              from   code_exer e, code_marche_four f
              where  f.cmf_ordre = cmf.cmf_ordre
              and    f.ce_ordre  = e.ce_ordre
     );

     insert into code_marche_four 
            (CE_ORDRE, CMF_ORDRE                    , CMF_ORIGINE , CMF_SUPPR     , FOU_ORDRE    )
     values (ceOrdre , code_marche_four_seq.nextval ,'MIGRATION'  , cmf.cmf_suppr , cmf.fou_ordre);

  end loop;
  close C1;

  commit;
   
  END;

-- ----------------------------- --
-- Bascule des CODE_MARCHE_FOUR  --
-- liés à un code monopolistique --
-- ----------------------------- --
 PROCEDURE bascule_monopole(
    exeOrdre NUMBER
   )   IS
   
   CURSOR C1 is select cmf.* 
                from   code_marche_four cmf,
                       code_exer ce,
                       code_marche cm,
                       type_code_marche tcm
                where  cmf.ce_ordre = ce.ce_ordre
                and    ce.cm_ordre  = cm.cm_ordre
                and    cm.tcm_id    = tcm.tcm_id
                and    ce.exe_ordre = (exeOrdre-1)
                and    ce_monopole  = 1                   
                and    not exists (select * from code_marche_four f, code_exer e
                                   where    f.fou_ordre = cmf.fou_ordre 
                                   and      f.ce_ordre  = e.ce_ordre
                                   and      e.exe_ordre = exeOrdre
                       )
                and    cm.cm_niveau <= 2
                and    tcm.tcm_code in ('N', 'L')
                order  by cmf.cmf_ordre;

  cmf     code_marche_four%rowtype;
  ceOrdre NUMBER;
  
  BEGIN
   
  open C1;
  loop
   fetch C1 into cmf;
   exit when C1%notfound;

     select ce.ce_ordre into ceOrdre 
     from   code_exer ce
     where  ce.exe_ordre = exeOrdre
     and    ce.cm_ordre = (
              select e.cm_ordre from code_exer e, code_marche_four f
              where  f.cmf_ordre = cmf.cmf_ordre
              and    f.ce_ordre = e.ce_ordre
     );

     insert into code_marche_four 
            (CE_ORDRE, CMF_ORDRE                    , CMF_ORIGINE , CMF_SUPPR     , FOU_ORDRE    )
     values (ceOrdre , code_marche_four_seq.nextval ,'MIGRATION'  , cmf.cmf_suppr , cmf.fou_ordre);

  end loop;
  close C1;

  commit;

  end;

END; 

/

commit;
