--
-- Patch DDL de JEFY_MARCHES du 09/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : JEFY_MARCHES
-- Numero de version : 2.0.9.1
-- Date de publication : 09/12/2013
-- Auteur(s) : Julien Blandineau
--
--
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- DB_VERSION 
INSERT INTO JEFY_MARCHES.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) 
  VALUES(JEFY_MARCHES.DB_VERSION_SEQ.NEXTVAL,'2.0.9.1',to_date('09/12/2013','DD/MM/YYYY'),sysdate,'NACRES : corrections FK / Drop procédures obsolètes');
COMMIT;
