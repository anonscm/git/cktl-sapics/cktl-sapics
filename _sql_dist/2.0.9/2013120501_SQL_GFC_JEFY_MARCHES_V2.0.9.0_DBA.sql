--
-- Patch DDL de JEFY_MARCHES du 05/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/3
-- Type : DBA
-- Schema : JEFY_MARCHES
-- Numero de version : 2.0.9.0
-- Date de publication : 05/12/2013
-- Auteur(s) : Julien Blandineau
--
--
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- Rebuild des index de JEFY_MARCHES sur GFC_INDX
declare
 l_sql  VARCHAR(2000);
CURSOR C1 IS
  SELECT index_name FROM DBA_indexes WHERE owner = 'JEFY_MARCHES' AND upper(tablespace_name) = 'GFC' AND index_type = 'NORMAL';
begin
  for t in C1 loop
    l_sql := 'ALTER INDEX JEFY_MARCHES.' || t.index_name || ' REBUILD tablespace GFC_INDX';
    dbms_output.put_line (l_sql);
    execute immediate l_sql;
  end loop;
  commit;
end;
/