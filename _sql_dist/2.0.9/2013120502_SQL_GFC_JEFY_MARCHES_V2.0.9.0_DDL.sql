--
-- Patch DDL de JEFY_MARCHES du 05/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/3
-- Type : DDL
-- Schema : JEFY_MARCHES
-- Numero de version : 2.0.9.0
-- Date de publication : 05/12/2013
-- Auteur(s) : Julien Blandineau
--
--
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
-- Rem : test de l'existence de la version precedente
declare
cpt integer;
reqVersion VARCHAR2(15);
newVersion VARCHAR2(15);
begin
  reqVersion := '2.0.8.3';
  newVersion := '2.0.9.0';
	
  select count(*) into cpt from JEFY_MARCHES.db_version where dbv_libelle = reqVersion;
    if cpt = 0 then
      raise_application_error(-20000,'Le user JEFY_MARCHES n''est pas à jour pour passer ce patch ! Version requise : ' || reqVersion);
    end if;

  select count(*) into cpt from JEFY_MARCHES.db_version where dbv_libelle = newVersion;
    if cpt > 0 then
      raise_application_error(-20000,'Le patch 2.0.9.0 a déjà été passé !');
  end if;
end;
/

-------------------------------------------------- ----------------- ---------------------------------------------------
-------------------------------------------------- correspondance_cn ---------------------------------------------------
-------------------------------------------------- ----------------- ---------------------------------------------------
-- Augmentation de la taille du nouveau code 
-- pour coller à la longueur de CODE_MARCHE.CM_CODE
ALTER TABLE JEFY_MARCHES.CORRESPONDANCE_CN  
MODIFY ( NOUVEAU_CN VARCHAR2(10 BYTE) );

------------------------------------------------------ --------- -------------------------------------------------------
------------------------------------------------------ code_exer -------------------------------------------------------
------------------------------------------------------ --------- -------------------------------------------------------
-- Suppression du CE_PERE
ALTER TABLE jefy_marches.code_exer
  DROP column ce_pere;

-- Ajout de l'activation du code 
ALTER TABLE jefy_marches.code_exer 
  ADD (ce_actif VARCHAR2(1) DEFAULT 'O' NOT NULL);

--------------------------------------------------- ---------------- ---------------------------------------------------
--------------------------------------------------- code_marche_four ---------------------------------------------------
--------------------------------------------------- ---------------- ---------------------------------------------------
-- Suppression du CMF_PERE
ALTER TABLE jefy_marches.code_marche_four
  DROP column cmf_pere;

-- Ajout FK vers CODE_EXER
ALTER TABLE JEFY_MARCHES.code_marche_four
  ADD (CE_ORDRE NUMBER DEFAULT -1 NOT NULL);

commit;
