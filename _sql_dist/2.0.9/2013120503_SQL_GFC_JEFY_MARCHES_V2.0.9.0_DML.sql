--
-- Patch DDL de JEFY_MARCHES du 05/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 3/3
-- Type : DML
-- Schema : JEFY_MARCHES
-- Numero de version : 2.0.9.0
-- Date de publication : 05/12/2013
-- Auteur(s) : Julien Blandineau
--
--
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- DB_VERSION 
INSERT INTO JEFY_MARCHES.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) 
  VALUES(JEFY_MARCHES.DB_VERSION_SEQ.NEXTVAL,'2.0.9.0',to_date('05/12/2013','DD/MM/YYYY'),sysdate,'NACRES : mise à jour pour la migration effective');
COMMIT;