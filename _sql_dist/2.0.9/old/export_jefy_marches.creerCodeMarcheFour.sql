--------------------------------------------------------
--  Fichier créé - lundi-octobre-07-2013   
--------------------------------------------------------

CREATE PROCEDURE JEFY_MARCHES.CREERCODEMARCHEFOUR (
    fouOrdre NUMBER,
    ceOrdre NUMBER,
    origine VARCHAR2
   )

 IS
  cm_ordre_courant NUMBER;
  cm_ordre_pere NUMBER;
  exe_ordre_courant NUMBER;
  cpt NUMBER;
  origineUp VARCHAR2(25);
  cmfSeq NUMBER;
 BEGIN

   -- verifier que les parametres sont bien remplis
   IF (fouOrdre IS NULL ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Parametre fouOrdre est null ');
   END IF;
    IF (ceOrdre IS NULL ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Parametre ceOrdre est null ');
   END IF;
    IF (origine IS NULL ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Parametre origine est null ');
   END IF;

   select count(*) into cpt from GRHUM.FOURNIS_ULR where fou_ordre=fouOrdre;

   IF (cpt=0 ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Le fournisseur n''existe pas ');
   END IF;

   select count(*) into cpt from code_exer where ce_ordre=ceOrdre;

   IF (cpt=0 ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Le code_exer n''existe pas ');
   END IF;

   ---------------------------------------------------------------------------

   select cm_ordre,exe_ordre into cm_ordre_courant,exe_ordre_courant from code_exer where ce_ordre=ceOrdre;

   select count(*) into cpt from code_marche_four where fou_ordre=fouOrdre and cm_ordre=cm_ordre_courant
      and exe_ordre=exe_ordre_courant;

   if cpt=0 then
      origineUp := UPPER(origine);

      IF (cm_ordre_courant=null or exe_ordre_courant=null ) THEN
          RAISE_APPLICATION_ERROR (-20001,'Problème d''integrité de la base, toutes les données ne sont pas renseignées ');
      END IF;

      select code_marche_four_seq.nextval into cmfSeq from dual;

      INSERT INTO CODE_MARCHE_FOUR ( EXE_ORDRE, CM_ORDRE, CMF_ORDRE, CMF_ORIGINE, CMF_PERE, CMF_SUPPR, FOU_ORDRE )
       VALUES
       (exe_ordre_courant, cm_ordre_courant, cmfSeq, origine, null, 'N', fouOrdre);

      cm_ordre_pere:=cm_ordre_courant;

      creerCodeMarcheFourFils(cmfSeq,cm_ordre_pere,fouOrdre,origine,exe_ordre_courant);

   end if;

 END;
