--------------------------------------------------------
--  Fichier créé - lundi-octobre-07-2013   
--------------------------------------------------------

CREATE PROCEDURE JEFY_MARCHES.CREERCODEMARCHEFOURFILS (
    cmfOrdrePere NUMBER,
 cmOrdrePere NUMBER,
 fouOrdre NUMBER,
    origine VARCHAR2,
 exeOrdre NUMBER
   )

 IS
  CURSOR C1 is select cm_ordre from code_marche where cm_pere=cmOrdrePere;
  cm_ordre_courant NUMBER;
  cm_ordre_pere NUMBER;
  cpt NUMBER;
  origineUp VARCHAR2(25);
  cmfSeq NUMBER;
 BEGIN

   -- verifier que les parametres sont bien remplis
   IF (cmfOrdrePere IS NULL ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Parametre cmfOrdrePere est null ');
   END IF;
   IF (cmOrdrePere IS NULL ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Parametre ceOrdre est null ');
   END IF;
   IF (fouOrdre IS NULL ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Parametre fouOrdre est null ');
   END IF;
   IF (origine IS NULL ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Parametre origine est null ');
   END IF;

   select count(*) into cpt from code_marche_four where cmf_ordre=cmfOrdrePere;

   IF (cpt=0 ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Le cmfOrdrePere n''existe pas ');
   END IF;

   select count(*) into cpt from code_marche where cm_ordre=cmOrdrePere;

   IF (cpt=0 ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Le cmOrdrePere n''existe pas ');
   END IF;

   select count(*) into cpt from GRHUM.FOURNIS_ULR where fou_ordre=fouOrdre;

   IF (cpt=0 ) THEN
       RAISE_APPLICATION_ERROR (-20001,'Le fournisseur n''existe pas ');
   END IF;


   origineUp := UPPER(origine);

   open C1;
   loop
   fetch C1 into cm_ordre_courant;
   exit when C1%notfound;

     select code_marche_four_seq.nextval into cmfSeq from dual;

     INSERT INTO CODE_MARCHE_FOUR ( EXE_ORDRE, CM_ORDRE, CMF_ORDRE, CMF_ORIGINE, CMF_PERE, CMF_SUPPR, FOU_ORDRE )
     VALUES
     (exeOrdre, cm_ordre_courant, cmfSeq, origine, cmfOrdrePere, 'N', fouOrdre);

     cm_ordre_pere:=cm_ordre_courant;

     creerCodeMarcheFourFils(cmfSeq,cm_ordre_pere,fouOrdre,origine,exeOrdre);


   end loop;
   close C1;

 END;
