-- Rebuild des index de JEFY_MARCHES sur GFC_INDX
declare
 l_sql  VARCHAR(2000);
CURSOR C1 IS
  SELECT index_name FROM DBA_indexes WHERE owner = 'JEFY_MARCHES' AND upper(tablespace_name) = 'GFC' AND index_type = 'NORMAL';
begin
  for t in C1 loop
    l_sql := 'ALTER INDEX JEFY_MARCHES.' || t.index_name || ' REBUILD tablespace GFC_INDX';
    dbms_output.put_line (l_sql);
    execute immediate l_sql;
  end loop;
end;
/