-- Suppression du CE_PERE
ALTER TABLE jefy_marches.code_exer
  DROP column ce_pere;

-- Ajout de la visibilité du code 
ALTER TABLE jefy_marches.code_exer 
  ADD (ce_visible VARCHAR2(1) DEFAULT 'O' NOT NULL);