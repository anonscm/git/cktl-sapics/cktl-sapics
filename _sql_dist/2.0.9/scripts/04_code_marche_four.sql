-- Suppression de l'arbo

alter table jefy_marches.code_marche_four
drop column cmf_pere;

--
--
--

-- Ajout FK vers CODE_EXER

ALTER TABLE JEFY_MARCHES.code_marche_four
add
(
    CE_ORDRE    NUMBER DEFAULT -1 NOT NULL 
)
;


-- Mise à jour du code_exer

update JEFY_MARCHES.code_marche_four cmf
set ce_ordre = nvl((
    select ce.ce_ordre
    from JEFY_MARCHES.code_exer ce 
    where ce.cm_ordre = cmf.cm_ordre 
    and ce.exe_ordre = cmf.exe_ordre 
    and ce.ce_suppr = 'N'
    ), -1)
;

-- Suppression des colonnes inutilisées
alter table jefy_marches.code_marche_four drop column exe_ordre;
alter table jefy_marches.code_marche_four drop column cm_ordre;


-- Ajout de la contrainte FK

ALTER TABLE JEFY_MARCHES.code_marche_four
add CONSTRAINT FK_CE_ORDRE
   FOREIGN KEY (ce_ordre)
   REFERENCES JEFY_MARCHES.code_exer (ce_ordre)
;

/*
select exe_ordre, cm_ordre

from JEFY_MARCHES.code_exer 
where ce_suppr = 'N'
group by exe_ordre, cm_ordre
having count(*) > 1;


select * from JEFY_MARCHES.code_exer  where exe_ordre = 2010 and cm_ordre = 9347;




select exe_ordre, cm_ordre from jefy_marches.code_marche_four
where (exe_ordre, cm_ordre) not in (
        select exe_ordre, cm_ordre
        from JEFY_MARCHES.code_exer 
        --where ce_suppr = 'N'
        )
;

select *
from JEFY_MARCHES.code_exer 
where exe_ordre = 2013 and cm_ordre = 11321;


select * from jefy_marches.code_marche
where cm_ordre = 11321;

select count(*) from jefy_marches.code_marche_four
where ce_ordre <> -1;

*/