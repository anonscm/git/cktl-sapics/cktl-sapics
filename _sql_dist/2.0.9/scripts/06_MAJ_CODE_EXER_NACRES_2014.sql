create or replace PROCEDURE JEFY_MARCHES.MAJ_CODE_EXER_NACRES_2014 AS 
  CURSOR C1
    IS SELECT CM.*
      FROM JEFY_MARCHES.CODE_MARCHE CM INNER JOIN JEFY_MARCHES.TYPE_CODE_MARCHE TCM ON CM.TCM_ID = TCM.TCM_ID
      WHERE TCM.TCM_CODE in ('N', 'L') AND CM.CM_NIVEAU < 3;
  
  currentCM   JEFY_MARCHES.CODE_MARCHE%ROWTYPE;
  CPT         INTEGER;
  oldCodeExer JEFY_MARCHES.CODE_EXER%ROWTYPE;
  strLog      CLOB;
  tcnId       NUMBER;
BEGIN
  strLog := '';
  OPEN C1;
    LOOP
      FETCH C1 INTO currentCM;
      EXIT WHEN C1%NOTFOUND;
        
        -- VÉRIFICATION DE L'EXISTENCE D'UNE ENTRÉE DANS CODE_EXER
        SELECT COUNT(*) INTO CPT FROM JEFY_MARCHES.CODE_EXER CE WHERE CE.CM_ORDRE = currentCM.CM_ORDRE AND CE.EXE_ORDRE = '2014';
        
        IF(CPT>0) THEN
            strLog := strLog || 'Un CODE_EXER existe déjà en 2014 pour le CM_CODE ' || currentCM.CM_CODE || chr(13);
            -- DBMS_OUTPUT.PUT_LINE ('UN CODE_EXER EXISTE DÉJÀ POUR 2014 et ce CM_ORDRE  : '||CURRENT_CM_ORDRE_NACRES||'');
            -- MISE À JOUR AVEC CODE NACRE 
        ELSE
          -- On tente de récupérerle TCN_ID si c'est un code NACRE de référence
          SELECT nvl(
            (SELECT t.tcn_id FROM JEFY_MARCHES.REFERENCE_NACRES r, JEFY_MARCHES.TYPE_CN t WHERE r.refn_ce_type = t.tcn_code AND refn_code = currentCM.CM_CODE)
            , null) INTO tcnId
          FROM DUAL;     
          
          IF (tcnId is null) THEN
            -- on tente de récupérer le type dans le CODE_EXER de 2013 du premier ancien code            
            SELECT nvl(
              (SELECT ce.tcn_id FROM JEFY_MARCHES.CORRESPONDANCE_CN corr, JEFY_MARCHES.CODE_MARCHE cm, JEFY_MARCHES.CODE_EXER ce 
                WHERE corr.NOUVEAU_CN = currentCM.CM_CODE AND corr.ANCIEN_CN = cm.CM_CODE AND cm.CM_ORDRE = ce.CM_ORDRE AND ce.EXE_ORDRE = 2013
                AND rownum < 2
              ), null) INTO tcnId
            FROM DUAL;
          END IF;
  
          SELECT count(*) INTO CPT FROM JEFY_MARCHES.CORRESPONDANCE_CN WHERE NOUVEAU_CN = currentCM.CM_CODE;
  
          IF (tcnId is null) 
            THEN strLog := strLog || 'ATTENTION : Le Type Code (A, F, S ou T) en 2014 pour le CM_CODE ' || currentCM.CM_CODE || ' n''a pas pu être déterminé.' || chr(13);
          ELSIF (CPT > 1) 
            THEN strLog := strLog || 'ATTENTION : Le Type Code (A, F, S ou T) en 2014 pour le CM_CODE ' || currentCM.CM_CODE || ' est à vérifier (plusieurs anciens codes correspondants).' || chr(13);
          END IF;
          
          -- On créé le code_exer
          INSERT INTO JEFY_MARCHES.CODE_EXER (CE_ORDRE, CM_ORDRE, EXE_ORDRE, CE_3CMP, CE_AUTRES, CE_CONTROLE, CE_MONOPOLE, CE_SUPPR, TCN_ID)
            VALUES (JEFY_MARCHES.CODE_EXER_SEQ.NEXTVAL, currentCM.CM_ORDRE, '2014', 0, 0, 0, 0, 'N', tcnId);
          
        END IF;
    END LOOP;
  CLOSE C1;
  add_log(strLog);
END MAJ_CODE_EXER_NACRES_2014;
