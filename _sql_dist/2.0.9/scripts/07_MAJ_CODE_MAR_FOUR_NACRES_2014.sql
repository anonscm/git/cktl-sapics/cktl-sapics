CREATE OR REPLACE PROCEDURE JEFY_MARCHES.MAJ_CODE_MAR_FOUR_NACRES_2014_OLD AS 
CURSOR C1
  IS SELECT CMF.*
    FROM JEFY_MARCHES.CODE_MARCHE_FOUR CMF 
    INNER JOIN JEFY_MARCHES.CODE_EXER CE 
      ON CMF.CE_ORDRE = CE.CE_ORDRE 
    INNER JOIN JEFY_MARCHES.CODE_MARCHE CM 
      ON CE.CM_ORDRE = CM.CM_ORDRE
    WHERE CE.EXE_ORDRE = 2013 AND CE_SUPPR = 'N';
  
  currentCMF      JEFY_MARCHES.CODE_MARCHE_FOUR%ROWTYPE;
  newCeOrdre      INTEGER;
  strLog          CLOB;
  strLogCreate    CLOB;
  strLogNotfound  CLOB;
  
  
BEGIN
  strLog          := 'Lancement NACRES_MIGRATION.create_code_exer_2014...' || chr(13);
  strLogCreate    := '';
  strLogNotfound  := '';
  OPEN C1;
      LOOP
        FETCH C1 INTO currentCMF;
        EXIT WHEN C1%NOTFOUND;
        
        BEGIN
            
            FOR newCeOrdre IN (
              SELECT CE2.CE_ORDRE
              FROM JEFY_MARCHES.CODE_MARCHE       CM1, 
                   JEFY_MARCHES.CODE_MARCHE       CM2, 
                   JEFY_MARCHES.CODE_EXER         CE1, 
                   JEFY_MARCHES.CODE_EXER         CE2, 
                   JEFY_MARCHES.CORRESPONDANCE_CN COCN
              WHERE CE1.CM_ORDRE    = CM1.CM_ORDRE 
                AND CM1.CM_CODE     = COCN.ANCIEN_CN 
                AND COCN.NOUVEAU_CN = CM2.CM_CODE 
                AND CM2.CM_ORDRE    = CE2.CM_ORDRE 
                AND CE1.CE_ORDRE    = currentCMF.CE_ORDRE
                AND CE2.EXE_ORDRE   = 2014
                AND NOT EXISTS (SELECT 1 FROM JEFY_MARCHES.CODE_MARCHE_FOUR cmftemp 
                                WHERE cmftemp.FOU_ORDRE = currentCMF.FOU_ORDRE 
                                  AND cmftemp.CE_ORDRE  = CE2.CE_ORDRE
                )
            ) LOOP
        
              INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR (CMF_ORDRE, CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE)
                VALUES (CODE_MARCHE_FOUR_SEQ.NEXTVAL, currentCMF.CMF_ORIGINE, currentCMF.CMF_SUPPR, currentCMF.FOU_ORDRE, newCeOrdre) ;
              
              IF (length(strLogCreate) > 0) THEN strLogCreate := strLogCreate || ', '; END IF;
              strLogCreate := strLogCreate || newCeOrdre;
              
            
            END LOOP;            
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
            IF (length(strLogNotfound) > 0) THEN strLogNotfound := strLogNotfound || ', '; END IF;
            strLogNotfound := strLogNotfound || currentCMF.CE_ORDRE;
            --  DBMS_OUTPUT.PUT_LINE ('PAS DE CORRESPONDANCE NACRES, CE_ORDRE  : '||currentCMF.CE_ORDRE||'');
        END;
    END LOOP;
  CLOSE C1;
  
  -- Logs
  IF (length(strLogCreate) > 0) THEN strLog := strLog || ' Création de code_marche_four pour le(s) ce_ordre suivant(s) : ' || strLogCreate || chr(13); END IF;
  IF (length(strLogNotfound) > 0) THEN strLog := strLog || 'Pas de correspondance pour le(s) ce_ordre suivant(s) : ' || strLogNotfound || chr(13); END IF;
  strLog := strLog || '... create_code_exer_2014 terminé.';
  add_log(strLog);
  
END MAJ_CODE_MAR_FOUR_NACRES_2014_OLD;


--
-- 
--


CREATE OR REPLACE PROCEDURE JEFY_MARCHES.MAJ_CODE_MAR_FOUR_NACRES_2014 
AS 
  CURSOR C1 IS
    SELECT cmf.CMF_ORIGINE, 
           cmf.CMF_SUPPR, 
           cmf.FOU_ORDRE, 
           ce2014.ce_ordre, 
           cm2014.cm_code,
           cm2.cm_code cm_code_orig
    FROM JEFY_MARCHES.CODE_MARCHE_FOUR   cmf, 
         JEFY_MARCHES.CODE_EXER          ce, 
         JEFY_MARCHES.CODE_MARCHE        cm,
         JEFY_MARCHES.correspondance_cn  corr,
         JEFY_MARCHES.CODE_MARCHE        cm2014,
         JEFY_MARCHES.CODE_EXER          ce2014
    WHERE cmf.CE_ORDRE  = ce.CE_ORDRE 
      AND ce.CM_ORDRE   = cm.CM_ORDRE
      AND ce.EXE_ORDRE  = 2013 
      AND ce.CE_SUPPR   = 'N'
      AND NOT EXISTS (
        SELECT 1 FROM JEFY_MARCHES.CODE_MARCHE_FOUR cmftemp, 
                      JEFY_MARCHES.CODE_EXER        cetemp
        WHERE cmftemp.ce_ordre  = cetemp.ce_ordre
          AND cmftemp.FOU_ORDRE = cmf.FOU_ORDRE 
          AND cmftemp.CE_ORDRE  = cetemp.CE_ORDRE
          AND cetemp.exe_ordre  = 2014
          AND cetemp.cm_ordre   = ce.cm_ordre
      )    
      AND cm.cm_code      = corr.ancien_cn (+)
      AND corr.nouveau_cn = cm2014.cm_code (+)
      AND cm2014.cm_ordre = ce2014.cm_ordre (+)
    ;
  
  strLog          CLOB;
  strLogCreate    CLOB;
  strLogNotfound  CLOB;

BEGIN
  strLog          := 'Lancement NACRES_MIGRATION.MAJ_CODE_MAR_FOUR_NACRES_2014...' || chr(13);
  strLogCreate    := '';
  strLogNotfound  := '';
  
  
  FOR myRes IN C1 LOOP
    IF (myRes.ce_ordre IS NULL) THEN
      -- Pas de correspondance
      IF (length(strLogNotfound) > 0) THEN strLogNotfound := strLogNotfound || ', '; END IF;
      strLogNotFound := strLogNotFound || myRes.cm_code_orig;
    ELSE
      -- Correspondance, on créé le code_marche_four
      INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR (CMF_ORDRE, CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE)
        VALUES (CODE_MARCHE_FOUR_SEQ.NEXTVAL, myRes.CMF_ORIGINE, myRes.CMF_SUPPR, myRes.FOU_ORDRE, myRes.ce_ordre) ;
      -- Logs   
      IF (length(strLogCreate) > 0) THEN strLogCreate := strLogCreate || ', '; END IF;
      strLogCreate := strLogCreate || '{' || myRes.cm_code || ',' || myRes.fou_ordre || '}';
    END IF;
  END LOOP;
  
  -- Logs
  IF (length(strLogCreate) > 0) THEN strLog := strLog || ' Création de code_marche_four pour les couples {CM_CODE, FOU_ORDRE} suivants : ' || strLogCreate || chr(13); END IF;
  IF (length(strLogNotfound) > 0) THEN strLog := strLog || 'Pas de correspondance pour le(s) cm_code suivant(s) : ' || strLogNotfound || chr(13); END IF;
  strLog := strLog || '... MAJ_CODE_MAR_FOUR_NACRES_2014 terminé.';
  add_log(strLog);
  
END MAJ_CODE_MAR_FOUR_NACRES_2014;


