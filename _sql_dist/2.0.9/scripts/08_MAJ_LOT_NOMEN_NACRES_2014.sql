create or replace PROCEDURE                                                     MAJ_LOT_NOMEN_NACRES_2014_OLD AS 

  CURSOR C1
  IS SELECT LN.*
    FROM JEFY_MARCHES.LOT LO INNER JOIN JEFY_MARCHES.LOT_NOMENCLATURE LN ON LO.LOT_ORDRE = LN.LOT_ORDRE
    WHERE LO.LOT_FIN>'31/12/2013' AND LO.LOT_VALIDE = 'O';
  
  CURRENT_LOT_NOMENCLATURE LOT_NOMENCLATURE%ROWTYPE;
  CM_ORDRE_NACRES INTEGER;
  CPT INTEGER;
  
BEGIN
  OPEN C1;
      LOOP
        FETCH C1 INTO CURRENT_LOT_NOMENCLATURE;
        EXIT WHEN C1%NOTFOUND;

        BEGIN
        -- VÉRIFICATION DE L'EXITENCE D'UNE CORRESPONDANCE NACRES
        SELECT CM2.CM_ORDRE INTO CM_ORDRE_NACRES
        FROM JEFY_MARCHES.CODE_MARCHE CM1, JEFY_MARCHES.CODE_MARCHE CM2, JEFY_MARCHES.CORRESPONDANCE_CN COCN
        WHERE CM1.CM_CODE = COCN.ANCIEN_CN AND COCN.CN_NACRES=CM2.CM_CODE AND CM1.CM_ORDRE=CURRENT_LOT_NOMENCLATURE.CM_ORDRE;
        
        -- VÉRIFICATION DE L'EXITENCE D'UNE ENTRÉE DANS LOT_NOMENCLATURE
          SELECT COUNT(*) INTO CPT FROM JEFY_MARCHES.LOT_NOMENCLATURE LN WHERE LN.CM_ORDRE = CM_ORDRE_NACRES AND LN.LOT_ORDRE=CURRENT_LOT_NOMENCLATURE.LOT_ORDRE;
          
          IF(CPT>0) THEN
              DBMS_OUTPUT.PUT_LINE ('UNE NOMENCLATURE EST DÉJÀ ASSOCIÉE A CE LOT, LOT_ORDRE  : '||CURRENT_LOT_NOMENCLATURE.LOT_ORDRE||', CM_ORDRE = ' || CM_ORDRE_NACRES ||'');
          
          ELSE
              INSERT INTO JEFY_MARCHES.LOT_NOMENCLATURE (CM_ORDRE, LOT_ORDRE, LN_ID)
              VALUES (CM_ORDRE_NACRES, CURRENT_LOT_NOMENCLATURE.LOT_ORDRE, LOT_NOMENCLATURE_SEQ.NEXTVAL) ;
              DBMS_OUTPUT.PUT_LINE(CM_ORDRE_NACRES);
  
          END IF;
        
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
                DBMS_OUTPUT.PUT_LINE ('PAS DE CORRESPONDANCE NACRES,  CM_ORDRE : '||CM_ORDRE_NACRES||'');
        END;
        
      END LOOP;
  CLOSE C1;
END MAJ_LOT_NOMEN_NACRES_2014_OLD;


create or replace PROCEDURE MAJ_LOT_NOMEN_NACRES_2014 AS 

  CURSOR C1
  IS 
    SELECT  cm2014.cm_ordre cm_ordre_2014, 
            cm2014.cm_code  cm_code_2014, 
            cm2013.cm_code  cm_code_2013,  
            lo.lot_ordre
    FROM  JEFY_MARCHES.LOT LO 
          INNER JOIN JEFY_MARCHES.LOT_NOMENCLATURE LN 
            ON lo.lot_ordre = ln.lot_ordre
          INNER JOIN JEFY_MARCHES.CODE_MARCHE cm2013
            ON ln.cm_ordre = cm2013.cm_ordre
          LEFT OUTER JOIN JEFY_MARCHES.CORRESPONDANCE_CN corr
            ON cm2013.cm_code = corr.ancien_cn
          LEFT OUTER JOIN JEFY_MARCHES.CODE_MARCHE cm2014
            ON corr.nouveau_cn = cm2014.cm_code
    WHERE LO.LOT_FIN > '31/12/2013' 
      AND LO.LOT_VALIDE = 'O'
      AND NOT EXISTS (
          SELECT 1 FROM JEFY_MARCHES.LOT_NOMENCLATURE lntmp 
          WHERE lntmp.cm_ordre  = cm2014.cm_ordre 
            AND lntmp.lot_ordre = lo.lot_ordre
      )
    ;
  
  strLog          CLOB;
  strLogCreate    CLOB;
  strLogNotfound  CLOB;
  
BEGIN
    strLog          := 'Lancement NACRES_MIGRATION.create_code_marche_four_2014...' || chr(13);
    strLogCreate    := '';
    strLogNotfound  := '';
    
  FOR myRes IN C1
  LOOP
    IF (cm_ordre_2014 IS NULL) THEN
      -- Pas de correspondance
      IF (length(strLogNotfound) > 0) THEN strLogNotfound := strLogNotfound || ', '; END IF;
      strLogNotFound := strLogNotFound || '{' || myRes.cm_code_2013 || ', ' || myRes.lot_ordre || '}';
    ELSE
      -- Correspondance, on créé le LOT_NOMENCLATURE
      -- INSERT INTO JEFY_MARCHES.LOT_NOMENCLATURE (CM_ORDRE, LOT_ORDRE, LN_ID)
      --  VALUES (myRes.cm_ordre_2014, myRes.lot_ordre, LOT_NOMENCLATURE_SEQ.NEXTVAL) ;

      DBMS_OUTPUT.PUT_LINE('('||myRes.cm_ordre_2014||'|, '||myRes.lot_ordre||', '||LOT_NOMENCLATURE_SEQ.NEXTVAL||')');
      -- Logs   
      IF (length(strLogCreate) > 0) THEN strLogCreate := strLogCreate || ', '; END IF;
      strLogCreate := strLogCreate || '{' || myRes.cm_code_2014 || ',' || myRes.lot_ordre || '}';
    END IF;
  END LOOP;
  
END MAJ_LOT_NOMEN_NACRES_2014;