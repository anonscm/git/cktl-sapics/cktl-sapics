/******************************
 * JEFY_DEPENSE.V_CODE_MARCHE *
 ******************************/
CREATE OR REPLACE FORCE VIEW JEFY_DEPENSE.V_CODE_MARCHE (
	CM_CODE,   CM_DETAIL, CM_LIB,   CM_LIB_COURT, CM_NIVEAU, 
	CM_ORDRE,  CM_PERE,   CM_SUPPR, TCM_ID)
AS
  SELECT
    CM_CODE,  CM_DETAIL, CM_LIB,   CM_LIB_COURT, CM_NIVEAU, 
    CM_ORDRE, CM_PERE,   CM_SUPPR, TCM_ID
  FROM
    jefy_marches.code_marche
;

/***********************************
 * JEFY_DEPENSE.V_CODE_MARCHE_FOUR *
 ***********************************/
CREATE OR REPLACE FORCE VIEW JEFY_DEPENSE.V_CODE_MARCHE_FOUR (
	CE_ORDRE, FOU_ORDRE)
AS
  SELECT
    e.ce_ordre, f.fou_ordre
  FROM
    jefy_marches.code_marche_four f,
    jefy_marches.code_exer e
  WHERE
      f.ce_ordre  = e.ce_ordre
  AND f.cmf_suppr = 'N'
  AND (
       e.ce_monopole = 1
    OR e.ce_3cmp     = 1
  )
;
 
/****************************
 * JEFY_DEPENSE.V_CODE_EXER *
 ****************************/
CREATE OR REPLACE FORCE VIEW JEFY_DEPENSE.V_CODE_EXER (
	CE_3CMP, CE_AUTRES, CE_CONTROLE, CE_MONOPOLE, CE_ORDRE, 
	CE_RECH, CE_SUPPR,  CE_TYPE,     CM_ORDRE,    EXE_ORDRE, CE_ACTIF)
AS
  SELECT
    CE_3CMP, CE_AUTRES, CE_CONTROLE, CE_MONOPOLE, CE_ORDRE,
    CE_RECH, CE_SUPPR,  CE_TYPE,     CM_ORDRE,    EXE_ORDRE, CE_ACTIF
  FROM
    jefy_marches.code_exer ;
