/**************************************************
 * PROCEDURE JEFY_MARCHES.creerCodeMarcheFourFils *
 **************************************************/
create or replace PROCEDURE JEFY_MARCHES.creerCodeMarcheFourFils(
  cmOrdrePere NUMBER,
  fouOrdre    NUMBER,
  origine     VARCHAR2,
  exeOrdre    NUMBER
)
IS
  CURSOR C1 is 
    select cm_ordre from code_marche where cm_pere = cmOrdrePere;
  cm_ordre_courant  NUMBER;
  ce_ordre_courant  NUMBER;
  cpt               NUMBER;
  origineUp         VARCHAR2(25);
  cmfSeq            NUMBER;
  
BEGIN

  ----------------------------------- verifier que les parametres sont bien remplis -----------------------------------
  IF (cmOrdrePere IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre ceOrdre est null ');
  END IF;
  IF (fouOrdre IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre fouOrdre est null ');
  END IF;
  IF (origine IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre origine est null ');
  END IF;
  
  select count(*) into cpt from code_marche where cm_ordre = cmOrdrePere;
  IF (cpt=0 ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le cmOrdrePere n''existe pas ');
  END IF;
  
  select count(*) into cpt from GRHUM.FOURNIS_ULR where fou_ordre = fouOrdre;
  IF (cpt=0 ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le fournisseur n''existe pas ');
  END IF;
  ----------------------------------------------------------------------------------------------------------------------
  origineUp := UPPER(origine);
  OPEN C1;
  LOOP
    fetch C1 into cm_ordre_courant;
    exit when C1%notfound;
    
    -- dbms_output.put_line('DEBUG :: DEBUT FOUR FILS : ' || cm_ordre_courant);
    -- Le code_exer existe ?
    select count(*) into cpt from code_exer where exe_ordre = exeOrdre and cm_ordre = cm_ordre_courant;
    IF cpt > 0 THEN
      select ce_ordre into ce_ordre_courant from code_exer 
      where exe_ordre = exeOrdre 
        and cm_ordre  = cm_ordre_courant;
  
      -- dbms_output.put_line('DEBUG :: ce_ordre_courant : ' || ce_ordre_courant);
      -- dbms_output.put_line('DEBUG :: INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR (CMF_ORDRE, CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE ) 
      --   VALUES ('||cmfSeq||', '||origine||', '||'''N'', '||fouOrdre||', '||ce_ordre_courant||');');
      INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR 
             (CMF_ORDRE,                    CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE)
      VALUES (code_marche_four_seq.nextval, origine,     'N',       fouOrdre,  ce_ordre_courant);
  
      -- Récursivité
      -- dbms_output.put_line('DEBUG :: Appel fils');      
      creerCodeMarcheFourFils(cm_ordre_courant, fouOrdre, origine, exeOrdre);
    END IF;
  END LOOP;
  CLOSE C1;
  
  dbms_output.put_line('DEBUG :: FIN FOUR FILS');

 END;
/

/**********************************************
 * PROCEDURE JEFY_MARCHES.creerCodeMarcheFour *
 **********************************************/
create or replace PROCEDURE JEFY_MARCHES.creerCodeMarcheFour(
  fouOrdre NUMBER,
  ceOrdre  NUMBER,
  origine  VARCHAR2
)
IS
  cm_ordre_courant  NUMBER;
  exe_ordre_courant NUMBER;
  cpt               NUMBER;
BEGIN
  ----------------------------------- verifier que les parametres sont bien remplis -----------------------------------
  IF (fouOrdre IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre fouOrdre est null ');
  END IF;
  IF (ceOrdre IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre ceOrdre est null ');
  END IF;
  IF (origine IS NULL ) THEN
    RAISE_APPLICATION_ERROR (-20001,'Parametre origine est null ');
  END IF;
  
  select count(*) into cpt from GRHUM.FOURNIS_ULR where fou_ordre = fouOrdre;
  IF (cpt = 0) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le fournisseur n''existe pas (fou_ordre = ' || fouOrdre || ')');
  END IF;
  
  select count(*) into cpt from code_exer where ce_ordre = ceOrdre;
  IF (cpt = 0) THEN
    RAISE_APPLICATION_ERROR (-20001,'Le code_exer n''existe pas (ce_ordre = ' || ceOrdre || ')');
  END IF;
  ----------------------------------------------------------------------------------------------------------------------
  
  -- On récupère le CM_ORDRE et EXE_ORDRE correspondant au ceOrdre
  select cm_ordre, exe_ordre into cm_ordre_courant, exe_ordre_courant from code_exer where ce_ordre = ceOrdre;
  -- Un CODE_MARCHE_FOUR correspondant ?
  select count(*) into cpt from code_marche_four where ce_ordre = ceOrdre;
  IF cpt = 0 THEN
    origine := UPPER(origine);

    -- dbms_output.put_line('DEBUG :: INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR (CMF_ORDRE, CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE ) 
    -- VALUES ('||cmfSeq||', '||origine||', '||'''N'', '||fouOrdre||', '||ceOrdre||');');

    INSERT INTO JEFY_MARCHES.CODE_MARCHE_FOUR 
              (CMF_ORDRE,                    CMF_ORIGINE, CMF_SUPPR, FOU_ORDRE, CE_ORDRE) 
      VALUES  (code_marche_four_seq.nextval, origine,     'N',       fouOrdre,  ceOrdre);
      
    -- Récursivité
    -- dbms_output.put_line('DEBUG :: creerCodeMarcheFourFils_test('||cm_ordre_pere||', '||fouOrdre||', '||origine||', '||exe_ordre_courant||');');
    creerCodeMarcheFourFils(cm_ordre_courant, fouOrdre, origine, exe_ordre_courant);
  END IF;
END;
 
/
