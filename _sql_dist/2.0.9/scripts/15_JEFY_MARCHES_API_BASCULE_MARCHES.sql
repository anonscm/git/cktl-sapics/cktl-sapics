--------------------------------------------------------
--  DDL for Package BASCULE_MARCHES
--------------------------------------------------------

CREATE OR REPLACE PACKAGE JEFY_MARCHES.BASCULE_MARCHES IS
   
PROCEDURE bascule_exercice(exeOrdre NUMBER);


   PROCEDURE bascule_code_exer(
    exeOrdre NUMBER
   ) ;
   
   PROCEDURE bascule_mapace(
    exeOrdre NUMBER
   ) ;
 
   PROCEDURE bascule_3CMP(
       exeOrdre NUMBER
   )   ;
   
   PROCEDURE bascule_monopole(
       exeOrdre NUMBER
   )   ;
END; 

/

create or replace PACKAGE BODY JEFY_MARCHES.BASCULE_MARCHES IS

-- -------------------- --
-- Procédure principale --
-- -------------------- --
PROCEDURE bascule_exercice(exeOrdre NUMBER)   
IS
begin

    bascule_code_exer(exeOrdre);
    bascule_mapace(exeOrdre);
    bascule_3cmp(exeOrdre);
    bascule_monopole(exeOrdre);

end;

-- --------------------- --
-- Bascule des CODE_EXER --
-- --------------------- --
 PROCEDURE bascule_code_exer(
    exeOrdre NUMBER
   )   IS


  cursor c1 is select ce.* 
               from   code_exer ce, code_marche cm
               where  ce.exe_ordre = exeordre 
               and    ce.cm_ordre  = cm.cm_ordre 
               and    cm.cm_niveau > 1 
               and    ce_suppr     = 'N';

   current_code_exer   code_exer%ROWTYPE;
   current_code_marche code_marche%ROWTYPE;
   cpt integer;

   BEGIN

     -- A-t-on deja passe cette procedure ?    
     select count(*) into cpt from jefy_marches.code_exer where exe_ordre=exeOrdre;
     if(cpt>0) THEN
            RAISE_APPLICATION_ERROR (-20001,'La bascule des codes nomenclatures a deja ete faite');
     end if;
 
    -- Insertion des codes marches pour l'annee exeordre a partir de exeordre - 1 
    -- 19/12/2012 Rod: on ne recupere que les niveaux 1 et 2
    -- 08/10/2013 JBL: on ne bascule que les codes NACRES et LOCAUX. 
    insert into jefy_marches.code_exer 
              (CE_3CMP, CE_AUTRES, CE_CONTROLE, CE_MONOPOLE, CE_ORDRE , 
               CE_RECH, CE_SUPPR , CE_TYPE    , CM_ORDRE   , EXE_ORDRE, TCN_ID)
        (
          select ce.ce_3cmp, ce.ce_autres, ce.ce_controle, ce.ce_monopole, code_exer_seq.nextval, 
                 ce.ce_rech, ce.ce_suppr , ce.ce_type    , ce.cm_ordre   , exeOrdre             , ce.tcn_id 
          from jefy_marches.code_exer ce 
               inner join jefy_marches.code_marche      cm  on (ce.CM_ORDRE = cm.cm_ordre)
               inner join jefy_marches.type_code_marche tcm on (tcm.tcm_id  = cm.tcm_id  )
          where ce.exe_ordre = (exeOrdre - 1) and cm.CM_NIVEAU <= 2 and tcm.tcm_code in ('N', 'L')
        );
    
      open C1;
        loop
        fetch C1 into current_code_exer;
        exit when C1%notfound;

            select * into current_code_marche from code_marche where cm_ordre = current_code_exer.cm_ordre;
                      
            if (current_code_marche.cm_pere is null)
            then
                raise_application_error(-20001, 'Pas de code MARCHE pere defini pour le code ' ||current_code_marche.cm_code|| ' , CM_ORDRE : '||current_code_marche.cm_ordre||')');
            end if;
            

            select count(*) into cpt from code_exer where exe_ordre = exeordre and cm_ordre = current_code_marche.cm_pere and ce_suppr = 'N';
            if (cpt = 0)
            then
                raise_application_error(-20001, 'Pas de code EXER pere defini pour le code ' ||current_code_marche.cm_code|| ' , CM_ORDRE_PERE : '||current_code_marche.cm_pere||')');
            end if;

            if (cpt > 1)
            then
                raise_application_error(-20001, 'Plusieurs occurences dans CODE_EXER pour le cm_ordre '||current_code_marche.cm_pere||' !');
            end if;

            -- 08/10/2013 JBL: Plus besoin de ce_pere
--            select ce_ordre into ceordrepere from code_exer where exe_ordre = exeordre and cm_ordre = current_code_marche.cm_pere and ce_suppr = 'N';
--            update code_exer set ce_pere = ceordrepere where ce_ordre = current_code_exer.ce_ordre;

        end loop;
        close C1;
    commit;
   END;

-- ------------------- --
-- Bascule des MAPA_CE --
-- ------------------- --
 PROCEDURE bascule_mapace(
    exeOrdre NUMBER
   )   IS
   
    CURSOR C1     is select m.* 
                     from   mapa_ce m 
                     where  ce_ordre in (
                            select ce.ce_ordre 
                            from   code_exer ce, code_marche cm, type_code_marche tcm
                            where  ce.cm_ordre  = cm.cm_ordre
                            and    cm.tcm_id    = tcm.tcm_id
                            and    ce.exe_ordre = (exeOrdre-1) 
                            and    ce.ce_suppr  = 'N'
                            and    cm.cm_niveau <= 2
                            and    tcm.tcm_code in ('N', 'L')
                            )
    ;
    CURSOR C2     is select m.* from mapa_tranche_exer m where exe_ordre = (exeOrdre-1);
    
    cmOrdre       NUMBER;
    ceOrdreBascul NUMBER;
    mce           mapa_ce%rowtype;
    mte           mapa_tranche_exer%rowtype;
    cnt           Integer;
    mteId         Integer;
 
 BEGIN
   
  select count(*) into cnt from jefy_marches.code_exer where exe_ordre=exeOrdre;
  if(cnt=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'Veuillez commencer par basculer les codes nomenclatures');
  end if;

  select count(*) into cnt from jefy_marches.MAPA_CE where ce_ordre in (select ce_ordre from jefy_marches.code_exer where exe_ordre=exeOrdre);
  if(cnt>0) THEN
         RAISE_APPLICATION_ERROR (-20001,'La bascule des seuils MaPA a deja ete faite');
  end if;

  
  open C1;
    loop
      fetch C1 into mce;
      exit when C1%notfound;
   
      select cm_ordre into cmOrdre       from code_exer where ce_ordre  = mce.ce_ordre;
      select ce_ordre into ceOrdreBascul from code_exer where exe_ordre = exeOrdre      and cm_ordre = cmOrdre;
      
      if(ceOrdreBascul IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001,'Un code nomenclature n a pas ete bascule sur le nouvel exercice');
      end if;
      
      INSERT INTO jefy_marches.MAPA_CE values (mapa_ce_seq.nextval,ceOrdreBascul,mce.MTE_ID, SYSDATE, mce.UTL_ORDRE, mce.MC_SUPP);
   
    end loop;
  close C1;
    
  open C2;
    loop
      fetch C2 into mte;
      exit when C2%notfound;
  
      select mapa_tranche_exer_seq.nextval into mteId from dual;
      -- INSERTING into MAPA_TRANCHE_EXER
      Insert into MAPA_TRANCHE_EXER 
             (MTE_ID, TCN_ID    , DATE_VOTE_CA    , EXE_ORDRE, MTE_LIBELLE    , MTE_MAX    , MTE_MIN    , UTL_ORDRE    , DATE_MODIFICATION, MOD_ID    ) 
      values (mteId , mte.TCN_ID, mte.DATE_VOTE_CA, exeOrdre , mte.MTE_LIBELLE, mte.MTE_MAX, mte.MTE_MIN, mte.UTL_ORDRE, SYSDATE          , mte.MOD_ID);
      
      update mapa_ce 
      set    mte_id = mteId 
      where  mte_id = mte.mte_id 
      and    ce_ordre in (
               select ce_ordre from code_exer where exe_ordre = exeOrdre
      );
      
    end loop;
  close C2;
  
  commit;
 END;


-- ------------------------------------------ --
-- Bascule des CODE_MARCHE_FOUR lié à un CM   --
-- de l'article 3 du Code des Marchés Publics --
-- ------------------------------------------ --
 PROCEDURE bascule_3CMP(
    exeOrdre NUMBER
   )   IS
   
   CURSOR C1 is select cmf.* 
                from   code_marche_four cmf,
                       code_exer ce,
                       code_marche cm,
                       type_code_marche tcm
                where  cmf.ce_ordre = ce.ce_ordre
                and    ce.cm_ordre  = cm.cm_ordre
                and    cm.tcm_id    = tcm.tcm_id
                and    ce.exe_ordre = (exeOrdre-1)
                and    ce.ce_3cmp   = 1                  
                and    not exists (select * from code_marche_four f, code_exer e
                                   where    f.fou_ordre = cmf.fou_ordre 
                                   and      f.ce_ordre  = e.ce_ordre
                                   and      e.exe_ordre = exeOrdre
                       )
                and    cm.cm_niveau <= 2
                and    tcm.tcm_code in ('N', 'L')
                order  by cmf.cmf_ordre
                ;

   cmf     code_marche_four%rowtype;
   ceOrdre NUMBER;
       
   BEGIN
   
  open C1;
  loop
   fetch C1 into cmf;
   exit when C1%notfound;

     select ce.ce_ordre into ceOrdre 
     from   code_exer ce
     where  ce.exe_ordre = exeOrdre
     and    ce.cm_ordre = (
              select e.cm_ordre 
              from   code_exer e, code_marche_four f
              where  f.cmf_ordre = cmf.cmf_ordre
              and    f.ce_ordre  = e.ce_ordre
     );

     insert into code_marche_four 
            (CE_ORDRE, CMF_ORDRE                    , CMF_ORIGINE , CMF_SUPPR     , FOU_ORDRE    )
     values (ceOrdre , code_marche_four_seq.nextval ,'MIGRATION'  , cmf.cmf_suppr , cmf.fou_ordre);

  end loop;
  close C1;

  commit;
   
  END;

-- ----------------------------- --
-- Bascule des CODE_MARCHE_FOUR  --
-- liés à un code monopolistique --
-- ----------------------------- --
 PROCEDURE bascule_monopole(
    exeOrdre NUMBER
   )   IS
   
   CURSOR C1 is select cmf.* 
                from   code_marche_four cmf,
                       code_exer ce,
                       code_marche cm,
                       type_code_marche tcm
                where  cmf.ce_ordre = ce.ce_ordre
                and    ce.cm_ordre  = cm.cm_ordre
                and    cm.tcm_id    = tcm.tcm_id
                and    ce.exe_ordre = (exeOrdre-1)
                and    ce_monopole  = 1                   
                and    not exists (select * from code_marche_four f, code_exer e
                                   where    f.fou_ordre = cmf.fou_ordre 
                                   and      f.ce_ordre  = e.ce_ordre
                                   and      e.exe_ordre = exeOrdre
                       )
                and    cm.cm_niveau <= 2
                and    tcm.tcm_code in ('N', 'L')
                order  by cmf.cmf_ordre;

  cmf     code_marche_four%rowtype;
  ceOrdre NUMBER;
  
  BEGIN
   
  open C1;
  loop
   fetch C1 into cmf;
   exit when C1%notfound;

     select ce.ce_ordre into ceOrdre 
     from   code_exer ce
     where  ce.exe_ordre = exeOrdre
     and    ce.cm_ordre = (
              select e.cm_ordre from code_exer e, code_marche_four f
              where  f.cmf_ordre = cmf.cmf_ordre
              and    f.ce_ordre = e.ce_ordre
     );

     insert into code_marche_four 
            (CE_ORDRE, CMF_ORDRE                    , CMF_ORIGINE , CMF_SUPPR     , FOU_ORDRE    )
     values (ceOrdre , code_marche_four_seq.nextval ,'MIGRATION'  , cmf.cmf_suppr , cmf.fou_ordre);

  end loop;
  close C1;

  commit;

  end;

END; 

/