
-- Nouvel emplacement pour les param Kiwi et Papaye
-- On ne prend pas la valeur actuelle

-- Paramère pour Kiwi dans JEFY_ADMIN
insert into jefy_admin.PARAMETRE ( PAR_ORDRE, EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_VALUE)
 select jefy_admin.parametre_seq.nextval, exe_ordre, 
        'Code de nomenclature des missions', 
        'org.cocktail.gfc.mission.codenomenclature',
        (select param_value from jefy_mission.mission_parametres where param_key = 'CODE_MARCHE')
        from jefy_admin.exercice where exe_stat<>'C'; 

-- Paramètre pour Papaye dans JEFY_ADMIN
insert into jefy_admin.PARAMETRE ( PAR_ORDRE, EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_VALUE)
 select jefy_admin.parametre_seq.nextval, exe_ordre, 
        'Code marche par defaut pour les engagements et liquidations', 
        'org.cocktail.gfc.paye.codenomenclature',
        (select param_value from JEFY_PAYE.paye_parametres where param_key = 'DEFAULT_CODE_MARCHE')
        from jefy_admin.exercice where exe_stat<>'C'; 
